<?php

session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/einvoice/aplicacion/modelos/dataBase.php");
date_default_timezone_set('America/Guayaquil');

class Permisos {

    function __construct($codigoUsuario) {
        $this->dbmysql = new database();
        $this->CodigoUsuario = $codigoUsuario;
    }

    function getCodigoPerfil() {
        $sql = "SELECT rol_id FROM `usuarios` WHERE id = $this->CodigoUsuario";
        $val = $this->dbmysql->query($sql);
        $row = $val->fetch_object();
        return $row->rol_id;
    }

    function construirMenu() {
        $activarMenu = $_SESSION["menu"];
        $opM = $_GET['opMenu'];

        $sqlMenu = "SELECT o.id as OPU_COD , o.nombre as OPU_NOMBRE, o.link OPU_LINK , o.nivel OPU_NIVEL, o.padre_id as OPU_COD_PADRE,o.icono as OPU_ICONO FROM `opciones` o,  `opciones_roles` op, `roles` p  WHERE o.id = op.opcion_id AND  p.id = op.rol_id AND op.rol_id =  " . $this->getCodigoPerfil() . " AND o.nivel = 1";
        $valMenu = $this->dbmysql->query($sqlMenu);
        $retval = "";
        $retval.='<li ' . $activarMenu . $_SESSION["menu"] . ' ><a href="inicio.php" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Principal</span></a></li>';
        while ($rowMenu = $valMenu->fetch_object()) {
            $retval.='<li> <a  href="' . $rowMenu->OPU_LINK . '"><i class="fa fa-lg fa-fw ' . $rowMenu->OPU_ICONO . '"></i><span class="menu-item-parent">' . $rowMenu->OPU_NOMBRE . '</span></a>';
            $sqlOpciones = "SELECT o.id as OPU_COD , o.nombre as OPU_NOMBRE, o.link as OPU_LINK , o.nivel as OPU_NIVEL, o.padre_id as OPU_COD_PADRE FROM `opciones` o,  `opciones_roles` op, `roles` p  WHERE o.id = op.opcion_id AND  p.id = op.rol_id AND op.rol_id= " . $this->getCodigoPerfil() . " AND o.nivel = 2 AND o.padre_id = " . $rowMenu->OPU_COD;
            $valOpciones = $this->dbmysql->query($sqlOpciones);
            $retval .= '<ul>';
            while ($rowOpciones = $valOpciones->fetch_object()) {
                $permiso= ($rowOpciones->OPU_COD==10)?$this->validarPermisoFacturacion():'S';
                $activarMenu = ($opM == $rowOpciones->OPU_COD) ? 'class="active"' : '';
                if($permiso=='S'){
                    $retval.='<li ' . $activarMenu . '> <a  href="' . $rowOpciones->OPU_LINK . '&opMenu=' . $rowOpciones->OPU_COD . '" ><span class="menu-item-parent">' . $rowOpciones->OPU_NOMBRE . '</span></a></li>';
                }
            }
            $retval .= '</ul></li>';
        }
        echo $retval;
    }

    public function getCajaLocal() {
        $local = $_SESSION["usu_local_id"];
        $ip = $this->obtenerDireccionIp();
        $sql = "SELECT * FROM cajas WHERE ip = '$ip'";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            $row = $val->fetch_object();
            $caja = $row->nombre;
            $_SESSION['direccion_ip'] = $row->ip;
            $_SESSION['caja'] = $row->nombre;
            $_SESSION['id_caja'] = $row->id;
            $_SESSION['punto_emision'] = $row->emision;

            return $caja;
        } else {
            return 'Registro';
        }
    }

    public function obtenerDireccionIp() {
        $exec = exec("hostname"); //the "hostname" is a valid command in both windows and linux
        $hostname = trim($exec); //remove any spaces before and after
        $ip = gethostbyname($hostname);
        $ip=$_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    public function obtenerEstadoCaja($idCaja) {
        $fecha = date('Y-m-d');
        $sql = "SELECT * FROM caja_diaria WHERE fecha = '$fecha' AND caja_id= $idCaja AND estado='A'";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            return 'abierta';
        } else {
            return 'cerrada';
        }
    }

    public function validarPermisoFacturacion(){
        if($_SESSION['id_caja']!==''){
            $estadoCaja=  $this->obtenerEstadoCaja($_SESSION['id_caja']);
            if($estadoCaja=='abierta'){
                return 'S';
            }else{
                return 'N';
            }
        }
       
    }
}
