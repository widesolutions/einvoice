<?php


include_once $_SERVER['DOCUMENT_ROOT'].'/einvoice/aplicacion/modelos/dataBase.php';

class login {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }

    public function autenticar($parametros) {
        $usuario = $parametros['usuario'];
        $clave = $parametros['clave'];
        $local = $parametros['local'];
        $sql = "SELECT u.id as usuario_id,u.nombre as usuario_nombre ,u.apellido as usuario_apellido ,u.cedula as usuario_cedula, u.email,u.avatar, u.estado, r.id as rol_id, r.descripcion as nombre_rol, l.id as local_id, l.nombre as nombre_local FROM usuarios u,roles r, locales l WHERE u.`rol_id`=r.`id` AND u.local_id=l.id
        AND u.local_id= $local  
        AND u.usuario ='" . $usuario . "' 
        AND u.clave = MD5('$clave');";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            $row = $consulta->fetch_object();
            $this->asignacionPermisos($row);
            echo "ok";
        } else {
            $sql2 = "SELECT u.id as usuario_id,u.nombre as usuario_nombre ,u.apellido as usuario_apellido ,u.cedula as usuario_cedula, u.email,u.avatar, u.estado, r.id as rol_id, r.descripcion as nombre_rol, l.id as local_id, l.nombre as nombre_local FROM usuarios u,roles r, locales l WHERE u.`rol_id`=r.`id` AND u.local_id=l.idSELECT u.*, r.*,l.* FROM usuarios u,roles r,locales l  WHERE u.`rol_id`=r.`id` AND u.local_id=l.id
                    AND u.local_id= $local
                    AND u.usuario ='" . $usuario . "' 
                    AND u.autorizacion = MD5('$clave')
                    AND u.estado='C'";
            $consulta2 =  $this->database->query($sql2);
            if ($consulta2->num_rows > 0) {
                $row = $consulta2->fetch_object();
                $this->asignacionPermisos($row);
                echo "cambio";
            } else {
                echo "no";
            }
        }
    }

    public function asignacionPermisos($row) {
        session_start();
        //print_r($row);
        $_SESSION["autenticado"] = "SI";
        $_SESSION["user_id"] = $row->usuario_id;
        $_SESSION["usu_real_nombre"] = $row->usuario_nombre . ' ' . $row->usuario_apellido;
        $_SESSION["usu_usuario"] = $row->usuario_usuario;
        $_SESSION["usu_mail"] = $row->email;
        $_SESSION["usu_avatar"] = $row->avatar;
        $_SESSION["usu_rol_cod"] = $row->rol_id;
        $_SESSION["usu_rol_descrip"] = $row->nombre_rol;
        $_SESSION["usu_local_id"] = $row->local_id;
        $_SESSION["usu_local_descrip"] = $row->nombre_local;
    }
    
    public function autenticacionAdministrador($parametros){
        $usuario = $parametros['usuario'];
        $clave = $parametros['clave'];
        $sql = "SELECT * FROM usuarios u,roles r WHERE u.`rol_id`=r.`id`
        AND u.usuario ='" . $usuario . "' 
        AND u.clave = MD5('$clave');";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            echo 'autorizado';
        }else{
            echo 'noautorizado';
        }
    }

}
