<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';

class tipoPagos {

    protected $database;
    protected $dbmysql;

    function __construct() {
        $this->dbmysql = new database();
    }

    function getTipoPagos() {
        $sql = "SELECT * FROM `tipos_pagos` WHERE estado='A';";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function listarTipoPagos() {
        $sql = "SELECT * FROM tipos_pagos WHERE estado='A'";
        $resul = $this->dbmysql->query($sql);
        return $resul;
    }

    public function guardaDatosPago() {
        $nombre = strtoupper($_POST["nombre"]);
        $valores_adicionales = $_POST['valores_adicionales'];
        $observacion = strtoupper($_POST['observacion']);
        $sqlVerifica = "SELECT * FROM tipos_pagos WHERE nombre='$nombre' AND valores_adicionales='$valores_adicionales';";
        $resulVerifica = $this->dbmysql->query($sqlVerifica);
        if ($resulVerifica->num_rows == 0) {
            $sql = "INSERT INTO tipos_pagos(nombre,valores_adicionales,observacion,estado) VALUES
            ('$nombre','$valores_adicionales','$observacion','A')";
            $resul = $this->dbmysql->query($sql);
            if ($resul) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function enviarDatosTipoPagos() {
        $idpago = $_POST['codigoPagos'];
        $sql = "SELECT * FROM tipos_pagos WHERE id =$idpago";
        $val = $this->dbmysql->query($sql);
        $row = $val->fetch_object();
        $lista['datosTipoPagos'] = array(
            "IDTipoPago" => $row->id,
            "nombre" => $row->nombre,
            "valores_adicionales" => $row->valores_adicionales,
        );
        echo $encode = json_encode($lista);
    }

    public function ActualizarDatos() {
        $idpago = $_POST['IDTipoPago'];
        $nombre = strtoupper($_POST["nombre"]);
        $valores_adicionales = $_POST['valores_adicionales'];
        $observacion = strtoupper($_POST['observacion']);
        $sql = "UPDATE tipos_pagos SET 
                    observacion='$observacion'
                WHERE id=$idpago ";
        $resul = $this->dbmysql->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function EliminarPago() {
        $pago = $_POST['idpago'];
        $sql = "UPDATE tipos_pagos SET estado='E' WHERE id=$pago";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
