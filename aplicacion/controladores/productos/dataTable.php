<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$dbmysql = new database();
date_default_timezone_set('America/Guayaquil');
$sOrder = '';
$aColumns = array('id','unidades_medida_id','nombre','modelo','codigo_barras','precio_compra','precio_venta','iva','estado');
$sIndexColumn = "id";
$sTable = "productos";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}
/* * *******************
 * ** Ordenamiento ****
 * ******************* */

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY GEN_ID") {
        $sOrder = "ORDER BY GEN_ID DESC";
    }
}

/* * *******************
 * ** Filtrado ****
 * ******************* */

$sWhere = "";
if ($_GET['sSearch'] != "") {
    $sWhere = "WHERE (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}
/* Filtrado por Columnas individuales */
for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        if ($sWhere == "") {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
    }
}

/* * ***********************
 * ** CREACION DEL SQL ****
 * *********************** */
$sWhere = ($sWhere == '') ? 'WHERE estado="A" ' : $sWhere . ' AND estado="A"';
$sQuery = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(",", $aColumns)) . "
            FROM $sTable
            $sWhere
            $sOrder
            $sLimit";
//echo $sQuery;
$rResult = $dbmysql->query($sQuery);
if (!$rResult) {
    printf("Error: %s\n", $mysqli->error);
}
/* * **********************************************************
 * ** Longitud conjunto de datos después de la filtración ****
 * ********************************************************** */
$sQuery = "SELECT FOUND_ROWS() as Validas";
$rResultFilterTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultFilterTotal = $rResultFilterTotal->fetch_object();
$iFilteredTotal = $aResultFilterTotal->Validas; //Total de Registros Validos

$sQuery = "SELECT COUNT(" . $sIndexColumn . ")AS contador FROM   $sTable $sOrder;";
$rResultTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultTotal = $rResultTotal->fetch_object();
$iTotal = $aResultTotal->Validas;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iFilteredTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array());
//print_r($output);
/* * **********************
 * ** RESULTADO FINAL ****
 * ********************** */
while ($aRow = $rResult->fetch_object()) {
    $cadenaParametros=$aRow->id.' , '."'$aRow->nombre'";
    $output['aaData'][] = array('' . $aRow->id . '',
                                '' . $aRow->codigo_barras . '',
                                '' . $aRow->nombre . '',
                                '' . $aRow->modelo . '',
                                '' . $aRow->precio_compra . '',
                                '' . $aRow->precio_venta . '',
                                '<div class="btnAccion" style="text-align: center; display: inline-block;"><a class="btn btn-default btn-xs txt-color-magenta" title="Actualizar Producto" href="javascript:ActualizarProducto(\'' . $aRow->id . '\')">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="btn btn-default btn-xs txt-color-green" title="Elimnar Producto" href="javascript:EliminarProducto(' . $cadenaParametros. ')">
                                            <i class="fa fa-trash-o"></i>
                                        </a></div>');
}
echo json_encode($output);
