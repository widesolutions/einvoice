<?php
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$producto = new productoApp();
echo $producto->getProductoxNombre();
class productoApp {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }

    public function getProductoxNombre() {
        $parametros = $_GET['term'];
        $sql = "SELECT * FROM productos WHERE nombre LIKE '%$parametros%' AND estado='A'";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datos[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva,
                    "productoServicio" => 1
                );
            }
        }
        $sql = "SELECT * FROM servicios WHERE nombre LIKE '%$parametros%' AND estado='A'";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datosServicios[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva,
                    "productoServicio" => 2
                );
            }
            if ($datos) {
                $datos = array_merge($datos, $datosServicios);
            } else {
                $datos = $datosServicios;
            }
        }
        return json_encode($datos);
    }

}
