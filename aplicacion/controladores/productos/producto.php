<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';

class producto {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }
    public function listarProducto() {
        $sql = "SELECT p.*,um.nombre AS nombreUnidad FROM productos p, unidades_medidas um WHERE um.id=p.unidades_medida_id";
        $resul = $this->database->query($sql);
        return $resul;
    }

    public function getProductoxNombre() {
        $parametros = $_GET['term'];
        $sql = "SELECT * FROM productos WHERE nombre LIKE '%$parametros%' AND estado='A'";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datos[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva,
                    "productoServicio" => 1
                );
            }
        }
        $sql = "SELECT * FROM servicios WHERE nombre LIKE '%$parametros%' AND estado='A'";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datosServicios[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva,
                    "productoServicio" => 2
                );
            }
            if ($datos) {
                $datos = array_merge($datos, $datosServicios);
            } else {
                $datos = $datosServicios;
            }
        }
        echo json_encode($datos);
    }
    public function getProductoxCodigo() {
        $parametros = $_POST['codigo'];
        $sql = "SELECT * FROM productos WHERE codigo_barras = '$parametros' AND estado='A';";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datos[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva
                );
            }
        }
        $sql = "SELECT * FROM servicios WHERE codigo_barras = '$parametros' AND estado='A';";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datosServicios[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva,
                    "productoServicio" => 2
                );
            }
            if ($datos) {
                $datos = array_merge($datos, $datosServicios);
            } else {
                $datos = $datosServicios;
            }
        }
        echo json_encode($datos);
    }
    public function BuscarBarras() {
        $barras = $_POST["cod_barras"];
        $busqueda = "SELECT codigo_barras FROM productos WHERE codigo_barras='$barras'";
        $resul = $this->database->query($busqueda);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function guardaDatosProducto() {
        $descripcion = strtoupper($_POST["descripcion"]);
        $preciocompra = $_POST["precio_compra"];
        $precioventa = $_POST["precio_venta"];
        $barras = strtoupper($_POST["cod_barras"]);
        $modelo = strtoupper($_POST["modelo"]);
        $medida = $_POST["uni_medida"];
        $iva = $_POST["iva"];
        $sqlVerifica = "SELECT * FROM productos WHERE nombre='$descripcion' AND codigo_barras='$barras';";
        $resulVerifica = $this->database->query($sqlVerifica);
        if ($resulVerifica->num_rows == 0) {
            $sql = "INSERT INTO productos(unidades_medida_id,nombre,modelo,codigo_barras,precio_compra,precio_venta,iva,estado) VALUES
            ('$medida','$descripcion','$modelo','$barras','$preciocompra','$precioventa','$iva', 'A')";
            $resul = $this->database->query($sql);
            if ($resul) {
                echo 1;
            } else {
                echo 0;
            }
        }else{
            echo 2;
        }
    }
    public function EliminarProducto() {
        $codigo = $_POST['idprod'];
        $sql = "UPDATE productos SET estado='E' WHERE id=$codigo";
        $val = $this->database->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function enviarDatosProducto() {
        $idpro = $_POST['codigoProducto'];
        $sql = "SELECT * FROM productos WHERE id =$idpro";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $lista['datosProducto'] = array(
            "IDProducto" => $row->id,
            "precio_compra" => $row->precio_compra,
            "precio_venta" => $row->precio_venta,
            "uni_medida" => $row->unidades_medida_id,
            "descripcion" => $row->nombre,
            "cod_barras" => $row->codigo_barras,
            "modelo" => $row->modelo,
            "iva" => $row->iva
        );
        echo $encode = json_encode($lista);
    }
    public function ActualizarProducto() {
        $id = $_POST['IDProducto'];
        $descripcion = strtoupper($_POST["descripcion"]);
        $preciocompra = $_POST["precio_compra"];
        $precioventa = $_POST["precio_venta"];
        $barras = strtoupper($_POST["cod_barras"]);
        $modelo = strtoupper($_POST["modelo"]);
        $medida = $_POST["uni_medida"];
        $iva = $_POST["iva"];
        $sql = "UPDATE productos SET 
                nombre    = '$descripcion',
                unidades_medida_id  = '$medida',
                modelo   = '$modelo',
                codigo_barras     = '$barras',
                precio_compra   = '$preciocompra',
                precio_venta    = '$precioventa',
                iva       = '$iva'
            WHERE id=$id";
        $resul = $this->database->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function productosMasVendido($fecha_inicio,$fecha_fin)
    {
        $select = "select count(*) as cantidad, p.nombre, p.id  from movimientos m, detalles_movimientos d, productos p where m.fecha between '$fecha_inicio' and '$fecha_fin' and  d.producto_id = p.id and m.id = d.movimiento_id and d.servicio_id is null group by d.producto_id order by cantidad desc limit 10";
        return $this->database->query($select);
        
    }

}
 
