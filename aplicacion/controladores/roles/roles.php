<?php

session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/modelos/dataBase.php");

class roles {

    protected $dbmysql;

    function __construct() {
        $this->dbmysql = new database();
    }

    public function enviarDatosRoles() {
        $idRol = $_POST['rol'];
        $sql = "SELECT * FROM `roles` WHERE id =$idRol";
        $val = $this->dbmysql->query($sql);
        $row = $val->fetch_object();
        $lista['datosRol'] = array(
            "id" => $row->id,
            "descripcion" => $row->descripcion,
            "estado" => $row->estado,
            "observacion" => $row->observacion
        );

        echo $encode = json_encode($lista);
    }

    public function listarRoles() {
        $sql = "SELECT * FROM `roles` WHERE estado != 'E';";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function buscarPermisosUsuario() {
        $codRol = $_POST['rol'];
        $sql = "SELECT o.id as opcion_id,o.`nombre`, o.`link`,o.`nivel`,o.`padre_id`,o.`icono`,orl.rol_id FROM `opciones` o, `opciones_roles` orl WHERE o.`id`=orl.`opcion_id` AND orl.`rol_id`=$codRol;";
        $val = $this->dbmysql->query($sql);

        if ($val->num_rows > 0) {
            while ($row = $val->fetch_object()) {
                $cadenaParametros = $row->opcion_id . ',\'' . $row->nombre . '\',' . $codRol;
                $retval .='<tr id="tablaHorarios">
                           <td align="center"><span class="glyphicon glyphicon-ok" style="color:green;"></span></td>
                           <td>' . $row->opcion_id . '</td>
                           <td>' . $row->nombre . '</td>
                           <td><a class="btn btn-danger btn-xs ' . $row->opcion_id . '" title="Eliminar Opción Usuario" href="javascript:eliminarPermiso(' . $cadenaParametros . ')">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                           </td>
                   </tr>';
            }
        } else {
            $retval .='<tr>
                           <td colspan="6" align="center"><div class="txtPabellonHorario">Ningun Horario Asignado</div><input type="hidden" class="nomDescripcionArchivo" id="nomDescripcionArchivo" name="nomDescripcionArchivo" value="' . $row->PRO_ID . '"></td>
                   </tr>';
        }
        echo $retval;
    }

    function mostrarPermisosDisponibles() {
        $codRol = $_POST['codRol'];
        $sql = "SELECT * FROM `opciones` WHERE nivel>1 OR `link` != '#'";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            while ($row = $val->fetch_object()) {
                $sql_mas = "SELECT * FROM `opciones_roles` WHERE rol_id=$codRol AND opcion_id=$row->id;";
                $sub = $this->dbmysql->query($sql_mas);
                if ($sub->num_rows == 0) {
                    $retval .='<tr id="tablaHorarios">
                               <td align="center">
                                     <input id="codRol" type="hidden" name="codRol" value="' . $codRol . '">
                                    <label class="checkbox">
                                        <input id="asignar" type="checkbox" name="asignar" value="' . $row->id . '">
                                        <i></i>
                                    </label>
                                </td>
                               <td>' . $row->id . '</td>
                               <td>' . $row->nombre . '</td>
                       </tr>';
                }
            }
        } else {
            $retval .='<tr>
                           <td colspan="6" align="center"><div class="txtPabellonHorario">Ningun Horario Asignado</div><input type="hidden" class="nomDescripcionArchivo" id="nomDescripcionArchivo" name="nomDescripcionArchivo" value="' . $row->PRO_ID . '"></td>
                   </tr>';
        }
//    $retval .=buscarArchivosProducto();
        echo $retval;
    }

    function guardarAsignaPermisos() {
        $x = 0;
        $permisos = explode(';', $_POST['permisos']);
        $codRol = $_POST["codRol"];
        $totalIns = count($permisos) - 1;
        for ($i = 1; $i < count($permisos); $i++) {
            //Consulta si tiene Padre
            $sql1 = "SELECT *  FROM `opciones` WHERE id=$permisos[$i];";
            $val1 = $this->dbmysql->query($sql1);
            $row1 = $val1->fetch_object();
            $padre = $row1->padre_id;
            if ($padre > 0) {
                $sql2 = "SELECT *  FROM `opciones_roles` WHERE rol_id=$codRol AND opcion_id=$padre;";
                $val2 = $this->dbmysql->query($sql2);
                if ($val2->num_rows == 0) {
                    $sql = "INSERT INTO  `opciones_roles` (opcion_id,rol_id) VALUES
                  ($padre,$codRol);";
                    $val = $this->dbmysql->query($sql);
                }
            }
            $sql = "INSERT INTO  `opciones_roles` (opcion_id,rol_id) VALUES
              ($permisos[$i],$codRol);";
            $val = $this->dbmysql->query($sql);
            if ($val) {
                $x++;
            }
        }
        if ($x == $totalIns) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function eliminarPermisoAcceso() {
        $codigo = $_POST['codigo'];
        $codRol = $_POST['codRol'];
        $sql = "DELETE FROM `opciones_roles` WHERE opcion_id =$codigo AND rol_id=$codRol;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function guardaDatosRol() {
        $descripcion = strtoupper($_POST["descripcion"]);
        $observacion = strtoupper($_POST["observacion"]);

        $sql = "INSERT INTO `roles`(descripcion,estado,observacion)VALUES
            ('$descripcion','A','$observacion');";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actualizarDatosRol() {
        $codigo = $_POST['IDrol'];
        $descripcion = strtoupper($_POST["descripcion"]);
        $observacion = strtoupper($_POST["observacion"]);

        $sql = "UPDATE `roles` SET 
                descripcion    = '$descripcion',
                observacion  = '$observacion'
            WHERE id=$codigo;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function eliminarRol() {
        $codigo = $_POST['codigo'];
        $sql_revisa = "SELECT * FROM usuarios WHERE rol_id=$codigo;";
        $val_revisa = $this->dbmysql->query($sql_revisa);
        if ($val_revisa->num_rows == 0) {
            $sql = "UPDATE `roles` SET estado='E' WHERE id=$codigo;";
            $val = $this->dbmysql->query($sql);
            if ($val) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

}
