<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';

class infante {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }

    public function crear($parametros) {


        $primer_nombre = strtoupper($parametros['primer_nombre']);
        $segundo_nombre = strtoupper($parametros['segundo_nombre']);
        $primer_apellido = strtoupper($parametros['primer_apellido']);
        $segundo_apellido = strtoupper($parametros['segundo_apellido']);
        $fecha_nacimiento = $parametros['fecha_nacimiento'];
        $institucion_educativa = strtoupper($parametros['institucion_educativa']);

        $sql = "INSERT INTO `infantes` (`primer_nombre`,`segundo_nombre`,`primer_apellido`,`segundo_apellido`,`fecha_nacimiento`,`institucion_educativa`)
                VALUES ('$primer_nombre','$segundo_nombre','$primer_apellido','$segundo_apellido','$fecha_nacimiento','$institucion_educativa')";

        $insert = $this->database->query($sql);
        $datos['insert'] = $insert;
        $datos['id'] = $this->database->lastid();

        if (isset($parametros['ajax']))
        {
            echo json_encode($datos);
         
        } else {
            
            return $datos;
        }
    }

    public function getInfante($parametros) {

        $id = $parametros['id'];


        $sql = "SELECT * FROM .`infantes` WHERE id = " . $id;

        $consulta = $this->database->query($sql);
        $row = $consulta->fetch_object();
        echo json_encode($row);
    }

    public function searchInfante() {
        $criterio = $_POST['search_infante'];

        if (ctype_alpha(preg_replace('/\s+/', '', $criterio)))
            $codigo = "'".$criterio."'";
        else
            $codigo = $criterio;
        
       $sql = "SELECT * FROM `infantes` WHERE CONCAT(primer_nombre,' ',primer_apellido) like '%$criterio%' or  primer_nombre like '%$criterio%' or segundo_nombre like '%$criterio%' or primer_apellido like '%$criterio%' or segundo_apellido like '%$criterio%' or id = $codigo or CONCAT(primer_nombre,' ',segundo_apellido) like '%$criterio%' or CONCAT(segundo_nombre,' ',primer_apellido) like '%$criterio%' or CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) like '%$criterio%' LIMIT 6 ";
        $val = $this->database->query($sql);

        while ($row = $val->fetch_object()) {
            $datos[] = array(
                "id" => $row->id,
                "primer_nombre" => $row->primer_nombre,
                "segundo_nombre" => $row->segundo_nombre,
                "primer_apellido" => $row->primer_apellido,
                "segundo_apellido" => $row->segundo_apellido,
            );
        }

        echo json_encode($datos);
        
        
        
    }

    function getInfantePorId() {
        $id = $_POST['id'];
        $sql = "SELECT * FROM `infantes` WHERE id= $id ";
        $val = $this->database->query($sql);
        if ($val->num_rows > 0) {
            $row = $val->fetch_object();
            $datos['datosInfante'] = array(
                "id" => $row->id,
                "primer_nombre" => $row->primer_nombre,
                "segundo_nombre" => $row->segundo_nombre,
                "primer_apellido" => $row->primer_apellido,
                "fecha_nacimiento" => $row->fecha_nacimiento,
                "institucion_educativa" => $row->institucion_educativa
            );

            echo json_encode($datos);
        }
    }

    public function editar($parametros) {

        $primer_nombre = $parametros['primer_nombre'];
        $segundo_nombre = $parametros['segundo_nombre'];
        $primer_apellido = $parametros['primer_apellido'];
        $segundo_apellido = $parametros['segundo_apellido'];
        $fecha_nacimiento = $parametros['fecha_nacimiento'];
        $institucion_educativa = $parametros['institucion_educativa'];
        $id = $parametros['id'];

        $sql = "UPDATE `infantes` SET `primer_nombre` = '$primer_nombre',`segundo_nombre` = '$segundo_nombre',`primer_apellido` = '$primer_apellido',`segundo_apellido` = '$segundo_apellido',`fecha_nacimiento` = '$fecha_nacimiento',`institucion_educativa` = '$institucion_educativa' WHERE id = $id";


        $insert = $this->database->query($sql);
        $datos['insert'] = $insert;
        $datos['id'] = $this->database->lastid();
        echo json_encode($datos);
    }
    
    public function infanteMasVisitas($fecha_inicio, $fecha_fin)
    {
        $select = "select count(*) as cantidad, n.id ,n.primer_nombre,  n.primer_apellido, n.fecha_nacimiento from sesiones s, sesiones_infantes i, infantes n
 where i.sesion_id = s.id and i.estado = 'T' and i.infante_id = n.id and s.fecha between '$fecha_inicio' and '$fecha_fin' group by i.infante_id order by cantidad desc LIMIT 10";
        return $this->database->query($select);

    }

}
