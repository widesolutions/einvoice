<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
date_default_timezone_set('America/Guayaquil');

class reportes {

    private $dbmysql;

    function __construct() {
        $this->dbmysql = new database();
    }

    public function reporteFacturas($fecha_inicio, $fecha_fin) {

        if ($fecha_inicio != "") {

            $query = "s.fecha between '" . $fecha_inicio . "' and '" . $fecha_fin . "' and";
        }

        $select = "select mv.fecha, day(mv.fecha) as dia, month(mv.fecha) as mes, year(mv.fecha) as año, mv.numero_factura, mv.subtotal, mv.iva, mv.subtotal_sin_iva, mv.total, concat(c.nombres,' ',c.apellidos) as cliente, c.identificacion, em.nombre as estado, (select count(*) from sesiones_infantes where mv.sesion_id = sesion_id and servicio_id = 4 and estado = 'T' ) as cantidad_niños, (select count(*) from sesiones_infantes where sesion_id = mv.sesion_id and servicio_id = 3 and estado = 'T' ) as cantidad_bebes, (select round(sum(d.cantidad)) from detalles_movimientos d where d.movimiento_id = mv.id and d.producto_id IS NULL ) as cantidad_horas, mv.fiestas from movimientos mv, clientes c, estados_movimientos em where mv.estados_movimiento_id = em.id and c.id = mv.cliente_id and mv.fecha BETWEEN '$fecha_inicio' and '$fecha_fin' order by mv.id desc";
        return $this->dbmysql->query($select);
    }

    public function reportePromociones($fecha_inicio, $fecha_fin) {



        if ($fecha_inicio != "") {

            $query = "s.fecha between '" . $fecha_inicio . "' and '" . $fecha_fin . "' and";
        }

        $select = "SELECT s.fecha,c.identificacion,concat(c.nombres,' ',c.apellidos) as cliente , i.primer_apellido, i.primer_nombre,p.nombre FROM `sesiones_infantes` si, `sesiones` s, `infantes` i,`clientes` c,promociones p WHERE s.fecha between '$fecha_inicio' and '$fecha_fin' and si.`sesion_id`=s.id AND i.id=si.`infante_id` AND c.id=s.cliente_id AND p.id=si.`promocion_id`";
        return $this->dbmysql->query($select);
    }

    public function reporteTiposPago($fecha_inicio, $fecha_fin) {

        if ($fecha_inicio != "") {

            $query = "s.fecha between '" . $fecha_inicio . "' and '" . $fecha_fin . "' and";
        }

        $select = ""
                . "select m.fecha, concat(c.nombres,' ',apellidos ) as cliente, tp.nombre as tipo_pago, p.valor as valor_tipo, m.total , m.numero_factura, p.entidad_documento  from movimientos m, pagos_movimientos p, clientes c, tipos_pagos tp where tp.id = p.tipos_pago_id and c.id = m.cliente_id and m.id = p.movimiento_id and fecha between '$fecha_inicio' and '$fecha_fin' order by m.id desc";
        return $this->dbmysql->query($select);
    }

}
