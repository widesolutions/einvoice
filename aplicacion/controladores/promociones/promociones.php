<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';

class promociones {

    protected $database;
    protected $dbmysql;

    function __construct() {
        $this->dbmysql = new database();
    }

    function getPromociones() {
        $sql = "SELECT * FROM `promociones` WHERE estado='A';";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function listarPromociones() {
        $sql = "SELECT * FROM promociones WHERE estado='A'";
        $resul = $this->dbmysql->query($sql);
        return $resul;
    }

    public function guardaPromociones($parametros) {
        $local_id=$_SESSION["usu_local_id"];
        $nombre = strtoupper($parametros["nombre"]);
        $dia = $parametros['dia'];
        $valor = $parametros['valor'];
        $fechaInicio = $parametros['fechaInicio'];
        $fechaFin = $parametros['fechaFin'];
        $mayor = $parametros['mayor'];
        $menor = $parametros['menor'];
        $codigo_productoServicio = $parametros['codigo_productoServicio'];
        $tipo_descuento = $parametros['tipo_descuento'];
        
        $sqlVerifica = "SELECT * FROM promociones WHERE (servicio_id=$codigo_productoServicio OR producto_id=$codigo_productoServicio) AND nombre='$nombre' AND dia='$dia' AND valor='$valor';";
        $resulVerifica = $this->dbmysql->query($sqlVerifica);
        if ($resulVerifica->num_rows == 0) {
            $sql = "INSERT INTO promociones(local_id,servicio_id,nombre,dia,fecha_inicio,fecha_fin,tipo_descuento,valor,tramo_mayor,tramo_menor,estado) VALUES
            ($local_id,$codigo_productoServicio,'$nombre','$dia','$fechaInicio','$fechaFin','$tipo_descuento','$valor',$mayor,$menor,'A')";
            $resul = $this->dbmysql->query($sql);
            if ($resul) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actualizarPromociones($parametros) {
        $idPromocion = $parametros['idpromocion'];
        $nombre = strtoupper($parametros["nombre"]);
        $dia = $parametros['dia'];
        $valor = $parametros['valor'];
        $fechaInicio = $parametros['fechaInicio'];
        $fechaFin = $parametros['fechaFin'];
        $mayor = $parametros['mayor'];
        $menor = $parametros['menor'];
        $codigo_productoServicio = $parametros['codigo_productoServicio'];
        $tipo_descuento = $parametros['tipo_descuento'];
        $sql = "UPDATE promociones SET 
                    servicio_id=$codigo_productoServicio,
                    nombre='$nombre', 
                    dia='$dia', 
                    fecha_inicio='$fechaInicio',
                    fecha_fin='$fechaFin',
                    tramo_mayor=$mayor,
                    tramo_menor=$menor,
                    tipo_descuento='$tipo_descuento',
                    valor='$valor'
                WHERE id=$idPromocion ";
        $resul = $this->dbmysql->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function enviarDatosPromociones($parametros){
        $idPromocion = $parametros['codigoPromocion'];
        $sql = "SELECT p.*, s.nombre AS servicio FROM `promociones` p, servicios s WHERE s.id=p.servicio_id AND p.id =$idPromocion";
        $val = $this->dbmysql->query($sql);
        $row = $val->fetch_object();
        $lista['datosPromociones'] = array(
            "IDPromociones" => $row->id,
            "local_id" => $row->local_id,
            "nombre" => $row->nombre,
            "valor" => $row->valor,
            "tipo_descuento" => $row->tipo_descuento,
            "estado" => $row->estado,
            "dia" => $row->dia,
            "fechaInicio" => $row->fecha_inicio,
            "fechaFin" => $row->fecha_fin,
            "mayor" => $row->tramo_mayor,
            "menor" => $row->tramo_menor,
            "servicio_id" => $row->servicio_id,
            "servicio" => $row->servicio
        );
        echo $encode = json_encode($lista);
        
    }
    public function eliminarPromociones() {
        $idpro = $_POST['idpromo'];
        $sql = "UPDATE promociones SET estado='E' WHERE id=$idpro";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
