<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$dbmysql = new database();
date_default_timezone_set('America/Guayaquil');
$sOrder = '';
$local = $_SESSION["usu_local_id"];
$aColumns = array('id','local_id', 'servicio_id','nombre', 'dia', 'fecha_inicio','fecha_fin','tipo_descuento', 'valor','tramo_mayor','tramo_menor','estado');
$sIndexColumn = "id";
$sTable = "promociones";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}
/* * *******************
 * ** Ordenamiento ****
 * ******************* */

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY id") {
        $sOrder = "ORDER BY id DESC";
    }
}

/* * *******************
 * ** Filtrado ****
 * ******************* */

$sWhere = "";
if ($_GET['sSearch'] != "") {
    $sWhere = "WHERE (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}
/* Filtrado por Columnas individuales */
for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        if ($sWhere == "") {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
    }
}

/* * ***********************
 * ** CREACION DEL SQL ****
 * *********************** */

$sWhere = ($sWhere == '') ? 'WHERE local_id=' . $local.' AND estado="A" ' : $sWhere . ' AND local_id=' . $local.' AND estado="A"';
$sQuery = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(",", $aColumns)) . "
            FROM $sTable
            $sWhere
            $sOrder
            $sLimit";
//echo $sQuery;
$rResult = $dbmysql->query($sQuery);
if (!$rResult) {
    printf("Error: %s\n", $mysqli->error);
}
/* * **********************************************************
 * ** Longitud conjunto de datos después de la filtración ****
 * ********************************************************** */
$sQuery = "SELECT FOUND_ROWS() as Validas";
$rResultFilterTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultFilterTotal = $rResultFilterTotal->fetch_object();
$iFilteredTotal = $aResultFilterTotal->Validas; //Total de Registros Validos

$sQuery = "SELECT COUNT(" . $sIndexColumn . ")AS contador FROM   $sTable $sOrder;";
$rResultTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultTotal = $rResultTotal->fetch_object();
$iTotal = $aResultTotal->Validas;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iFilteredTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array());
//print_r($output);
/* * **********************
 * ** RESULTADO FINAL ****
 * ********************** */
while ($aRow = $rResult->fetch_object()) {
    $sql="SELECT nombre FROM servicios WHERE id=$aRow->servicio_id";
    $rRServ = $dbmysql->query($sql);
    $aRowServ = $rRServ->fetch_object();
    switch ($aRow->dia) {
        case 1:$dia='Lunes';break;
        case 2:$dia='Martes';break;
        case 3:$dia='Miercoles';break;
        case 4:$dia='Jueves';break;
        case 5:$dia='Viernes';break;
        case 6:$dia='Sabado';break;
        case 7:$dia='Domingo';break;
        case 8:$dia='Todos';break;
    }
    $aplica='Cantidad Mayor a: '.$aRow->tramo_mayor.' y Menor a: '.$aRow->tramo_menor;
    $descuento=($aRow->tipo_descuento=='P')?'%':'$';
    $output['aaData'][] = array('' . $aRow->id . '',
        '' . $dia . '',
        '' . $aRow->nombre . '',
        '' . $aRowServ->nombre . '',
        '' . $aplica . '',
        '' .$descuento.' '. $aRow->valor . '',
        '<div class="btnAccion" style="text-align: center; display: inline-block;"><a class="btn btn-default btn-xs txt-color-magenta" title="Actualizar Promoción" href="javascript:actualizarPromociones(\'' . $aRow->id . '\')">
                    <i class="fa fa-pencil"></i>
                </a>
                <a class="btn btn-default btn-xs txt-color-green" title="Elimnar Promoción" href="javascript:eliminarPromociones(\'' . $aRow->id . '\')">
                    <i class="fa fa-trash-o"></i>
                </a></div>');
}
echo json_encode($output);
