<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
date_default_timezone_set('America/Guayaquil');

class cajas {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }

    public function listarCajas() {
        $sql = "SELECT * FROM cajas";
        $resul = $this->database->query($sql);
        return $resul;
    }

    public function enviarDatosCaja() {
        $idcaja = $_POST['codigoCaja'];
        $sql = "SELECT * FROM cajas WHERE id =$idcaja";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $lista['datosCaja'] = array(
            "IDCaja" => $row->id,
            "local_id" => $row->local_id,
            "ip" => $row->ip,
            "nombre" => $row->nombre,
            "emision" => $row->emision,
            "serie" => $row->serie,
            "estado" => $row->estado
        );
        echo $encode = json_encode($lista);
    }

    public function guardaDatosCaja() {
        $nombre = strtoupper($_POST["nombre"]);
        $ip = $_POST['ip'];
        $local_id = $_SESSION["usu_local_id"];
        $emision = $_POST['emision'];
        $serie = $_POST['serie'];
        $sqlConsulta = "SELECT * FROM cajas WHERE ip='$ip' AND nombre='$nombre' AND local_id='$local_id';";
        $resul = $this->database->query($sqlConsulta);
        if ($resul->num_rows == 0) {
            $sql = "INSERT INTO cajas(local_id,ip,nombre,emision,serie,estado) VALUES
            ('$local_id','$ip','$nombre','$emision','$serie','A')";
            $resul = $this->database->query($sql);
            if ($resul) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function ActualizarDatos() {
        $idcaja = $_POST['IDCaja'];
        $nombre = strtoupper($_POST["nombre"]);
        $ip = $_POST['ip'];
        $emision = $_POST['emision'];
        $serie = $_POST['serie'];

        $sql = "UPDATE cajas SET 
                    nombre='$nombre',    
                    emision='$emision',
                    serie='$serie',
                    ip='$ip'
                WHERE id=$idcaja;";
        $resul = $this->database->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function EliminarCaja() {
        $caja = $_POST['idscaja'];
        $sql = "UPDATE cajas SET estado='E' WHERE id=$caja";
        $val = $this->database->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function CierreCaja() {

        $fecha = date('Y-m-d');

        $sql_resumen = "SELECT sum(p.valor) as suma, p.tipos_pago_id, t.nombre  FROM pagos_movimientos p, tipos_pagos t, movimientos m where m.id = p.movimiento_id and t.id = p.tipos_pago_id and m.caja_id = " . $_SESSION['id_caja'] . " and m.fecha = '$fecha' and m.estados_movimiento_id = 1 group  by tipos_pago_id";
        $result_resumen = $this->database->query($sql_resumen);

        $detalles['rubros'] = array();
        $detalles['tipo_id'] = array();
        $detalles['nombre'] = array();

        while ($row_resumen = mysqli_fetch_object($result_resumen)) {

            $detalles['rubros'][] = $row_resumen->suma;
            $detalles['tipo_id'][] = $row_resumen->tipos_pago_id;
            $detalles['nombre'][] = $row_resumen->nombre;
        }

        $sql_tipos = "SELECT * FROM tipos_pagos";
        $result_tipos = $this->database->query($sql_tipos);


        while ($row_tipos = mysqli_fetch_object($result_tipos)) {


            if (!in_array($row_tipos->id, $detalles['tipo_id'])) {

                $detalles['rubros'][] = 0;
                $detalles['nombre'][] = $row_tipos->nombre;
                $detalles['tipo_id'][] = $row_tipos->id;
            }
        }

        if (isset($_POST['parametros']))
            echo json_encode($detalles);
        else
            return $detalles;
    }

    public function cerrarCaja() {

        try {
            $fecha = date('Y-m-d');
            $hora = date('H:i');
            $total = 0;

            $totales = $this->CierreCaja();

            $sqlCajaId = "SELECT id from caja_diaria where caja_id = " . $_SESSION['id_caja'] . " and fecha = '$fecha'";
            $resultCajaId = $this->database->query($sqlCajaId);
            $rowCajaId = mysqli_fetch_object($resultCajaId);
            $cajaId = $rowCajaId->id;

            foreach ($totales['rubros'] as $key => $value) {
                $idRubro = $totales['tipo_id'][$key];
                $totalRubro = number_format($value, 2);
                $sqlDetallesCierre = "INSERT INTO rubros_caja_diaria (caja_diaria_id, monto, tipos_pago_id) VALUES ( $cajaId , $totalRubro, $idRubro )";
                $total = $total + $value;
                $this->database->query($sqlDetallesCierre);
            }

            $sqlCerrarCaja = "UPDATE caja_diaria SET hora_cierre = '$hora', usuario_cierre = " . $_SESSION['user_id'] . ", estado = 'C' , monto_cierre = $total where caja_id = " . $_SESSION['id_caja'] . " and fecha = '$fecha'";
            $this->database->query($sqlCerrarCaja);
            
            $sqlCerrarSesiones = "UPDATE sesiones_infantes set estado = 'T' where estado = 'A' ";
            $this->database->query($sqlCerrarSesiones);
            
            echo 1;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function aperturaCaja($parametros) {
        $monto = $parametros['monto'];
        $observacion = $parametros['observacion'];
        $fecha = date('Y-m-d');
        $hora_apertura = date('H:i');
        $caja_id = $_SESSION['id_caja'];
        $usuario_apertura = $_SESSION["user_id"];
        $sqlValida = "SELECT * FROM caja_diaria WHERE fecha='$fecha' AND caja_id=$caja_id AND estado='A';";
        $val = $this->database->query($sqlValida);
        if ($val->num_rows == 0) {
            $sql = "INSERT INTO caja_diaria (fecha,caja_id,hora_apertura,usuario_apertura,monto_apertura,estado) "
                    . "VALUES('$fecha',$caja_id,'$hora_apertura',$usuario_apertura,'$monto','A')";
            $valResult = $this->database->query($sql);
            if ($valResult) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }
    public function MostrarCierreCaja(){
       $sql="SELECT cd.id as cajadiaria_id,u.nombre as nombreapertura, cd.hora_apertura as hora_apertura, cd.hora_cierre as hora_cierre, cd.fecha as fecha,  cd.monto_apertura as apertura, cd.monto_cierre as cierre, c.nombre as nombrecaja FROM cajas c, caja_diaria cd, usuarios u where cd.usuario_apertura=u.id AND cd.caja_id=c.id;";
        $resul = $this->database->query($sql);
        return $resul;
    }
    public function EnviarDatosCierre(){
        $codigo=$_POST['codigoCaja'];
        $sql="SELECT cd.id as cajadiaria_id, cd.hora_apertura as hora_apertura,
                cd.hora_cierre as hora_cierre ,cd.monto_apertura as monto_apertura,
                cd.monto_cierre as monto_cierre , tp.nombre as tipopago, 
                u.nombre as usuario, cd.hora_apertura as h_apertura,cd.hora_cierre as h_cierre 
                FROM usuarios u, caja_diaria cd, rubros_caja_diaria rcd, tipos_pagos tp 
                WHERE cd.id=5 AND rcd.caja_diaria_id=5 AND rcd.tipos_pago_id=tp.id AND cd.usuario_cierre=u.id ;";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $lista['datosCierreCaja'] = array(
            "IDCaja" => $row->cajadiaria_id,
            "hora_apertura" => $row->hora_apertura,
            "hora_cierre" => $row->hora_cierre,
            "monto_apertura" => $row->monto_apertura,
            "monto_cierre" => $row->monto_cierre,
            "tipopago" => $row->tipopago,
            "hora_apertura" => $row->hora_apertura,
            "hora_cierre" => $row->hora_cierre,
            "usuario" => $row->usuario
        );
        echo $encode = json_encode($lista);
    }
    public function getproductosCierre($fecha){
        if ($fecha == "")
        $fecha = date('y-m-d');        
        $sql = "SELECT p.nombre, (select FORMAT(sum(ds.cantidad),2) from detalles_movimientos ds , movimientos ms where ms.id = ds.movimiento_id and ds.producto_id = d.producto_id and ms.estados_movimiento_id=1 and ms.fecha = '$fecha' ) as numero, (select sum(ds.valor_total) from detalles_movimientos ds , movimientos ms, productos ps where ms.estados_movimiento_id=1 and ms.id = ds.movimiento_id and ps.id = ds.producto_id and ds.producto_id = d.producto_id and ms.fecha = '$fecha' ) as total FROM `detalles_movimientos` d, `movimientos` m, productos p WHERE  m.id = d.movimiento_id and m.estados_movimiento_id=1 and p.id = d.producto_id and m.fecha = '$fecha' and d.servicio_id IS NULL GROUP by d.producto_id";
        $val = $this->database->query($sql);
        return $val;        
    }
    
    
    public function geServiciosCierre($fecha )
    {
        if ($fecha == "")
        $fecha = date('y-m-d');          
        $sql = "SELECT s.nombre , (select FORMAT(sum(ds.cantidad),2) from detalles_movimientos ds , movimientos ms where ms.id = ds.movimiento_id and ds.servicio_id = d.servicio_id and ms.estados_movimiento_id=1 and ms.fecha = '$fecha' ) as numero, (select sum(ds.valor_total) from detalles_movimientos ds , movimientos ms, servicios ss where ms.estados_movimiento_id=1 and ms.id = ds.movimiento_id and ss.id = ds.servicio_id and ds.servicio_id = d.servicio_id and ms.fecha = '$fecha' ) as total FROM `detalles_movimientos` d, `movimientos` m, servicios s WHERE m.id = d.movimiento_id and m.estados_movimiento_id=1 and s.id = d.servicio_id and m.fecha = '$fecha' and d.producto_id IS NULL GROUP by d.servicio_id";
        $val = $this->database->query($sql);
        return $val;          
    }

}
