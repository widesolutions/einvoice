<?php

require_once('./class.phpmailer.php');
require_once('./PHPMailerAutoload.php');
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$dbmysql = new database();
$fechaCierre = date('Y-m-d');
$contenidoCierre = $_POST['contenido'];
$body = bodyMail($contenidoCierre);
$asunto = utf8_decode('Cierre de Caja Kidz .:. ' . $fechaCierre);
$mail = new PHPMailer();
$mail->IsSMTP();
try {
    /*     * ***** DATOS SERVIDOR ******** */
    $mail->Host = 'mail.wiesolutions.com';
    $mail->SMTPAuth = TRUE;
    $mail->Port = 26;
    $mail->Username = 'invoice@wiesolutions.com';
    $mail->Password = 'Invoice123*';
    $mail->ConfirmReadingTo = 'invoice@wiesolutions.com';
    $mail->AltBody = "Por favor confirme su recepción...!";
//    /*     * ***** DESTINATARIOS ******** */
    $mail->AddReplyTo('invoice@wiesolutions.com', "Facturacion KidzTime");
    $mail->AddAddress("lsubia@kidztime.com.ec", "Lore Subia");
    $mail->AddBCC("wespinosa86@gmail.com", "Willian Espinosa");
    $mail->AddBCC("dannypads1@gmail.com", "Danny Uribe");
    $mail->SetFrom('invoice@wiesolutions.com', "Facturacion KidzTime");
    /*     * ***** ENVIO DE MAIL ******** */
    $mail->Subject = $asunto;
    $mail->MsgHTML(utf8_decode($body));
    $exito = $mail->Send();
} catch (phpmailerException $e) {
    $error = $e->errorMessage();
} catch (Exception $e) {
    $error = $e->getMessage();
}

function bodyMail($contenidoCierre) {
    include ($_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/sesiones/sesiones.php');
    include ($_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/cajas/cajas.php');
    global $dbmysql;
    $sesiones = new sesiones();
    $cajas = new cajas();
    
    $fechaActual = date('Y-m-d');
    $retval = '';
    $retval .='<html><body>';
    $retval .='<div style="margin:10px;padding:20px;border-radius: 50px 0px 50px 0px;-moz-border-radius: 50px 0px 50px 0px;-webkit-border-radius: 50px 0px 50px 0px;border: 6px solid #9e999e;">
                <div style="text-align:center;"><img src="http://181.113.62.34/krayon/publico/img/logo_kidztime.png" width="180px"></div><br>
                <div style="border-bottom:1px solid #9e999e;"></div><br>
                <div>
                    <b>Estimados Administradores: </b><br>
                    <b>Presente </b><br><br>
                </div>
                <p>Detallo Cierre de Caja realizado el dia de hoy ' . $fechaActual . '...</b>
                <br>
                <p>Gracias por su antención,</p><br>
                <div style="border-bottom:1px solid #9e999e;"></div><br>
                ';
    $valorCaja = 0;
    $valorTotal = 0;
    $sqlMov = "SELECT pm.`id`,pm.tipos_pago_id,pm.`movimiento_id`,tp.nombre ,ROUND(SUM(pm.`valor`),2) as valor,m.fecha FROM `pagos_movimientos` pm,tipos_pagos tp, movimientos m WHERE tp.id=pm.tipos_pago_id AND m.id=pm.movimiento_id AND m.fecha='$fechaActual' AND m.estados_movimiento_id=1 GROUP BY `tipos_pago_id`";
    $valMov = $dbmysql->query($sqlMov);
    $retval .= '<html lang="es">
                    <table border="0">
                        <thead>
                            <tr>
                                <th colspan="3" style="width: 400px;border-bottom: 1px #000 dashed;">REPORTE DE VENTAS</th>
                            </tr>';
    $sqlCaja = "SELECT * FROM caja_diaria where fecha='$fechaActual'";
    $valCaja = $dbmysql->query($sqlCaja);
    $rowCaja = $valCaja->fetch_object();
    $valorAperturaCaja = $rowCaja->monto_apertura;

            $retval .= '</thead>
                        <tr>
                            <th>Apertura de Caja :</th>
                            <td colspan="2" style="text-align:right;">$ ' . number_format($valorAperturaCaja, 2) . '</td>
                        </tr>';
    while ($rowMov = $valMov->fetch_object()) {
        if ($rowMov->tipos_pago_id == 1) {
            $valorCaja = $valorCaja + $rowMov->valor;
        } else {
            $valorCredito = $rowMov->valor;
        }
        $retval .= '<tr>
                    <th>Ventas '.strtolower($rowMov->nombre).':</th>
                    <td colspan="2" style="text-align:right;">$ ' . number_format($rowMov->valor, 2) . '</td>
                </tr>';
    }
    $valorTotal = $valorCaja + $valorCredito;

    $retval .= '<tr>
                        <th>Total en Caja :</th>
                        <td colspan="2" style="text-align:right;">$ ' . number_format($valorCaja + $valorAperturaCaja, 2) . '</td>
                    </tr>
                    <tr>
                        <th>Total Vendido :</th>
                        <td colspan="2" style="text-align:right;">$ ' . number_format($valorTotal, 2) . '</td>
                    </tr>
                </table>
                <table border="0">
                    <thead>
                        <tr>
                            <th colspan="3" style="width: 400px;border-bottom: 1px #000 dashed;">REPORTE DE CONCURRENCIA</th>
                        </tr>     
                        <tr>
                            <th>Nombre</th>
                            <th style="text-align:right;">Cantidad</th>
                            <th style="text-align:right;">Total</th>
                        </tr> 
                    </thead>
                    <tbody>';
    $sesionesCierre = $sesiones->getConcurrenciCierre($fechaActual);
    foreach ($sesionesCierre as $sesion) {
        $retval .= '<tr>
                            <td>TOTAL ' . $sesion['nombre'] . '</td>
                            <td style="text-align:right;">' . $sesion['numero'] . '</td>
                            <td style="text-align:right;">' . $sesion['total'] . '</td>
                        </tr>';
    }
    $retval .= '</tbody>
                </table>
                <table border="0">
                    <thead>
                        <tr>
                            <th colspan="3" style="width: 400px;border-bottom: 1px #000 dashed;">REPORTE DE PRODUCTOS</th>
                        </tr>     
                        <tr>
                            <th>Nombre</th>
                            <th style="text-align:right;">Cant.</th>
                            <th style="text-align:right;">Total</th>
                        </tr> 
                    </thead>
                    <tbody>';
    $productosSesiones = $cajas->getproductosCierre($fechaActual);
    foreach ($productosSesiones as $producto) {
        $retval .= '<tr>
                            <td>TOTAL ' . $producto['nombre'] . '</td>
                            <td style="text-align:right;">' . $producto['numero'] . '</td>
                            <td style="text-align:right;">$ ' . $producto['total'] . ' </td>
                        </tr>';
    }
    $retval .= '</tbody>
            </table>    
        </html>';
    $retval .= '</div><p style="font-size:10px;color:#666"><b>Sistema realizado por WieSolutions - 2015-2016</b></p></body></html>';
    return $retval;
}
