<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
date_default_timezone_set('America/Guayaquil');

class clientes {

    function __construct() {
        $this->dbmysql = new database();
    }

    public function crear($parametros) {

        $nombres = strtoupper($parametros['nombres']);
        $apellidos = strtoupper($parametros['apellidos']);
        $identificacion = $parametros['identificacion'];
        $tipoDocumento = $parametros['tipoDocumento'];
        $telefono1 = $parametros['telefono1'];
        $telefono2 = $parametros['telefono2'];
        $direccion = strtoupper($parametros['direccion']);
        $email = $parametros['email'];

        $sqlConsulta = "SELECT * FROM clientes WHERE identificacion='$identificacion';";
        $ins = $this->dbmysql->query($sqlConsulta);
        if ($ins->num_rows == 0) {
            $sql = "INSERT INTO `clientes` (`nombres`,`apellidos`,tipo_documento,`identificacion`,`telefono1`,`telefono2`,`direccion`,`email`)
                VALUES ('" . strtoupper($nombres) . "','" . strtoupper($apellidos) . "','$tipoDocumento','$identificacion','$telefono1','$telefono2','" . strtoupper($direccion) . "','$email')";


            $insert = $this->dbmysql->query($sql);
            $datos['insert'] = $insert;
            $datos['id'] = $this->dbmysql->lastid();
        } else {
            $row = $ins->fetch_object();
            $datos['insert'] = false;
            $datos['id'] = $row->id;
        }
        echo json_encode($datos);
    }

    public function editar($parametros) {
        $nombres = strtoupper($parametros['nombres']);
        $apellidos = strtoupper($parametros['apellidos']);
        $tipoDocumento = $parametros['tipoDocumento'];
        $identificacion = $parametros['identificacion'];
        $telefono1 = $parametros['telefono1'];
        $telefono2 = $parametros['telefono2'];
        $direccion = strtoupper($parametros['direccion']);
        $email = $parametros['email'];
        $id = $parametros['id'];


        $sql = "UPDATE `clientes` SET  `nombres` = '$nombres' ,`apellidos` = '$apellidos',tipo_documento='$tipoDocumento',`identificacion` = '$identificacion',`telefono1` = '$telefono1' ,`telefono2` = '$telefono2',`direccion` = '$direccion',`email` = '$email' where id = $id;";

        $update = $this->dbmysql->query($sql);
        $datos['insert'] = $update;
        $datos['id'] = $id;
        echo json_encode($datos);
    }

    public function getClientes() {
        $sql = "SELECT id,nombre FROM `locales`;";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function validarCedulaExistente() {
        $cedula = $_POST['cedula'];
        $sql = "SELECT * FROM `clientes` WHERE identificacion= $cedula;";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function getClientePorId() {
        $id = $_POST['id'];
        $sql = "SELECT * FROM `clientes` WHERE id= $id ";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            $row = $val->fetch_object();
            $datos['datosCliente'] = array(
                "id" => $row->id,
                "nombres" => $row->nombres,
                "apellidos" => $row->apellidos,
                "tipoDocumento" => $row->tipo_documento,
                "identificacion" => $row->identificacion,
                "telefono1" => $row->telefono1,
                "telefono2" => $row->telefono2,
                "direccion" => $row->direccion,
                "email" => $row->email
            );
            $datosSesiones = $this->getSesionesCliente($datos, $row->id);
            $datosComboPromo = $this->getPromociones($datosSesiones);
            $datosFinales = $this->getFiestasCliente($datosComboPromo, $row->id);
            echo json_encode($datosFinales);
        }
    }

    public function getClientexDocumento() {
        $cedula = $_POST['cedula'];

        $sql = "SELECT * FROM `clientes` WHERE identificacion='$cedula';";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            $row = $val->fetch_object();
            $datos['datosCliente'] = array(
                "id" => $row->id,
                "nombres" => $row->nombres,
                "apellidos" => $row->apellidos,
                "identificacion" => $row->identificacion,
                "telefono1" => $row->telefono1,
                "telefono2" => $row->telefono2,
                "direccion" => $row->direccion,
                "email" => $row->email
            );
            $datosSesiones = $this->getSesionesCliente($datos, $row->id);
            $datosComboPromo = $this->getPromociones($datosSesiones);
            $datosFiestas = $this->getFiestasCliente($datosComboPromo, $row->id);
            $datosFinales = $this->obtenerDetalleFiestas($datosFiestas, $fiesta);

            echo json_encode($datosFinales);
        }
    }

    public function getSesionesCliente($datos, $idCliente) {
        $fechaActual = date('Y-m-d');
        $sqlSesiones = "SELECT s.id AS sesion_id,s.cliente_id,s.fecha,si.id AS sesionInfantes_id,si.*,i.primer_nombre,i.primer_apellido,i.fecha_nacimiento, se.nombre AS nombre_servicio FROM `sesiones_infantes` si, sesiones s, infantes i, servicios se WHERE si.servicio_id=se.id AND si.`sesion_id`=s.id AND si.infante_id = i.id AND si.estado='A' AND s.fecha='$fechaActual' AND s.cliente_id='$idCliente';";
        $valSesiones = $this->dbmysql->query($sqlSesiones);
        if ($valSesiones->num_rows > 0) {
            while ($rowSesiones = $valSesiones->fetch_object()) {
                $transcurrido = ($rowSesiones->todo_dia == 1) ? 1 : $this->calcularTiempoTranscurrido($rowSesiones->hora_inicio);
//                $cantidad = ($rowSesiones->todo_dia == 1) ? 1 : $this->calculoCantidad($rowSesiones->id, $transcurrido, $tipo);
//                $valorApagar = ($rowSesiones->todo_dia == 1) ? $cantidad * $rowSesiones->precio_venta : $this->calculoValorpagar($rowSesiones->id, $transcurrido, $rowSesiones->precio_venta, $tipo);
                $datos2['datosSesiones'][] = array(
                    "id" => $rowSesiones->sesionInfantes_id,
                    "sesion_id" => $rowSesiones->sesion_id,
                    "fecha" => $rowSesiones->fecha,
                    "hora_inicio" => $rowSesiones->hora_inicio,
                    "transcurrido" => $transcurrido,
                    "estado" => $rowSesiones->estado,
                    "observacion" => $rowSesiones->observacion,
                    "servicio_id" => $rowSesiones->servicio_id,
                    "servicio_nombre" => $rowSesiones->nombre_servicio,
                    "primer_nombre" => $rowSesiones->primer_nombre,
                    "primer_apellido" => $rowSesiones->primer_apellido,
                );
            }
        }
        if ($datos2) {
            $datosFinales = array_merge($datos, $datos2);
        } else {
            $datosFinales = $datos;
        }

        return $datosFinales;
    }

    public function getFiestasCliente($datos, $idCliente) {
        $sqlFiestas = "SELECT f.id as codigoFiesta,f.*,s.* FROM `fiestas` f, servicios s WHERE s.id=f.servicio_id AND f.estado='C' AND f.cliente_id='$idCliente';";
        $valFiestas = $this->dbmysql->query($sqlFiestas);
        if ($valFiestas->num_rows > 0) {
            $rowFiestas = $valFiestas->fetch_object();
            $datos3['datosFiestas'] = array(
                "id" => $rowFiestas->codigoFiesta,
                "anfitrion" => $rowFiestas->anfitrion,
                "fecha" => $rowFiestas->fecha,
                "detalles" => 'Anticipo',
                "servicio" => $rowFiestas->nombre
            );
        } else {
            $sqlFiestas = "SELECT f.id as codigoFiesta,f.*,s.* FROM `fiestas` f, servicios s WHERE s.id=f.servicio_id AND f.estado='PL' AND  f.cliente_id='$idCliente';";
            $valFiestas = $this->dbmysql->query($sqlFiestas);
            if ($valFiestas->num_rows > 0) {
                $rowFiestas = $valFiestas->fetch_object();

                $datos3['datosFiestas'] = array(
                    "id" => $rowFiestas->codigoFiesta,
                    "anfitrion" => $rowFiestas->anfitrion,
                    "fecha" => $rowFiestas->fecha,
                    "detalles" => 'Pago Total',
                    "servicio" => $rowFiestas->nombre
                );
            }
        }
        if ($datos3) {
            $datosFinales = array_merge($datos, $datos3);
        } else {
            $datosFinales = $datos;
        }
        return $datosFinales;
    }

    public function getPromociones($datos) {
        $hoy = date('N');
        $fechaActual = date('Y-m-d');
        if ($datos['datosSesiones']) {
            foreach ($datos['datosSesiones'] as $key => $value) {
                $servicioId = $value['servicio_id'];
                $sqlPromo2 = "SELECT * FROM `promociones` WHERE (dia=$hoy OR dia=8) AND fecha_inicio <= '$fechaActual' AND fecha_fin >= '$fechaActual' AND servicio_id=$servicioId AND estado = 'A';";
                $valPromo2 = $this->dbmysql->query($sqlPromo2);
                if ($valPromo2->num_rows > 0) {
                    $rowPromo2 = $valPromo2->fetch_object();
                    $datos['datosSesiones'][$key]['diaPromocion'] = $rowPromo2->dia;
                    $datos['datosSesiones'][$key]['promocion'] = $rowPromo2->nombre;
                    $datos['datosSesiones'][$key]['codigoPromocion'] = $rowPromo2->id;
                } else {
                    $datos['datosSesiones'][$key]['diaPromocion'] = 0;
                    $datos['datosSesiones'][$key]['promocion'] = '';
                    $datos['datosSesiones'][$key]['codigoPromocion'] = 0;
                }
            }
        }
        $sqlPromo = "SELECT * FROM `promociones` WHERE (dia=$hoy OR dia=8) AND fecha_inicio <= '$fechaActual' AND fecha_fin >= '$fechaActual' AND estado = 'A';";
        $valPromo = $this->dbmysql->query($sqlPromo);
        if ($valPromo->num_rows > 0) {
            while ($rowPromo = $valPromo->fetch_object()) {
                $datos2['comboPromociones'][] = array(
                    "id" => $rowPromo->id,
                    "nombre" => $rowPromo->nombre,
                    "tipo_descuento" => $rowPromo->tipo_descuento,
                    "valor" => $rowPromo->valor
                );
            }
        }
        if ($datos2) {
            $datosFinales = array_merge($datos, $datos2);
        } else {
            $datosFinales = $datos;
        }
        return $datosFinales;
    }

    public function searchCliente() {
        $criterio = $_POST['search'];

        $codigo = strstr($criterio, ':', true);

        if ($codigo != "") {
            $sql = "SELECT * FROM `clientes` WHERE id = $codigo";
        } else {
            $sql = "SELECT * FROM `clientes` WHERE CONCAT(nombres,' ',apellidos) like '%$criterio%' or  nombres like '%$criterio%' or apellidos like '%$criterio%' or identificacion like '%$criterio%' or CONCAT(apellidos,' ',nombres) like '%$criterio%' LIMIT 6 ";
        }

        $val = $this->dbmysql->query($sql);
        while ($row = $val->fetch_object()) {
            $datos[] = array(
                "id" => $row->id,
                "nombres" => $row->nombres,
                "apellidos" => $row->apellidos,
                "identificacion" => $row->identificacion,
            );
        }
        echo json_encode($datos);
    }

    public function calcularTiempoTranscurrido($horaInicio) {
        $hora = date('H:i') . ' ';
        $hora_entrada = date('H:i', strtotime($horaInicio));
        $tiempoTrans = (strtotime($hora) - strtotime($hora_entrada)) / 60;
        return $timepotrans = $this->transformarTiempo($tiempoTrans);
    }

    public function transformarTiempo($tt) {
        if ($tt == 60) {
            $hora = 1;
            $minutos = '00';
        } elseif ($tt < 60) {
            $hora = '00';
            $minutos = ($tt > 0) ? $tt : '00';
        } else {
            do {
                $hora++;
                $tt = $tt - 60;
            } while ($tt > 60);
            $minutos = $tt;
        }
        $horaI = ($hora . ':' . $minutos);
        return $horaI;
    }

    public function guardarCabeceraFactura($parametros) {
        $idCliente = $parametros['idCliente'];
        $tipoDocumento = $parametros['tipoDocumento'];
        $cedula = $parametros['cedula'];
        $nombre = $parametros['nombre'];
        $apellido = $parametros['apellido'];
        $direccion = $parametros['direccion'];
        $email = $parametros['email'];
        $telefono = $parametros['telefono'];
        $sesion = $_POST['sesiones'];
        $fiesta = $parametros['fiestas'];
        $sesiones = explode(',', $sesion);
        $sesionPromo = explode(',', $_POST['sesionPromo']);
        $tipo = $_POST['tipo'];
        $numeroSesiones = count($sesiones);
        $fechaActual = date('Y-m-d');
        $valorDescuento = 0;

        $sqlSesion = "SELECT * FROM `sesiones` WHERE cliente_id=$idCliente AND fecha = '$fechaActual' AND estado='A';";
        $valSesion = $this->dbmysql->query($sqlSesion);
        if ($valSesion->num_rows > 0) {
            $rowSesion = $valSesion->fetch_object();
            $codSesion = $rowSesion->id;
            $datos['datosFactura'] = array(
                "id" => $idCliente,
                "tipoDocumento" => $tipoDocumento,
                "cedula" => $cedula,
                "nombre" => $nombre,
                "apellido" => $apellido,
                "direccion" => $direccion,
                "email" => $email,
                "telefono" => $telefono,
                "sesion" => $codSesion,
                "infantes" => $sesion,
                "fiesta" => $fiesta
            );
            $sqlSesionInfantes = "SELECT si.id as codigoInfante, si.*,s.* FROM sesiones_infantes si, servicios s  WHERE si.servicio_id=s.id AND si.id IN ($sesion) AND si.estado ='A';";
            $valSesionesInfantes = $this->dbmysql->query($sqlSesionInfantes);
            if ($valSesionesInfantes->num_rows > 0) {
                $i = 0;
                while ($rowSesiones = $valSesionesInfantes->fetch_object()) {


                    $codigoSesion = $rowSesiones->sesion_id;
                    $codigoSesionInfante = $rowSesiones->codigoInfante;
                    $transcurrido = ($rowSesiones->todo_dia == 1) ? 1 : $this->calcularTiempoTranscurrido($rowSesiones->hora_inicio);
                    $cantidad = ($rowSesiones->todo_dia == 1) ? 1 : $this->calculoCantidad($rowSesiones->id, $transcurrido, $tipo);
                    //BUSCAR PROMOCIONES DEL DIA
//                    echo $cantidad;
                    if ($sesionPromo[$i] != "") {
                        $promocion = $sesionPromo[$i];
                        $datosPromocion = $this->aplicarPromocion($cantidad, $sesionPromo[$i], $rowSesiones->precio_venta);
                        $valorDescuento = $datosPromocion['valorDescuento'];
                        $cantidad = $datosPromocion['cantidad'];
                        $this->actualizaSesionInfante($codigoSesionInfante, $promocion);
                    } else {
                        $promocion = 0;
                        $valorDescuento = 0;
                        $cantidad = $cantidad;
                        $this->actualizaSesionInfante($codigoSesionInfante, $promocion);
                    }
                    $valorApagar = ($rowSesiones->todo_dia == 1) ? ($cantidad * $rowSesiones->precio_venta) - $valorDescuento : $this->calculoValorpagar($rowSesiones->id, $transcurrido, $rowSesiones->precio_venta, $tipo, $valorDescuento);

                    $datos['sesionesInfantes'][] = array(
                        "id" => $rowSesiones->id,
                        "sesion_id" => $codigoSesion,
                        "sesiones" => $sesion,
                        "promocion" => $promocion,
                        "descripcion" => $rowSesiones->nombre,
                        "todo_dia" => $rowSesiones->todo_dia,
                        "hora_inicio" => $rowSesiones->hora_inicio,
                        "cantidad" => $cantidad,
                        "codigo_barras" => $rowSesiones->codigo_barras,
                        "vunitario" => $rowSesiones->precio_venta,
                        "descuento" => $valorDescuento,
                        "vtotal" => $valorApagar,
                        "ivanoiva" => $rowSesiones->iva,
                        "productoServicio" => 2,
                        "tipo" => $tipo
                    );

                    $i++;
                }
            }
            $sqlProductos = "SELECT sp.*,p.* FROM sesiones_productos sp, productos p WHERE sp.producto_id=p.id AND sp.sesion_id = $codSesion AND sp.estado='A';";
            $valProductos = $this->dbmysql->query($sqlProductos);
            if ($valProductos->num_rows > 0) {
                while ($rowProductos = $valProductos->fetch_object()) {
                    $valorApagar = $rowProductos->cantidad * $rowProductos->precio_venta;
                    $datos['sesionesProductos'][] = array(
                        "id" => $rowProductos->id,
                        "producto_id" => $rowProductos->fecha,
                        "nombre" => $rowProductos->nombre,
                        "cantidad" => $rowProductos->cantidad,
                        "codigo_barras" => $rowProductos->codigo_barras,
                        "vunitario" => $rowProductos->precio_venta,
                        "vtotal" => $valorApagar,
                        "ivanoiva" => $rowProductos->iva,
                        "productoServicio" => 1
                    );
                }
            }
            $sqlFiestas = "SELECT f.id as codigoFiesta,f.*,s.* FROM `fiestas` f, servicios s WHERE s.id=f.servicio_id AND f.estado='C' AND  f.id=$fiesta;";
            $valFiestas = $this->dbmysql->query($sqlFiestas);
            if ($valFiestas->num_rows > 0) {
                $rowFiestas = $valFiestas->fetch_object();
                $datos['datosFiestas'] = array(
                    "id" => $rowFiestas->codigoFiesta,
                    "servicio_id" => $rowFiestas->servicio_id,
                    "nombre" => 'Anticipo ' . $rowFiestas->nombre,
                    "cantidad" => '1',
                    "codigo_barras" => $rowFiestas->codigo_barras,
                    "vunitario" => $rowFiestas->anticipo,
                    "descuento" => 0,
                    "vtotal" => $rowFiestas->anticipo,
                    "ivanoiva" => '1',
                    "productoServicio" => 2
                );
            }

            $sqlFiestas = "SELECT f.id as codigoFiesta,f.*,s.* FROM `fiestas` f, servicios s WHERE s.id=f.servicio_id AND f.estado='PL' AND  f.id=$fiesta;";
            $valFiestas = $this->dbmysql->query($sqlFiestas);
            if ($valFiestas->num_rows > 0) {
                $rowFiestas = $valFiestas->fetch_object();
                $valorMinimo = $rowFiestas->cantidad_minima;
                $asistentes = $rowFiestas->total_infantes;
                $cantidadTotal = ($asistentes <= $valorMinimo) ? $valorMinimo : $asistentes;
                $valorXinfante = (($rowFiestas->precio_venta / ($valorMinimo !== 0) ? $valorMinimo : 1))/1.12;
                $valorPagarInfantes = ($valorXinfante * $cantidadTotal) - ($rowFiestas->anticipo / 1.12);
                $datos['datosFiestas'] = array(
                    "id" => $rowFiestas->codigoFiesta,
                    "producto_id" => $rowFiestas->fecha,
                    "nombre" => 'Servicio de Fiesta ' . $rowFiestas->nombre . ', Aplica descuento por pago de Anticipo: $ ' . $rowFiestas->anticipo,
                    "cantidad" => (int)$cantidadTotal,
                    "codigo_barras" => $rowFiestas->codigo_barras,
                    "vunitario" => $valorXinfante,
                    "descuento" => ($rowFiestas->anticipo)/1.12,
                    "vtotal" => $valorPagarInfantes,
                    "ivanoiva" => $rowFiestas->iva,
                    "productoServicio" => 2
                );
                $sqlProductos = "SELECT df.id as detalleFiestaId, df.*,p.* FROM `fiestas_detalles` df, productos p WHERE df.producto_id=p.id AND df.fiesta_id=$rowFiestas->codigoFiesta;";
                $valProductos = $this->dbmysql->query($sqlProductos);
                if ($valProductos->num_rows > 0) {
                    while ($rowProductos = $valProductos->fetch_object()) {
                        $valorApagar = $rowProductos->cantidad * ($rowProductos->valor/1.12);
                        $datos['detallesFiesta'][] = array(
                            "id" => $rowProductos->detalleFiestaId,
                            "producto_id" => $rowProductos->producto_id,
                            "nombre" => $rowProductos->nombre,
                            "cantidad" => $rowProductos->cantidad,
                            "codigo_barras" => $rowProductos->codigo_barras,
                            "vunitario" => ($rowProductos->valor/1.12),
                            "vtotal" => $valorApagar,
                            "ivanoiva" => $rowProductos->iva,
                            "productoServicio" => 1
                        );
                    }
                }
            }
        } else {
            $datos['datosFactura'] = array(
                "id" => $idCliente,
                "tipoDocumento" => $tipoDocumento,
                "cedula" => $cedula,
                "nombre" => $nombre,
                "apellido" => $apellido,
                "direccion" => $direccion,
                "email" => $email,
                "telefono" => $telefono,
                "sesion" => 0,
                "infantes" => $sesion,
                "fiesta" => $fiesta
            );
            $sqlFiestas = "SELECT f.id as codigoFiesta,f.*,s.* FROM `fiestas` f, servicios s WHERE s.id=f.servicio_id AND f.estado='C' AND  f.id=$fiesta;";
            $valFiestas = $this->dbmysql->query($sqlFiestas);
            if ($valFiestas->num_rows > 0) {
                $rowFiestas = $valFiestas->fetch_object();
                $datos['datosFiestas'] = array(
                    "id" => $rowFiestas->codigoFiesta,
                    "servicio_id" => $rowFiestas->servicio_id,
                    "nombre" => 'Anticipo ' . ucwords(strtolower($rowFiestas->nombre)) . ', para ' . $rowFiestas->total_infantes . ' niños, del Anfition: ' . strtoupper($rowFiestas->anfitrion),
                    "cantidad" => '1',
                    "codigo_barras" => $rowFiestas->codigo_barras,
                    "vunitario" => $rowFiestas->anticipo,
                    "descuento" => 0,
                    "vtotal" => ($rowFiestas->anticipo)/1.12,
                    "ivanoiva" => '1',
                    "productoServicio" => 2
                );
            }

            $sqlFiestas = "SELECT f.id as codigoFiesta,f.*,s.* FROM `fiestas` f, servicios s WHERE s.id=f.servicio_id AND f.estado='PL' AND  f.id=$fiesta;";
            $valFiestas = $this->dbmysql->query($sqlFiestas);
            if ($valFiestas->num_rows > 0) {
                $rowFiestas = $valFiestas->fetch_object();
                $valorMinimo = $rowFiestas->cantidad_minima;
                $asistentes = $rowFiestas->total_infantes;
                $valorXinfante = (($rowFiestas->precio_venta / ($valorMinimo !== 0) ? $valorMinimo : 1))/1.12;
                $cantidadTotal = ($asistentes <= $valorMinimo) ? $valorMinimo : $asistentes;
                $valorPagarInfantes = ( $valorXinfante * $cantidadTotal) - ($rowFiestas->anticipo / 1.12);
                $datos['datosFiestas'] = array(
                    "id" => $rowFiestas->codigoFiesta,
                    "producto_id" => $rowFiestas->fecha,
                    "nombre" => 'Servicio de Fiesta ' . $rowFiestas->nombre . ', Aplica descuento por pago de Anticip: $ ' . $rowFiestas->anticipo,
                    "cantidad" => (int)$cantidadTotal,
                    "codigo_barras" => $rowFiestas->codigo_barras,
                    "vunitario" => $valorXinfante,
                    "descuento" => $rowFiestas->anticipo,
                    "vtotal" => $valorPagarInfantes,
                    "ivanoiva" => $rowFiestas->iva,
                    "productoServicio" => 2
                );
                $sqlProductos = "SELECT df.id as detalleFiestaId, df.*,p.* FROM `fiestas_detalles` df, productos p WHERE df.producto_id=p.id AND df.fiesta_id=$rowFiestas->codigoFiesta;";
                $valProductos = $this->dbmysql->query($sqlProductos);
                if ($valProductos->num_rows > 0) {
                    while ($rowProductos = $valProductos->fetch_object()) {
                        $valorApagar = $rowProductos->cantidad * ($rowProductos->valor/1.12);
                        $datos['detallesFiesta'][] = array(
                            "id" => $rowProductos->detalleFiestaId,
                            "producto_id" => $rowProductos->producto_id,
                            "nombre" => $rowProductos->nombre,
                            "cantidad" => $rowProductos->cantidad,
                            "codigo_barras" => $rowProductos->codigo_barras,
                            "vunitario" => ($rowProductos->valor/1.12),
                            "vtotal" => $valorApagar,
                            "ivanoiva" => $rowProductos->iva,
                            "productoServicio" => 1
                        );
                    }
                }
            }
        }
        echo json_encode($datos);
    }

    public function obtenerDetalleFiestas($datos, $fiesta) {
        $sqlProductos = "SELECT df.id as detalleFiestaId, df.*,p.* FROM `fiestas_detalles` df, productos p WHERE df.producto_id=p.id AND df.fiesta_id=$fiesta;";
        $valProductos = $this->dbmysql->query($sqlProductos);
        if ($valProductos->num_rows > 0) {
            while ($rowProductos = $valProductos->fetch_object()) {
                $valorApagar = $rowProductos->cantidad * $rowProductos->valor;
                $datos2['detallesFiesta'][] = array(
                    "id" => $rowProductos->detalleFiestaId,
                    "producto_id" => $rowProductos->producto_id,
                    "nombre" => $rowProductos->nombre,
                    "cantidad" => $rowProductos->cantidad,
                    "codigo_barras" => $rowProductos->codigo_barras,
                    "vunitario" => $rowProductos->valor,
                    "vtotal" => $valorApagar,
                    "ivanoiva" => $rowProductos->iva,
                    "productoServicio" => 1
                );
            }
        }
        if ($datos2) {
            $datosFinales = array_merge($datos, $datos2);
        } else {
            $datosFinales = $datos;
        }

        return $datosFinales;
    }

    public function actualizaSesionInfante($codigo, $promocion) {
        $sql = "UPDATE sesiones_infantes SET promocion_id=$promocion WHERE id=$codigo;";
        $this->dbmysql->query($sql);
    }

    public function calculoValorpagar($servicio, $transcurrido, $valor, $tipo, $valorDescuento) {
        $tiempo = explode(':', $transcurrido);
        $hora = $tiempo[0];
        $minuto = $tiempo[1];

        if ($tipo == 'F') {
            $valorFraccionMinuto = ($minuto * $valor) / 60;
            $valorFraccionHora = $hora * $valor;
            return $valorFraccion = number_format(($valorFraccionMinuto + $valorFraccionHora) - $valorDescuento, 2);
        } else {
            $sqlTolera = "SELECT * FROM `servicios_sesiones` WHERE servicio_id=$servicio";
            $valtolera = $this->dbmysql->query($sqlTolera);
            $rowTolera = $valtolera->fetch_object();
            $tiempoTolerancia = $rowTolera->tiempo_tolerancia;
            if ($minuto > $tiempoTolerancia) {
                $hora = $hora + 1;
            } else {
                $hora = $hora;
            }
            $valorPago = $hora * $valor;

            return $valorPagar = number_format($valorPago - $valorDescuento, 2);
        }
    }

    public function calculoCantidad($servicio, $transcurrido, $tipo) {
        $tiempo = explode(':', $transcurrido);
        $hora = $tiempo[0];
        $minuto = $tiempo[1];
        if ($tipo == 'F') {
            $valor = $hora . '.' . $minuto;
            return $valor = number_format($valor, 2);
        } else {
            $sqlTolera = "SELECT * FROM `servicios_sesiones` WHERE servicio_id=$servicio";
            $valtolera = $this->dbmysql->query($sqlTolera);
            $rowTolera = $valtolera->fetch_object();
            $tiempoTolerancia = $rowTolera->tiempo_tolerancia;
            if ($minuto > $tiempoTolerancia) {
                $hora = $hora + 1;
            }
            return $hora;
        }
    }

    private function aplicarPromocion($cantidad, $sesionPromo, $valoItem) {
        $select = "SELECT * FROM `promociones` WHERE id= $sesionPromo";
        $result = $this->dbmysql->query($select);
        $row = $result->fetch_object();
//        print_r($row);
        if (($row->tramo_mayor != 0 || $row->tramo_menor != 0) && $row->tramo_mayor <= $cantidad && $row->tramo_menor >= $cantidad) {
            $resultado['cantidad'] = ($cantidad % 2 == 0) ? $cantidad / 2 : ($cantidad + 1) / 2;
            $resultado['valorDescuento'] = 0;
        } else {
            $resultado['cantidad'] = $cantidad;
            if ($row->tipo_descuento == 'V') {
                for ($x = 1; $x <= $cantidad; $x++) {
                    $resultado['valorDescuento'] = $resultado['valorDescuento'] + $row->valor;
                }
            } elseif ($row->tipo_descuento == 'P') {
                for ($x = 1; $x <= $cantidad; $x++) {
                    $resultado['valorDescuento'] = $resultado['valorDescuento'] + (( $valoItem * $row->valor) / 100);
                }
            }
//            
        }
//        print_r( $resultado);
        return $resultado;
    }

}
