<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
date_default_timezone_set('America/Guayaquil');

class facturas {

    function __construct() {
        $this->dbmysql = new database();
    }

    public function getNumeroFactura() {
        $valor = '';
        $ip = $_SESSION['direccion_ip'];
        $sql = "SELECT * FROM `cajas` WHERE ip='$ip';";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            $row = $val->fetch_object();
            $establecimiento = '001';
            $emision = $row->emision;
            switch (strlen($row->serie)) {
                case 1:$secuencial = '000000' . $row->serie;
                    break;
                case 2:$secuencial = '00000' . $row->serie;
                    break;
                case 3:$secuencial = '0000' . $row->serie;
                    break;
                case 4:$secuencial = '000' . $row->serie;
                    break;
                case 5:$secuencial = '00' . $row->serie;
                    break;
                case 6:$secuencial = '0' . $row->serie;
                    break;
                case 7:$secuencial = $row->serie;
                    break;
            }
            $valor = $establecimiento . '-' . $emision . '-' . $secuencial;
            return $valor;
        }
    }

    public function guardarCabeceraFactura($parametros) {
        $local = $_SESSION["usu_local_id"];
        $cajaId = $_SESSION["id_caja"];
        $fecha = date('Y-m-d');
        $fechaHora = date('Y-m-d H:i');
        $usuario = $_SESSION["user_id"];
        $cliente = $parametros['idCliente'];
        $codSesion = $parametros['codSesion'];
        $codInfantes = $parametros['codInfantes'];
        $codFiesta = $parametros['codFiesta'];
        $tipoMovimiento = 1; //VENTAS
        $numeroFactura = $parametros['numeroFactura'];
        $subtotal = $parametros['subtotal'];
        $iva = $parametros['iva'];
        $subtotalSinIva = $parametros['subtotalSinIva'];
        $total = $parametros['total'];
        $fiestas=($codFiesta!=='0')?'S':'N';
        $estado = 1;
        $sqlConsulta = "SELECT * FROM movimientos WHERE numero_factura='$numeroFactura';";
        $val = $this->dbmysql->query($sqlConsulta);
        if ($val->num_rows == 0) {
            $sqlMovimientos = "INSERT INTO movimientos( 
                            `fecha` ,
                            `local_id` ,
                            `usuario_id` ,
                            `tipos_movimiento_id` ,
                            `fiestas` ,
                            `cliente_id` ,
                            `sesion_id`,
                            `numero_factura` ,
                            `subtotal` ,
                            `iva` ,
                            `subtotal_sin_iva` ,
                            `total` ,
                            `estados_movimiento_id`,
                            `caja_id`
                        )VALUES ('$fechaHora',$local, $usuario, $tipoMovimiento, '$fiestas',$cliente, $codSesion,'$numeroFactura', $subtotal, $iva, $subtotalSinIva, $total, $estado,$cajaId);";
            $valcod = $this->dbmysql->query($sqlMovimientos);
            $codigoMovimiento = $this->dbmysql->lastid();
            $respDetalle = $this->guardarDetalleFactura($codigoMovimiento, $parametros['items']);
            $respPago = $this->guardarPagoFactura($codigoMovimiento, $parametros['pagos']);
            $respSesiones = $this->cerrarSesiones($codSesion, $parametros['items']);
              $respFiestas = ($codFiesta!='0')?$this->actualizarFiestas($codFiesta):''; 
            if ($valcod === true) {
                $this->actualizarSerieDocumento();
                echo $codigoMovimiento;
            } else {
                echo 0;
            }
        }
    }

    public function guardarDetalleFactura($codigoMovimiento, $items) {
        $cantidadItems = count($items);
        $resp = 0;
        for ($x = 0; $x < $cantidadItems; $x++) {
            $tabla = ($items[$x]['productoServicio'] == 1) ? 'productos' : 'servicios';
            $sql = "SELECT id FROM $tabla WHERE codigo_barras='{$items[$x]['codigo']}'";
            $val = $this->dbmysql->query($sql);
            $row = $val->fetch_object();
            $codigoPS = $row->id;
            $rowItem = ($items[$x]['productoServicio'] == 1) ? 'producto_id' : 'servicio_id';
            $sqlDetalle = "INSERT INTO detalles_movimientos($rowItem,cantidad,movimiento_id,valor_total)"
                    . "VALUES($codigoPS,'{$items[$x]['cantidad']}',$codigoMovimiento,'{$items[$x]['vtotal']}');";
            $val2 = $this->dbmysql->query($sqlDetalle);
            if ($val2) {
                $resp++;
            }
        }
        if ($val2 == $cantidadItems) {
            return 1;
        } else {
            return 0;
        }
    }

    public function guardarPagoFactura($codigoMovimiento, $pagos) {
        $cantidadPagos = count($pagos);
        $resp = 0;
        for ($x = 0; $x < $cantidadPagos; $x++) {
            $sqlPagos = "INSERT INTO pagos_movimientos(tipos_pago_id,movimiento_id,valor,numero_documento,entidad_documento)"
                    . "VALUES('{$pagos[$x]['tipoPago']}',$codigoMovimiento,'{$pagos[$x]['valorPagar']}','{$pagos[$x]['numeroDocumento']}','{$pagos[$x]['entidadDocumento']}')";
            $val2 = $this->dbmysql->query($sqlPagos);
            if ($val2) {
                $resp++;
            }
        }
        if ($val2 == $cantidadPagos) {
            return 1;
        } else {
            return 0;
        }
    }

    public function cerrarSesiones($sesion, $items) {
        $cantidadItems = count($items);
        $contInfantes = 0;
        $resProducto = 0;
        $resInfante = 0;
        $horaFin = date('H:i:s');
        $sql = "SELECT * FROM `sesiones_infantes` WHERE sesion_id=$sesion AND estado='A';";
        $val = $this->dbmysql->query($sql);
        $cantidadInfantes = $val->num_rows;
        for ($x = 0; $x < $cantidadItems; $x++) {
            if ($items[$x]['productoServicio'] == 2) {
                $sesionInfante = explode(',', $items[$x]['sesionesInfantes']);
                $cantidadInfantesCerrar = count($sesionInfante);
                //$contInfantes = $contInfantes + $cantidadInfantesCerrar;
                for ($i = 0; $i < $cantidadInfantesCerrar; $i++) {
                    $sqlInfante = "UPDATE sesiones_infantes SET "
                            . "estado = 'T', "
                            . "hora_fin = '$horaFin' "
                            . "WHERE id={$sesionInfante[$i]};";
                    $valInfante = $this->dbmysql->query($sqlInfante);
                    if ($valInfante) {
                        $resInfante++;
                    }
                }
            }
        }
        $sqlproductos = "UPDATE sesiones_productos SET "
                . "estado='T' "
                . "WHERE sesion_id=$sesion;";
        $valProducto = $this->dbmysql->query($sqlproductos);
        if ($valProducto) {
            $resProducto++;
        }
        if ($cantidadInfantes == $cantidadInfantesCerrar) {
            $sqlSesion = "UPDATE sesiones SET "
                    . "estado='T', "
                    . "observacion='Sesion Finalizada por sistema a las: $horaFin' "
                    . "WHERE id=$sesion;";
            $valSesion = $this->dbmysql->query($sqlSesion);
        }
        if ($valSesion) {
            return 1;
        } else {
            return 0;
        }
    }

    public function actualizarFiestas($codigo) {
        $sql="SELECT * FROM fiestas WHERE id=$codigo AND estado != 'F';";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            $row = $val->fetch_object();
            if($row->estado=='C'){
                $sqlFiesta = "UPDATE fiestas SET "
                    .   "estado = 'A' "
                    . "WHERE id=$codigo;";
                $valFiesta = $this->dbmysql->query($sqlFiesta);
            }elseif($row->estado=='PL'){
                $sqlFiesta = "UPDATE fiestas SET "
                    .   "estado = 'F' "
                    . "WHERE id=$codigo;";
                $valFiesta = $this->dbmysql->query($sqlFiesta);
            }
        }
        if ($valFiesta) {
            return 1;
        } else {
            return 0;
        }
    }

    public function actualizarSerieDocumento() {
        $idCaja = $_SESSION['id_caja'];
        $sqlConsulta = "SELECT * FROM `cajas` WHERE id=$idCaja;";
        $val = $this->dbmysql->query($sqlConsulta);
        $row = $val->fetch_object();
        $serie = $row->serie;
        $numero = $serie + 1;
        $sql = "UPDATE `cajas` SET serie=$numero WHERE id=$idCaja;";
        $this->dbmysql->query($sql);
    }

    public function anularFactura() {
        $idMovimento = $_POST['codigo'];
        $sql = "UPDATE `movimientos` SET estados_movimiento_id=2 WHERE id=$idMovimento;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function impresionDocumento($parametros) {
        $hora = date('H:i');
        $fecha = date('Y-m-d');
        $contenido = '';
        $codCliente = $parametros["idCliente"];
        $sqlCliente = "SELECT * FROM clientes WHERE id=$codCliente";
        $val = $this->dbmysql->query($sqlCliente);
        $rowCliente = $val->fetch_object();
        $contenido = '<form id="areaImpresionDocumento" style="width: 300px;" accept-charset="UTF-8">
                                <table border="0" style="width: 100%;">
                                    <tr><td><b>Fecha:</b></td><td>' . $fecha . '</td></tr>
                                    <tr><td><b>Hora:</b></td><td>' . $hora . '</td></tr>
                                    <tr><td><b>Cliente:</b></td><td>' . $rowCliente->nombres . ' ' . $rowCliente->apellidos . '</td></tr>
                                    <tr><td><b>Ruc/Ci:</b></td><td>' . $rowCliente->identificacion . '</td></tr>
                                    <tr><td><b>Tel&eacute;fono:</b></td><td>' . $rowCliente->telefono2 . '</td></tr>
                                    <tr><td colspan="2" style="border-bottom: 1px #000 dashed;">&nbsp;</td></tr>
                                </table>
                                <table border="0" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="border-bottom: 1px #000 dashed;"><b>Cant.</b></th> 
                                            <th style="border-bottom: 1px #000 dashed;"><b>Detalle</b></th>
                                            <th style="border-bottom: 1px #000 dashed;"><b>Valor</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        $items = $parametros['items'];
        $cantidadItems = count($items);
        $resp = 0;
        for ($x = 0; $x < $cantidadItems; $x++) {
            $tabla = ($items[$x]['productoServicio'] == 1) ? 'productos' : 'servicios';
            $sql = "SELECT * FROM $tabla WHERE codigo_barras='{$items[$x]['codigo']}'";
            $val = $this->dbmysql->query($sql);
            $row = $val->fetch_object();
            $descripcionPS = $row->nombre;
            $contenido .='<tr>
                                                  <td style="text-align: center;">' . $items[$x]['cantidad'] . '</td> 
                                                  <td>' . $descripcionPS . '</td> 
                                                  <td style="text-align: right;">' . $items[$x]['vtotal'] . '</td> 
                                              </tr>';
        }
        $contenido .='<tr><td colspan="3" style="border-bottom: 1px #000 dashed;">&nbsp;</td></tr>
                                    </tbody>
                                </table>
                                <table border="0" style="width: 100%;">
                                    <tr><td style="width: 230px;text-align: right;"><b>Sub-Total 12:</b></td> 
                                        <td style="text-align: right;">$ ' . $parametros['subtotal'] . '</td></tr>
                                    <tr><td style="width: 230px;text-align: right;"><b>Sub-Total 0:</b></td> 
                                        <td style="text-align: right;">$ ' . $parametros['subtotalSinIva'] . '</td></tr>
                                    <tr><td style="width: 230px;text-align: right;"><b>Iva:</b></td> 
                                        <td style="text-align: right;">$ ' . $parametros['iva'] . '</td></tr>
                                    <tr><td style="width: 230px;text-align: right;"><b>Total:</b></td> 
                                        <td style="text-align: right;">$ ' . $parametros['total'] . '</td></tr>
                                </table>
                                <br>
                                <br>
                                <table border="0" style="width: 100%;">
                                    <tr><td colspan="2">&nbsp;</td></tr>
                                    <tr><td colspan="2" style="border: 1px #000 dotted;"><b>Forma de Pago:</b></td></tr>';
        $pagos = $parametros['pagos'];
        $cantidadPagos = count($pagos);
        $x = 0;
        for ($x = 0; $x < $cantidadPagos; $x++) {
            $sqlPago = "SELECT nombre FROM tipos_pagos WHERE id=" . $pagos[$x]['tipoPago'];
            $val = $this->dbmysql->query($sqlPago);
            $rowPago = $val->fetch_object();
            $contenido .='<tr><td><b>' . $rowPago->nombre . ':</b></td><td>$ ' . $pagos[$x]['valorPagar'] . '</td></tr>';
        }
        $usuario = $_SESSION["usu_usuario"];
        $contenido .='<tr><td colspan="2" style="border: 1px #000 dotted;"><b>Atendido por: </b> ' . $usuario . '</td></tr>
                                </table>
                            </form>';

        return $contenido;
    }

    //METODOS PARA USAR EN GESTOR DE FACTURAS/////////////////////////////////
    public function GestorFacturas() {
        // tipo movientos 
        $factual = date("Y-m-d");
        $sql = "SELECT *, c.id as cliente_id, m.id as movimiento_id FROM movimientos m, clientes c WHERE m.cliente_id=c.id AND m.fecha='$factual' AND m.tipos_movimiento_id =1; "; // fecha actual
        $resul = $this->dbmysql->query($sql);
        return $resul;
    }

    public function detalleFactura($parametros) {
        $idmovimiento = $parametros['movimiento_id'];
        $sql = "SELECT m.id as movimiento_id, m.fecha as fecha, m.subtotal as subtotal,
        m.subtotal_sin_iva as subsiniva, m.numero_factura as nfactura, m.total as total , m.iva as iva,
        c.nombres as nombres, c.identificacion as ruc, c.apellidos as apellidos
        FROM movimientos m, clientes c, detalles_movimientos d 
        WHERE m.id=$idmovimiento AND m.id=d.movimiento_id AND m.cliente_id=c.id";
        $resul = $this->dbmysql->query($sql);
        $row = $resul->fetch_object();
        $lista['DetalleFactura'] = array(
            "IDMovimiento" => $row->movimiento_id,
            "ruc" => $row->ruc,
            "fecha" => $row->fecha,
            "subtotal" => $row->subtotal,
            "subsiniva" => $row->subsiniva,
            "total" => $row->total,
            "cliente" => $row->nombres . ' ' . $row->apellidos,
            "nfactura" => $row->nfactura,
            "iva" => $row->iva
        );
        $sqltbl = "SELECT *  FROM  detalles_movimientos 
                 WHERE movimiento_id=$idmovimiento;";
        $resultbl = $this->dbmysql->query($sqltbl);
        while ($rowtb = $resultbl->fetch_object()) {
            $table = ($rowtb->producto_id == null) ? 'servicios' : 'productos';
            $campo = ($rowtb->producto_id == null) ? $rowtb->servicio_id : $rowtb->producto_id;
            $sqlb = "SELECT * FROM $table WHERE id=$campo;";
            $resulprod = $this->dbmysql->query($sqlb);
            $rowprod = $resulprod->fetch_object();
            $listadep['DetalleTblProducto'][] = array(
                "cantidad" => $rowtb->cantidad,
                "descripcionproducto" => $rowprod->nombre,
                "valor" => $rowtb->valor_total
            );
        }
        $sqltp = "SELECT * FROM pagos_movimientos pg,tipos_pagos tp 
               WHERE pg.tipos_pago_id=tp.id AND pg.movimiento_id=$idmovimiento;";
        $resultp = $this->dbmysql->query($sqltp);
        while ($rowtp = $resultp->fetch_object()) {
            $listapago['DetalleTipoPago'][] = array(
                "pago_id" => $rowtp->id,
                "valor" => $rowtp->valor,
                "nombre" => $rowtp->nombre
            );
        }
        $result = array_merge($lista, $listadep, $listapago);
        echo $encode = json_encode($result);
    }

    public function anularDetalleFactura($parametros1) {
        //anulado estado N. 2
        $idmovimiento = $parametros1['movimiento_id'];
        echo $anulado = "UPDATE 'movimientos' SET estados_movimiento_id='2' WHERE id='$idmovimiento' ";
        $resulanulado = $this->dbmysql->query($anulado);
        echo $resulanulado;
    }

    //////////////////////////////////////////////////////////////////////////
}
