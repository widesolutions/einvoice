<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$dbmysql = new database();
date_default_timezone_set('America/Guayaquil');
$sOrder = '';
$fecha=date('Y-m-d');
$rol=$_SESSION["usu_rol_cod"];
$caja=$_SESSION['id_caja'];
$aColumns = array( 'apellidos','nombres','identificacion','cliente_id','movimiento_id','caja_id','fecha','local_id','numero_factura','estados_movimiento_id','subtotal','subtotal_sin_iva','total');
$sIndexColumn = "movimiento_id";
$sTable = "vw_gestion_movimientos";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}
/* * *******************
 * ** Ordenamiento ****
 * ******************* */

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY GEN_ID") {
        $sOrder = "ORDER BY GEN_ID DESC";
    }
}

/* * *******************
 * ** Filtrado ****
 * ******************* */

$sWhere = "";
if ($_GET['sSearch'] != "") {
    $sWhere = "WHERE (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}
/* Filtrado por Columnas individuales */
for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        if ($sWhere == "") {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
    }
}

/* * ***********************
 * ** CREACION DEL SQL ****
 * *********************** */
$sOrder = "ORDER BY movimiento_id DESC";
if($rol>3){
    $sWhere = ($sWhere == '') ? "WHERE fecha='$fecha' AND caja_id='$caja' " : $sWhere . " AND fecha='$fecha' AND caja_id='$caja'";
}
$sQuery = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(",", $aColumns)) . "
            FROM $sTable
            $sWhere
            $sOrder
            $sLimit";
//echo $sQuery;
$rResult = $dbmysql->query($sQuery);
if (!$rResult) {
    printf("Error: %s\n", $mysqli->error);
}
/* * **********************************************************
 * ** Longitud conjunto de datos después de la filtración ****
 * ********************************************************** */
$sQuery = "SELECT FOUND_ROWS() as Validas";
$rResultFilterTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultFilterTotal = $rResultFilterTotal->fetch_object();
$iFilteredTotal = $aResultFilterTotal->Validas; //Total de Registros Validos

$sQuery = "SELECT COUNT(" . $sIndexColumn . ")AS contador FROM   $sTable $sOrder;";
$rResultTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultTotal = $rResultTotal->fetch_object();
$iTotal = $aResultTotal->Validas;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iFilteredTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array());
//print_r($output);
/* * **********************
 * ** RESULTADO FINAL ****
 * ********************** */
while ($aRow = $rResult->fetch_object()) {
    $btn=($aRow->estados_movimiento_id==2)?'<span class="label label-danger">Anulada</span>':'<a class="btn btn-labeled btn-danger" title="Anular Factura" href="javascript:anularFactura('.$aRow->movimiento_id.');">
                                                                    <span class="btn-label">
                                                                        <i class="glyphicon glyphicon-remove"></i>
                                                                    </span>
                                                                    Anular
                                                                </a>
                                                                <a class="btn btn-labeled btn-info" title="Imprimir Factura" href="javascript:imprimirDocumento('.$aRow->cliente_id.','.$aRow->movimiento_id.');">
                                                                    <span class="btn-label">
                                                                        <i class="glyphicon glyphicon-print"></i>
                                                                    </span>
                                                                    Imprimir
                                                                </a>
                                                                <a class="btn btn-labeled btn-primary" title="Ver Items Factura" href="javascript:verDetalleFactura('.$aRow->movimiento_id.');">
                                                                    <span class="btn-label">
                                                                        <i class="glyphicon glyphicon-th-list"></i>
                                                                    </span>
                                                                    Ver
                                                                </a>';
    $cadenaParametros=$aRow->id.' , '."'$aRow->nombre'";
    $output['aaData'][] = array('' . $aRow->numero_factura . '',
                                '' . $aRow->nombres.' '.$aRow->apellidos . '',
                                '' . $aRow->fecha . '',
                                '' . $aRow->total . '',
                                '' . $btn . '');
}
echo json_encode($output);
