<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';

class locales {

    function __construct() {
        $this->dbmysql = new database();
    }

    function getLocales() {
        $sql = "SELECT id,nombre FROM `locales`;";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function listarLocales() {
        $sql = "SELECT * FROM locales";
        $resul = $this->dbmysql->query($sql);
        //$val1 ='resultado';
        //$resul ="saludos";
        return $resul;

        //regresa la varibale con la consulta.
    }

    public function guardaDatosLocal() {
        $nombre = strtoupper($_POST["nombre"]);
        $telefono = $_POST['telefono'];
        $ubicacion = strtoupper($_POST['ubicacion']);
        $sqlVerifica = "SELECT * FROM locales WHERE nombre='$nombre' AND ubicacion='$ubicacion';";
        $resulVerifica = $this->dbmysql->query($sqlVerifica);
        if ($resulVerifica->num_rows == 0) {
            $sql = "INSERT INTO locales(nombre,ubicacion,telefono) 
            VALUES ('$nombre','$ubicacion','$telefono')";
            $resul = $this->dbmysql->query($sql);
            if ($resul) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function enviarDatosLocal() {
        $idlocal = $_POST['codigolocal'];

        $sql = "SELECT * FROM locales WHERE id =$idlocal";
        $val = $this->dbmysql->query($sql);
        $row = $val->fetch_object();
        //IDProducto el nombre del id en el frmproducto
        $lista['datosLocal'] = array(
            "IDLocal" => $row->id,
            "nombre" => $row->nombre,
            "ubicacion" => $row->ubicacion,
            "telefono" => $row->telefono
        );
        echo $encode = json_encode($lista);
    }

    public function EliminarLocal() {
        $local = $_POST['idlocal'];
        $sql = "UPDATE locales SET estado='E' WHERE id=$local";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function ActualizarDatos() {
        $idlocal = $_POST['IDLocal'];
        $nombre = strtoupper($_POST["nombre"]);
        $telefono = $_POST['telefono'];
        $ubicacion = $_POST['ubicacion'];
        $sql = "UPDATE locales SET 
                    nombre='$nombre',    
                    telefono='$telefono',
                    ubicacion='$ubicacion'
                WHERE id=$idlocal ";
        $resul = $this->dbmysql->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
