<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
date_default_timezone_set('America/Guayaquil');

class salones {

    protected $database;

    function __construct() {
        $this->database = new database();
    }

    public function listarSalones() {
        $sql = "SELECT * FROM salones";
        $resul = $this->database->query($sql);
        return $resul;
    }
    public  function guardaDatosSalones()
    {
        $nombre = strtoupper($_POST["nombre"]);
        $capacidad = $_POST["capacidad"];
        $ubicacion = strtoupper($_POST["ubicacion"]);
        $sql = "INSERT INTO salones(nombre,capacidad,ubicacion) VALUES
            ('$nombre','$capacidad','$ubicacion')";
        $resul = $this->database->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function enviarDatosSalones()
    {
        $idsalon = $_POST['codigoSalon'];
        $sql = "SELECT * FROM salones WHERE id =$idsalon";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $lista['datosSalon'] = array(
            "IDSalon" => $row->id,
            "capacidad" => $row->capacidad,
            "nombre" => $row->nombre,
            "ubicacion" => $row->ubicacion
        );
        echo $encode = json_encode($lista);
    }
    public function EliminarSalon()
    {
        $codigo = $_POST['idsalon'];
        $sql = "DELETE FROM salones WHERE id=$codigo";
        $val = $this->database->query($sql);
        if ($val) {
            //$sql = "DELETE  FROM `sys_usuario_centro` WHERE USU_COD=$codigo;";
            //$dbmysql->query($sql);
            echo 1;
        } else {
            echo 0;
        }
    }
    public function ActualizarSalon() {
        $idsalon = $_POST['IDSalon'];
        $nombre = strtoupper($_POST["nombre"]);
        $capacidad = $_POST["capacidad"];
        $ubicacion = strtoupper($_POST["ubicacion"]);
        $sql = "UPDATE salones SET 
            nombre='$nombre',
            capacidad='$capacidad',
            ubicacion='$ubicacion'   
              WHERE id=$idsalon";
        $resul = $this->database->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }
}
