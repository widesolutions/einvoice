<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';

class servicios {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }

    public function getServicioxNombre(){
        $parametros = $_GET['term'];
        $sql = "SELECT * FROM servicios WHERE nombre LIKE '%$parametros%' AND estado='A'";
        $consulta = $this->database->query($sql);
        if ($consulta->num_rows > 0) {
            while ($row = $consulta->fetch_object()) {
                $datos[] = array(
                    "value" => $row->nombre,
                    "id" => $row->id,
                    "codigo" => $row->codigo_barras,
                    "minimo" => $row->cantidad_minima,
                    "vunitario" => $row->precio_venta,
                    "ivanoiva" => $row->iva,
                    "productoServicio" => 2
                );
            }
        }
        echo json_encode($datos);
    }
    
    public function listarServicios() {
        $sql = "SELECT * FROM servicios";
        $resul = $this->database->query($sql);
        return $resul;
    }

    public function guardaDatosServicio() {
        $descripcion = strtoupper($_POST["descripcion"]);
        $precio_compra = $_POST['precio_compra'];
        $precio_venta = $_POST['precio_venta'];
        $codigo_barras = strtoupper($_POST['codigo_barras']);
        $minimo = ($_POST['minimo'])?$_POST['minimo']:0;
        $todo_dia = $_POST['todo_dia'];
        $iva = $_POST["iva"];
        $sqlVerifica = "SELECT * FROM servicios WHERE nombre='$descripcion' AND codigo_barras='$codigo_barras';";
        $resulVerifica = $this->database->query($sqlVerifica);
        if ($resulVerifica->num_rows == 0) {
            $sql = "INSERT INTO servicios(nombre,codigo_barras,precio_compra,precio_venta,cantidad_minima,iva,todo_dia,estado) VALUES
            ('$descripcion','$codigo_barras','$precio_compra','$precio_venta',$minimo,'$iva','$todo_dia','A')";
            $resul = $this->database->query($sql);

            if ($resul) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function enviarDatosServicios() {
        $idser = $_POST['codigoServicio'];
        $sql = "SELECT * FROM servicios WHERE id =$idser";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $lista['datosServicios'] = array(
            "IDServicio" => $row->id,
            "descripcion" => $row->nombre,
            "codigo_barras" => $row->codigo_barras,
            "precio_compra" => $row->precio_compra,
            "minimo" => $row->cantidad_minima,
            "iva" => $row->iva,
            "todo_dia" => $row->todo_dia,
            "precio_venta" => $row->precio_venta
        );
        echo $encode = json_encode($lista);
    }

    public function ActualizarDatos() {
        $idser = $_POST['IDServicio'];
        $descripcion = strtoupper($_POST["descripcion"]);
        $precio_compra = $_POST['precio_compra'];
        $precio_venta = $_POST['precio_venta'];
        $codigo_barras = $_POST['codigo_barras'];
        $minimo = ($_POST['minimo'])?$_POST['minimo']:0;
        $todo_dia = $_POST['todo_dia'];
        $iva = $_POST["iva"];
        $sql = "UPDATE servicios SET 
                    nombre='$descripcion',    
                    codigo_barras='$codigo_barras',
                    precio_compra='$precio_compra',
                    cantidad_minima =$minimo,
                    precio_venta='$precio_venta',
                    iva='$iva'
                WHERE id=$idser ";
        $resul = $this->database->query($sql);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function EliminarServicio() {
        $servicio = $_POST['idserv'];
        $sql = "UPDATE servicios SET estado='E' WHERE id=$servicio";
        $val = $this->database->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function BuscarBarras() {
        $barras = $_POST["codigo_barras"];
        $busqueda = "SELECT codigo_barras FROM servicios WHERE codigo_barras='$barras'";
        $resul = $this->database->query($busqueda);
        if ($resul) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function getServiciosSesion() {
        $idser = $_POST['codigoServicio'];
        $sql_servicios = "SELECT servicio_id FROM servicios_sesiones";
        $val_servicios = $this->database->query($sql_servicios);
        while ($row_servicios = $val_servicios->fetch_object()) {
            $servicio[] = $row_servicios->servicio_id;
        }

        $sql = "SELECT * FROM servicios where id IN (" . implode(", ", $servicio) . ")";
        $val = $this->database->query($sql);
        while ($row = $val->fetch_object()) {
            $datos[] = array(
                "id" => $row->id,
                "nombre" => $row->nombre,
            );
        }
        echo $encode = json_encode($datos);
    }

    public function getSesionesServicios() {
        $select = "SELECT ss.dia ,ss.tiempo_tolerancia, s.nombre, s.id, ss.id as id_servicio_sesiones FROM servicios_sesiones ss, servicios s where s.id = ss.servicio_id";
        $val = $this->database->query($select);
        while ($row = $val->fetch_object()) {
            $datos[] = array(
                "dia" => $row->dia,
                "tiempo_tolerancia" => $row->tiempo_tolerancia,
                "nombre" => $row->nombre,
                "id" => $row->id,
                "id_servicio_sesiones" => $row->id_servicio_sesiones
            );
        }

        echo json_encode($datos);
    }

    public function getDatosServicio($parametros) {

        $select = "SELECT dia , tiempo_tolerancia, id FROM servicios_sesiones where id = " . $parametros['id'];
        $val = $this->database->query($select);
        echo json_encode($val->fetch_object());

    }

    public function editarDatosServicioSesion() {
        $update = "UPDATE servicios_sesiones set tiempo_tolerancia = " . $_POST['tiempo_tolerancia'] . ", dia = " . $_POST['dia'] . " WHERE id = " . $_POST['servicio_sesiones'];
        return $this->database->query($update);
    }

    public function crearDatosServicioSesion() {
        $insert = "INSERT INTO servicios_sesiones (`servicio_id`,`tiempo_tolerancia`,`dia`) VALUES (" . $_POST['servicio_id'] . ", " . $_POST['tiempo_tolerancia_c'] . ", " . $_POST['dia_c'] . " )";
        return $this->database->query($insert);
    }    
    


    public function serviciosMasVendido($fecha_inicio, $fecha_fin) {
        $select = "select count(*) as cantidad, s.nombre, s.id  from movimientos m, detalles_movimientos d, servicios s where m.fecha between '$fecha_inicio' and '$fecha_fin' and  d.servicio_id = s.id and m.id = d.movimiento_id and d.producto_id is null group by d.servicio_id order by cantidad desc limit 10";
        return $this->database->query($select);
    }

    public function serviciosPorEdades($parametros) {

        if ($_POST['fecha_inicio'] != "") {

            $query = "ss.fecha between '" . $_POST['fecha_inicio'] . "' and '" . $_POST['fecha_fin'] . "' and";
        }

        $data = array();
        for ($i = 0; $i <= 12; $i++) {

            $select = "SELECT  s.servicio_id, se.nombre, count(*) as cantidad from infantes i, sesiones_infantes s, sesiones ss, servicios se where " . $query . " ss.id = s.sesion_id and s.servicio_id = se.id  and s.infante_id = i.id and YEAR(CURRENT_TIMESTAMP) - YEAR(i.fecha_nacimiento) - (RIGHT(CURRENT_TIMESTAMP, 5) < RIGHT(i.fecha_nacimiento, 5)) = $i group by s.servicio_id";
            $val = $this->database->query($select);

            $temp = array("x" => $i . ' años');
            while ($row = $val->fetch_object()) {

                $temp[$row->servicio_id] = $row->cantidad;
            }

            array_push($data, $temp);
        }
        echo json_encode($data);
    }

    public function consumoDeServicios() {

        if ($_POST['fecha_inicio'] != "") {

            $query = "m.fecha between '" . $_POST['fecha_inicio'] . "' and '" . $_POST['fecha_fin'] . "' and";
        }

        $data = array();
        for ($i = 1; $i <= 3; $i++) {

            $cantidad = ($i < 3) ? " = " . $i : " > 2";
            $select = "select count(*) as cantidad from movimientos m, detalles_movimientos d where ".$query." d.movimiento_id = m.id and d.servicio_id = 4 and d.cantidad  " . $cantidad;
            $val = $this->database->query($select);

            $temp = array("x" => $i . ' años');
            $parametros = [['rgba(220,220,220,0.9)', 'rgba(220,220,220,0.8)', 1, '1 hora'], ['rgba(151,187,205,1)', 'rgba(151,187,205,0.8)', 2, '2 horas'], ['rgba(169, 3, 41, 0.7)', 'rgba(169, 3, 41, 0.7)', 3, '+ 2 horas']];
            $row = $val->fetch_object();

            $data[] = array(
                value => (int) $row->cantidad,
                color => $parametros[$i-1][0],
                highlight => $parametros[$i-1][1],
                label => $parametros[$i-1][3]
            );
        }
        echo json_encode($data);
    }

}
