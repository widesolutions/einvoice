<?php

session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/modelos/dataBase.php");

class Usuario {

    protected $dbmysql;

    function __construct() {
        $this->dbmysql = new database();
    }

    public function listarUsuarios() {
        $sql = "SELECT u.*,r.* FROM `usuarios` u,`roles` r  WHERE u.rol_id=r.id AND u.estado='A';";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function comboTipoUsuario() {
        $retval = '';
        $sql = "SELECT * FROM `roles` WHERE estado='A';";
        $val = $this->dbmysql->query($sql);
        if ($val->num_rows > 0) {
            while ($row = $val->fetch_object()) {
                $retval.='<option value="' . $row->id . '">' . $row->descripcion . '</option>';
            }
        }
        return $retval;
    }

    public function getUsuariosAdministradores() {
        $sql = "SELECT * FROM `usuarios` WHERE rol_id <= 3;";
        $val = $this->dbmysql->query($sql);
        return $val;
    }

    public function enviarDatosUsuario() {
        $idpro = $_POST['codigoUsu'];
        $sql = "SELECT * FROM `usuarios` WHERE id =$idpro";
        $val = $this->dbmysql->query($sql);
        $row = $val->fetch_object();
        $lista['datosUsuario'] = array(
            "USU_COD" => $row->id,
            "USU_NOMBRE" => $row->nombre,
            "USU_APELLIDO" => $row->apellido,
            "USU_USUARIO" => $row->usuario,
            "USU_EMAIL" => $row->email,
            "USU_CELULAR" => $row->celular,
            "USU_AVATAR" => $row->avatar,
            "USU_CEDULA" => $row->cedula,
            "ROL_COD" => $row->rol_id
        );
        echo $encode = json_encode($lista);
    }

    public function cambioClaveInicial() {
        $codigo = $_POST['codUsuario'];
        $password = $_POST["clave"];
        $sql = "UPDATE `usuarios` SET 
                clave = MD5('$password'),
                estado = 'A'    
           WHERE id=$codigo;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
//        $sql = "SELECT * FROM `usuarios`WHERE id=$codigo;";
//        $val = $this->dbmysql->query($sql);
//        $row = $val->fetch_object();
//        $cGeneral->auditoria('A', 'usuarios', 'Cambio de Contraseña al Usuario: ' . $row->USU_USUARIO);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function guardaDatosUsuario() {
        $nombre = strtoupper($_POST["nombre"]);
        $apellido = strtoupper($_POST["apellido"]);
        $usuario = strtolower($_POST["usuario"]);
        $password = $_POST["password"];
        $email = strtolower($_POST["email"]);
        $celular = $_POST["celular"];
        $cedula = $_POST["cedula"];
        $tipoUsuario = $_POST["tipoUsuario"];
        $local = $_SESSION['usu_local_id'];

        $sql_rol = "SELECT * FROM `usuarios` WHERE (usuario='$usuario' AND nombre='$nombre' AND apellido='$apellido') OR `cedula` ='$cedula';";
        $val_rol = $this->dbmysql->query($sql_rol);
        if ($val_rol->num_rows == 0) {
            $sql = "INSERT INTO `usuarios`(nombre,local_id,apellido,usuario,clave,email,celular,cedula,rol_id,estado)VALUES
            ('$nombre',$local,'$apellido','$usuario',MD5('$password'),'$email','$celular','$cedula','$tipoUsuario','A');";
            $val = $this->dbmysql->query($sql);
            if ($val) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actualizarDatosUsuario() {
        $codigo = $_POST['IDuser'];
        $nombre = strtoupper($_POST["nombre"]);
        $apellido = strtoupper($_POST["apellido"]);
        $usuario = strtolower($_POST["usuario"]);
        $email = strtolower($_POST["email"]);
        $celular = $_POST["celular"];
        $cedula = $_POST["cedula"];
        $tipoUsuario = $_POST["tipoUsuario"];
        $sql = "UPDATE `usuarios` SET 
                nombre    = '$nombre',
                apellido  = '$apellido',
                usuario   = '$usuario',
                email     = '$email',
                celular   = '$celular',
                cedula    = '$cedula',
                rol_id       = '$tipoUsuario'
            WHERE id=$codigo;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function eliminarUsuario() {
        $codigo = $_POST['codigo'];
        $sql = "DELETE  FROM `usuarios` WHERE id=$codigo;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function cambioClaveUsuario() {
        $codigo = $_POST['codigo'];
        $password = $_POST["clave"];
        $sql = "UPDATE `usuarios` SET 
            autorizacion = MD5('$password'),
            estado='C'
           WHERE id=$codigo;";
        $val = $this->dbmysql->query($sql);
        if ($val) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
