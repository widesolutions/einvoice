<?php

session_start();
include_once '../generales.php';
$cGeneral = new general();
include_once '../conexiones/db_local.inc.php';
$dbmysql = new database();
date_default_timezone_set('America/Bogota');
$funcion = isset($_GET['opcion']) ? $_GET['opcion'] : 'ninguno';
switch ($funcion) {
    case 'enviarDatosUsuario':
        enviarDatosUsuario();
        break;
    case 'guardaDatosUsuario':
        guardaDatosUsuario();
        break;
    case 'actualizarDatosUsuario':
        actualizarDatosUsuario();
        break;
    case 'eliminarUsuario':
        eliminarUsuario();
        break;
    case 'cambioClaveUsuario':
        cambioClaveUsuario();
        break;
    case 'obtenerCentrosMenu':
        obtenerCentrosMenu();
        break;
    case 'obtenerCentrosListado':
        obtenerCentrosListado();
        break;
    case 'agregaCentrosTabla':
        agregaCentrosTabla();
        break;
    case 'cambioClaveInicial':
        cambioClaveInicial();
        break;
}





function guardaDatosUsuario() {
    global $dbmysql, $cGeneral;
    $nombre = strtoupper($_POST["nombre"]);
    $apellido = strtoupper($_POST["apellido"]);
    $usuario = strtolower($_POST["usuario"]);
    $password = $_POST["password"];
    $email = strtolower($_POST["email"]);
    $celular = $_POST["celular"];
    $cedula = $_POST["cedula"];
    $tipoUsuario = $_POST["tipoUsuario"];
    $centro = $_POST["centro"];
    $sql_rol = "SELECT * FROM `usuarios` WHERE (usuario='$usuario' AND nombre='$nombre' AND apellido='$apellido') OR `cedula` ='$cedula';";
    $val_rol = $dbmysql->query($sql_rol);
    if ($val_rol->num_rows==0) {
        $sql = "INSERT INTO `usuarios`(nombre,apellido,usuario,clave,email,celular,cedula,rol_id)VALUES
            ('$nombre','$apellido','$usuario',MD5('$password'),'$email','$celular','$cedula','$tipoUsuario');";
        $val = $dbmysql->query($sql);
        if ($val) {
            $cGeneral->auditoria('I', 'usuarios', 'Creación Usuario: ' . $usuario . ', Nombre: ' . $nombre . ' Apellido: ' . $apellido);
            echo 1;
        } else {
            echo 0;
        }
    }
}




function cambioClaveInicial() {
    global $dbmysql, $cGeneral;
    $codigo = $_POST['codUsuario'];
    $password = $_POST["clave"];
    $sql = "UPDATE `usuarios` SET 
            clave = MD5('$password'),
            estado = 'A'    
           WHERE id=$codigo;";
    $val = $dbmysql->query($sql);
    if ($val) {
        $sql = "SELECT * FROM `usuarios`WHERE id=$codigo;";
        $val = $dbmysql->query($sql);
        $row = $val->fetch_object();
        $cGeneral->auditoria('A', 'usuarios', 'Cambio de Contraseña al Usuario: ' . $row->USU_USUARIO);
        echo 1;
    } else {
        echo 0;
    }
}
