<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$dbmysql = new database();
date_default_timezone_set('America/Guayaquil');
$aColumns = array('id', 'nombre', 'apellido', 'usuario', 'clave', 'email', 'celular', 'avatar', 'cedula', 'rol_id');
/* Campo de Index */
$sIndexColumn = "id";
/* Tabla a Usar */
$sTable = "usuarios";
$local=$_SESSION["usu_local_id"];

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}
/* * *******************
 * ** Ordenamiento ****
 * ******************* */

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY GEN_ID") {
        $sOrder = "ORDER BY GEN_ID DESC";
    }
}

/* * *******************
 * ** Filtrado ****
 * ******************* */

$sWhere = "";
if ($_GET['sSearch'] != "") {
    $sWhere = "WHERE (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}
/* Filtrado por Columnas individuales */
for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        if ($sWhere == "") {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
    }
}

/* * ***********************
 * ** CREACION DEL SQL ****
 * *********************** */

$sWhere = ($sWhere == '') ? 'WHERE estado="A" ' : $sWhere . ' AND estado="A"';
$sQuery = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(",", $aColumns)) . "
            FROM $sTable
            $sWhere
            $sOrder
            $sLimit";
//echo $sQuery;
$rResult = $dbmysql->query($sQuery);
if (!$rResult) {
    printf("Error: %s\n", $mysqli->error);
}
/* * **********************************************************
 * ** Longitud conjunto de datos después de la filtración ****
 * ********************************************************** */
$sQuery = "SELECT FOUND_ROWS() as Validas";
$rResultFilterTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultFilterTotal = $rResultFilterTotal->fetch_object();
$iFilteredTotal = $aResultFilterTotal->Validas; //Total de Registros Validos

$sQuery = "SELECT COUNT(" . $sIndexColumn . ")AS contador FROM   $sTable $sOrder;";
$rResultTotal = $dbmysql->query($sQuery) or die($dbmysql->error);
$aResultTotal = $rResultTotal->fetch_object();
$iTotal = $aResultTotal->Validas;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iFilteredTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array());
//print_r($output);
while ($aRow = $rResult->fetch_object()) {
    $sql = "SELECT * FROM `roles` WHERE id=" . $aRow->rol_id;
    $val = $dbmysql->query($sql);
    $row = $val->fetch_object();
    $nombre = $aRow->nombre . ' ' . $aRow->apellido;
    $cadenaParametros = utf8_encode($aRow->id . ',' . "'$nombre'");
    $output['aaData'][] = array('' . $aRow->id . '',
                                '' . $nombre . '',
                                '' . $aRow->usuario . '',
                                '' . $aRow->email . '',
                                '' . $row->descripcion . '',
                                '<a class="btn btn-success btn-xs" title="Cambio de Clave" href="javascript:cambiarClaveUsuario(' . $aRow->id . ')">
                                    <i class="fa fa-key"></i>
                                </a>
                                <a class="btn btn-success btn-xs" title="Editar Usuario" href="javascript:editarUsuario(' . $aRow->id . ')">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a class="btn btn-danger btn-xs ' . $aRow->id . ' eliminaParticipante" title="Eliminar Usuario" href="javascript:eliminarUsuario(' . $cadenaParametros . ')">
                                    <i class="fa fa-trash-o"></i>
                                </a>');
}
echo json_encode($output);