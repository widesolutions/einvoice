<?php

session_start();
date_default_timezone_set('America/Guayaquil');
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/infante/infante.php';

class sesiones {

    protected $database;
    protected $infante;

    public function __construct() {
        $this->database = new database();
        $this->infante = new infante();
    }

    public function crear() {
        try {
            $fecha_actual = date('y-m-d');

            $sql_sesion = "INSERT INTO sesiones(cliente_id,fecha,estado,usuario_id) VALUES
            (" . $_POST['cliente_id'] . ",'$fecha_actual','A'," . $_SESSION['user_id'] . ")";
            $this->database->query($sql_sesion);
            $sesion_id = $this->database->lastid();

            foreach ($_POST['infantes_id'] as $key => $val) {

                $hora_actual = date('H:i:s');

                $sql = "INSERT INTO sesiones_infantes(sesion_id,infante_id,hora_inicio,estado,servicio_id) VALUES
            ($sesion_id,$val,'$hora_actual','A'," . $_POST['servicios_infantes_id'][$key] . ")";
                $this->database->query($sql);
            }

            // Productos de las sesiones
            if (is_array($_POST['adicionales_id']) and isset($_POST['adicionales_id'])) {
                foreach ($_POST['adicionales_id'] as $key => $val) {
                    $sql = "INSERT INTO sesiones_productos(sesion_id,producto_id,cantidad,estado) VALUES
            ($sesion_id,$val," . $_POST['cantidad'][$key] . ",'A')";
                    $this->database->query($sql);
                }
            }

            $data['insert'] = 1;
            $data['hora'] = $hora_actual;
            echo json_encode($data);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function cancelar($parametros) {
        $id = $parametros['id'];
        $updateSesion = "UPDATE sesiones SET estado = 'C' where id = $id";
        $resultSesion = $this->database->query($updateSesion);

        $updateSesionInfantes = "UPDATE sesiones_infantes SET estado = 'C' where sesion_id = $id";
        $resultSesionInfantes = $this->database->query($updateSesionInfantes);
        if ($resultSesion == 1 and $resultSesionInfantes == 1)
            echo 1;
        else
            echo 0;
    }

    public function cancelarAdicional($parametros) {
        $adicional_id = $parametros['adicional_id'];
        $sesion_id = $parametros['sesion_id'];

        $deleteAdicionalSesion = "DELETE FROM sesiones_productos where sesion_id = $sesion_id and producto_id = $adicional_id";
        $resultAdicionalSesion = $this->database->query($deleteAdicionalSesion);

        if ($resultAdicionalSesion == 1)
            echo 1;
        else
            echo 0;
    }

    public function cancelarInfante($parametros) {
        $infante_id = $parametros['infante_id'];
        $sesion_id = $parametros['sesion_id'];
        $fecha_actual = date('y-m-d');
        $updateSesionInfante = "UPDATE sesiones_infantes SET estado = 'C' where sesion_id = $sesion_id and infante_id = $infante_id";
        $resultSesionInfante = $this->database->query($updateSesionInfante);

        if ($resultSesionInfante == 1) {
            $sql = "SELECT * FROM sesiones_infantes i, sesiones s where s.id = i.sesion_id and i.sesion_id = $sesion_id and i.estado = 'A' and s.fecha = '$fecha_actual'";
            $val = $this->database->query($sql);
            $numero = $val->num_rows;
            if ($numero == 0) {
                $updateSesion = "UPDATE sesiones SET estado = 'C' where id = $sesion_id";
                $resultSesion = $this->database->query($updateSesion);
            }
            echo 1;
        } else
            echo 0;
    }

    public function verificarInfanteActivo($parametros) {
        $id = $parametros['id'];
        $fecha_actual = date('y-m-d');
        $sql = "SELECT * FROM sesiones s, sesiones_infantes i where s.id = i.sesion_id and i.infante_id = $id and s.estado = 'A' and s.fecha = '$fecha_actual'";
        $val = $this->database->query($sql);
        echo $val->num_rows;
    }

    public function verificarClienteActivo($parametros) {
        $id = $parametros['id'];
        $fecha_actual = date('y-m-d');
        $sql = "SELECT * FROM sesiones where cliente_id = $id and estado = 'A' and fecha = '$fecha_actual' ";
        $val = $this->database->query($sql);
        echo $val->num_rows;
    }

    public function getHistorialPorCliente() {

        $id = $_POST['id'];

        $sql = "SELECT distinct si.infante_id as id, i.primer_nombre, i.segundo_nombre, i.primer_apellido, i.segundo_apellido FROM sesiones s, sesiones_infantes si, infantes i
                where s.id = si.sesion_id and si.infante_id = i.id and s.cliente_id = $id";
        $val = $this->database->query($sql);

        while ($row = $val->fetch_object()) {
            $datos[] = array(
                "id" => $row->id,
                "primer_nombre" => $row->primer_nombre,
                "segundo_nombre" => $row->segundo_nombre,
                "primer_apellido" => $row->primer_apellido,
                "segundo_apellido" => $row->segundo_apellido,
            );
        }

        echo json_encode($datos);
    }

    public function getSesionesActivas() {

        $fecha_actual = date('y-m-d');
        $sql = "SELECT  s.*,c.*, c.id as cliente_id, s.id as sesion_id FROM  sesiones s, clientes c where c.id = s.cliente_id and s.fecha = '$fecha_actual'";
        $val = $this->database->query($sql);
        $finfo = $val->fetch_fields();

        while ($row = $val->fetch_object()) {
            $detalles = "";
            foreach ($finfo as $valor) {
                $name = $valor->name;
                $detalles[$valor->name] = $row->$name;
            }

            //infantes
            $parametro['id'] = $row->sesion_id;
            $detalles['infantes'] = $this->getInfantesPorIdSesion($parametro);

            //adicionales
            $parametro['id'] = $row->sesion_id;
            $detalles['adicionales'] = $this->getAdicionalesPorIdSesion($parametro);


            $datos[] = $detalles;
        }

        echo json_encode($datos);
    }

    public function getInfantesPorIdSesion($parametros) {
        $id = $parametros['id'];
        $sql_infante = "select si.*, i.* , si.id as id_sesiones_infantes, i.id as id_infante, s.nombre as nombre_servicio, s.id as id_servicio from sesiones_infantes si, infantes i, servicios s where s.id = si.servicio_id and si.infante_id = i.id and si.sesion_id = $id";
        $val_infante = $this->database->query($sql_infante);
        while ($row_infante = $val_infante->fetch_object()) {
            $detalles[] = array(
                "id_infante" => $row_infante->id_infante,
                "primer_nombre" => $row_infante->primer_nombre,
                "segundo_nombre" => $row_infante->segundo_nombre,
                "primer_apellido" => $row_infante->primer_apellido,
                "segundo_apellido" => $row_infante->segundo_apellido,
                "hora_inicio" => $row_infante->hora_inicio,
                "id_infante" => $row_infante->id_infante,
                "nombre_servicio" => $row_infante->nombre_servicio,
                "id_servicio" => $row_infante->id_servicio,
                "estado" => $row_infante->estado,
            );
        }

        if (isset($_POST['parametros']))
            echo json_encode($detalles);
        else
            return $detalles;
    }

    public function getAdicionalesPorIdSesion($parametro) {

        $id = $parametro['id'];
        $sql_adicionales = "SELECT * FROM sesiones_productos sp, sesiones s, productos p where sp.sesion_id = s.id and sp.producto_id = p.id and s.id = $id";
        $val_adicionales = $this->database->query($sql_adicionales);
        while ($row_adicionales = $val_adicionales->fetch_object()) {
            $detalles[] = array(
                "producto_id" => $row_adicionales->producto_id,
                "cantidad" => $row_adicionales->cantidad,
                "nombre" => $row_adicionales->nombre
            );
        }
        if (isset($_POST['parametros']))
            echo json_encode($detalles);
        else
            return $detalles;
    }

    public function agregarAdicional($parametros) {

        $id_adicional = $parametros['id_adicional'];
        $cantidad = $parametros['cantidad'];
        $id_sesion = $parametros['id_sesion'];
        $sql_adicionales = "SELECT * FROM sesiones_productos where sesion_id = $id_sesion and producto_id = $id_adicional";
        $val_adicionales = $this->database->query($sql_adicionales);
        if ($val_adicionales->num_rows > 0) {
            $row = $val_adicionales->fetch_object();
            $cantidad = $cantidad + $row->cantidad;
            $update = "UPDATE sesiones_productos SET cantidad = $cantidad where id = $row->id ";
            echo $update;
            $this->database->query($update);
        } else {

            $insert = "INSERT INTO  sesiones_productos (sesion_id, producto_id, cantidad, estado)"
                    . " values ($id_sesion, $id_adicional ,$cantidad,'A') ";
            echo $insert;
            $this->database->query($insert);
        }
    }

    public function agregarInfanteSesion($parametros) {
        try {

            $hora_actual = date('H:i:s');
            $sql = "INSERT INTO sesiones_infantes(sesion_id,infante_id,hora_inicio,estado,servicio_id) VALUES
            (" . $parametros['sesion_id'] . "," . $parametros['infante_id'] . ",'$hora_actual','A'," . $parametros['servicio_id'] . ")";
            $this->database->query($sql);
            $datos['insert'] = 1;
            $datos['hora_inicio'] = $hora_actual;

            echo json_encode($datos);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function crearAgregarInfanteSesion($parametros) {
        try {


            $datos = $this->infante->crear($parametros);

            $hora_actual = date('H:i:s');
            $sql = "INSERT INTO sesiones_infantes(sesion_id,infante_id,hora_inicio,estado,servicio_id) VALUES
            (" . $parametros['sesion_id'] . "," . $datos['id'] . ",'$hora_actual','A'," . $parametros['servicio_id'] . ")";
            $this->database->query($sql);


            $sql_hora = "SELECT * FROM sesiones_infantes where infante_id = " . $datos['id'] . " and sesion_id = " . $parametros['sesion_id'];
            $val_hora = $this->database->query($sql_hora);
            $row_hora = $val_hora->fetch_object();


            $datos['result'] = 1;
            $datos['id_infante'] = $datos['id'];
            $datos['hora_inicio'] = $row_hora->hora_inicio;

            echo json_encode($datos);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function cambiarServicio($parametros) {
        $infante_id = $parametros['infante_id'];
        $sesion_id = $parametros['sesion_id'];
        $servicio_id = $parametros['servicio_id'];
        $hora_actual = date('H:i:s');

        if ($servicio_id == 4) {
            //tomar tiempo de tolerancia

            $sql_tolerancia = "SELECT * FROM sesiones_infantes where infante_id = " . $infante_id . " and sesion_id = " . $parametros['sesion_id'];
            $val_tolerancia = $this->database->query($sql_tolerancia);
            $row_tolerancia = $val_tolerancia->fetch_object();
            $tolerancia = $row_tolerancia->tiempo_tolerancia;


            $sqlHoraInicioInfante = "SELECT hora_inicio FROM sesiones_infantes where infante_id = $infante_id and sesion_id = $sesion_id";
            $valHoraInicioInfante = $this->database->query($sql_tolerancia);
            $rowHoraInicioInfante = $valHoraInicioInfante->fetch_object();
            $horaInicioInfante = $row_tolerancia->hora_inicio;

            $minutosTranscurrios = $this->diferenciaMinutos($horaInicioInfante, $hora_actual);

            if ($minutosTranscurrios <= 60) {
                $updateCambio = "UPDATE sesiones_infantes SET servicio_id = 3 WHERE infante_id = $infante_id and sesion_id = $sesion_id";
                $resultCambio = $this->database->query($updateCambio);
                echo $resultCambio;
            } else {
                echo "Es necesario facturar la sesión en fracción debido que supero el tiempo de tolerancia";
            }
        } elseif ($servicio_id == 3) {
            
        }
    }

    public function diferenciaMinutos($fecha1, $fecha2) {
        $datetime1 = strtotime($fecha1);
        $datetime2 = strtotime($fecha2);
        $interval = abs($datetime2 - $datetime1);
        $minutes = round($interval / 60);
        return $minutes;
    }

    public function getConcurrenciaActiva($tipo) {
        $fecha_actual = date('y-m-d');
        $valor = ($tipo == 'N') ? 4 : 3;
        $sql = "SELECT COUNT(*) AS TotalActivos FROM `sesiones_infantes` si, sesiones s WHERE s.id=si.sesion_id AND s.`fecha`='$fecha_actual' AND si.`estado` LIKE 'A' AND servicio_id=$valor; ";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $totalActivos = $row->TotalActivos;
        return $totalActivos;
    }

    public function getConcurrenciaTotalDia($tipo) {
        $fecha_actual = date('y-m-d');
        $valor = ($tipo == 'N') ? 4 : 3;
        $sql = "SELECT COUNT(*) AS TotalActivos FROM `sesiones_infantes` si, sesiones s WHERE s.id=si.sesion_id AND s.`fecha`='$fecha_actual' AND servicio_id=$valor; ";
        $val = $this->database->query($sql);
        $row = $val->fetch_object();
        $totalActivos = $row->TotalActivos;
        return $totalActivos;
    }

    public function getConcurrenciCierre($fecha) {
        if ($fecha == "")
            $fecha = date('y-m-d');

        $sql = "SELECT count(*) as numero, se.nombre, FORMAT(se.precio_venta * count(*),2) as total FROM `sesiones_infantes` si, sesiones s, servicios se WHERE se.id = si.servicio_id and s.id=si.sesion_id AND s.`fecha`='$fecha' and si.estado = 'T' GROUP by si.servicio_id";
        $val = $this->database->query($sql);
        return $val;
    }

    public function ListaConcurrenciaPorFecha($fecha_inicio, $fecha_fin) {
        $select = "select se.fecha, sr.nombre as servicio , concat(c.nombres,' ',c.apellidos) as nombre_cliente, c.telefono1 as telefono_cliente, c.email as correo_cliente ,si.infante_id, i.primer_nombre, i.primer_apellido, i.fecha_nacimiento, si.hora_inicio, si.hora_fin   from servicios sr ,clientes c, sesiones se, sesiones_infantes  si, infantes i
                    where sr.id = si.servicio_id and se.cliente_id = c.id and se.fecha between '$fecha_inicio' and '$fecha_fin' and se.id = si.sesion_id and i.id = si.infante_id and si.estado in ('T','A')  order by se.id desc";
        return $this->database->query($select);
    }

    public function afluenciaPorHoras() {

        if ($_POST['fecha_inicio'] != "") {

            $query = "s.fecha between '" . $_POST['fecha_inicio'] . "' and '" . $_POST['fecha_fin'] . "' and";
        }

        $data = array();
        $horas = [['10:00', '12:00'], ['12:01', '14:00'], ['14:01', '16:00'], ['16:01', '18:00'], ['18:01', '20:00'], ['20:01', '22:00']];
        for ($i = 0; $i <= 5; $i++) {

            $select = "select count(*) as cantidad, si.servicio_id, r.nombre from sesiones s, sesiones_infantes si, servicios r where " . $query . " r.id = si.servicio_id  and si.sesion_id = s.id and si.hora_inicio > '" . $horas[$i][0] . "' and si.hora_fin < '" . $horas[$i][1] . "' and si.estado = 'T' group by si.servicio_id";
            $val = $this->database->query($select);

            $temp = array("x" => $horas[$i][0] . " - " . $horas[$i][1]);
            while ($row = $val->fetch_object()) {

                $temp[$row->servicio_id] = $row->cantidad;
            }

            array_push($data, $temp);
        }
        echo json_encode($data);
    }

    public function SesionesCanceladas($fecha_inicio,$fecha_fin) {
        
        if ($fecha_inicio != "") {

            $query = "s.fecha between '" . $fecha_inicio . "' and '" . $fecha_fin . "' and";
        }
        
        $select = "select s.fecha, n.primer_nombre, n.primer_apellido, i.hora_inicio, u.nombre from sesiones s, sesiones_infantes i, usuarios u, infantes n where ".$query."   n.id = i.infante_id and s.id = i.sesion_id and u.id = s.usuario_id and i.estado = 'C'";
        return $this->database->query($select);
    }

}
