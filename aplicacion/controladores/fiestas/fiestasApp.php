<?php

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
date_default_timezone_set('America/Guayaquil');
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/infante/infante.php';
$prueba = new fiestasApp();
echo $prueba->obtenerFiestasActivas();

class fiestasApp {

    protected $database;
    protected $infante;

    public function __construct() {
        $this->database = new database();
        $this->infante = new infante();
    }

    public function obtenerFiestasActivas() {
        $sql = "SELECT * FROM fiestas where estado = 'C' ";
        $val = $this->database->query($sql);
        while ($row = $val->fetch_object()) {
            $datetime = new DateTime($row->fecha);
            $sqlCliente = "SELECT nombres, apellidos FROM clientes where id = $row->cliente_id ";
            $valClienete = $this->database->query($sqlCliente);
            $rowCiente = $valClienete->fetch_object();
            $salon = $this->obtenerSalon($row->salon_id);
            $servicio=  $this->obtenerServicio($row->servicio_id);
            $datos[] = array(
                "titulo" => $rowCiente->nombres . " " . $rowCiente->apellidos,
                "inicia" => $datetime->format('D') . ' ' . $datetime->format('M') . ' ' . $datetime->format('j') . ' ' . $datetime->format('Y') . ' ' . date($row->hora_inicio) . ' GMT-0500 (ECT)',
                "descripcion" => $row->observaciones . '<div class="fiesta_id" style="display:none;">' . $row->id . '</div>',
                "className" => array("event", $row->color),
                "icon" => $row->icono,
                "anfitrion" => $row->anfitrion,
                "servicio" => $servicio,
                "salon" => $salon
            );
        }

        $json = json_encode($datos);
        echo $json;
    }

    public function obtenerSalon($codigoSalon) {
        $sqlSalon = "SELECT nombre FROM salones where id = $codigoSalon ";
        $valSalon = $this->database->query($sqlSalon);
        $rowSalon = $valSalon->fetch_object();
        return $rowSalon->nombre;
    }
    public function obtenerServicio($codigoServicio) {
        $sqlSalon = "SELECT nombre FROM servicios where id = $codigoServicio ";
        $valSalon = $this->database->query($sqlSalon);
        $rowSalon = $valSalon->fetch_object();
        return $rowSalon->nombre;
    }

}
