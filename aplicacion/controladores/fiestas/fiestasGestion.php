<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day
 

date_default_timezone_set('America/Guayaquil');
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/infante/infante.php';

class fiestasGestion {

    protected $database;
    protected $infante;

    public function __construct() {
        $this->database = new database();
        $this->infante = new infante();
    }

    public function actualizarFiesta($parametros) {
        $id = $parametros['id'];
        $fecha = $parametros['fecha'];
        $hora_inicio = $parametros['hora_inicio'];
        $minutos_inicio = $parametros['minutos_inicio'];


        $sqlDisponiblidad = "SELECT * FROM fiestas where fecha = '$fecha' and hora_inicio = '$hora_inicio:$minutos_inicio' and estado = 'C' ";
        $valDisponibilidad = $this->database->query($sqlDisponiblidad);
        $numeroFiestas = $valDisponibilidad->num_rows;

        if ($numeroFiestas == 0) {


            $update = "UPDATE fiestas SET fecha = '$fecha', estado = 'C', hora_inicio = '$hora_inicio:$minutos_inicio' where id = $id  ";
            $result = $this->database->query($update);
            echo $result;
        } else {
            echo "falso";
        }
    }

    public function cambiarFechaFiesta($parametros) {
        $id = $parametros['id'];
        $fecha = $parametros['fecha'];
        $tipo = $parametros['tipo'];

        $update = "UPDATE fiestas SET fecha = '$fecha' where id = $id";
        $result = $this->database->query($update);
        echo $result;
    }

    public function obtenerFiestasActivas() {
        $sql = "SELECT * FROM fiestas where estado = 'C' OR estado='A' ";
        $val = $this->database->query($sql);



        while ($row = $val->fetch_object()) {


            $datetime = new DateTime($row->fecha);

            $sqlCliente = "SELECT nombres, apellidos FROM clientes where id = $row->cliente_id ";
            $valClienete = $this->database->query($sqlCliente);
            $rowCiente = $valClienete->fetch_object();

            $datos[] = array(
                "title" => $rowCiente->nombres . " " . $rowCiente->apellidos,
                "start" => $datetime->format('D') . ' ' . $datetime->format('M') . ' ' . $datetime->format('j') . ' ' . $datetime->format('Y') . ' ' . date($row->hora_inicio) . ' GMT-0500 (ECT)',
                "description" => $row->observaciones . '<div class="fiesta_id" style="display:none;">' . $row->id . '</div>',
                "className" => array("event", $row->color),
                "icon" => $row->icono
            );
        }

        echo json_encode($datos);
    }

}

//  "start" => 'Tue Feb 24 2015 10:30:00 GMT-0500 (ECT)',

