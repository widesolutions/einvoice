<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
date_default_timezone_set('America/Guayaquil');

class fiestas {

    protected $database;

    public function __construct() {
        $this->database = new database();
    }

    public function listarFiestas() {
        $sql = "SELECT c.apellidos as apellido, c.nombres as nombre, 
                f.contratante as contratante, f.fecha as fecha,
                f.id as fiesta_id, f.hora_fin as hora_inicio, 
                f.hora_fin as hora_fin, f.tema as tema, f.estado as estado, 
                f.total_infantes as infantes, sa.nombre as salon 
                FROM fiestas f, clientes c, salones sa 
                WHERE c.id=f.cliente_id AND f.salon_id= sa.id;";
        $resul = $this->database->query($sql);
        return $resul;
    }

    public function getCabeceraFiesta($parametros) {
        $id = $parametros['id'];

        $select = "SELECT * FROM fiestas f, servicios s, clientes c where f.cliente_id = c.id and f.servicio_id = s.id and  f.id = $id";
        $result = $this->database->query($select);
        while ($row = $result->fetch_object()) {
            $datos[] = array(
                "nombres" => $row->nombres,
                "apellidos" => $row->apellidos,
                "anfitrion" => $row->anfitrion,
                "anticipo" => $row->anticipo,
                "observaciones" => $row->observaciones,
                "servicio_id" => $row->servicio_id,
                "fecha"=>$row->fecha,
                "hora_inicio" => $row->hora_inicio,
                "total_infantes" => $row->total_infantes,
                "tema" => $row->tema,
                "salon_id" => $row->salon_id,
                "anfitrion" => $row->anfitrion,
                "servicio_id" => $row->servicio_id
                
            );
        }

        $selectDetalles = "SELECT * FROM fiestas_detalles d where  d.fiesta_id = $id";
        $resultDetalles = $this->database->query($selectDetalles);
        while ($rowDetalles = $resultDetalles->fetch_object()) {


            if ($rowDetalles->producto_id == 0) {
                $tabla = "servicios";
                $idps = $rowDetalles->servicio_id;
                $tipo = 2;
            } else {
                $tabla = "productos";
                $idps = $rowDetalles->producto_id;
                $tipo = 1;
            }

            $selectProductosServicios = "SELECT nombre from $tabla where id = $idps";
            $resultProductosServicios = $this->database->query($selectProductosServicios);
            $rowProductosServicios = $resultProductosServicios->fetch_object();

            $datos['detalles'][] = array(
                "nombre" => $rowProductosServicios->nombre,
                "valor" => $rowDetalles->valor,
                "cantidad" => $rowDetalles->cantidad,
                "tipo" => $tipo,
                "adicional_id" => $idps
            );
        }

        echo json_encode($datos);
    }

    public function actualizarFiesta($parametros) {

        try {
            $anfitrion = $parametros[0]['value'];
            $anticipo = $parametros[1]['value'];
            $observacion = $parametros[2]['value'];
            $servicio = $parametros[3]['value'];
            $hora = $parametros[4]['value'];
            $fiesta_id = $parametros[5]['value'];
            $numero_infantes = $parametros[6]['value'];
            $salon = $parametros[7]['value'];
            $tema = $parametros[8]['value'];

            $update = "UPDATE `fiestas` SET `servicio_id`= $servicio, `hora_inicio`= '$hora',`total_infantes`=$numero_infantes ,`salon_id`= $salon, `tema` = '$tema', `anfitrion` = '$anfitrion', `anticipo` = '$anticipo', `observaciones` = '$observacion'  WHERE id = $fiesta_id";
            
            $this->database->query($update);

            $adicional_id = json_decode(stripslashes($_POST['adicional_id']));
            $cantidad = json_decode(stripslashes($_POST['cantidad']));
            $valor = json_decode(stripslashes($_POST['valor']));
            $adicional_tipo = json_decode(stripslashes($_POST['adicional_tipo']));

            $delete = "DELETE FROM  fiestas_detalles WHERE fiesta_id = $fiesta_id";
            $this->database->query($delete);

            foreach ($adicional_id as $key => $value) {
                $tipo = ($adicional_tipo[$key] == '2' ) ? "servicio_id" : "producto_id";
                $insert = "INSERT INTO fiestas_detalles ( " . $tipo . ",cantidad,valor,fiesta_id) VALUES ( " . $adicional_id[$key] . " , " . $cantidad[$key] . "," . $valor[$key] . ", $fiesta_id )";
                $this->database->query($insert);
            }

            echo 1;
            
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }
    
    public function finalizarFiesta($parametros){
        $id = $parametros['id'];
        $update = "UPDATE fiestas set estado = 'PL' where id = $id";
        return $this->database->query($update);
        
    }
    
    public function cancelarFiesta($parametros){
        $id = $parametros['id'];
        $update = "DELETE from fiestas where id = $id";
        return $this->database->query($update);
        
    }    

}


