<?php

session_start();
date_default_timezone_set('America/Guayaquil');
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/clientes/clientes.php';

class fiestasIngresos {

    protected $database;
    protected $clientes;

    public function __construct() {
        $this->database = new database();
        $this->clientes = new clientes();
    }

    public function guardarFiesta($parametros) {
        $eventos = $parametros['eventos'];
        $cliente = $parametros['cliente'];
        $titulo = $parametros['titulo'];
        $description = $parametros['description'];
        $color = $parametros['color'];
        $icono = $parametros['icono'];
        $tabla = ($eventos == 'evento') ? 'eventos' : 'fiestas';
        $contratante = $parametros['contratante'];
        $correoContratante = $parametros['correoContratante'];
        $telefonoContratante = $parametros['telefonoContratante'];
        $servicio = $parametros['servicio'];
        $cantidadInfantes = $parametros['cantidadInfantes'];
        $salon = $parametros['salon'];
        $anticipo = $parametros['anticipo'];
        $anfitrion = $parametros['anfitrion'];
        
       
        $sql = "INSERT INTO fiestas (`cliente_id`,`servicio_id`,`observaciones`,`total_infantes`, salon_id,`contratante`,`correo_contratante`,`telefono_contratante`,`tema`,`color`,`icono`,`estado`,`anticipo`,`anfitrion`)
                VALUES ('$cliente',$servicio,'$description',$cantidadInfantes,$salon, '$contratante','$correoContratante','$telefonoContratante','$titulo','$color','$icono','P',$anticipo,'$anfitrion');";
        $insert = $this->database->query($sql);
        $datos['insert'] = $insert;
        $datos['id'] = $this->database->lastid();

        if ($insert) {
            echo $datos['id'];
        } else {

            return 0;
        }
    }

    public function getEventosPendientes() {
        $sqlfiestas = "SELECT f.id AS idFiesta,f.*,c.* FROM `fiestas` f, clientes c WHERE f.cliente_id=c.id AND f.estado ='P' ";
        $consultafiestas = $this->database->query($sqlfiestas);
        if($consultafiestas->num_rows>0){
            while ($row = $consultafiestas->fetch_object()) {
                $datos['datos'][] = array("id"=>$row->idFiesta,
                    "tipo"=>$row->tipo,
                    "idCliente" => $row->cliente_id,
                    "title" => $row->nombres.' '.$row->apellidos,
                    "description" => $row->tema,
                    "icon" => $row->icono,
                    "className" => $row->color
                );
            }
            echo json_encode($datos);
        }else{
            echo 0;
        }
        
    }

}
