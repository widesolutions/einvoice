$(document).ready(function () {
    recargarLocales();
    var $registerForm = $("#localForm").validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            messages: {
                nombre: {required: "el nombre es necesario", minlength: "debe ingresar mas de 3 caracteres"},
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        }
    });
});

function nuevoLocal() {
    limpiarFormularioLocal();
    $('#frmLocalesModal').modal('show');
}
function guardarLocal() {
    var idlocal = $('#IDLocal').val();
    if (idlocal === '') {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=locales&controlador=locales&metodo=guardaDatosLocal',
            datetype: "json",
            type: 'POST',
            data: $("#localForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $('#frmLocalesModal').modal('hide');
                    $.smallBox({
                        title: "Local Guardado",
                        content: "<i class='fa fa-clock-o'></i> <i>Local Guardado correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    limpiarFormularioLocal();
                    recargarLocales();
                }
            }
        });
        $('#frmLocalesModal').modal('hide');    //revisar si da error    
    } else {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=locales&controlador=locales&metodo=ActualizarDatos',
            datetype: "json",
            type: 'POST',
            data: $("#localForm").serialize(),
            success: function (res){
                if (res === '1'){
                    $('#frmLocalesModal').modal('hide');
                    $.smallBox({
                        title: "Datos Actualizados",
                        content: "<i class='fa fa-clock-o'></i> <i>Servicio Actualizado correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    recargarLocales();
                }
            }
        });
    }
}
function limpiarFormularioLocal(){
    $("#IDLocal").val('');
    $("#nombre").val('');  /*nombre*/
    $("#telefono").val('');  /*telefono*/
    $("#ubicacion").val('');/*ubicacion*/
}
function ActualizarLocal(id)
{
    // alert("actualizar111");
    $.ajax({
        // url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=enviarDatosProducto',
        url: 'aplicacion/rutasMetodos.php?modulo=locales&controlador=locales&metodo=enviarDatosLocal',
        datetype: "json",
        type: 'POST',
        data: {codigolocal: id},
        success: function (res) {
            var json_obj = $.parseJSON(res);
            // alert("mensaje de"+res);
            //var json_obj = $.parseJSON(res);                  
            limpiarFormularioLocal();
            carga_DatosIncialesLocal(json_obj);
            $('#frmLocalesModal').modal('show');
            $('#localForm >header').text('Actualización de Datos del Local');
            //recargarLocales();

        }
    });
}
function carga_DatosIncialesLocal(edt) {
    $("#IDLocal").val(edt.datosLocal.IDLocal); //id del servicio
    $("#nombre").val(edt.datosLocal.nombre);  /*precio compra*/
    $("#ubicacion").val(edt.datosLocal.ubicacion);  /*precio servicio*/
    $("#telefono").val(edt.datosLocal.telefono);  /*cod barras serv*/
}
function EliminarLocal(id,nombre) {
    $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro que desea eliminar el Local <span class='txt-color-orangeDark'><strong>" + nombre + " </strong></span> ?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            var idlocal = $('#IDLocal').val();
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=locales&controlador=locales&metodo=EliminarLocal',
                datetype: "json",
                type: 'POST',
                data: {idlocal: id},
                success: function (res) {
                    if (res === '1') {
                        $.smallBox({
                            title: "Local Eliminado",
                            content: "<i class='fa fa-clock-o'></i> <i>Local eliminado correctamente...</i>",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                        recargarLocales();
                    }
                }
            });
        }
    });
}
function recargarLocales() {
    var dtTable = $('#listaLocales').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/locales/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}
