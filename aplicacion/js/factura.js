$(document).ready(function () {
    validacionesCajastexto();
    busquedasproductos();
    recargarFacturacion();
    validacionFormularioCliente();
    $('#frmImpresionModal').on('hidden.bs.modal', function () {
        location.reload();
    });
    $('#btnDescuentos').click(function () {
        if ($('#tipoItem').val() == 1) {
            asignarDecuentoxCodigo();
        } else {
            asignarDecuento();
        }
    });
    $('#listaFacturasGestion').dataTable();
    $(document).ajaxStop(function () {
        $('#cargando').css({display: 'none'});
    });
    $(document).ajaxStart(function () {
        $('#cargando').css({display: 'block'});
    });
});
function validacionFormularioCliente() {
    $("#rucCliente").validarCedulaEC({
        events: "blur",
        onValid: function () {
            $(this).parent('label').removeClass('state-error');
            $(this).parent('label').addClass('state-success');
            $(this).parent('label').parent('section').children('em').remove();
            $("button[type=submit]").removeAttr("disabled");
        },
        onInvalid: function () {
            $(this).parent('label').removeClass('state-success');
            $(this).parent('label').addClass('state-error');
            $(this).parent('label').parent('section').children('em').remove();
            $(this).parent('label').parent('section').append('<em class="invalid errorCedula" for="identificacion">Cédula o Ruc no Valido</em>');
            $("button[type=submit]").attr("disabled", "disabled");
        }
    });


    var $registerForm = $("#fromClientesFactura").validate({
        rules: {
            tipoDocumentoCliente: {required: true},
            rucCliente: {required: true, digits: true},
            nombreCliente: {required: true},
            apellidoCliente: {required: true},
            telefonoCliente: {required: true, digits: true},
            correoCliente: {required: true, email: true},
            direccionCliente: {required: true}
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
}
function editarCliente() {
    $('#frmClienteModal').modal('show');
}
function obtenerDescuento(codigo) {
    var codigoNew, cod = '';
    if (codigo !== '') {
        codigoNew = codigo + '/1';
    } else {
        cod = $('#codigoProducto1').val();
        codigoNew = cod + '/0';
    }
    $('#token').val('descuentos');
    $('#parametros').val(codigoNew);
    $('#dialog-autoriza').dialog('open');
}
function anularFactura(codigo) {
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro de Anular la Factura?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Si") {
            $('#token').val('anularFactura');
            $('#parametros').val(codigo);
            $('#dialog-autoriza').dialog('open');
        }
    });

}
function recargarFacturacion() {
    var dtTable = $('#listaFacturasGestion').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/facturas/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}
function metodosdePago() {
    if ($('#total').val() !== '' && $('#total').val() !== '0.00') {
        $('#valorApagar').val($('#total').val());
        $('#valorApagarTxt >b').html($('#total').val());
        $('#txtValorApagar').html('$ ' + $('#total').val());
        $("#efectivo").removeAttr("disabled");
        $("#efectivo").parent('label').removeClass("state-disabled");
        $("#efectivo").parent('label').parent('td').siblings().children('label').children('.checkMetodoPago').prop("checked", true);
        $("#efectivo").val($('#total').val());
        calcularValorPago();
        $('#botonFacturar').removeClass('disabled');
        $('#frmFormaPagoModal').modal('show');
    } else {
        $.smallBox({
            title: "Sin Items",
            content: "<i class='fa fa-clock-o'></i> <i>No existen items para generar la Factura...</i>",
            color: "#C46A69",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 7000
        });
    }
}
function validarMetodoPago() {

    var valorApagar = $('#valorApagar').val(), pago = 0;
    $.each($('.checkMetodoPago'), function () {
        var valorPago = $(this).parent('label').parent('td').siblings('td').children('label').children('.valorPagar').val();
        if ($(this).is(':checked') && valorPago !== '') {
            pago = pago + parseFloat($(this).parent('label').parent('td').siblings().children('label').children('.valorPagar').val());
        }
    });
    var cancelado = parseFloat(valorApagar) - parseFloat(pago);
    if (cancelado === 0) {
        $(document).keypress(function (e) {
            if (e.which === 13 || e.which === 0 || e.which === 32 || e.which === 8) {
                return false;
            }
        });
        $.SmartMessageBox({
            title: "Alerta!",
            content: "Esta seguro que desea Emitir la Factura # " + $('#numeroFactura').val() + " ?",
            buttons: '[No][Si]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Si") {
                $('#botonFacturar').addClass('disabled');
                var parametros = {};
                parametros['idCliente'] = $('#idCliente').val();
                parametros['codSesion'] = $('#codSesion').val();
                parametros['codInfantes'] = $('#codInfantes').val();
                parametros['codFiesta'] = $('#codFiesta').val();
                parametros['numeroFactura'] = $('#numeroFactura').val();
                parametros['subtotal'] = parseFloat($('#txtSubTotal').html());
                parametros['iva'] = parseFloat($('#txtIva').html());
                parametros['subtotalSinIva'] = parseFloat($('#txtSubTotalSinIva').html());
                parametros['total'] = parseFloat($('#txtTotal').html());
                parametros['items'] = [];
                parametros['pagos'] = [];
                $.each($('.codigoProducto'), function (key) {
                    var items = {};
                    items['codigo'] = $(this).children('spam').html();
                    items['sesionesInfantes'] = $(this).parent('td').siblings('td').children('label').children('.sesionInfantes').val();
                    items['cantidad'] = parseFloat($(this).parent('td').siblings('.cantidad').children('label').html());
                    items['vtotal'] = parseFloat($(this).parent('td').siblings('.subtotalProducto').children('label').children('spam').html());
                    items['productoServicio'] = $(this).children('.productoServicio').val();
                    parametros['items'].push(items);
                });
                $.each($('.checkMetodoPago'), function () {
                    var pagos = {};

                    if ($(this).is(':checked')) {
                        var valorPago = $(this).parent('label').parent('td').siblings('td').children('label').children('.valorPagar').val();
                        if (valorPago !== '') {
                            pagos['tipoPago'] = parseInt($(this).parent('label').parent('td').siblings().children('label').children('.codigoTipoPago').val());
                            pagos['valorPagar'] = parseFloat($(this).parent('label').parent('td').siblings().children('label').children('.valorPagar').val());
                            pagos['numeroDocumento'] = $(this).parent('label').parent('td').siblings().children('label').children('.numeroDocumento').val();
                            pagos['entidadDocumento'] = $(this).parent('label').parent('td').siblings().children('label').children('.tipoDocumentoAdicional').val();
                            parametros['pagos'].push(pagos);
                        }
                    }

                });
                $.ajax({
                    url: 'aplicacion/rutasMetodos.php?modulo=facturas&controlador=facturas&metodo=guardarCabeceraFactura',
                    type: 'POST',
                    data: {parametros: parametros},
                    success: function (codMovimiento) {
                        if (codMovimiento !== '0') {
                            $('#frmFormaPagoModal').modal('hide');
                            imprimirDocumento(parametros['idCliente'], codMovimiento, parametros['codSesion']);
                        }
                    }
                });
            }
            if (ButtonPressed === "No") {
                $('#botonFacturar').removeClass('disabled');
            }
        });
    } else {
        $.smallBox({
            title: "Error en Cálculos",
            content: "<i class='fa fa-clock-o'></i> <i>Existe un faltante de pago de: <b style='color:yellow;'>" + cancelado.toFixed(2) + "</b>...</i>",
            color: "#C46A69",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 7000
        });
    }

}
function asignarDecuento() {
    var descuentoAsigna = $('#descuentoAsigna').val();
    var tipoDescuento = $('input:radio[name=tipoDescuento]:checked').val();
    var tipo, txtTipoDescuento = '';
    if (tipoDescuento === 'P') {
        var valorTotal = parseFloat($('#totalProducto1').val());
        var valorAplica = (valorTotal * descuentoAsigna) / 100;
        valorAplica = roundNumber(parseFloat(valorAplica), 3);
        var tipo = valorAplica;
        $('#descuento1').val(valorAplica);
        txtTipoDescuento = descuentoAsigna + ' %';

    } else {
        valorAplica = descuentoAsigna;
        $('#descuento1').val(valorAplica);
        var txtTipoDescuento = '$ ' + descuentoAsigna;
    }

    $('#txtDescuento').html(tipo);
    $('#txtTipoDescuento').html(txtTipoDescuento);
    $('#totalProducto1').val($('#totalProducto1').val() - valorAplica);
    $('#frmDescuentoModal').modal('hide');
//    calcular();
}
function asignarDecuentoxCodigo() {
    var codigoAsigna = $('#codigoProSer').val();
    var descuentoAsigna = $('#descuentoAsigna').val();
    var tipoDescuento = $('input:radio[name=tipoDescuento]:checked').val();
    var tipo, txtTipoDescuento = '';
    var valorTotal = parseFloat($('.itemFac-' + codigoAsigna).children('.subtotalProducto').children('label').children('.totales').val());
    if (tipoDescuento === 'P') {
        var valorAplica = (valorTotal * descuentoAsigna) / 100;
        valorAplica = roundNumber(parseFloat(valorAplica), 3);
        var tipo = valorAplica;
        $('.itemFac-' + codigoAsigna).children('.descuento').children('label').children('spam').html(valorAplica);
        txtTipoDescuento = descuentoAsigna + ' %';

    } else {
        valorAplica = parseFloat(descuentoAsigna);
        $('.itemFac-' + codigoAsigna).children('.descuento').children('label').children('spam').html(valorAplica);
        var txtTipoDescuento = '$ ' + descuentoAsigna;
    }

//    $('#txtDescuento').html(tipo);
//    $('#txtTipoDescuento').html(txtTipoDescuento);
    var valorconDescuento = roundNumber(parseFloat(valorTotal - valorAplica), 3);
    $('.itemFac-' + codigoAsigna).children('.subtotalProducto').children('label').children('spam').html(valorconDescuento);
    $('.itemFac-' + codigoAsigna).children('.subtotalProducto').children('label').children('.totales').val(valorconDescuento);

    $('#frmDescuentoModal').modal('hide');
    calcular();
}
function busquedasproductos() {
    $("#codigoProducto1").keyup(function () {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=getProductoxCodigo',
            datetype: "json",
            type: 'POST',
            data: {codigo: $(this).val()},
            success: function (res) {
                if (res !== '') {
                    var json_obj = $.parseJSON(res);
                    $("#codigoProducto1").val(json_obj[0].codigo);
                    $("#descripcionProducto1").val(json_obj[0].value);
                    $("#vunitarioProducto1").val(json_obj[0].vunitario);
                    var vtotal = roundNumber((json_obj[0].vunitario * $("#cantidadProducto1").val()), 3);
                    $("#totalProducto1").val(vtotal);
                    $("#ivanoiva1").val(json_obj[0].ivanoiva);
                    calcular();
                }
            }
        });
    });
    $("#descripcionProducto1").autocomplete({
        source: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=getProductoxNombre',
        select: function (event, ui) {
            localStorage["descripcionProducto"] = ui.item.value;

            $('#codigoProducto1').val(ui.item.codigo);
            $('#descripcionProducto1').val(ui.item.value);
            var vunitario_producto = ui.item.vunitario;
            var cantidad_producto = $("#cantidadProducto1").val();
            var vunitario = (vunitario_producto / 1.12);
            //VALORES PARA CALCULO
            var vtotal = vunitario * cantidad_producto;
            $('#vunitarioProducto1').val(vunitario);
            $("#totalProducto1").val(vtotal);
            //VALORES PARA MOSTRAR
            var vunitarioTxt = roundNumber((vunitario_producto / 1.12), 2);
            var vtotalTxt = roundNumber((vunitario * cantidad_producto), 2);
            $('#txtProductoTotales1').html(vtotalTxt);
            $('#txtProductoUnidateios1').html(vunitarioTxt);
            ///////////////////////
            $("#ivanoiva1").val(ui.item.ivanoiva);
            $("#productoServicio1").val(ui.item.productoServicio);
            calcular();
            return false;
        }
    });
}
function round(x, places) {
    var shift = Math.pow(10, places);
    return Math.round(x * shift) / shift;
}
function validacionesCajastexto() {
    localStorage["descripcionProducto"] = '';
    $("#cantidadProducto1").keyup(function () {
        var vunitario = ($("#vunitarioProducto1").val() / 1.12);
        var vunitarioTxt = roundNumber(($("#vunitarioProducto1").val() / 1.12), 2);
        var vtotal_calculo = $("#vunitarioProducto1").val() * $(this).val() - $('#descuento1').val();
        var vtotal = (($(this).val() * $("#vunitarioProducto1").val()) - $('#descuento1').val());
        vtotal = roundNumber(vtotal, 2);
        $('#txtProductoTotales1').html(vtotal);
        $("#totalProducto1").val(vtotal_calculo);
//        calcular();
    });

}
function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
    var newnumber = Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
    return newnumber; // Output the result to the form field (change for your purposes)
}
function calcular() {
    var subTotal = 0, subTotalsinIva = 0, iva = 0, total = 0, descuento = 0;
    $.each($('.totales'), function () {
        if ($(this).siblings('spam').html() !== undefined) {
            if ($(this).siblings('#ivanoiva').val() === '1') {
                subTotal = (subTotal + parseFloat($(this).val()));
            } else {
                subTotalsinIva = (subTotalsinIva !== 0) ? (subTotalsinIva + parseFloat($(this).val())) : 0;
            }
        }
    });
    subTotal = parseFloat(subTotal);
    subTotalsinIva = parseFloat(subTotalsinIva);
    iva = subTotal * 0.12;
    iva = iva;
    total = parseFloat(subTotal) + parseFloat(iva) + parseFloat(subTotalsinIva);

    var total = roundNumber(total, 2);

    $('#txtSubTotal').html(roundNumber(subTotal, 2));
    $('#subTotal').val(subTotal);
    $('#txtSubTotalSinIva').html(roundNumber(subTotalsinIva, 2));
    $('#subTotalSinIva').val(subTotalsinIva);
    $('#txtIva').html(roundNumber(iva, 2));
    $('#iva').val(iva);
    $('#txtTotal').html(total);
    $('#total').val(total);
    $('#totalGeneral').html(total);

}
function agregarItemFactura() {
    if (validarCamposFactura() === true) {
        var codigo = $('#codigoProducto1').val();
        var descripcion = $('#descripcionProducto1').val();
        var descuento = $('#descuento1').val();
        var vunitario = $('#vunitarioProducto1').val();
        var vunitarioTxt = $('#txtProductoUnidateios1').html();
        var total = $('#totalProducto1').val();
        var totalTxt = $('#txtProductoTotales1').html();
        var cantidad = $('#cantidadProducto1').val();
        var ivanoiva = $('#ivanoiva1').val();
        var productoServicio = $('#productoServicio1').val();
        var cantidadAnterior = $('.itemFac-' + codigo).children('.cantidad').children().html();
        var valorAnteriorTxt = $('.itemFac-' + codigo).children('.valUnitarioProducto').children().html();
        var valorAnterior = $('.itemFac-' + codigo).children('.valUnitarioProducto').children('#valorUnitario').val();

        if (cantidadAnterior !== undefined) {
            var nuevaCantidad = parseFloat(cantidadAnterior) + parseInt(cantidad);
            $('.itemFac-' + codigo).children('.cantidad').children().html(nuevaCantidad);
            var valorNuevo = parseFloat(nuevaCantidad) * parseFloat(valorAnterior);
            var valorNuevoTxt = roundNumber(valorNuevo, 2);
            $('.itemFac-' + codigo).children('.subtotalProducto').children('label').children('spam').html(valorNuevoTxt);
            $('.itemFac-' + codigo).children('.subtotalProducto').children('label').children('#totales').val(valorNuevo);

            limpiarCamposFactura();
            calcular();
        } else {
            $('#listaUsuarios >tbody').append('<tr class="itemFac-' + codigo + '">' +
                    '<td><label class="input codigoProducto txtValoresEnteros"><spam>' + codigo + '</spam><input type="hidden" class="productoServicio" name="productoServicio" id="productoServicio" value="' + productoServicio + '"></label></td>' +
                    '<td><label class="input">' + descripcion + '</label></td>' +
                    '<td class="cantidad txtValoresEnteros" ><label class="input">' + cantidad + '</label></td>' +
                    '<td class="descuento txtValoresDecimales"><label class="input"><spam class="pull-left labelDescuento">' + descuento + '</spam><a class="btn btn-xs btn-default" href="javascript:obtenerDescuento(\'' + codigo + '\');"><i class="glyphicon glyphicon-pencil"></i></a></label></td>' +
                    '<td class="valUnitarioProducto txtValoresDecimales" style="vertical-align: middle"><label class="input">' + vunitarioTxt + '</label><input type="hidden" id="valorUnitario" name="valorUnitario" class="valorUnitario" value="' + vunitario + '"></td>' +
                    '<td class="subtotalProducto txtValoresDecimales" style="display: inline-flex;width: 150px;vertical-align: middle;"><label class="input" style="width: 75px; text-align: center; font-weight: bold; font-size: medium;"><spam>' + totalTxt + '</spam><input class="totales" type="hidden" name="totales" id="totales" value="' + total + '"><input type="hidden" name="ivanoiva" id="ivanoiva" class="ivanoiva" value="' + ivanoiva + '"></label>' +
                    '<a class="btn btn-danger btn-circle" href="javascript:eliminarItemFactura(\'' + codigo + '\');" style="margin: 0px 0px 0px 36px;">' +
                    '<i class="glyphicon glyphicon-remove"></i>' +
                    '</a>' +
                    '</td>' +
                    '</tr>');
            limpiarCamposFactura();
            calcular();
        }
    } else {
        if (descripcion !== localStorage["descripcionProducto"]) {
            $.smallBox({
                title: "Campos Incorrectos",
                content: "<i class='fa fa-clock-o'></i> <i>La descripción ingresada no es Valida...</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 7000
            });
        } else {
            $.smallBox({
                title: "Campos en Blanco",
                content: "<i class='fa fa-clock-o'></i> <i>Todos los campos deben estar llenos...</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 4000
            });
        }
    }
}
function eliminarItemFactura(codigo) {
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro de Eliminar este Item de la Factura?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Si") {
            $('#token').val('eliminaItem');
            $('#parametros').val(codigo);
            $('#dialog-autoriza').dialog('open');
        }
    });
}
function validarCamposFactura() {
    var codigo = $('#codigoProducto1').val();
    var descripcion = $('#descripcionProducto1').val();
    var vunitario = $('#vunitarioProducto1').val();
    var total = $('#totalProducto1').val();
    var cantidad = $('#cantidadProducto1').val();
    if (cantidad === '' || descripcion === '' || vunitario === '' || total === '' || cantidad === '' || descripcion !== localStorage["descripcionProducto"]) {

        return false;
    } else {
        return true;
    }
}
function validarCamposMetodosPago() {
    var codigo = $('#codigoProducto1').val();
    var descripcion = $('#descripcionProducto1').val();
    var vunitario = $('#vunitarioProducto1').val();
    var total = $('#totalProducto1').val();
    var cantidad = $('#cantidadProducto1').val();
    if (cantidad === '' || descripcion === '' || vunitario === '' || total === '' || cantidad === '') {
        return false;
    } else {
        return true;
    }
}
function limpiarCamposFactura() {
    $('#txtTipoDescuento').html('');
    $('#descuento1').val(0);
    $('#codigoProducto1').val('');
    $('#descripcionProducto1').val('');
    $('#vunitarioProducto1').val('');
    $('#totalProducto1').val('');
    $('#cantidadProducto1').val('');
    $('#txtProductoUnidateios1').html('0.00');
    $('#txtProductoTotales1').html('0.00');
    $('#descripcionProducto1').focus();
}
function modificarClienteFactura() {
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro de Modificar al Cliente de esta Factura?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Si") {
            $('#idCliente').val('');
            $('#nombreCliente').val('');
            $('#apellidoCliente').val('');
            $('#rucCliente').val('');
            $('#direccionCliente').val('');
            $('#correoCliente').val('');
            $('#telefonoCliente').val('');
            $('.txtOpcionalInput').show();
            $('.txtOpcionalLabel').hide();
            $('#cambioCliente').val('S');
        }
    });
}
function gurdarClienteFactura() {
    guardarDatosCliente();
    var id = $('#idCliente').val();
    if (id !== '') {
        debugger;
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientePorId',
            type: 'POST',
            async: true,
            cache: false,
            data: {id: id},
            success: function (res) {
                var obj = $.parseJSON(res);
                $('.txtOpcionalInput').hide();
                $('.txtOpcionalLabel').show();
                $('#txtNombreCliente').html(obj.datosCliente.nombres + ' ' + obj.datosCliente.apellidos);
                $('#nombreCliente').val(obj.datosCliente.nombres);
                $('#apellidoCliente').val(obj.datosCliente.apellidos);
                $('#txtRucCliente').html(obj.datosCliente.identificacion);
                $('#rucCliente').val(obj.datosCliente.identificacion);
                $('#txtDireccionCliente').html(obj.datosCliente.direccion);
                $('#direccionCliente').val(obj.datosCliente.direccion);
                $('#txtCorreoCliente').html(obj.datosCliente.email);
                $('#correoCliente').val(obj.datosCliente.email);
                $('#txtTelefonoCliente').html(obj.datosCliente.telefono1);
                $('#telefonoCliente').val(obj.datosCliente.telefono1);
                $('#cambioCliente').val('N');
            }
        });
    }


}
function imprimirDocumento(idCliente, codMovimiento, codSesion) {
    $('#frmImpresionModal').modal('show');
    var url = 'aplicacion/vistas/facturacion/frmImpresionDocumento.php?idCliente=' + idCliente + '&mov=' + codMovimiento + '&sesion=' + codSesion;
    var divEl = document.getElementById('contenidoFactura');
    var objEl = document.getElementById('contenidoFactura');
    objEl.src = url;
    divEl.innerHTML = divEl.innerHTML;
}
function VerDetalle() {
    $('#frmDetalleFacturaModal').modal('show');
}
function carga_DetalleFactura(obj) {
    //detalle de la factura
    $("#IDMovimiento").val(obj.DetalleFactura.IDMovimiento); //id del producto
    $("#cliente").html(obj.DetalleFactura.cliente);  /*Apellido*/
    $("#IDCliente").val(obj.DetalleFactura.IDCliente);
    $("#identificacion").html(obj.DetalleFactura.ruc);/*Usuario*/
    $("#nfactura").html(obj.DetalleFactura.nfactura);/*Celular*/
    $("#total").html(obj.DetalleFactura.total);
    $("#subtotal").html(obj.DetalleFactura.subtotal);
    $("#iva").html(obj.DetalleFactura.iva);
    $("#subsiniva").html(obj.DetalleFactura.subsiniva);
    $("#fecha").html(obj.DetalleFactura.fecha);

    var detalle = '';
    $.each(obj.DetalleTblProducto, function (key, value) {
        detalle += '<tr><td>' + value.cantidad + '</td>' +
                '<td><label ><spam>' + value.descripcionproducto + '</spam></td>' +
                '<td>' + value.valor + '</td>' +
                '</tr>';
    });
    $('#listaDetalle >tbody').html(detalle);
    var pago = '';
    $.each(obj.DetalleTipoPago, function (key, value) {
        pago += '<tr><td>' + value.nombre + '</td>' +
                '<td><label ><spam>' + value.valor + '</spam></td>' +
                '</tr>';
    });
    $('#listaDetallePago >tbody').html(pago);
}
function verDetalleFactura(movimiento_id) {
    var parametros = {};
    parametros['movimiento_id'] = movimiento_id;
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=facturas&controlador=facturas&metodo=detalleFactura',
        type: 'POST',
        data: {parametros: parametros},
        success: function (res) {
            var obj = $.parseJSON(res);
            carga_DetalleFactura(obj);
            $('#frmDetalleFacturaModal').modal('show');
        }
    });
}
