$(document).ready(function () {
    validarTarjeta();
});

function validarTarjeta() {
    var path = 'publico/img/invoice/', img = '', nom = '';
    $('.numeroDocumento').keyup(function () {
        $(this).validateCreditCard(function (result) {
            if (result.card_type !== null) {
                console.log(result);
                console.log(result.card_type.name);
                switch (result.card_type.name) {
                    case 'amex':
                        img = 'americanexpress.png';
                        nom = 'American Express';
                        break;
                    case 'mastercard':
                        img = 'mastercard.png';
                        nom = 'Mastercard';
                        break;
                    case 'visa':
                        img = 'visa.png';
                        nom = 'Visa';
                        break;
                    case 'maestro':
                        img = 'maestro.png';
                        nom = 'Maestro';
                        break;
                    case 'diners':
                        img = 'dinersclub.png';
                        nom = 'Diners Club';
                        break;
                    case 'discover':
                        img = 'discover.png';
                        nom = 'Discover';
                        break;
//                    default :img='americanexpress.png'; nom='American Express';break;
                }

                $(this).parent("label").parent("td").siblings().children(".imagenTarjeta").attr("src", path + img);
                $(this).parent("label").parent("td").siblings().children("label").children('.tipoDocumentoAdicional').val(nom);
            }
        });
    });
    $('.valorPagar').keyup(function () {
        calcularValorPago();

    });
    $('.checkMetodoPago').click(function () {
        $(this).val();
        if ($(this).is(':checked')) {
            $(this).parent('label').parent('td').siblings().children('label').children('.valorPagar').removeAttr("disabled");
            $(this).parent('label').parent('td').siblings().children('label').children('.numeroDocumento').removeAttr("disabled");
            $(this).parent('label').parent('td').siblings().children('label').removeClass("state-disabled");
        }else{
            $(this).parent('label').parent('td').siblings().children('label').children('.valorPagar').attr("disabled", "disabled");
            $(this).parent('label').parent('td').siblings().children('label').children('.numeroDocumento').attr("disabled", "disabled");
            $(this).parent('label').parent('td').siblings().children('label').children('.numeroDocumento').val("");
            $(this).parent('label').parent('td').siblings().children('label').children('.valorPagar').val("");
            $(this).parent('label').parent('td').siblings().children('label').addClass("state-disabled");
        }
    });
}
function calcularValorPago() {
    var valorFijo = $('#valorApagar').val(), valorPagando = 0,cantidad=0;
//    console.log(valor);
    $.each($('.valorPagar'), function () {
        if ($(this).val() !== '') {
            if ($('#txtValorApagar').html() !== '0') {
                valorPagando = valorPagando + parseFloat($(this).val());
                if (parseFloat(valorFijo) >= parseFloat(valorPagando)) {
                    cantidad = parseFloat(valorFijo) - parseFloat(valorPagando);
                } else {
                    alert('Cubrio el Valor adeudado');
                    $(this).val('');
                }
            }
        }
    });
$('#txtValorApagar').html('$ ' +cantidad.toFixed(2));
    
}
function agregarMasItems(nombre,id) {
    var item = '';
    var valores={};
    valores['valorPagar']=$('#'+nombre).val();
    valores['numeroDocumento']=$('#'+nombre).parent('label').parent('td').siblings().children('label').children('.numeroDocumento').val();
    
            
    console.log(valores);
    if(valores['valorPagar']!=='' && valores['numeroDocumento']!==''){
    var imagen = (nombre === 'tarjeta') ? '<label class="input"><input type="hidden" name="tipoDocumentoAdicional" id="tipoDocumentoAdicional" class="tipoDocumentoAdicional" value="' + nombre + '"></label><img class="imagenTarjeta" width="44" height="44" alt="visa" src="publico/img/invoice/visa.png">' : '';
    item = '<tr class="fila-' + nombre + '">' +
            '<td><label class="checkbox">' +
            '<input type="checkbox" disabled="disabled" class="checkMetodoPago" name="check-' + nombre + '" id="check-' + nombre + '" checked="checked" style="left: 31px;top: 11px; "></label></td>' +
            '<td><label class="input"><input type="hidden" name="codigoTipoPago" id="codigoTipoPago" class="codigoTipoPago" value="' + id + '">' + nombre.toUpperCase() + '</label></td>' +
            '<td><label class="input"><input type="text" name="valor' + nombre + '" id="valor' + nombre + '" class="valorPagar" placeholder="$ 00.00" ></label></td>' +
            '<td><label class="input"><input type="text" name="numeroDocumento" id="numeroDocumento" class="numeroDocumento" placeholder="NUMERO"></label></td>' +
            '<td>' + imagen + '</td>' +
            '<td></td>' +
            '</tr>';
    $("#fila-" + nombre).closest("tr").after(item);
    validarTarjeta();
}else{
    $.smallBox({
            title: "Campos en Blanco",
            content: "<i class='fa fa-clock-o'></i> <i>Antes debe llenar el Valor y el numero de documento de la tarjeta...</i>",
            color: "#C46A69",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 7000
        });
}

}