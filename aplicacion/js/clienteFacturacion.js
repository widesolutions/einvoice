$(document).ready(function () {
    parametrizacionInicial();
});
function parametrizacionInicial() {
    var $registerForm = $("#fromClientesFacturacion").validate({
        rules: {
            tipoDocumento: {required: true},
            pasaporte: {required: true},
            cedula: {required: true, digits: true},
            nombre: {required: true},
            apellido: {required: true},
            telefono2: {required: true, digits: true},
            email: {required: true, email: true},
            direccion: {required: true}
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    var $registerForm = $("#fromClientes").validate({
        rules: {
            tipoDocumentoCliente: {required: true},
            rucCliente: {required: true, digits: true},
            nombreCliente: {required: true},
            direccionCliente: {required: true},
            telefonoCliente: {required: true, digits: true}
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    $("#cedula").validarCedulaEC({
        events: "blur",
        onValid: function () {
            $(this).parent('label').removeClass('state-error');
            $(this).parent('label').addClass('state-success');
            $(this).parent('label').parent('section').children('em').remove();
            $("button[type=submit]").removeAttr("disabled");
        },
        onInvalid: function () {
            $(this).parent('label').removeClass('state-success');
            $(this).parent('label').addClass('state-error');
            $(this).parent('label').parent('section').children('em').remove();
            $(this).parent('label').parent('section').append('<em class="invalid errorCedula" for="identificacion">Cédula o Ruc no Valido</em>');
            $("button[type=submit]").attr("disabled", "disabled");
        }
    });
    $('#cedula').focusout(function () {
        if($(this).val()!==''){
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientexDocumento',
            datetype: "json",
            type: 'POST',
            data: {cedula: $(this).val()},
            success: function (res) {
                if (res !== '') {
                    var obj = $.parseJSON(res);
                    colocarDatosCliente(obj);

                }
            }
        });
    }
    });

    $('#rucCliente').focusout(function () {
        if($(this).val()!==''){
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientexDocumento',
            datetype: "json",
            type: 'POST',
            data: {cedula: $(this).val()},
            success: function (res) {
                if (res !== '') {
                    var obj = $.parseJSON(res);
                    colocarDatosOtroCliente(obj);
                }
            }
        });
    }
    });

    $('#pasaporte').focusout(function () {
        if($(this).val()!==''){
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientexDocumento',
            datetype: "json",
            type: 'POST',
            data: {cedula: $(this).val()},
            success: function (res) {
                if (res !== '') {
                    var obj = $.parseJSON(res);
                    colocarDatosCliente(obj);

                }
            }
        });
    }
    });
    $('#codigo').focusout(function () {
        if($(this).val()!==''){
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientePorId',
            datetype: "json",
            type: 'POST',
            data: {id: $(this).val()},
            success: function (res) {
                if (res !== '') {
                    var obj = $.parseJSON(res);
                    colocarDatosCliente(obj);

                }
            }
        });
    }
    });

}

function colocarDatosOtroCliente(obj) {
    var result = '', resultFiestas = '';
    $('#idCliente').val(obj.datosCliente.id);
    $('#codigo').val(obj.datosCliente.id);
    $('#tipoDocumentoCliente option[value="' + obj.datosCliente.tipoDocumento + '"]').attr("selected", true);
//    cambioCedulaPasaporteNuevo();
    $("#rucCliente").val(obj.datosCliente.identificacion);
    $("#pasaporte").val(obj.datosCliente.identificacion);
    $('#nombreCliente').val(obj.datosCliente.nombres);
    $('#apellidoCliente').val(obj.datosCliente.apellidos);
    $('#direccionCliente').val(obj.datosCliente.direccion);
    $('#telefonoCliente').val(obj.datosCliente.telefono1);
    $('#correoCliente').val(obj.datosCliente.email);

}

function colocarDatosCliente(obj) {
    var result = '', resultFiestas = '';
    $('#IDcliente').val(obj.datosCliente.id);
    $('#codigo').val(obj.datosCliente.id);
    $('#tipoDocumento option[value="' + obj.datosCliente.tipoDocumento + '"]').attr("selected", true);
    cambioCedulaPasaporte();
    $("#cedula").val(obj.datosCliente.identificacion);
    $("#pasaporte").val(obj.datosCliente.identificacion);
    $('#cedula').val(obj.datosCliente.identificacion);
    ($('#cedula').val() !== '') ? $('#cedula').attr('disable', true) : $('#cedula').attr('disable', false);
    $('#nombre').val(obj.datosCliente.nombres);
    ($('#nombre').val() !== '') ? $('#nombre').attr('disable', true) : $('#nombre').attr('disable', false);
    $('#apellido').val(obj.datosCliente.apellidos);
    ($('#apellido').val() !== '') ? $('#apellido').attr('disable', true) : $('#apellido').attr('disable', false);
    $('#telefono1').val(obj.datosCliente.telefono1);
    ($('#telefono1').val() !== '') ? $('#telefono1').attr('disable', true) : $('#telefono1').attr('disable', false);
    $('#telefono2').val(obj.datosCliente.telefono2);
    ($('#telefono2').val() !== '') ? $('#telefono2').attr('disable', true) : $('#telefono2').attr('disable', false);
    $('#email').val(obj.datosCliente.email);
    ($('#email').val() !== '') ? $('#email').attr('disable', true) : $('#email').attr('disable', false);
    $('#direccion').val(obj.datosCliente.direccion);
    ($('#direccion').val() !== '') ? $('#direccion').attr('disable', true) : $('#direccion').attr('disable', false);
    if (obj.datosSesiones !== undefined) {
        $.each(obj.datosSesiones, function (key, value) {
            result += '<tr>' +
                    '<td><input id="sesionId" class="sesionId" type="hidden" name="sesionId" value="' + value.id + '"><input id="terms" type="checkbox" checked="" name="terms"><i></i></td>' +
                    '<td>' + value.primer_nombre + ' ' + value.primer_apellido + '</td>' +
                    '<td>' + value.hora_inicio + '</td>' +
                    '<td style="font-size: 1.5em;text-align:center;"><i class="text-danger"><b>' + value.transcurrido + '</b></i></td>' +
                    '<td style="text-align:center;"><i>' + value.servicio_nombre + '</i></td>' +
                    '<td style="text-align:center;">' +
                    '<select name="promocion" id="promocion" >' +
                    '<option value="">-- Ninguna --</option>';
            if (obj.comboPromociones !== undefined) {
                $.each(obj.comboPromociones, function (key, values) {
                    if (value.diaPromocion === '8') {
                        var select = 'selected=""';
                    } else {
                        var select = (value.codigoPromocion == values.id) ? 'selected=""' : '';
                    }
                    result += '<option value="' + values.id + '" ' + select + '>' + values.nombre + '</option>';
                });
            }
            result += '</select>' +
                    '</td>' +
                    '<tr>';
        });
    } else {
        result = '<tr><td colspan="4" align="center"><spam style="color:red;"><b>No Tiene sesiones Abiertas</b></spam></td></tr>';
    }
    $('#tablaSesionesActivas >tbody').html(result);
    if (obj.datosFiestas !== undefined) {
        resultFiestas += '<tr>' +
                '<td><input id="fiestaId" class="fiestaId" type="hidden" name="fiestaId" value="' + obj.datosFiestas.id + '">' + obj.datosFiestas.id + '</td>' +
                '<td style="text-align:center;"><i class="text-danger"><b>' + obj.datosFiestas.anfitrion + '</b></i></td>' +
                '<td>' + obj.datosFiestas.fecha + '</td>' +
                '<td style="text-align:center;">' + obj.datosFiestas.detalles + '</td>' +
                '<td><i>' + obj.datosFiestas.servicio + '</i></td>' +
                '<tr>';
    } else {
        resultFiestas = '<tr><td colspan="5" align="center"><spam style="color:red;"><b>No Tiene Fiestas Asignadas</b></spam></td></tr>';
    }
    $('#tablaFiestasEventos >tbody').html(resultFiestas);

}
function guardarDatosCliente() {
    var url = '', tipo = $('#cambioCliente').val();

    if (tipo === 'S') {
        var nombre = $('#nombreCliente').val();
        var apellido = $('#apellidoCliente').val();
        var parametros = {};
        parametros['id'] = $('#idCliente').val(),
                parametros['identificacion'] = ($('#rucCliente').val()) ? $('#rucCliente').val() : $('#pasaporte').val(),
                parametros['tipoDocumento'] = $('#tipoDocumentoCliente').val(),
                parametros['nombres'] = nombre,
                parametros['apellidos'] = apellido,
                parametros['direccion'] = $('#direccionCliente').val(),
                parametros['email'] = $('#correoCliente').val(),
                parametros['telefono1'] = $('#telefonoCliente').val(),
                parametros['telefono2'] = '';
    } else {
        var parametros = {};
        parametros['id'] = $('#IDcliente').val(),
                parametros['identificacion'] = $('#cedula').val(),
                parametros['tipoDocumento'] = $('#tipoDocumento').val(),
                parametros['nombres'] = $('#nombre').val(),
                parametros['apellidos'] = $('#apellido').val(),
                parametros['direccion'] = $('#direccion').val(),
                parametros['email'] = $('#email').val(),
                parametros['telefono1'] = $('#telefono1').val(),
                parametros['telefono2'] = $('#telefono2').val();
    }
    if (parametros['id'] !== '') {
        url = 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=editar';
    } else {
        url = 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=crear';
    }
    $.ajax({
        url: url,
        type: 'POST',
        data: {parametros: parametros},
        success: function (res) {
            var obj = $.parseJSON(res);
            if (obj.insert === true) {
                $.smallBox({
                    title: "Datos del Cliente",
                    content: "<i class='fa fa-clock-o'></i> <i>Los Datos del Cliente Fueron Actualizados con éxito...</i>",
                    color: "#659265",
                    iconSmall: "fa fa-check fa-2x fadeInRight animated",
                    timeout: 4000
                });
                if (tipo === 'S') {
                    $('#idCliente').val(obj.id);
                    var id=obj.id;
                    $.ajax({
                        url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientePorId',
                        type: 'POST',
                        data: {id: id},
                        success: function (res) {
                            var obj = $.parseJSON(res);
                            $('.txtOpcionalInput').hide();
                            $('.txtOpcionalLabel').show();
                            $('#txtNombreCliente').html(obj.datosCliente.nombres + ' ' + obj.datosCliente.apellidos);
                            $('#nombreCliente').val(obj.datosCliente.nombres);
                            $('#apellidoCliente').val(obj.datosCliente.apellidos);
                            $('#txtRucCliente').html(obj.datosCliente.identificacion);
                            $('#rucCliente').val(obj.datosCliente.identificacion);
                            $('#txtDireccionCliente').html(obj.datosCliente.direccion);
                            $('#direccionCliente').val(obj.datosCliente.direccion);
                            $('#txtCorreoCliente').html(obj.datosCliente.email);
                            $('#correoCliente').val(obj.datosCliente.email);
                            $('#txtTelefonoCliente').html(obj.datosCliente.telefono1);
                            $('#telefonoCliente').val(obj.datosCliente.telefono1);
                            $('#cambioCliente').val('N');
                        }
                    });
                }
            }
        }
    });
}
function agregarDatosFacturacion() {

    var sesiones = '', sesionPromo = '', tipo = '';
    var parametros = {};
    parametros['idCliente'] = $('#IDcliente').val();
    parametros['tipoDocumento'] = $('#tipoDocumento').val();
    parametros['cedula'] = $('#cedula').val();
    parametros['nombre'] = $('#nombre').val();
    parametros['apellido'] = $('#apellido').val();
    parametros['direccion'] = $('#direccion').val();
    parametros['email'] = $('#email').val();
    parametros['telefono'] = ($('#telefono1').val() === '') ? $('#telefono2').val() : $('#telefono1').val();
    parametros['fiestas'] = $('#fiestaId').val();
    console.log(parametros);
    var i = 0;
    $("input[name='terms']").each(function (i) {
        if ($(this).is(':checked')) {
            sesiones += (sesiones === '') ? $(this).siblings('.sesionId').val() : ',' + $(this).siblings('.sesionId').val();
            sesionPromo += (sesionPromo === '') ? $(this).parent('td').siblings('td').children('#promocion').val() : ',' + $(this).parent('td').siblings('td').children('#promocion').val();

        }
    });

    if (parametros['cedula'] !== '' && parametros['nombre'] !== '' && parametros['direccion'] !== '' && parametros['telefono'] !== '' && parametros['email'] !== '') {
        guardarDatosCliente();
        if (parametros['cedula'] !== '') {
            if (sesiones !== '') {
                $.SmartMessageBox({
                    title: "Espere..!",
                    content: "Desea generar el pago en modo Fracción o Tarifa?",
                    buttons: '[Fracción][Tarifa]'
                }, function (ButtonPressed) {
                    if (ButtonPressed === "Fracción") {
                        tipo = 'F';
                        $.ajax({
                            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=guardarCabeceraFactura',
                            type: 'POST',
                            data: {parametros: parametros, sesiones: sesiones, sesionPromo: sesionPromo, tipo: tipo},
                            success: function (res) {
                                if (res !== '') {
                                    colocarDatosFactura(res);
                                    $.smallBox({
                                        title: "Sesiones Agregadas",
                                        content: "<i class='fa fa-clock-o'></i> <i>Metodo Fracción...</i>",
                                        color: "#659265",
                                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                        timeout: 4000
                                    });
                                }
                            }
                        });
                    }
                    if (ButtonPressed === "Tarifa") {
                        tipo = 'T';
                        $.ajax({
                            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=guardarCabeceraFactura',
                            type: 'POST',
                            data: {parametros: parametros, sesiones: sesiones, sesionPromo: sesionPromo, tipo: tipo},
                            success: function (res) {
                                if (res !== '') {
                                    colocarDatosFactura(res);
                                    $.smallBox({
                                        title: "Sesiones Agregadas",
                                        content: "<i class='fa fa-clock-o'></i> <i>Metodo Tarifa...</i>",
                                        color: "#659265",
                                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                        timeout: 4000
                                    });
                                }
                            }
                        });
                    }
                });
            } else {
                tipo = 'F';
                console.log(parametros);
                $.ajax({
                    url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=guardarCabeceraFactura',
                    type: 'POST',
                    data: {parametros: parametros, sesiones: sesiones, tipo: tipo},
                    success: function (res) {
                        if (res !== '') {
                            colocarDatosFactura(res);
                            $.smallBox({
                                title: "Productos Agregadas",
                                content: "<i class='fa fa-clock-o'></i> <i>Metodo Fracción...</i>",
                                color: "#659265",
                                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                timeout: 4000
                            });
                        }
                    }
                });
            }
            $('#frmClienteModal').modal('hide');
        } else {
            $.smallBox({
                title: "Cancelado..!!",
                content: "<i class='fa fa-clock-o'></i> <i>No selecciono ningun Cliente...</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 7000
            });
            $('#frmClienteModal').modal('hide');
        }
    } else {
        $.smallBox({
            title: "Error..!!",
            content: "<i class='fa fa-clock-o'></i> <i>Los datos del Cliente deben estar completos, para proceder con la Factura...</i>",
            color: "#C46A69",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 9000
        });
    }



}
function colocarDatosFactura(res) {
    var obj = $.parseJSON(res);
    console.log(obj);
    $('#idCliente').val(obj.datosFactura.id);
    $('#codSesion').val(obj.datosFactura.sesion);
    $('#codInfantes').val(obj.datosFactura.infantes);
    $('#codFiesta').val(obj.datosFactura.fiesta);
    $('#tipoDocumentoCliente option[value="' + obj.datosFactura.tipoDocumento + '"]').attr("selected", true);
    cambioCedulaPasaporteNuevo();
    $('#txtNombreCliente').html(obj.datosFactura.nombre + ' ' + obj.datosFactura.apellido);
    $('#nombreCliente').val(obj.datosFactura.nombre);
    $('#apellidoCliente').val(obj.datosFactura.apellido);
    $('#txtRucCliente').html(obj.datosFactura.cedula);
    $('#rucCliente').val(obj.datosFactura.cedula);
    $("#pasaporte").val(obj.datosFactura.cedula);
    $('#txtDireccionCliente').html(obj.datosFactura.direccion);
    $('#direccionCliente').val(obj.datosFactura.direccion);
    $('#txtCorreoCliente').html(obj.datosFactura.email);
    $('#correoCliente').val(obj.datosFactura.email);
    $('#txtTelefonoCliente').html(obj.datosFactura.telefono);
    $('#telefonoCliente').val(obj.datosFactura.telefono);
    if (obj.sesionesInfantes !== undefined) {
        $.each(obj.sesionesInfantes, function (key, value) {
            var cantidadAnterior = $('.itemFac-' + value.codigo_barras).children('.cantidad').children('label').html();
            var descuentoAnterior = $('.itemFac-' + value.codigo_barras).children('.descuento').children('label').children('spam').html();
            var sesionesAnterior = $('.itemFac-' + value.codigo_barras).children('td').children('.sesionInfantes').val();
            var valorAnterior = $('.itemFac-' + value.codigo_barras).children('.valUnitarioProducto').children('#valorUnitario').val();
            if (cantidadAnterior !== undefined) {
                var nuevaCantidad = parseFloat(cantidadAnterior) + parseInt(value.cantidad);
                var nuevoDescuento = parseFloat(descuentoAnterior) + (parseFloat(value.descuento) / 1.12);
                var nuevoDescuentoTxt = roundNumber(nuevoDescuento, 2);
                $('.itemFac-' + value.codigo_barras).children('.descuento').children('label').children('spam').html(nuevoDescuentoTxt);
                $('.itemFac-' + value.codigo_barras).children('.descuento').children('label').children('#valorDescuento').val(nuevoDescuento);
                $('.itemFac-' + value.codigo_barras).children('.cantidad').children().html(nuevaCantidad);
                var valorNuevo = (parseFloat(nuevaCantidad) * parseFloat(valorAnterior)) - nuevoDescuento;
                if(value.tipo==='F' && value.todo_dia!=='1'){
                    cantidad=nuevaCantidad;
                    var cantidad=cantidad.toString().split('.');
                    var hora=cantidad[0];
                    var minutos=cantidad[1];
                    hora=hora*parseFloat(valorAnterior);
                    minutos=(minutos*parseFloat(valorAnterior))/60;
                    var valorNuevo = (hora + minutos) - nuevoDescuento;
                }
                var valorNuevoTxt = roundNumber(valorNuevo, 2);
                $('.itemFac-' + value.codigo_barras).children('.subtotalProducto').children('label').children('spam').html(valorNuevoTxt);
                $('.itemFac-' + value.codigo_barras).children('.subtotalProducto').children('label').children('#totales').val(valorNuevo);
                calcular();
            } else {
                var vunitario = (parseFloat(value.vunitario) / 1.12);
                var vdescuento = (parseFloat(value.descuento) / 1.12);
                var vtotal = (vunitario * value.cantidad) - vdescuento;
                if(value.tipo==='F' && value.todo_dia!=='1'){
                    var cantidad=value.cantidad.toString().split('.');
                    var hora=cantidad[0];
                    var minutos=cantidad[1];
                    hora=hora*vunitario;
                    minutos=(minutos*vunitario)/60;
                    var vtotal = (hora + minutos) - vdescuento;
                }
                //Revision
                $('#listaUsuarios >tbody').append('<tr class="itemFac-' + value.codigo_barras + '">' +
                        '<td><label class="input codigoProducto txtValoresEnteros"><spam>' + value.codigo_barras + '</spam><input type="hidden" class="productoServicio" name="productoServicio" id="productoServicio" value="' + value.productoServicio + '"></label></td>' +
                        '<td><label class="input"><input type="hidden" class="sesionInfantes" name="sesionInfantes" id="sesionInfantes" value="' + value.sesiones + '">' + value.descripcion + '</label></td>' +
                        '<td class="cantidad txtValoresEnteros" ><label class="input">' + value.cantidad + '</label></td>' +
                        '<td class="descuento txtValoresDecimales"><label class="input"><spam class="pull-left labelDescuento">' + roundNumber(vdescuento, 2) + '</spam><input type="hidden" id="valorDescuento" name="valorDescuento" class="valorDescuento" value="' + vdescuento + '"> <a class="btn btn-xs btn-default" href="javascript:obtenerDescuento(\'' + value.codigo_barras + '\');"><i class="glyphicon glyphicon-pencil"></i></a></label></td>' +
                        '<td class="valUnitarioProducto txtValoresDecimales"><label class="input">' + roundNumber(vunitario, 2) + '</label><input type="hidden" id="valorUnitario" name="valorUnitario" class="valorUnitario" value="' + vunitario + '"></td>' +
                        '<td class="subtotalProducto txtValoresDecimales" style="display: inline-flex;width: 150px;"><label class="input" style="width: 75px; text-align: center; font-weight: bold; font-size: medium;"><spam>' + roundNumber(vtotal, 2) + '</spam><input class="totales" type="hidden" name="totales" id="totales" value="' + parseFloat(vtotal) + '"><input type="hidden" name="ivanoiva" id="ivanoiva" class="ivanoiva" value="' + value.ivanoiva + '"></label>' +
                        '<a class="btn btn-danger btn-circle" href="javascript:eliminarItemFactura(\'' + value.codigo_barras + '\');" style="margin: 0px 0px 0px 36px;">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</a>' +
                        '</td>' +
                        '</tr>');
            }
        });
    }
    if (obj.sesionesProductos !== undefined) {
        $.each(obj.sesionesProductos, function (key, value) {
            var vunitario = (parseFloat(value.vunitario) / 1.12);
            var vtotal = vunitario * value.cantidad;
            $('.itemFac-' + value.codigo_barras).remove();
            $('#listaUsuarios >tbody').append('<tr class="itemFac-' + value.codigo_barras + '">' +
                    '<td><label class="input codigoProducto txtValoresEnteros"><spam>' + value.codigo_barras + '</spam><input type="hidden" class="productoServicio" name="productoServicio" id="productoServicio" value="' + value.productoServicio + '"></label></td>' +
                    '<td><label class="input">' + value.nombre + '</label></td>' +
                    '<td class="cantidad txtValoresEnteros" ><label class="input">' + value.cantidad + '</label></td>' +
                    '<td class="descuento txtValoresDecimales"><label class="input"><spam class="pull-left labelDescuento">0</spam> <a class="btn btn-xs btn-default" href="javascript:obtenerDescuento(\'' + value.codigo_barras + '\');"><i class="glyphicon glyphicon-pencil"></i></a></label></td>' +
                    '<td class="valUnitarioProducto txtValoresDecimales"><label class="input">' + roundNumber(vunitario, 2) + '</label><input type="hidden" id="valorUnitario" name="valorUnitario" class="valorUnitario" value="' + vunitario + '"></td>' +
                    '<td class="subtotalProducto txtValoresDecimales" style="display: inline-flex;width: 150px;"><label class="input" style="width: 75px; text-align: center; font-weight: bold; font-size: medium;"><spam>' + roundNumber(vtotal, 2) + '</spam><input class="totales" type="hidden" name="totales" id="totales" value="' + parseFloat(vtotal) + '"><input type="hidden" name="ivanoiva" id="ivanoiva" class="ivanoiva" value="' + value.ivanoiva + '"></label>' +
                    '<a class="btn btn-danger btn-circle" href="javascript:eliminarItemFactura(\'' + value.codigo_barras + '\');" style="margin: 0px 0px 0px 36px;">' +
                    '<i class="glyphicon glyphicon-remove"></i>' +
                    '</a>' +
                    '</td>' +
                    '</tr>');
        });
    }
    console.log(obj.datosFiestas);
    if (obj.datosFiestas !== undefined) {
        var vunitario = parseFloat(obj.datosFiestas.vunitario);
        var vtotal = parseFloat(obj.datosFiestas.vtotal);
        $('.itemFac-' + obj.datosFiestas.codigo_barras).remove();
        $('#listaUsuarios >tbody').append('<tr class="itemFac-' + obj.datosFiestas.codigo_barras + '">' +
                '<td><label class="input codigoProducto txtValoresEnteros"><spam>' + obj.datosFiestas.codigo_barras + '</spam><input type="hidden" class="productoServicio" name="productoServicio" id="productoServicio" value="' + obj.datosFiestas.productoServicio + '"></label></td>' +
                '<td><label class="input">' + obj.datosFiestas.nombre + '</label></td>' +
                '<td class="cantidad txtValoresEnteros" ><label class="input">' + obj.datosFiestas.cantidad + '</label></td>' +
                '<td class="descuento txtValoresDecimales"><label class="input"><spam class="pull-left labelDescuento">' + roundNumber((obj.datosFiestas.descuento/1.12),2) + '</spam> <a class="btn btn-xs btn-default" href="javascript:obtenerDescuento(\'' + obj.datosFiestas.codigo_barras + '\');"><i class="glyphicon glyphicon-pencil"></i></a></label></td>' +
                '<td class="valUnitarioProducto txtValoresDecimales"><label class="input">' + roundNumber(vunitario, 2) + '</label><input type="hidden" id="valorUnitario" name="valorUnitario" class="valorUnitario" value="' + vunitario + '"></td>' +
                '<td class="subtotalProducto txtValoresDecimales" style="display: inline-flex;width: 150px;"><label class="input" style="width: 75px; text-align: center; font-weight: bold; font-size: medium;"><spam>' + roundNumber(vtotal, 2) + '</spam><input class="totales" type="hidden" name="totales" id="totales" value="' + parseFloat(vtotal) + '"><input type="hidden" name="ivanoiva" id="ivanoiva" class="ivanoiva" value="' + obj.datosFiestas.ivanoiva + '"></label>' +
                '<a class="btn btn-danger btn-circle" href="javascript:eliminarItemFactura(\'' + obj.datosFiestas.codigo_barras + '\');" style="margin: 0px 0px 0px 36px;">' +
                '<i class="glyphicon glyphicon-remove"></i>' +
                '</a>' +
                '</td>' +
                '</tr>');
    }
    if (obj.detallesFiesta !== undefined) {
        $.each(obj.detallesFiesta, function (key, value) {
            var vunitario = parseFloat(value.vunitario);
            var vtotal = vunitario * value.cantidad;
            $('.itemFac-' + value.codigo_barras).remove();
            $('#listaUsuarios >tbody').append('<tr class="itemFac-' + value.codigo_barras + '">' +
                    '<td><label class="input codigoProducto txtValoresEnteros"><spam>' + value.codigo_barras + '</spam><input type="hidden" class="productoServicio" name="productoServicio" id="productoServicio" value="' + value.productoServicio + '"></label></td>' +
                    '<td><label class="input">' + value.nombre + '</label></td>' +
                    '<td class="cantidad txtValoresEnteros" ><label class="input">' + value.cantidad + '</label></td>' +
                    '<td class="descuento txtValoresDecimales"><label class="input"><spam class="pull-left labelDescuento">0</spam> <a class="btn btn-xs btn-default" href="javascript:obtenerDescuento(\'' + value.codigo_barras + '\');"><i class="glyphicon glyphicon-pencil"></i></a></label></td>' +
                    '<td class="valUnitarioProducto txtValoresDecimales"><label class="input">' + roundNumber(vunitario, 2) + '</label><input type="hidden" id="valorUnitario" name="valorUnitario" class="valorUnitario" value="' + vunitario + '"></td>' +
                    '<td class="subtotalProducto txtValoresDecimales" style="display: inline-flex;width: 150px;"><label class="input" style="width: 75px; text-align: center; font-weight: bold; font-size: medium;"><spam>' + roundNumber(vtotal, 2) + '</spam><input class="totales" type="hidden" name="totales" id="totales" value="' + parseFloat(vtotal) + '"><input type="hidden" name="ivanoiva" id="ivanoiva" class="ivanoiva" value="' + value.ivanoiva + '"></label>' +
                    '<a class="btn btn-danger btn-circle" href="javascript:eliminarItemFactura(\'' + value.codigo_barras + '\');" style="margin: 0px 0px 0px 36px;">' +
                    '<i class="glyphicon glyphicon-remove"></i>' +
                    '</a>' +
                    '</td>' +
                    '</tr>');
        });
    }
    limpiarCamposFactura();
    calcular();
}
function cambioCedulaPasaporte() {
    var tipoDocumento = $('#tipoDocumento').val();
    var html = '';
    if (tipoDocumento === 'C') {
        html = '<label class="control-label">Número de Cédula:</label>' +
                '<label class="input"> ' +
                '<i class="icon-prepend fa fa-asterisk"></i>' +
                '<input type="text" class="soloNumero" id="cedula" name="cedula" placeholder="Cedula/Ruc del Cliente">' +
                '</label>';
    } else {
        html = '<label class="control-label">Número de Pasaporte:</label>' +
                '<label class="input"> ' +
                '<i class="icon-prepend fa fa-asterisk"></i>' +
                '<input type="text" class="text-uppercase" id="pasaporte" name="pasaporte" placeholder="Número de Pasaporte">' +
                '</label>';
    }
    $('#cedulaPasaporte').empty();
    $('#cedulaPasaporte').append(html);
    $('.soloNumero').keydown(function (event) {
        validacionNumeros(event);
    });
    parametrizacionInicial();
}
function cambioCedulaPasaporteNuevo() {
    var tipoDocumento = $('#tipoDocumentoCliente').val();
    var html = '';
    if (tipoDocumento === 'C') {
        html = '<label class="input"> ' +
                '<i class="icon-prepend fa fa-asterisk"></i>' +
                '<input type="text" class="soloNumero" id="rucCliente" name="rucCliente" placeholder="Cedula/Ruc del Cliente">' +
                '</label>';
    } else {
        html = '<label class="input"> ' +
                '<i class="icon-prepend fa fa-asterisk"></i>' +
                '<input type="text" class="text-uppercase" id="pasaporte" name="pasaporte" placeholder="Número de Pasaporte">' +
                '</label>';
    }
    $('#cedulaPasaporteNuevo').empty();
    $('#cedulaPasaporteNuevo').append(html);
    parametrizacionInicial();
}
function limpiaFormularioCliente() {
    $('#IDcliente').val('');
    $('#cedula').val('');
    $('#nombre').val('');
    $('#apellido').val('');
    $('#telefono1').val('');
    $('#telefono2').val('');
    $('#email').val('');
    $('#direccion').val('');
    $('#cantidadSesiones').html('0');
    $('#tablaSesionesActivas >tbody').empty();
}