$(document).ready(function () {
    $("#dialog-autoriza").dialog({
        autoOpen: false,
        modal: true,
        title: "Contraseña de Autorización",
        buttons: [{
                html: "Cancel",
                "class": "btn btn-default",
                click: function () {
                    $(this).dialog("close");
                }
            }, {
                html: "<i class='fa fa-check'></i>&nbsp; OK",
                "class": "btn btn-primary",
                click: function () {

                    asignaPermisoCambio();
                }
            }]

    });
    $('.soloNumero').keydown(function (event) {
        validacionNumeros(event);
    });
});

function validacionNumeros(event) {
    if (event.shiftKey) {
        event.preventDefault();
    }
    if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 190 || event.keyCode === 110) {

    }
    else {
        if (event.keyCode < 95) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
        else {
            if (event.keyCode < 96 || event.keyCode > 105) {
                event.preventDefault();
            }
        }
    }
}
function asignaPermisoCambio() {
    var usuario = $('#usuario').val();
    var clave = $('#claveAdmin').val();
    var opcion = $('#token').val();
    var parametro = $('#parametros').val();
    if (usuario !== '' && clave !== '') {
        var parametros = {};
        parametros['usuario'] = usuario;
        parametros['clave'] = clave;
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=seguridad&controlador=login&metodo=autenticacionAdministrador',
            type: 'POST',
            data: {parametros: parametros},
            success: function (res) {
                if (res === 'autorizado') {
                    $.smallBox({
                        title: "Autorizado..!",
                        content: "Usted esta Autorizado para realizar este cambio...",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    $('#dialog-autoriza').dialog("close");
                    $('#usuario').prop('selectedIndex', 0);
                    $('#claveAdmin').val('');
                    procesoAutoriza(opcion, parametro);
                } else {
                    $.smallBox({
                        title: "No Autorizado..!",
                        content: "Datos Mal ingresados, No Autorizado para realizar este cambio...",
                        color: "#C46A69",
                        timeout: 7000,
                        icon: "fa fa-bell swing animated"
                    });
                }
            }
        });
    }
}

function procesoAutoriza(opcion, parametro) {
    console.log('Opcion: ' + opcion);
    console.log('Parametros: ' + parametro);
    switch (opcion) {
        case 'descuentos':
            var param = parametro.split("/");
            $('#codigoProSer').val(param[0]);
            $('#txtCodigoProSer').html(param[0]);
            $('#tipoItem').val(param[1]);
            $('#frmDescuentoModal').modal('show');
            break;
        case 'anularFactura':
            guardarAnulacionFactura(parametro);
            break;
        case 'cancelar_sesiones':
            cancelarSesionGeneral(parametro);
            break;
        case 'cancelar_sesion_infantes':
            cancelarSesionInfanteGeneral(parametro);
            break;
        case 'eliminaItem':
                    $('.itemFac-' + parametro).remove();
                    calcular();
                    $.smallBox({
                        title: "Item Eliminado",
                        content: "<i class='fa fa-clock-o'></i> <i>Item Eliminado Correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
            break;
    }
}

function cancelarSesionInfanteGeneral(parametro) {
    var parametros = {};
    var param = parametro.split(';');
    parametros['infante_id'] = param[0];
    parametros['sesion_id'] = param[1];

    console.log(parametros);
    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=cancelarInfante",
        type: 'post',
        data: {parametros: parametros},
        success: function (res) {

            if (res == 1)
            {
                $.smallBox({
                    title: "Sesión de infante cancelada",
                    color: "#659265",
                    iconSmall: "fa fa-check fa-2x fadeInRight animated",
                    timeout: 4000
                });

                $('#infante_' + parametros['infante_id']).attr('class', 'label label-danger');

                var i = 0;
                $('#infante_' + parametros['infante_id']).parent().find('.label-primary').each(function (a, obj) {
                    i++;
                });

                if (i == 0)
                    location.reload();

                $('#frmGestionarInfante').modal('hide');



            }
        }
    });
}

function cancelarSesionGeneral(id) {
    var parametros = {};

    parametros['id'] = id

    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=cancelar",
        type: 'post',
        data: {parametros: parametros},
        success: function (res) {

            if (res == 1)
            {
                $.smallBox({
                    title: "Sesión cancelada",
                    color: "#659265",
                    iconSmall: "fa fa-check fa-2x fadeInRight animated",
                    timeout: 4000
                });

                location.reload();
            }
        }
    });
}

function guardarAnulacionFactura(codigo) {
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=facturas&controlador=facturas&metodo=anularFactura',
        datetype: "json",
        type: 'POST',
        data: {codigo: codigo},
        success: function (res) {
            if (res === '1') {
                $.smallBox({
                    title: "Anulado..!",
                    content: "Factura Anulada con éxito...",
                    color: "#659265",
                    timeout: 7000,
                    icon: "fa fa-thumbs-up bounce animated"
                });
                recargarFacturacion();
            } else {
                $.smallBox({
                    title: "Error..!",
                    content: "Problas en la Base para Anular...",
                    color: "#C46A69",
                    timeout: 7000,
                    icon: "fa fa-bell swing animated"
                });
            }
        }
    });
}
