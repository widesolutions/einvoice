$(document).ready(function () {
    recargarTipoPago();
    var $registerForm = $("#tipoPagosForm").validate({
        rules: {
            nombre: {required: true},
            valores_adicionales: {required: true}
        }, errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});
function limpiarFormularioPago() {
    $("#IDTipoPago").val('');
    $("#nombre").val('');  /* precio del servicio*/
    $("#observacion").val('');  /*nombre*/
    $('#valores_adicionales').prop('selectedIndex', 0);
}
function nuevoTipoPago() {
    limpiarFormularioPago();
    $('#frmTipoPagoModal').modal('show');
}
function ActualizarTipoPago(id) {
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=tipoPagos&controlador=tipoPagos&metodo=enviarDatosTipoPagos',
        datetype: "json",
        type: 'POST',
        data: {codigoPagos: id},
        success: function (res) {
            var json_obj = $.parseJSON(res);
            carga_DatosIncialesTipoPagos(json_obj);
            $('#frmTipoPagoModal').modal('show');
            $('#TipoPagoForm >header').text('Actualización de Datos Tipo Pagos');
           
        }
        
    });
    recargarTipoPago();
}
function carga_DatosIncialesTipoPagos(edt) {
    $("#IDTipoPago").val(edt.datosTipoPagos.IDTipoPago); //id del servicio
    $("#nombre").val(edt.datosTipoPagos.nombre);  /*precio compra*/
    $("#observacion").val(edt.datosTipoPagos.observacion);  /*cod barras serv*/
    $('#valores_adicionales option[value="' + edt.datosTipoPagos.valores_adicionales + '"]').attr("selected", true);
}
function guardarTipoPagos() {
    var idtipopago = $('#IDTipoPago').val();
    if (idtipopago === '') {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=tipoPagos&controlador=tipoPagos&metodo=guardaDatosPago',
            datetype: "json",
            type: 'POST',
            data: $("#tipoPagosForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $.smallBox({
                        title: "Correcto..!",
                        content: "Tipo Pago Guardado correctamente...",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    recargarTipoPago();
                    $('#frmTipoPagoModal').modal('hide');
                } else {
                    $.smallBox({
                        title: "Error..!",
                        content: "Problemas al Ingresar los Datos, consulte con Sistemas...",
                        color: "#C46A69",
                        timeout: 7000,
                        icon: "fa fa-bell swing animated"
                    });
                }
            }
        });
    } else {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=tipoPagos&controlador=tipoPagos&metodo=ActualizarDatos',
            datetype: "json",
            type: 'POST',
            data: $("#tipoPagosForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $.smallBox({
                        title: "Correcto..!",
                        content: "Tipo Pagos Actualizado correctamente...",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    recargarTipoPago();
                }
            }
        });
    }
}
function EliminarTipoPago(id) {
    $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro de eliminar este tipo de pago?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=tipoPagos&controlador=tipoPagos&metodo=EliminarPago',
                datetype: "json",
                type: 'POST',
                data: {idpago: id},
                success: function (res) {
                    if (res === '1') {
                        $.smallBox({
                            title: "Dato Eliminado",
                            content: "<i class='fa fa-clock-o'></i> <i>Tipo de Pago eliminado correctamente...</i>",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                        recargarTipoPago();
                    } else {
                        $.smallBox({
                            title: "Error..!",
                            content: "Problema para Eliminar el tipo de Pago, consulte con Sistemas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });
                    }
                }
            });
        }
    });
}
function recargarTipoPago() {
    var dtTable = $('#listaTipoPagos').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/tipoPagos/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}
