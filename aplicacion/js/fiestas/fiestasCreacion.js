$(document).ready(function () {
    "use strict";
    var $registerForm = $("#add-event-form").validate({
        rules: {
            eventos: {required: true},
            contratante: {required: true},
            correoContratante: {required: true, email: true},
            telefonoContratante: {required: true, digits: true},
            codigoCliente: {required: true},
            cliente: {required: true},
            servicio: {required: true},
            cantidadInfantes: {required: true, digits: true},
            title: {required: true},
            salon: {required: true}
        }, errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    cargarIniciales();
    $('#external-events > li').each(function () {
        initDrag($(this));
    });
});
function cargarIniciales() {
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestasIngresos&metodo=getEventosPendientes',
        type: 'POST',
        success: function (res) {
            if (res !== '0') {
                var obj = $.parseJSON(res);
                $.each(obj['datos'], function (key, value) {
                    var html = $('<li><span class="' + value.className + '" data-description="' + value.description + '" data-icon="' +
                            value.icon + '">' + value.title + '</span><input type="hidden" id="codigoEvento" name="codigoEvento"  value="' + value.id + '"><input type="hidden" id="tipoEvento" name="tipoEvento"  value="' + value.tipo + '"></li>').prependTo('ul#external-events').hide().fadeIn();
                    initDrag(html);
                });
            }
        }
    });
}
function initDrag(e) {
    var eventObject = {
        title: $.trim(e.children().text()), // use the element's text as the event title
        description: $.trim(e.children('span').attr('data-description')),
        icon: $.trim(e.children('span').attr('data-icon')),
        className: $.trim(e.children('span').attr('class')) // use the element's children as the event class
    };
    e.data('eventObject', eventObject);
    e.draggable({
        zIndex: 999,
        revert: true, // will cause the event to go back to its
        revertDuration: 0 //  original position after the drag
    });
}
function consultarCliente() {
    $('#frmBuscarClienteModal').modal('show');
    $('#llamado').val('1');
}
function nuevoClienteEventos() {
    $('#frmClienteModal').modal('show');
    $('#fromClientes').attr('action', 'javascript:crearClienteEventos()');
}
function agregarEvento(id, title, priority, description, icon,evento) {
    title = title.length === 0 ? "No tiene Titulo" : title;
    description = description.length === 0 ? "Sin Cliente" : description;
    icon = icon.length === 0 ? " " : icon;
    priority = priority.length === 0 ? "label label-default" : priority;
    var tipo=(evento==='evento')?'e':'f';
    var html = $('<li><span class="' + priority + '" data-description="' + description + '" data-icon="' +
                            icon + '">' + title + '</span><input type="hidden" id="codigoEvento" name="codigoEvento"  value="' + id + '"><input type="hidden" id="tipoEvento" name="tipoEvento"  value="' + tipo + '"></li>').prependTo('ul#external-events').hide().fadeIn();
    $("#event-container").effect("highlight", 800);
    initDrag(html);
}
function guardarEvento(title, priority, cliente, icon) {
    var parametros = {};
    parametros['eventos'] = $('input:radio[name=eventos]:checked').val();
    parametros['cliente'] = $('#codigoCliente').val();
    parametros['titulo'] = title;
    parametros['description'] = $('#description').val();
    parametros['color'] = priority;
    parametros['icono'] = icon;
    parametros['contratante'] = $('#contratante').val();
    parametros['correoContratante'] = $('#correoContratante').val();
    parametros['telefonoContratante'] = $('#telefonoContratante').val();
    parametros['servicio'] = $('#servicio').val();
    parametros['cantidadInfantes'] = $('#cantidadInfantes').val();
    parametros['salon'] = $('#salon').val();
    parametros['anticipo'] = $('#anticipo').val();
    parametros['anfitrion'] = $('#anfitrion').val();
    
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestasIngresos&metodo=guardarFiesta',
        type: 'POST',
        data: {parametros: parametros},
        success: function (res) {
            if (res !== '0') {
                var id = res;
                agregarEvento(id, title, priority, cliente, icon,parametros['eventos']);
            }
        }
    });
}
function addEvent() {
    var title = $('#title').val(),
            priority = $('input:radio[name=priority]:checked').val(),
            cliente = $('#cliente').val(),
            icon = ($('input:radio[name=eventos]:checked').val() === 'evento') ? 'fa fa-user' : 'fa fa-clock-o';
    guardarEvento(title, priority, cliente, icon);
}
;