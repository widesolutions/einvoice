$(document).ready(function () {

//    $(".fc-content").mousedown(function (e) 
//    {
//        if (e.which == 3)
//        {
//            alert('click');
//        }
//    })

    $("#detalle_adicional").autocomplete({
        source: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=getProductoxNombre',
        select: function (event, ui) {
            $('#adicional_id').val(ui.item.id);
            $('#adicional_tipo').val(ui.item.productoServicio);
            $('#valor_adicional').val(ui.item.vunitario);
            localStorage.setItem("nombre_producto_calendario", ui.item.value);
        }
    });
    var hdr = {
        left: 'title',
        center: 'month,agendaWeek,agendaDay',
        right: 'prev,today,next'
    };


    $('#hora').clockpicker({
        placement: 'top',
        donetext: 'Done'
    });

    var form = $("#form_adicionales");
    form.validate();
    $("#agregar_adicional").click(function () {

        var adicional_id = $('#adicional_id').val();
        var adicional_tipo = $('#adicional_tipo').val();
        if ($('#detalle_adicional').valid() && $('#cantidad_adicional').valid() && $('#valor_adicional').valid())
        {
            if ($("#" + adicional_id).length > 0) {

                var nueva_cantidad = parseInt($('#cantidad_' + adicional_id).val()) + parseInt($('#cantidad_adicional').val());
                var nuevo_valor = parseInt($('#valor_' + adicional_id).val()) + parseInt($('#valor_adicional').val());

                $('#cantidad_' + adicional_id).val(nueva_cantidad);
                $('#valor_' + adicional_id).val(nuevo_valor);

                $('#detalle_adicional').val('');
                $('#cantidad_adicional').val('');
                $('#valor_adicional').val('');
            }
            else
            {

                var detalle_adicional = $('#detalle_adicional').val();
                var cantidad_adicional = $('#cantidad_adicional').val();
                var valor_adicional = $('#valor_adicional').val();
                var adicional_id = $('#adicional_id').val();
                detalles = '<div class="row"><div class="col-xs-6 col-md-6"><div class="form-group"><input type="text" name="detalle[]" id="detalle_' + adicional_id + '" class="form-control" value="' + detalle_adicional + '" readonly><input type="hidden"  value="' + adicional_id + '" name="adicional_id[]"></div></form></div>';
                cantidad = '<div class="col-xs-2 col-md-2"><div class="form-group"><input type="text" name="cantidad[]" id="cantidad_' + adicional_id + '" class="form-control" value="' + cantidad_adicional + '" readonly><input type="hidden"  value="' + adicional_tipo + '" name="adicional_tipo[]"></div></form></div>';
                valor = '<div class="col-xs-2 col-md-2"><div class="form-group"><input type="text" name="valor[]" id="valor_' + adicional_id + '" class="form-control" value="' + valor_adicional + '" readonly=></div></form></div>';
                eliminar = '<div class="col-xs-1 col-md-2"><div class="form-group"><button type="button" onclick="QuitarAdicional(this)" class="btn btn-warning btn-sm" style="margin-left: -10px;"><span class="glyphicon glyphicon-remove"></span> Eliminar </button></div></div></div>';

                detalle_total = detalles + cantidad + valor + eliminar;

                $('#adicionales').append(detalle_total);

                $('#detalle_adicional').val('');
                $('#cantidad_adicional').val('');
                $('#valor_adicional').val('');
            }

        }




    });


    $('#calendar').fullCalendar({
        header: hdr,
        buttonText: {
            prev: '<i class="fa fa-chevron-left"></i>',
            next: '<i class="fa fa-chevron-right"></i>'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!

        drop: function (date, allDay) { // this function is called when something is dropped


            var date = new Date(date._d);
            var fecha = date.getFullYear() + "-" + verificarNumerosFecha(date.getMonth() + 1) + "-" + verificarNumerosFecha(date.getDate() + 1);

            var parametros = {};

            parametros['id'] = $(this).find('#codigoEvento').val();
            parametros['fecha'] = fecha;
            parametros['tipo'] = $(this).find('#tipoEvento').val();



            $.SmartMessageBox({
                title: "Seleccion de hora",
                content: "Seleccione la hora de la fiesta",
                buttons: "[Aceptar]",
                input: "select",
                options: "[10][11][12][13][14][15][16][17][18][19][20]",
                onImageShow: function () {
                    alert('aa');
                },
            },
                    function (ButtonPress, Value) {

                        minutos_inicio = $('#minutos').val();


                        parametros['hora_inicio'] = Value;
                        parametros['minutos_inicio'] = minutos_inicio;


                        $.ajax({
                            url: "aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestasGestion&metodo=actualizarFiesta",
                            type: 'post',
                            data: {parametros: parametros},
                            success: function (res) {

                                if (res == 1) {
                                    $(".ui-draggable").each(function () {
                                        var id_evento = $(this).find('#codigoEvento').val();

                                        if (id_evento == parametros['id']) {
                                            $(this).remove();
                                        }

                                    });
                                    $("#calendar").fullCalendar("refetchEvents");
                                }
                                else {

                                    $.smallBox({
                                        title: "Ya existe un evento creado para la hora seleccionada!!",
                                        color: "#F78181",
                                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                        timeout: 4000
                                    });
                                }

                            }
                        });
                    });
            options = "";
            for (i = 0; i <= 60; i++)
            {
                if (i < 10)
                    i = '0' + i;

                options += "<option value='" + i + "'  >" + i + "</option>"
            }
            $('.MessageBoxMiddle select').attr('style', 'float:left');
            $('.MessageBoxMiddle select').after('<p style="float:left;color:white;font-size: 20px;">:</p><select name="minutos" id="minutos"  style="float:left;" class="form-control" id="txt1">' + options + '</select>');


        },
        select: function (start, end, allDay) {
            var title = prompt('Event Title:');
            if (title) {
                calendar.fullCalendar('renderEvent', {
                    title: title,
                    start: start,
                    end: end,
                    allDay: allDay
                }, true // make the event "stick"
                        );
            }
            calendar.fullCalendar('unselect');



        },
        events: 'aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestasGestion&metodo=obtenerFiestasActivas',
        eventRender: function (event, element, icon) {

            if (!event.description == "") {
                element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description +
                        "</span>");

            }
            if (!event.icon == "") {
                element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon +
                        " '></i>");
            }

        },
        windowResize: function (event, ui) {
            $('#calendar').fullCalendar('render');
        },
        eventDrop: function (event, delta, revertFunc) {

            var id = $.trim($(this).find('.ultra-light').find('.fiesta_id').html());

            var date = event.start.toDate();
            var fecha = date.getFullYear() + "-" + verificarNumerosFecha(date.getMonth() + 1) + "-" + verificarNumerosFecha(date.getDate());
            console.log(fecha);
            var parametros = {};
            parametros['id'] = id;
            parametros['fecha'] = fecha;
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestasGestion&metodo=cambiarFechaFiesta",
                type: 'post',
                async: false,
                cache: false,
                data: {parametros: parametros},
                success: function (res) {

                    console.log(res);
                    if (res == 1)
                    {
                        $("#calendar").fullCalendar("refetchEvents");
                    }
                    else
                    {
                        $.smallBox({
                            title: "Se genero un errror al insertar en la base de datos.",
                            color: "#F78181",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                        return false;
                    }

                }
            });
        },
        eventClick: function (calEvent, jsEvent, view) {

            var id = $.trim($(this).find('.ultra-light').find('.fiesta_id').html());
            $('#fiesta_id').val(id);
            var parametros = {};
            parametros['id'] = id;
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestas&metodo=getCabeceraFiesta",
                type: 'post',
                data: {parametros: parametros},
                success: function (res) {
                    var obj = $.parseJSON(res);
                    $('#cliente_calendario').val(obj[0].nombres + " " + obj[0].apellidos);
                    $('#anfitrion_calendario').val(obj[0].anfitrion);
                    $('#anticipo_calendario').val(obj[0].anticipo);
                    $('#observacion_calendario').val(obj[0].observaciones);
                    $('#hora').val(obj[0].hora_inicio);
                    $('#numero_infantes').val(obj[0].total_infantes);
                    $('#tema').val(obj[0].tema);
                    $('#salon_calendario option[value="' + obj[0].salon_id + '"]').attr("selected", "selected");
                    $('#servicio_calendario option[value="' + obj[0].servicio_id + '"]').attr("selected", "selected");
                    $('#seccion_impresion').html('<table><tr><td>*** Fiesta: ' + obj[0].anfitrion + '***</td></tr><tr><td>' + obj[0].fecha + ' ' + obj[0].hora_inicio + '</td></tr></table>')
                    var detalle_total = '';
                    $.each(obj['detalles'], function (i, val) {
                        detalles = '<div class="row"><div class="col-md-6"><div class="form-group"><input type="text" name="detalle[]" id="detalle_' + val.adicional_id + '" class="form-control" value="' + val.nombre + '" readonly><input type="hidden"  value="' + val.adicional_id + '" name="adicional_id[]"></div></form></div>';
                        cantidad = '<div class="col-md-2"><div class="form-group"><input type="text" name="cantidad[]" id="cantidad_' + val.adicional_id + '" class="form-control" value="' + val.cantidad + '" readonly><input type="hidden"  value="' + val.tipo + '" name="adicional_tipo[]"></div></form></div>';
                        valor = '<div class="col-md-2"><div class="form-group"><input type="text" name="valor[]" id="valor_' + val.adicional_id + '" class="form-control" value="' + val.valor + '" readonly=></div></form></div>';
                        eliminar = '<div class="col-md-2"><div class="form-group"><button type="button" onclick="QuitarAdicional(this)" class="btn btn-danger btn-sm" style="margin-left: -10px;"><span class="glyphicon glyphicon-remove"></span> Eliminar </button></div></div></div>';

                        detalle_total += detalles + cantidad + valor + eliminar;


                    });

                    $('#adicionales').html(detalle_total);

                }
            });
            $('#frmDetalleFiesta').modal('show');

        }
    });

    /* initialize the calendar
     -----------------------------------------------------------------*/


    /* hide default buttons */
    $('.fc-header-right, .fc-header-center').hide();




    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });


    $('#calendar-buttons #btn-prev').click(function () {
        $('.fc-button-prev').click();
        return false;
    });

    $('#calendar-buttons #btn-next').click(function () {
        $('.fc-button-next').click();
        return false;
    });

    $('#mt').click(function () {
        $('#calendar').fullCalendar('changeView', 'month');
    });

    $('#ag').click(function () {
        $('#calendar').fullCalendar('changeView', 'agendaWeek');
    });

    $('#td').click(function () {
        $('#calendar').fullCalendar('changeView', 'agendaDay');
    });

    $('#btn_enviar').click(function () {

        $('#form_detalles_fiestas').submit();

    });

    $('#form_detalles_fiestas').validate({});
});

function verificarNumerosFecha(numero)
{
    if (numero < 10)
        numero = '0' + numero;

    return numero;
}


function QuitarAdicional(elemento)
{
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea eliminar el adicional de la fiesta?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Si") {
            $(elemento).parent().parent().parent().remove();
        }
    });

}

function guardarDetallesFiesta()
{
    var adicional_id = new Array();
    var adicional_tipo = new Array();
    var cantidad = new Array();
    var valor = new Array();

    $("input[name='adicional_id\\[\\]']").each(function (index, element) {
        adicional_id.push($(element).val());
    });
    $("input[name='cantidad\\[\\]']").each(function (index, element) {
        cantidad.push($(element).val());
    });
    $("input[name='valor\\[\\]']").each(function (index, element) {
        valor.push($(element).val());
    });
    $("input[name='adicional_tipo\\[\\]']").each(function (index, element) {
        adicional_tipo.push($(element).val());
    });

    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestas&metodo=actualizarFiesta",
        type: 'post',
        cache: false,
        data: {parametros: $('#form_detalles_fiestas').serializeArray(), adicional_id: JSON.stringify(adicional_id), cantidad: JSON.stringify(cantidad), valor: JSON.stringify(valor), adicional_tipo: JSON.stringify(adicional_tipo)},
        success: function (res) {

                $.smallBox({
                    title: "Actualización",
                    content: "Fiesta Actualizada correctamente!",
                    color: "#659265",
                    timeout: 7000,
                    icon: "fa fa-thumbs-up bounce animated"
                });
                $('#frmDetalleFiesta').modal('hide');


        }
    });
}



function finalizarFiesta()
{

    var parametros = {};
    parametros['id'] = $('#fiesta_id').val();
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea finalizar la fiesta?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        $.ajax({
            url: "aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestas&metodo=finalizarFiesta",
            type: 'post',
            cache: false,
            data: {parametros: parametros},
            success: function (res) {

                if (res == 1)
                {
                    $.smallBox({
                        title: "Finalización",
                        content: "Fiesta finalizada correctamente!",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    $('#frmDetalleFiesta').modal('hide');
                    $("#calendar").fullCalendar("refetchEvents");
                }
            }
        });
    });


}

function CancelarFiesta()
{

    var parametros = {};
    parametros['id'] = $('#fiesta_id').val();
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea cancelar la fiesta?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        $.ajax({
            url: "aplicacion/rutasMetodos.php?modulo=fiestas&controlador=fiestas&metodo=cancelarFiesta",
            type: 'post',
            cache: false,
            data: {parametros: parametros},
            success: function (res) {

                if (res == 1)
                {
                    $.smallBox({
                        title: "Cancelación",
                        content: "Fiesta cancelada correctamente!",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    $('#frmDetalleFiesta').modal('hide');
                    $("#calendar").fullCalendar("refetchEvents");
                }
            }
        });
    });


}

function imprimirBrazaletes()
{
    $("#seccion_impresion").print({
        globalStyles: true,
        mediaPrint: true,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
}