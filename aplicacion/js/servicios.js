$(document).ready(function () {
    recargarServicio();
    $.validator.addMethod("comparacionprecios", function (value, element, param) {
        var $precio_venta = $(param);
        return parseFloat(value) > parseFloat($precio_venta.val());
    });
    var $registerForm = $("#servicioForm").validate({
        rules: {
            descripcion: {required: true,minlength: 3},
            iva: {required: true},
            precio_compra: {required: true,number: true},
            precio_venta: {comparacionprecios: '#precio_compra',required: true,number: true},
            codigo_barras:{required: true,minlength: 3,alphanumeric: true},
            todo_dia:{required: true}
        },
        messages: {
            descripcion: {required: "la descripcion es necesaria", minlength: "debe ingresar mas de 3 caracteres"},
            precio_compra: {required: "Es necesario ingresar el precio compra"},
            codigo_barras: {alphanumeric: "No caracteres especiales"},
            precio_venta: {required: "Es necesario ingresar el precio venta", comparacionprecios: "no puede ser menor o igual al precio compra"},
            todo_dia: {required: "Es necesario ingresar el dia"},
            iva: {required: "debe ingresar el iva"}
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});
function limpiarFormularioServicio(){
    $('#IDServicio').val('');
    $("#precio_compra").val('');  /*precio compra*/
    $("#precio_venta").val('');  /* precio venta*/
    $("#descripcion").val('');  /*nombre*/
    $("#codigo_barras").val('');  /*codigo de barras*/
    $("#minimo").val('');  /*codigo de barras*/
    $('#todo_dia').prop('selectedIndex', 0);/*Tipo de Usuario*/
    $('#iva').prop('selectedIndex', 0);/*Centro*/
}
function guardarServicio() {
    var idserv = $('#IDServicio').val();
    if (idserv === ''){
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=guardaDatosServicio',
            datetype: "json",
            type: 'POST',
            data: $("#servicioForm").serialize(),
            success: function (res){
                if (res === '1'){
                    $('#frmServicioModal').modal('hide');
                    $.smallBox({
                        title: "Servicio Guardado",
                        content: "<i class='fa fa-clock-o'></i> <i>Servicio Guardado correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    recargarServicio();
                }else {
                    if(res === '2'){
                        $.smallBox({
                        title: "Error..!",
                        content: "Servicio Duplicado, ya existe en la base de datos...",
                        color: "#C46A69",
                        timeout: 7000,
                        icon: "fa fa-bell swing animated"
                    });
                    }else{
                    $.smallBox({
                        title: "Error..!",
                        content: "Problema para Almacenar el Producto, consulte con Sistemas...",
                        color: "#C46A69",
                        timeout: 7000,
                        icon: "fa fa-bell swing animated"
                    });
                }
            }
            }
        });
        $('#frmServicioModal').modal('hide');
    }else{
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=ActualizarDatos',
            datetype: "json",
            type: 'POST',
            data: $("#servicioForm").serialize(),
            success: function (res)
            {
                if (res === '1')
                {
                    $.smallBox({
                        title: "Datos Actualizados",
                        content: "<i class='fa fa-clock-o'></i> <i>Servicio Actualizado correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    $('#frmServicioModal').modal('hide');
                    recargarServicio();
                }
            }
        });
    }
}
function carga_DatosIncialesServicio(edt) {
    $("#IDServicio").val(edt.datosServicios.IDServicio); //id del servicio
    $("#precio_compra").val(edt.datosServicios.precio_compra);  /*precio compra*/
    $("#precio_venta").val(edt.datosServicios.precio_venta);  /*precio servicio*/
    $("#codigo_barras").val(edt.datosServicios.codigo_barras);  /*cod barras serv*/
    $("#todo_dia").val(edt.datosServicios.todo_dia);  /*todo dia*/
    $("#descripcion").val(edt.datosServicios.descripcion);  /*nombre*/
    $("#minimo").val(edt.datosServicios.minimo);  /*nombre*/
    $('#iva').prop('selectedIndex', edt.datosServicios.iva); /*datos servicio*/
}
function nuevoServicio(){
    limpiarFormularioServicio();
    $('#frmServicioModal').modal('show');
}
function ActualizarServicio(id){
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=enviarDatosServicios',
        datetype: "json",
        type: 'POST',
        data: {codigoServicio: id},
        success: function (res) {
            var json_obj = $.parseJSON(res);
            // alert("mensaje de"+res);
            //var json_obj = $.parseJSON(res);


            limpiarFormularioServicio();
            carga_DatosIncialesServicio(json_obj);
            $('#frmServicioModal').modal('show');
            $('#servicioForm >header').text('Actualización de Datos Servicios');
            recargarServicio();

        }
    });
}

// funcion para eliminar un producto seleccionado
function EliminarServicio(id, nombre) {
    $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro que desea eliminar el Servicio <span class='txt-color-orangeDark'><strong>" + nombre + " </strong></span> ?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            var idserv = $('#IDServicio').val();
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=EliminarServicio',
                datetype: "json",
                type: 'POST',
                data: {idserv: id},
                success: function (res){
                    if (res === '1'){
                        $.smallBox({
                            title: "Servicio Eliminado",
                            content: "<i class='fa fa-clock-o'></i> <i>Servicio eliminado correctamente...</i>",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                        recargarServicio();
                    }
                }
            });
        }
    });
}

function recargarServicio() {
    var dtTable = $('#listaServicios').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/servicios/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}
