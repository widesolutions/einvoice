

// PAGE RELATED SCRIPTS

$(document).ready(function () {

    generarReporte();

    pageSetUp();

    $('#fecha_inicio').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'
    });

    $('#fecha_fin').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'
    });


    $("#generar").click(function () {
        generarReporte();
    });





});


function generarReporte()
{
    var parametros = {};
    var datos = {}
    parametros['fecha_inicio'] = $('#fecha_inicio').val();
    parametros['fecha_fin'] = $('#fecha_fin').val();
    
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=serviciosPorEdades',
        datetype: "json",
        type: 'POST',
        async: false,
        data : $("#fechas").serialize(),
        success: function (res) {

            datos = res;
        }
    });

    datos1 = $.parseJSON(datos);

    if ($('#normal-bar-graph').length) {

        Morris.Bar({
            element: 'normal-bar-graph',
            data: datos1,
            xkey: 'x',
            ykeys: ['3', '4'],
            labels: ['Zona Bebe', 'Zona Nino']
        });

    }
}
