

// PAGE RELATED SCRIPTS

$(document).ready(function () {

    generarReporte();

    pageSetUp();

    $('#fecha_inicio').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'
    });

    $('#fecha_fin').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'
    });


    $("#generar").click(function () {
        generarReporte();
    });





});


function generarReporte()
{
    var parametros = {};
    var datos = {}
    parametros['fecha_inicio'] = $('#fecha_inicio').val();
    parametros['fecha_fin'] = $('#fecha_fin').val();

    $.ajax({
        url: 'http://localhost/krayon/aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=consumoDeServicios',
        datetype: "json",
        type: 'POST',
        async: false,
        data: $("#fechas").serialize(),
        success: function (res) {

            datos = res;
        }
    });

    datos1 = $.parseJSON(datos);
    console.log(datos);
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - types of animation
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - Re-draw chart on page resize
        responsive: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };

    var pieData = datos1;
    

    // render chart
    var ctx = document.getElementById("pieChart").getContext("2d");
    var myNewChart = new Chart(ctx).Pie(pieData, pieOptions);
}
