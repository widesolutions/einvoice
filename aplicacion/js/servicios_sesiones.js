$(document).ready(function () {
    mostrarSesiones();

    $("#servicio").autocomplete({
        source: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=getProductoxNombre',
        select: function (event, ui) {
            $('#servicio_id').val(ui.item.id);
        }
    });

    var $registerForm = $("#servicioForm").validate({
        rules: {
            descripcion: {required: true, minlength: 3},
            iva: {required: true},
            precio_compra: {required: true, number: true},
            precio_venta: {comparacionprecios: '#precio_compra', required: true, number: true},
            codigo_barras: {required: true, minlength: 3, alphanumeric: true},
            todo_dia: {required: true}
        },
        messages: {
            descripcion: {required: "la descripcion es necesaria", minlength: "debe ingresar mas de 3 caracteres"},
            precio_compra: {required: "Es necesario ingresar el precio compra"},
            codigo_barras: {alphanumeric: "No caracteres especiales"},
            precio_venta: {required: "Es necesario ingresar el precio venta", comparacionprecios: "no puede ser menor o igual al precio compra"},
            todo_dia: {required: "Es necesario ingresar el dia"},
            iva: {required: "debe ingresar el iva"}
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});





function mostrarSesiones()
{
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=getSesionesServicios',
        datetype: "json",
        type: 'POST',
        success: function (res) {
            var obj = $.parseJSON(res);

            var tabla = "";
            $.each(obj, function (index, value) {

                tabla += "<tr>";
                tabla += "<td class='id_servicios_sesiones'>" + value.id + "</td>";
                tabla += "<td>" + value.nombre + "</td>";
                tabla += "<td>" + value.tiempo_tolerancia + " minutos</td>";
                tabla += "<td>" + diaLista(value.dia) + "</td>";
                tabla += "<td><a class='btn btn-default' title='Actualizar Servicio' href='javascript:actualizarServicio(" + value.id_servicio_sesiones + ")' ><i class='fa fa-pencil'></i></a>"
                        + "  <a class='btn btn-default title='Eliminar Servicio' href='javascript:actualizarServicio()'><i class='fa fa-remove'></i></a></td>";
                tabla += "</tr>";
            });

            $('#listaServicios tbody').append(tabla);
        }
    });
}

var dias = ['Lunes', 'Martes', 'Miercoles', 'jueves', 'Viernes', 'Sabado', 'Domingo', 'Todos los días'];

function diaAplicacion(dia)
{
    console.log(dia);
    var opciones = "";
    for (var i = 0; i <= dias.length - 1; i++)
    {
        if (i == dia)
            selected = "selected";
        else
            selected = "";

        opciones += "<option value=" + i + " " + selected + ">" + dias[i] + "</option>";
    }

    $('#dia').html(opciones);

}

function diaLista(dia)
{
    return dias[dia];
}

function actualizarServicio(id)
{
    $('#frmServiciosModal').modal('show');
    $('#servicio_sesiones').val(id);
    getDatosServicio(id);

}

function crearServicio()
{
    $('#frmServiciosCrearModal').modal('show');

}

function getDatosServicio(id)
{
    var parametros = {};
    parametros['id'] = id;
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=getDatosServicio',
        datetype: "json",
        type: 'POST',
        data: {parametros: parametros},
        success: function (res) {
            var obj = $.parseJSON(res);

            $('#tiempo_tolerancia').val(obj.tiempo_tolerancia);
            diaAplicacion(obj.dia);

        }
    });
}

function editarRegistro()
{
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=editarDatosServicioSesion',
        datetype: "json",
        type: 'POST',
        data: $('#servicioEditarForm').serialize(),
        success: function (res) {

            if (res == 1)
            {
                $.smallBox({
                    title: "Editado",
                    content: "Parametros de servicio editados correctamente!",
                    color: "#659265",
                    timeout: 7000,
                    icon: "fa fa-thumbs-up bounce animated"
                });

                location.reload();
            }
        }
    });
}

function storeRegistro()
{
    var j = 0;
    $('.id_servicios_sesiones').each(function (i, obj) {
        
        if ( parseInt($.trim($(obj).html())) === parseInt($.trim($('#servicio_id').val())))
        {
            j++;
        }
        
    });

    if (j == 0)
    {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=crearDatosServicioSesion',
            datetype: "json",
            type: 'POST',
            data: $('#servicioCrearForm').serialize(),
            success: function (res) {

                if (res == 1)
                {
                    $.smallBox({
                        title: "Creado",
                        content: "servicio de sesiones agregado correctamente!",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });

                    location.reload();
                }
            }
        });
    }
}