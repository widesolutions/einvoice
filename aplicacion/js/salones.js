/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    recargarSalon();
    
    //metodo para comparar precios de compra y venta.
    //recargarServicio();
    var $registerForm=$("#salonForm").validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            capacidad : {
                number: true
            }
        },
        messages:{
            nombre: { required:"la descripcion es necesaria", minlength :"debe ingresar mas de 3 caracteres"    },
            capacidad: { required: "Es necesario ingresar el precio compra" },
            ubicacion: { alphanumeric:"No caracteres especiales"}
              },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        }
    });
});
function eliminarSalon(id)
{
   $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro que desea eliminar el Salon <span class='txt-color-orangeDark'><strong> </strong></span> ?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=salones&controlador=salones&metodo=EliminarSalon',
                datetype: "json",
                type: 'POST',
                data: {idsalon: id},
                success: function (res) {
                    if (res === '1') {
                        $.smallBox({
                            title: "Dato Eliminado..!",
                            content: "Salon eliminado correctamente...",
                            color: "#659265",
                            timeout: 4000,
                            icon: "fa fa-thumbs-up bounce animated"
                        });
                        location.reload();
                        //recargarProducto();
                    }else {
                        $.smallBox({
                            title: "Problema para Eliminar..!",
                            content: "Existe algun problema con la operación, contacte a sistemas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });location.reload();
                    }
                }
            });
        }
    });
    
}
function nuevoSalon()
{
    limpiarFormularioSalon();
     $('#frmSalonModal').modal('show');
}
function guardarSalon()
{
    //alert('alertaguardar');
    var idserv = $('#IDSalon').val();
   if ( idserv==='') 
   {         
   $.ajax({
       
       url: 'aplicacion/rutasMetodos.php?modulo=salones&controlador=salones&metodo=guardaDatosSalones',       
       datetype: "json",
       type:'POST',
       data:$("#salonForm").serialize(),
       success: function(res)
       {
           //console.log(res);
           //alert("");
           //alert("datos "+res);           
            if (res==='1') 
            {
                $.smallBox({
                        title: "Dato Guardado",
                        content: "<i class='fa fa-clock-o'></i> <i>Salon Guardado correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    limpiarFormularioSalon();
                    $('#listaSalon').modal('hide');
            }
            else
            {
                alert("problemas al ingresar datos..");
            } 
       }       
        });
        location.reload();
         $('#salonForm').modal('hide');        
    }
    else
    {
        $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=salones&controlador=salones&metodo=ActualizarSalon',
       datetype: "json",
       type:'POST',
       data:$("#salonForm").serialize(),
       success: function(res)
       {
                if (res==='1')
                {
                  $.smallBox({
                        title: "Datos Actualizados",
                        content: "<i class='fa fa-clock-o'></i> <i>Salon Actualizado correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                        
                    });  
                }        location.reload(); 
       }     
       });   limpiarFormularioSalon();  
       $('#salonForm').modal('hide'); 
    }
    
}
function carga_DatosIncialesSalon(edt) {
    $("#IDSalon").val(edt.datosSalon.IDSalon); //id del servicio
    $("#nombre").val(edt.datosSalon.nombre);  /*precio compra*/    
    $("#capacidad").val(edt.datosSalon.capacidad);  /*precio servicio*/    
    $("#ubicacion").val(edt.datosSalon.ubicacion);  /*cod barras serv*/    
}
function actualizarSalon(id)
{
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=salones&controlador=salones&metodo=enviarDatosSalones',
        datetype: "json",
        type: 'POST',
        data: {codigoSalon: id},
        success: function(res) {
            var json_obj = $.parseJSON(res);
            limpiarFormularioSalon();
            carga_DatosIncialesSalon(json_obj);
            $('#frmSalonModal').modal('show');
            $('#salonForm >header').text('Actualización de Datos Salon');
                
        }
    });
}
 function limpiarFormularioSalon()
{
    $("#nombre").val('');  /* precio del servicio*/
    $("#capacidad").val('');  /*nombre*/
    $("#ubicacion").val('');/*iva*/
}
function recargarSalon() {
    var dtTable = $('#listaSalon').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    
        //dtTable.fnReloadAjax();
}

