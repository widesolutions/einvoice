$(document).ready(function () {

    $("#nombre_adicional").autocomplete({
        source: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=getProductoxNombre',
        select: function (event, ui) {
            $('#id_adicional').val(ui.item.id);
            localStorage.setItem("nombre_producto", ui.item.value);

        }
    });
    pageSetUp();
    getServicios();
    $('#servicios_infantes').select2();
    //Bootstrap Wizard Validations


    var $validator = $("#wizard-1").validate({
        rules: {
            cliente_nombre: {
                required: true
            },
            cliente_identificacion: {
                required: true
            }

        },
        messages: {
            cliente_nombre: "Información Requerida",
            cliente_identificacion: "Información Requerida",
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    tabla_clientes = $('#tabla_detalles').html();
    tabla_infantes = $('#tabla_detalles_infantes').html();
    tabla_historial = $('#tabla_detalles_historial').html();
    $('#bootstrap-wizard-1').bootstrapWizard({
        'tabClass': 'form-wizard',
        'onNext': function (tab, navigation, index) {
            var $valid = $("#wizard-1").valid();
            if (!$valid) {
                $validator.focusInvalid();
                return false;
            } else {
                $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                        'complete');
                $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                        .html('<i class="fa fa-check"></i>');
            }

            if (index == 4)
            {
                var nombre_cliente = $('#cliente_nombre').val();
                var tabla_infantes = $('#lista_infantes tbody').html();
                if (nombre_cliente === "" || tabla_infantes.length === 0)
                {
                    $.smallBox({
                        title: (nombre_cliente == "") ? "Ingrese la información del cliente" : "" + "<br>" + (tabla_infantes == "") ? "Ingrese los infantes" : "",
                        color: "#F78181",
                        iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                        timeout: 4000
                    });
                    return false;
                }
                $.SmartMessageBox({
                    title: "Alerta!",
                    content: "Esta seguro que desea iniciar la sesión?",
                    buttons: '[No][Si]'
                }, function (ButtonPressed) {

                    if (ButtonPressed === "Si") {
                        $.ajax({
                            url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=crear",
                            type: 'post',
                            data: $("#wizard-1").serialize(),
                            success: function (data) {
                                res = $.parseJSON(data);
                                if (res['insert'] == 1)
                                {
                                    $('#bot2-Msg1').attr('disabled', 'disabled');
                                    var hora = res['hora'];
                                    $('#titulo_completo').attr('style', 'display:none');
                                    $('#mensaje_completo').attr('style', 'display:none');
                                    $('#paso4').html('Impresión de las manillas');
                                    $('#lista_codigos').show();
                                    $("li[data-target='#step1']").children('a').attr('href', '#');
                                    $("li[data-target='#step2']").children('a').attr('href', '#');
                                    $("li[data-target='#step3']").children('a').attr('href', '#');
                                    $("li[data-target='#step4']").children('a').attr('href', '#');
                                    $("a[data-toggle='tab']").attr('data-toggle', '');
                                    $('.previous').hide();
                                    $('.next').hide();
                                    $("#codigos").append("<tr><td id='p_code" + $('#cliente_id').val() + "'></td><td class='danger' id='p_nombre_cod" + $('#cliente_id').val() + "'></td><td class='danger'><a href='javascript:imprimirManilla(" + $('#cliente_id').val() + ");' class='btn btn-labeled btn-primary'> <span class='btn-label'><i class='fa fa-print'></i></span>Imprimir </a></td></tr>");
                                    $("#p_code" + $('#cliente_id').val()).barcode($('#cliente_id').val(), "codabar");
                                    $("#p_nombre_cod" + $('#cliente_id').val()).append($('#cliente_nombre').val());
                                    var tabla = "";
                                    $('.infante_id').each(function (index, obj) {

                                        var index = $(obj).children().attr('alt');
                                        tabla = "<div class='manillas'  id='cod_imp" + index + "'><div class='rp' id='bar_imp" + index + "'></div><div class='rp'>" + $(obj).next().html() + "</div></div>";

                                        $("#impresiones").append(tabla);
                                        $("#codigos").append("<tr><td id='code" + index + "'></td><td class='success' id='nombre_cod" + index + "'></td><td class='success'><a href='javascript:imprimirManilla(" + index + ");' class='btn btn-labeled btn-primary'> <span class='btn-label'><i class='fa fa-print'></i></span>Imprimir </a></td></tr>");
                                        $("#code" + index).barcode(index, "codabar");
                                        $("#bar_imp" + index).barcode(index, "codabar");
                                        $("#bar_imp" + index).append(hora);
                                        $("#nombre_cod" + index).append($(obj).next().html());

                                    });

                                    var padre = "<div class='manillas'  id='cod_imp" + $('#cliente_id').val() + "'><div class='rp' id='bar_imp_p" + $('#cliente_id').val() + "'></div><div class='rp'>" + $('#cliente_nombre').val() + "</div></div>";
                                    $("#impresiones").append(padre);
                                    $("#bar_imp_p" + $('#cliente_id').val()).barcode($('#cliente_id').val(), "codabar");
                                    $("#bar_imp_p" + $('#cliente_id').val()).append(hora);
                                }
                                else
                                {
                                    $.smallBox({
                                        title: res,
                                        color: "#F78181",
                                        iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                                        timeout: 4000
                                    });
                                }
                            },
                            error: function (xhr, status, error) {

                                $.smallBox({
                                    title: xhr.responseText,
                                    color: "#F78181",
                                    iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                                    timeout: 4000
                                });
                            }
                        });


                    }
                });
            }

        }
    });
});

function imprimirManilla(id)
{
    $("#cod_imp" + id).print({
        globalStyles: true,
        mediaPrint: true,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
}

function imprimirTodasManillas()
{

    $(".manillas").each(function () {
        
        $(this).print({
            globalStyles: true,
            mediaPrint: true,
            stylesheet: null,
            noPrintSelector: ".no-print",
            iframe: true,
            append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred()
        });

    });
}

var tabla_clientes;
var tabla_infantes;
function nuevoCliente()
{
    $('#frmClienteModal').modal('show');
    $('#fromClientes').attr('action', 'javascript:crearCliente(1)');
}


function nuevoInfante()
{
    $('#frmInfanteModal').modal('show');
    $('#form_infantes').attr('action', 'javascript:crearInfante(1)');
}

function buscarInfante()
{
    $('#frmBuscarInfanteModal').modal('show');
}


function quitarInfante(element)
{
    $(element).parent().parent().remove();
}

function quitarAdicional(element)
{
    $(element).parent().parent().remove();
}


function agregarInfante()
{

    var nombre_infante = $('#nombre_infante').val();
    var id_infante = $('#id_infante').val();
    var nombre_servicio = $('#select2-chosen-1').html();
    var id_servicio = $('#servicios_infantes').val();
    var parametros = {};
    var mensaje_error = "";
    parametros['id'] = id_infante;
    var activo = false;
    if (nombre_infante != "" && nombre_servicio != "&nbsp;")
    {
        $.ajax({
            url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=verificarInfanteActivo",
            type: 'post',
            async: false,
            data: {parametros: parametros},
            success: function (res) {
                if (res != 0)
                {
                    activo = true;
                    mensaje_error += "El infante ya tiene una sesiòn activa"
                }
            }
        });

        var i = 0;
        $('.lista_infantes').each(function (a, obj) {

            if ($(obj).val() == id_infante)
                i++;
        });

        if (i === 0 && activo === false)
        {
            var parametros = {};
            parametros['id'] = $('#id_infante').val();
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=infante&controlador=infante&metodo=getInfante",
                type: 'post',
                data: {parametros: parametros},
                success: function (res) {
                    respuesta = $.parseJSON(res);
                    var tabla = "<tr>";
                    tabla += "<td class='infante_id' >" + respuesta['id'] + "<input name='infantes_id[]' id='infantes_id[]' class='lista_infantes'  value='" + id_infante + "' alt='" + id_infante + "' type='hidden'><input name='servicios_infantes_id[]' value='" + id_servicio + "' type='hidden'></td>";
                    tabla += "<td class='infante_nombre'>" + respuesta['primer_nombre'] + " " + respuesta['primer_apellido'] + "</td>";
                    tabla += "<td>" + respuesta['fecha_nacimiento'] + "</td>";
                    tabla += "<td>" + nombre_servicio + "</td>";
                    tabla += '<td><a class="btn btn-danger" onclick="javascript:quitarInfante(this)" ><i class="fa fa-remove"></i> Quitar</a></td>';
                    $('#lista_infantes tbody').append(tabla);
                    $('#nombre_infante').val('');
                    $('#id_infante').val('');
                    $('#editar_infante').attr('style', 'background: #eee;color: black;cursor:pointer;');
                }
            });
        }
        else
        {
            if (i > 0)
                mensaje_error += "El infante ya esta agregado";

            $.smallBox({
                title: mensaje_error,
                color: "#F78181",
                iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                timeout: 4000
            });
        }
    }
    else
    {
        $.smallBox({
            title: "Debe seleccionar el infante y el servicio antes de agregar    ",
            color: "#F78181",
            iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
            timeout: 4000
        });
    }
}

function buscarCliente()
{

    $('#frmBuscarClienteModal').modal('show');
    $('#llamado').val('0');

}

function buscarInfante()
{

    $('#frmBuscarInfanteModal').modal('show');
}

function searchClientes(name){
    var search = $('#search_cliente').val();
    var llamado = $('#llamado').val();
    if (llamado == '0') {
        $.ajax({
            type: "POST",
            url: "aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=searchCliente",
            data: {search: search}

        }).done(function (data) {
            $('#tabla_detalles').html(tabla_clientes);
            if (search != "")
            {
                var obj = $.parseJSON(data);
                var tabla = "";
                $.each(obj, function (index, value) {
                    tabla += "<tr>";
                    tabla += "    <td>" + value.identificacion + "</td>";
                    tabla += "    <td>" + value.nombres + " " + value.apellidos + "</td>";
                    tabla += "    <td><button onclick='javascript:seleccionarCliente(\"" + $.trim(value.nombres + " " + value.apellidos) + "\",\"" + $.trim(value.identificacion) + "\"," + value.id + ")' type='button' class='btn btn-success'><i class='fa fa-check-circle' style='cursor:pointer;' ></i></button></td>";
                    tabla += "</tr>";
                });
                if ($.trim(data))
                {
                    $('#tabla_detalles').append(tabla);
                }
            }

        });
    } else {
        $.ajax({
            type: "POST",
            url: "aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=searchCliente",
            data: {search: search}

        }).done(function (data) {
            $('#tabla_detalles').html(tabla_clientes);
            if (search != "")
            {
                var obj = $.parseJSON(data);
                var tabla = "";
                $.each(obj, function (index, value) {
                    tabla += "<tr>";
                    tabla += "    <td>" + value.identificacion + "</td>";
                    tabla += "    <td>" + value.nombres + " " + value.apellidos + "</td>";
                    tabla += "    <td><button onclick='javascript:seleccionarClienteEventos(\"" + $.trim(value.nombres + " " + value.apellidos) + "\",\"" + $.trim(value.identificacion) + "\"," + value.id + ")' type='button' class='btn btn-success'><i class='fa fa-check-circle' style='cursor:pointer;' ></i></button></td>";
                    tabla += "</tr>";
                });
                if ($.trim(data))
                {
                    $('#tabla_detalles').append(tabla);
                }
            }

        });
    }
}

function seleccionarCliente(nombre, identificacion, id)
{
    var parametros = {};
    parametros['id'] = id;
    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=verificarClienteActivo",
        type: 'post',
        async: false,
        data: {parametros: parametros},
        success: function (res) {
            if (res != 0)
            {
                $.SmartMessageBox({
                    title: "Alerta!",
                    content: "El cliente ya tiene una sesion activa.",
                    buttons: '[Cancelar][Continuar]'
                }, function (ButtonPressed) {

                    if (ButtonPressed === "Continuar") {

                        window.location = "?modulo=sesion&vista=index&opMenu=17&redirect=" + identificacion;

                    }
                });
            }
            else
            {
                $('#cliente_nombre').val(nombre);
                $('#cliente_identificacion').val(identificacion);
                $('#cliente_id').val(id);
                $('#frmBuscarClienteModal').modal('hide');
                $('#editar_cliente').attr('style', 'background: #739e73;color: white;cursor:pointer;');
            }
        }
    });


}

function seleccionarClienteEventos(nombre, identificacion, id) {
    var parametros = {};
    parametros['id'] = id;
    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=verificarClienteActivo",
        type: 'post',
        async: false,
        data: {parametros: parametros},
        success: function (res) {
            $('#cliente').val(nombre);
            $('#codigoCliente').val(id);
            $('#frmBuscarClienteModal').modal('hide');
        }
    });
}

function searchInfantes(name)
{
    var search = $('#search_infante').val();
    $.ajax({
        type: "POST",
        url: "aplicacion/rutasMetodos.php?modulo=infante&controlador=infante&metodo=searchInfante",
        data: {search_infante: search}

    })

            .done(function (data) {
                $('#tabla_detalles_infantes').html(tabla_infantes);
                if (search != "")
                {
                    var obj = $.parseJSON(data);
                    var tabla = "";
                    $.each(obj, function (index, value) {
                        var nombre = value.primer_nombre + " " + value.segundo_nombre + " " + value.primer_apellido + " " + value.segundo_apellido;
                        tabla += "<tr>";
                        tabla += "    <td>" + value.id + "</td>";
                        tabla += "    <td>" + nombre + "</td>";
                        tabla += "    <td><button onclick='javascript:seleccionarInfante(\"" + $.trim(nombre) + "\"," + value.id + ")' type='button' class='btn btn-success'><i class='fa fa-check-circle' style='cursor:pointer;' ></i></button></td>";
                        tabla += "</tr>";
                    });
                    if ($.trim(data))
                    {
                        $('#tabla_detalles_infantes').append(tabla);
                    }
                }

            });
}

function seleccionarInfante(nombre, id)
{
    $('#nombre_infante').val(nombre);
    $('#id_infante').val(id);
    $('#frmBuscarInfanteModal').modal('hide');
    $('#frmHistorialModal').modal('hide');
    $('#editar_infante').attr('style', 'background: #739e73;color: white;cursor:pointer;');
}

function getServicios(name)
{
    $.ajax({
        type: "POST",
        url: "aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=getServiciosSesion",
    })

            .done(function (data) {
                var obj = $.parseJSON(data);
                var options = "";
                var i = 1;
                $.each(obj, function (index, value) {
                    if (i == 1)
                        var selected = "selected";
                    else
                        var selected = "";
                    options += "<option value='" + value.id + "'  " + selected + ">";
                    options += value.nombre + "</option>";
                    i++;
                });
                $('#servicios_infantes').html(options);
            });
}


function agregarAdicional()
{

    var nombre_adicional = $('#nombre_adicional').val();
    var id_adicional = $('#id_adicional').val();
    var cantidad = $('#cantidad_adicional').val();
    if (nombre_adicional !== "" && cantidad !== "" && localStorage.getItem("nombre_producto") == nombre_adicional && id_adicional != "")
    {
        var i = 0;
        $('.adicionales_class').each(function (a, obj) {

            if ($(obj).val() == id_adicional)
            {
                i++;
                var cantidad_actual = $(obj).parent().parent().find('.adicional_cantidad').html();
                var nueva_cantidad = parseInt(cantidad_actual) + parseInt(cantidad);
                $(obj).parent().parent().find('.adicional_cantidad').html(nueva_cantidad);
                $('#nombre_adicional').val('');
                $('#cantidad_adicional').val('');
                localStorage.removeItem('nombre_producto');
            }

        });
        if (i == 0)
        {

            var tabla = "<tr>";
            tabla += "<td class='adicional_id' >" + id_adicional + "<input name='adicionales_id[]' class='adicionales_class' id='adicionales_id[]' value='" + id_adicional + "' type='hidden'></td>";
            tabla += "<td class='adicional_nombre'>" + nombre_adicional + "</td>";
            tabla += "<td class='adicional_cantidad' style='text-align:right;'>" + cantidad + "<input name='cantidad[]' id='cantidad[]' value='" + cantidad + "' type='hidden'></td>";
            tabla += '<td><a class="btn btn-danger" onclick="javascript:quitarAdicional(this)" ><i class="fa fa-remove"></i> Quitar</a></td>';
            $('#lista_adicionales').append(tabla);
            $('#nombre_adicional').val('');
            $('#cantidad_adicional').val('');
            $('#id_adicional').val('');
            localStorage.removeItem('nombre_producto');
        }

    }
    else
    {
        $.smallBox({
            title: "Atención..!",
            content: "Debe seleccionar el adicional y el servicio antes de agregar...",
            color: "#C46A69",
            timeout: 7000,
            icon: "fa fa-bell swing animated"
        });
    }
}

function editarInfante()
{
    var id = $('#id_infante').val();
    if (id !== "")
    {
        $('#form_infantes').attr('action', 'javascript:crearInfante(2)');
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=infante&controlador=infante&metodo=getInfantePorId',
            datetype: "json",
            type: 'POST',
            data: {id: id},
            success: function (res) {
                var obj = $.parseJSON(res);
                $('#primer_nombre').val(obj.datosInfante.primer_nombre);
                $('#segundo_nombre').val(obj.datosInfante.segundo_nombre);
                $('#primer_apellido').val(obj.datosInfante.primer_apellido);
                $('#segundo_apellido').val(obj.datosInfante.segundo_apellido);
                $('#fecha_nacimiento').val(obj.datosInfante.fecha_nacimiento);
                $('#institucion_educativa').val(obj.datosInfante.institucion_educativa);
                $('#frmInfanteModal').modal('show');
            }
        });

    }
}

function historialSesiones()
{
    var id = $('#cliente_id').val();
    if (id != "") {
        $('#frmHistorialModal').modal('show');
        $('#tabla_detalles_historial').html(tabla_historial);
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=getHistorialPorCliente',
            datetype: "json",
            type: 'POST',
            data: {id: id},
            success: function (res) {

                var obj = $.parseJSON(res);
                var tabla = "";
                $.each(obj, function (index, value) {
                    var nombre = value.primer_nombre + " " + value.segundo_nombre + " " + value.primer_apellido + " " + value.segundo_apellido;
                    tabla += "<tr>";
                    tabla += "    <td>" + value.id + "</td>";
                    tabla += "    <td>" + nombre + "</td>";
                    tabla += "    <td><button onclick='javascript:seleccionarInfante(\"" + $.trim(nombre) + "\"," + value.id + ")' type='button' class='btn btn-success'><i class='fa fa-check-circle' style='cursor:pointer;' ></i></button></td>";
                    tabla += "</tr>";
                });

                $('#tabla_detalles_historial').append(tabla);

            }
        });
    }
    else
    {
        $.smallBox({
            title: "Atención..!",
            content: "Debe seleccionar primero un cliente...",
            color: "#C46A69",
            timeout: 7000,
            icon: "fa fa-bell swing animated"
        });
    }

}

