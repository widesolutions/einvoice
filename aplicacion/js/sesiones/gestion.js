var nuevo;
var buscar;
var historial;


$(document).ready(function () {
    mostrarSesionesActivas();

    getServicios();

    $("#fromClientes").submit(function (event) {

        var nombres = $('#nombres').val();
        var apellidos = $('#apellidos').val();
        var telefono1 = $('#telefono1').val();
        var cliente_id = $('#cliente_id').val();


        $('.codigo_cliente').each(function (a, obj) {



            if (cliente_id == $.trim($(this).html()))
            {
                $(this).parent().find('.informacion_cliente').find('.nombre_cliente').html('');
                $(this).parent().find('.informacion_cliente').find('.nombre_cliente').html(nombres.toUpperCase() + " " + apellidos.toUpperCase());
                $(this).parent().find('.telefono_cliente').html('');
                $(this).parent().find('.telefono_cliente').html(telefono1);

            }


        });


    });

    $('body').on('click', '.informacion_cliente', function () {

        $('#cliente_id').val($.trim($(this).parent().find('.codigo_cliente').html()))
        editarCliente();
    });

    var redirect = $('#cliente_redirect').val();
    if (redirect != "")
    {

        setTimeout(function () {
            $('input[type="search"]').val(redirect);
            $('input[type="search"]').trigger("keyup");
        }, 400);


    }


    nuevo = '<div class="panel panel-default" id="nuevo">' + $('#nuevo').html() + '</div>';
    buscar = $('#buscar').wrap();
    historial = $('#historial').wrap();


    $("#form_infantes").validate({
        rules: {
            primer_apellido: {
                required: true
            },
            primer_nombre: {
                required: true
            },
            servicio_nuevo: {
                required: true

            }
        }
    });



    $('#fecha_nacimiento').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'

    });

    $("#nombre_adicional").autocomplete({
        source: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=getProductoxNombre',
        select: function (event, ui) {
            $('#id_adicional').val(ui.item.id);
            localStorage.setItem("nombre_producto_gestion", ui.item.value);
        }
    });

});

tabla_detalles_infantes = $('#tabla_detalles_infantes').html();
tabla_detalles_adicionales = $('#tabla_detalles_adicionales').html();

function mostrarSesionesActivas()
{
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=getSesionesActivas',
        datetype: "json",
        type: 'POST',
        success: function (res) {
            var obj = $.parseJSON(res);
            var tabla = "";
            if (res != "null") {

                $.each(obj, function (index, value) {
                    var nombre_cliente = value.nombres + " " + value.apellidos;
                    if (value.estado == 'A')
                        label_parent = 'success';
                    else
                        label_parent = 'danger';

                    tabla += "<tr class='" + label_parent + "'>";
                    tabla += "    <td class='codigo_cliente'>" + value.cliente_id + "</td>";
                    tabla += "    <td class='informacion_cliente'  style='cursor:pointer;'><div class='nombre_cliente'>" + nombre_cliente + "</div><p style='display:none;'>" + value.identificacion + "</p><div style='display:none;' class='sesion'>" + value.sesion_id + "</div></td>";
                    tabla += "    <td class='telefono_cliente'>" + value.telefono1 + "</td>";
                    tabla += "    <td class='informacion_infantes' style='cursor:pointer;' onclick=\"javascript:mostrarInfantes(" + value.sesion_id + "," + value.cliente_id + ",'" + value.estado + "')\">";

                    $.each(value.infantes, function (index, value_infante_array) {
                        if (value_infante_array['estado'] == 'A')
                            label = 'primary';
                        else
                            label = 'danger';

                        tabla += " <span class='label label-" + label + "' style='margin-right: 1px;' id='infante_" + value_infante_array['id_infante'] + "'>";
                        $.each(value_infante_array, function (index, value_infantes) {
                            if (value_infantes != null && $.trim(index) == "primer_nombre" || $.trim(index) == "primer_apellido")
                            {

                                tabla += value_infantes + " ";
                            }
                        });

                        tabla += " </span>";

                    });
                    tabla += "    </td>";
                    tabla += "    <td class='informacion_adicionales' style='cursor:pointer;' onclick=\"javascript:mostrarAdicionales(" + value.sesion_id + ",'" + value.estado + "')\">";
                    if (value.adicionales != null) {
                        $.each(value.adicionales, function (index, value_adicional_array) {
                            tabla += " <span class='label label-success' style='margin-right: 1px;' id='adicionale_" + value_adicional_array['producto_id'] + "' >";

                            $.each(value_adicional_array, function (index, value_adcionales) {
                                if (value_adcionales != null && $.trim(index) != "producto_id")
                                    tabla += value_adcionales + " ";
                            });

                            tabla += " </span>";

                        });
                    }
                    tabla += "    </td>";

                    if (value.estado == 'A')
                        tabla += '<td><a href="javascript:cancelarSesion(' + value.sesion_id + ')" class="deletebutton btn btn-default"><strong><i class="fa fa-trash-o fa-lg"></i></strong></a></td>';

                    else
                        tabla += '<td></td>';

                    tabla += "</tr>";

                });

                $('#sesiones tbody').append(tabla);

            }
            $('#sesiones').DataTable({
                "aoColumnDefs": [
                    {"bSearchable": false, "aTargets": [2, 4, 5]}
                ]
            });

        }
    });
}

function mostrarInfantes(sesion, cliente, estado)
{
    var parametros = {};
    parametros['id'] = sesion;
    $('#cliente_id').val(cliente);
    $('#sesion_id').val(sesion);

    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=getInfantesPorIdSesion',
        data: {parametros: parametros},
        type: 'POST',
        success: function (res) {
            $('#tabla_detalles_infantes').html(tabla_detalles_infantes);
            tabla = "";
            var obj = $.parseJSON(res);

            $.each(obj, function (index, value) {
                tabla = "<tr>";

                tabla += "<td class='infante_id'>" + value.id_infante + "</td> ";
                tabla += "<td>" + value.primer_nombre + " " + value.primer_apellido + "</td> ";
                tabla += "<td>" + value.nombre_servicio + "</td> ";
                tabla += "<td>" + value.hora_inicio + "<div class='imprime' id='impresion_" + value.id_infante + "' style='display:none;'><div  id='cod_imp" + value.id_infante + "'><div class='rp' id='bar_imp" + value.id_infante + "'></div><div class='rp'>" + value.primer_nombre + " " + value.primer_apellido + "</div><div class='rp'>" + value.hora_inicio + "</div></div></div></td> ";
                if (estado == 'A')
                {

                    if (value.estado == 'A')
                    {
                        contenido_cancelar = '<a title="cancelar sesión" href="javascript:cancelarSesionInfante(' + value.id_infante + ',' + sesion + ')" class="deletebutton btn btn-default"><strong><i class="fa fa-trash-o fa-lg"></i></strong></a>';
                        if (value.id_servicio == 4)
                            contenido_traspasar = '<a title="Cambiar de servicio" href="javascript:cambiarServicio(' + value.id_infante + ',' + sesion + ',' + value.id_servicio + ')" class="deletebutton btn btn-default"><strong><i class="fa fa-exchange"></i></strong></a>';
                        else
                            contenido_traspasar = '';
                        contenido_imprimir = '<a title="Imprimr manilla" href="javascript:imprimirManilla(' + value.id_infante + ')" class="deletebutton btn btn-default"><strong><i class="fa fa-print"></i></strong></a>';
                    }
                    else
                    {
                        contenido_cancelar = '';
                        contenido_traspasar = '';
                        contenido_imprimir = '';

                    }

                }
                else
                {
                    contenido_cancelar = '';
                    contenido_traspasar = '';
                }
                tabla += '<td>' + contenido_cancelar + " " + contenido_traspasar + " " + contenido_imprimir + '</td>';
                tabla += "</tr>";

                $('#tabla_detalles_infantes').append(tabla);
                $("#bar_imp" + value.id_infante).barcode(value.id_infante, "codabar");


            });

            nuevoInfante();



            $('#frmGestionarInfante').modal('show');
            if (estado != "A") {
                $('#botones_ifantes').hide();
                $('#nuevo').hide();
            }

            else {
                $('#botones_ifantes').show();
                $('#nuevo').show();

            }
        }
    });
}


function imprimirManilla(id)
{
    $("#cod_imp" + id).print({
        globalStyles: true,
        mediaPrint: true,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
}

function mostrarAdicionales(sesion_id, estado)
{
    var parametros = {};
    $('#id_sesion').val(sesion_id);
    parametros['id'] = sesion_id;
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=getAdicionalesPorIdSesion',
        data: {parametros: parametros},
        type: 'POST',
        success: function (res) {
            $('#tabla_detalles_adicionales').html(tabla_detalles_adicionales);

            if (res != "null") {
                tabla = "";
                var obj = $.parseJSON(res);

                $.each(obj, function (index, value) {
                    tabla += "<tr>";
                    tabla += "<td class='producto_id'>" + value.producto_id + "</td> ";
                    tabla += "<td class='cantidad'>" + value.cantidad + "</td> ";
                    tabla += "<td>" + value.nombre + "</td> ";
                    if (estado != 'A')
                        tabla += "<td></td>";
                    else
                        tabla += '<td><a title="remover adicional" href="javascript:cancelarAdicionalSesion(' + value.producto_id + ',' + sesion_id + ')" class="deletebutton btn btn-default"><strong><i class="fa fa-trash-o fa-lg"></i></strong></a> </td>';
                    tabla += "</tr>";



                });


                $('#tabla_detalles_adicionales').append(tabla);
            }


            $('#frmGestionarAdicionales').modal('show');
        }


    });

    if (estado != "A") {
        $('#panel_adicionales div:first').hide();
    }

    else {
        $('#panel_adicionales div:first').show();

    }


}

function agregarAdicional()
{

    var nombre_adicional = $('#nombre_adicional').val();
    var id_adicional = $('#id_adicional').val();
    var cantidad = $('#cantidad_adicional').val();
    var id_sesion = $('#id_sesion').val();
    var parametros = {};
    parametros['id_adicional'] = id_adicional;
    parametros['cantidad'] = cantidad;
    parametros['id_sesion'] = id_sesion;

    if (nombre_adicional !== "" && cantidad !== "" && id_adicional != "" && localStorage.getItem("nombre_producto") != nombre_adicional)
    {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=agregarAdicional',
            data: {parametros: parametros},
            type: 'POST',
            success: function (res) {

                tabla = "";
                tabla += "<tr>";
                tabla += "<td class='producto_id'>" + id_adicional + "</td> ";
                tabla += "<td class='cantidad'>" + cantidad + "</td> ";
                tabla += "<td>" + nombre_adicional + "</td> ";
                tabla += "</tr>";
                $('#tabla_detalles_adicionales').append(tabla);
                $('#nombre_adicional').val('');
                $('#cantidad_adicional').val('');
                localStorage.removeItem('nombre_producto_gestion');

                $('.informacion_cliente').each(function (a, obj) {

                    if ($(obj).find('.sesion').html() == id_sesion)
                        $(obj).parent().find('.informacion_adicionales').append("<span class='label label-success' style='margin-right: 1px;' id='adicionale_" + id_adicional + "'>" + cantidad + " " + nombre_adicional + "</span>");

                });



            }
        });

    }
    else
    {
        $.smallBox({
            title: "Debe seleccionar el adicional y el servicio antes de agregar    ",
            color: "#F78181",
            iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
            timeout: 4000
        });
    }
}



tabla_historial = $('#tabla_detalles_historial').html();
tabla_buscar_infantes = $('#tabla_buscar_detalles_infantes').html();

function nuevoInfante()
{

    $('#nuevo').show();
    $('#nuevo').find('#none').attr('id', 'collapseThree-1');
    $('.modal-body').find('.panel').not("#nuevo").remove();


}

function buscarInfante()
{
    $('.modal-body').find('.panel').not("#nuevo").remove();
    $('#tabla_detalles_infantes').parent().append(buscar);
    $('#nuevo').hide();
    $('#nuevo').find('#collapseThree-1').attr('id', 'none');


}

function historialSesiones()
{
    $('.modal-body').find('.panel').not("#nuevo").remove();
    $('#tabla_detalles_infantes').parent().append(historial);
    $('#nuevo').hide();
    $('#nuevo').find('#collapseThree-1').attr('id', 'none');
    desplegarHistorialSesiones();

}

function desplegarHistorialSesiones()
{
    var id = $('#cliente_id').val();
    if (id != "") {
        $('#tabla_detalles_historial').html(tabla_historial);
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=getHistorialPorCliente',
            datetype: "json",
            type: 'POST',
            data: {id: id},
            success: function (res) {

                var obj = $.parseJSON(res);
                var tabla = "";
                $.each(obj, function (index, value) {
                    var nombre = value.primer_nombre + " " + value.segundo_nombre + " " + value.primer_apellido + " " + value.segundo_apellido;
                    tabla += "<tr>";
                    tabla += "    <td>" + value.id + "</td>";
                    tabla += "    <td>" + nombre + "</td>";
                    tabla += "    <td class='servicio'>" + $("#servicios").html() + "</td>";
                    tabla += "    <td><button onclick='javascript:seleccionarInfante(\"" + $.trim(nombre) + "\"," + value.id + ",this)' type='button' class='btn btn-success'><i class='fa fa-check-circle' style='cursor:pointer;' ></i></button></td>";
                    tabla += "</tr>";
                });

                $('#tabla_detalles_historial').append(tabla);

            }
        });
    }
    else
    {
        $.smallBox({
            title: "Debe seleccionar primero un cliente   ",
            color: "#F78181",
            iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
            timeout: 4000
        });
    }

}

function getServicios(name)
{
    $.ajax({
        type: "POST",
        url: "aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=getServiciosSesion",
    })

            .done(function (data) {
                var obj = $.parseJSON(data);
                var options = "";
                var i = 1;
                $.each(obj, function (index, value) {

                    options += "<option value='" + value.id + "'  >";
                    options += value.nombre + "</option>";
                    i++;
                });
                $('#servicios_infantes').html(options);
                $('#servicio_nuevo').append('<option value="">Seleccione el servicio</option>' + options);
            });
}

function searchInfantes(name)
{
    var search = $('#search_infante').val();
    $.ajax({
        type: "POST",
        url: "aplicacion/rutasMetodos.php?modulo=infante&controlador=infante&metodo=searchInfante",
        data: {search_infante: search}

    })

            .done(function (data) {
                $('#tabla_buscar_detalles_infantes').html(tabla_buscar_infantes);
                if (search != "")
                {
                    var obj = $.parseJSON(data);
                    var tabla = "";
                    $.each(obj, function (index, value) {
                        var nombre = value.primer_nombre + " " + value.segundo_nombre + " " + value.primer_apellido + " " + value.segundo_apellido;
                        tabla += "<tr>";
                        tabla += "    <td>" + value.id + "</td>";
                        tabla += "    <td>" + nombre + "</td>";
                        tabla += "    <td class='servicio'>" + $("#servicios").html() + "</td>";
                        tabla += "    <td><button onclick='javascript:seleccionarInfante(\"" + $.trim(nombre) + "\"," + value.id + ",this)' type='button' class='btn btn-success'><i class='fa fa-check-circle' style='cursor:pointer;' ></i></button></td>";
                        tabla += "</tr>";
                    });
                    if ($.trim(data))
                    {
                        $('#tabla_buscar_detalles_infantes  ').append(tabla);
                    }
                }

            });
}

function seleccionarInfante(nombre, id, obj)
{
    var parametros = {};
    parametros['id'] = id
    var activo = false;
    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=verificarInfanteActivo",
        type: 'post',
        async: false,
        data: {parametros: parametros},
        success: function (res) {

            if (res != 0)
            {
                activo = true;
                mensaje_error = "El infante ya tiene una sesiòn activa";
            }
        }
    });

    if (activo == true)
    {
        $.smallBox({
            title: mensaje_error,
            color: "#F78181",
            iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
            timeout: 4000
        });
    }
    else
    {
        $.SmartMessageBox({
            title: "Alerta!",
            content: "Esta seguro que desea iniciar la sesión?",
            buttons: '[No][Si]'
        }, function (ButtonPressed) {

            if (ButtonPressed === "Si") {
                var parametros = {};
                parametros['sesion_id'] = $('#sesion_id').val();
                parametros['infante_id'] = id;
                parametros['servicio_id'] = $(obj).parent().parent().find('.servicio select').val();
                $.ajax({
                    url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=agregarInfanteSesion",
                    type: 'post',
                    beforeSend: function ()
                    {
                        //$('#loading').show();
                        $('.btn').attr('disabled', 'disabled');
                    },
                    async: false,
                    data: {parametros: parametros},
                    success: function (result) {
                        var res = $.parseJSON(result);
                        $('.btn').removeAttr('disabled');

                        if (res['insert'] == 1)
                        {

                            $.smallBox({
                                title: "Infante agregado",
                                color: "#659265",
                                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                timeout: 4000
                            });

                            var tabla = "<tr>";
                            tabla += "<td>" + id + "</td>";
                            tabla += "<td>" + nombre + "</td>";
                            tabla += "<td>" + $(obj).parent().parent().find('.servicio select option:selected').text();
                            +"</td>";
                            tabla += "<td>" + res['hora_inicio'] + "<div class='imprime' id='impresion_" + id + "' style='display:none;'><div  id='cod_imp" + id + "'><div class='rp' id='bar_imp" + id + "'></div><div class='rp'>" + nombre + "</div><div class='rp'>" + res['hora_inicio'] + "</div></div></div></td>";
                            var contenido_imprimir = '<a title="Imprimr manilla" href="javascript:imprimirManilla(' + id + ')" class="deletebutton btn btn-default"><strong><i class="fa fa-print"></i></strong></a>';
                            tabla += "<td>" + contenido_imprimir + "</td>";
                            tabla += "</tr>";

                            $('#tabla_detalles_infantes').append(tabla);
                            $("#bar_imp" + id).barcode(id, "codabar");

                            var bomba = '<span class="label label-primary" style="margin-right: 1px;">' + nombre + '</span>';
                            $('.informacion_cliente').each(function (a, obj) {

                                if ($(obj).find('.sesion').html() == parametros['sesion_id'])
                                    $(obj).parent().find('.informacion_infantes').append(bomba);

                            });
                        }
                        else
                        {
                            $.smallBox({
                                title: res,
                                color: "#F78181",
                                iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                                timeout: 4000
                            });
                        }
                    }
                });

            }
        });

    }
}



function crearInfante() {

    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea iniciar la sesión con el nuevo infante?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Si") {
            var parametros = {};
            parametros['primer_nombre'] = $('#primer_nombre').val();
            parametros['segundo_nombre'] = $('#segundo_nombre').val();
            parametros['primer_apellido'] = $('#primer_apellido').val();
            parametros['segundo_apellido'] = $('#segundo_apellido').val();
            parametros['fecha_nacimiento'] = $('#fecha_nacimiento').val();
            parametros['institucion_educativa'] = $('#institucion_educativa').val();
            parametros['sesion_id'] = $('#sesion_id').val();
            parametros['servicio_id'] = $('#servicio_nuevo').val();
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=crearAgregarInfanteSesion",
                type: 'post',
                async: false,
                data: {parametros: parametros},
                success: function (res) {

                    var obj = $.parseJSON(res);
                    if (obj['result'] == 1)
                    {
                        $.smallBox({
                            title: "Infante agregado",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });

                        var tabla = "<tr>";
                        tabla += "<td>" + obj['id_infante'] + "</td>";
                        tabla += "<td>" + parametros['primer_nombre'] + " " + parametros['primer_apellido'] + "</td>";
                        tabla += "<td>" + $('#servicio_nuevo option:selected').text() + "</td>";
                        tabla += "<td>" + obj['hora_inicio'] + "</td>";
                        tabla += "</tr>";

                        $('#tabla_detalles_infantes').append(tabla);

                        var bomba = '<span class="label label-primary" style="margin-right: 1px;">' + parametros['primer_nombre'] + " " + parametros['primer_apellido'] + '</span>';
                        $('.informacion_cliente').each(function (a, obj) {

                            if ($(obj).find('div').html() == parametros['sesion_id'])
                                $(obj).parent().find('.informacion_infantes').append(bomba);

                        });

                        $('#form_infantes')[0].reset();
                    }
                    else
                    {
                        $.smallBox({
                            title: res,
                            color: "#F78181",
                            iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                            timeout: 4000
                        });
                    }
                }
            });

        }
    });



}

function cancelarSesion(id)
{


    var parametros = {};
    parametros['id'] = id;
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea cancelar la sesión?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Si") {

            $('#token').val('cancelar_sesiones');
            $('#parametros').val(id);
            $('#dialog-autoriza').dialog('open');

        }
    });

}

function cancelarSesionInfante(infante_id, sesion_id)
{

    var parametros = infante_id + ';' + sesion_id;
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea cancelar la sesión del infante?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Si") {
            $('#frmGestionarInfante').modal('hide');

            $('#token').val('cancelar_sesion_infantes');
            $('#parametros').val(parametros);
            $('#dialog-autoriza').dialog('open');
        }
    });
}

function cancelarAdicionalSesion(adicional_id, sesion_id)
{
    var parametros = {};
    parametros['adicional_id'] = adicional_id;
    parametros['sesion_id'] = sesion_id;
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea remover el adicional de la sesión?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Si") {
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=cancelarAdicional",
                type: 'post',
                data: {parametros: parametros},
                success: function (res) {

                    if (res == 1)
                    {
                        $.smallBox({
                            title: "Adicional removido",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });


                        $('.producto_id').each(function (a, obj) {

                            if ($(obj).html() == adicional_id)
                            {
                                $(obj).parent().remove();
                            }
                        });

                        $('#adicionale_' + adicional_id).remove();


                    }
                }
            });
        }
    });
}


function cambiarServicio(infante_id, sesion_id, servicio_id)
{
    var parametros = {};
    parametros['infante_id'] = infante_id;
    parametros['sesion_id'] = sesion_id;
    parametros['servicio_id'] = servicio_id;

    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea cambiar de servicio al infante?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Si") {
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=sesiones&controlador=sesiones&metodo=cambiarServicio",
                type: 'post',
                data: {parametros: parametros},
                success: function (res) {

                    if (res == 1)
                    {
                        $.smallBox({
                            title: 'Se cambio de servicio correctamente',
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });

                        $('.infante_id').each(function (a, obj) {

                            if ($(obj).html() == sesion_id)
                            {
                               $(obj).parent().find("td:nth-child(3)").html('ZONA BEBE');
                               $(obj).parent().find("td:nth-child(5)").find('a:nth-child(2)').remove();
                            }
                            

                        });
                        location.reload();

                    }
                    else
                    {
                        $.smallBox({
                            title: res,
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });

                    }
                }
            });
        }
    });

}