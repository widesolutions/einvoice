$(document).ready(function () {

    $("#form_infantes").validate({
        rules: {
            primer_apellido: {
                required: true
            },
            primer_nombre: {
                required: true
            }
        }
    });

    $('#fecha_nacimiento').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'

    });

});

function crearInfante(tipo) {

    var parametros = {};
    parametros['primer_nombre'] = $('#primer_nombre').val();
    parametros['segundo_nombre'] = $('#segundo_nombre').val();
    parametros['primer_apellido'] = $('#primer_apellido').val();
    parametros['segundo_apellido'] = $('#segundo_apellido').val();
    parametros['fecha_nacimiento'] = $('#fecha_nacimiento').val();
    parametros['institucion_educativa'] = $('#institucion_educativa').val();
    parametros['ajax'] = 'SI';

    if (tipo == 1) {
        var metodo = "crear";
        var mensaje = "Infante Creado con exito.";
    }
    else {
        var metodo = "editar";
        var mensaje = "Infante Editado con exito.";
        parametros['id'] = $('#id_infante').val();
    }

    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=infante&controlador=infante&metodo=" + metodo,
        type: 'post',
        data: {parametros: parametros},
        success: function (res) {
            var respuesta = $.parseJSON(res);
            if (respuesta['insert'] == 1)
            {
                $.smallBox({
                    title: mensaje,
                    color: "#659265",
                    iconSmall: "fa fa-check fa-2x fadeInRight animated",
                    timeout: 4000
                });

                $('#nombre_infante').val(parametros['primer_nombre'] + " " + parametros['primer_apellido']);
                $('#id_infante').val(respuesta['id']);
                $('#editar_infante').attr('style', 'background: #739e73;color: white;cursor:pointer;');
                $('#frmInfanteModal').modal('hide');
                $('#form_infantes')[0].reset();
                $("form#form_infantes :input[type=text]").each(function () {
                    $(this).attr('class','');
                });

            }
            else
            {
                $.smallBox({
                    title: "Se genero un errror al insertar en la base de datos.",
                    color: "#C46A69",
                    iconSmall: "fa fa-error fa-2x fadeInRight animated",
                    timeout: 4000
                });
            }


        }
    });
}