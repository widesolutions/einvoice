$(document).ready(function () {
    recargarCaja();
    var $registerForm = $("#cajaForm").validate({
        rules: {
            ip: {required: true, ipv4: true},
            nombre: {required: true},
            emision: {required: true},
            serie: {required: true}
        }, errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});
function nuevaCaja() {
    limpiarFormularioCaja();
    $('#frmCajasModal').modal('show');
}
function ActualizarCaja(id) {
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=cajas&controlador=cajas&metodo=enviarDatosCaja',
        datetype: "json",
        type: 'POST',
        data: {codigoCaja: id},
        success: function (res) {
            var json_obj = $.parseJSON(res);
            limpiarFormularioCaja();
            carga_DatosIncialesCaja(json_obj);
            $('#frmCajasModal').modal('show');
            $('#CajaForm >header').text('Actualización de Datos Caja');
        }
    });
}
function limpiarFormularioCaja() {
    $("#IDCaja").val('');
    $("#ip").val('');  /* precio venta*/
    $("#nombre").val('');  /*nombre*/
    $("#emision").val('');  /*codigo de barras*/
    $("#serie").val('');  /*modelo*/
    $('em').remove();
}
function carga_DatosIncialesCaja(edt) {
    $("#IDCaja").val(edt.datosCaja.IDCaja); //id del servicio
    $("#ip").val(edt.datosCaja.ip);  /*precio servicio*/
    $("#nombre").val(edt.datosCaja.nombre);  /*cod barras serv*/
    $('#serie').val(edt.datosCaja.serie); /*datos servicio*/
    $('#emision').val(edt.datosCaja.emision); /*datos servicio*/
}
function EliminarCaja(id) {
    $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro de eliminar esta caja?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=cajas&controlador=cajas&metodo=EliminarCaja',
                datetype: "json",
                type: 'POST',
                data: {idscaja: id},
                success: function (res) {
                    if (res === '1') {
                        $.smallBox({
                            title: "Correcto..!",
                            content: "Caja Eliminada correctamente...",
                            color: "#659265",
                            timeout: 7000,
                            icon: "fa fa-thumbs-up bounce animated"
                        });
                        recargarCaja();
                    }

                }
            });
        }
    });
}
function guardarCaja() {
    var idcaja = $('#IDCaja').val();
    if (idcaja === '') {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=cajas&controlador=cajas&metodo=guardaDatosCaja',
            datetype: "json",
            type: 'POST',
            data: $("#cajaForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $.smallBox({
                        title: "Correcto..!",
                        content: "Caja Guardado correctamente...",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    limpiarFormularioCaja();
                    $('#frmCajasModal').modal('hide');
                    recargarCaja();
                } else {
                    if (res === '2') {
                        $.smallBox({
                            title: "Error..!",
                            content: "Ya existe una caja de las mismas caracteristicas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            title: "Error..!",
                            content: "Problemas al Guardar consulte con Sistemas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });
                    }
                }
            }
        });
    } else {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=cajas&controlador=cajas&metodo=ActualizarDatos',
            datetype: "json",
            type: 'POST',
            data: $("#cajaForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $.smallBox({
                        title: "Datos Actualizados",
                        content: "<i class='fa fa-clock-o'></i> <i>Caja Actualizada correctamente...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    limpiarFormularioCaja(0);
                    recargarCaja();
                    $('#frmCajasModal').modal('hide');
                }
            }
        });
    }
}
function recargarCaja() {
    var dtTable = $('#listaCajas').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/cajas/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}