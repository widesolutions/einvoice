$(document).ready(function () {
});

function cerrarCaja() {
    $('#detalles_cierre_caja_fin').load('aplicacion/vistas/cajas/frmImpresionCaja.php');
    $('#frmCierreCajaModal').modal('show');
}

function imprimirCierre() {
    $("#body_impresion").print({
        globalStyles: true,
        mediaPrint: true,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
}

function ejecutarCierreCaja() {
    $.SmartMessageBox({
        title: "Alerta!",
        content: "Esta seguro que desea cerrar la caja del día?",
        buttons: '[No][Si]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Si") {
            $.ajax({
                url: "aplicacion/rutasMetodos.php?modulo=cajas&controlador=cajas&metodo=cerrarCaja",
                type: 'post',
                data: $("#wizard-1").serialize(),
                success: function (res) {
                    if (res === '1') {
                        envioMail();
                        var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                        var f = new Date();
                        $('#body_impresion').prepend(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
                        var boton_imprimir = '<a href="javascript:imprimirCierre();" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="fa fa-print"></i></span>Imprimir Cierre</a>';
                        $('#footer-cierre-caja').html(boton_imprimir);
                        $.smallBox({
                            title: "Caja cerrada..!",
                            content: "Finalizado el cierre de la Caja...",
                            color: "#659265",
                            timeout: 7000,
                            icon: "fa fa-thumbs-up bounce animated"
                        });
                        $('#frmCierreCajaModal').on('hidden.bs.modal', function (e) {
                            location.reload();
                        });
                    } else {
                        $.smallBox({
                            title: res,
                            color: "#F78181",
                            iconSmall: "fa fa-exclamation-triangle fa-1 fadeInRight animated",
                            timeout: 4000
                        });
                    }
                }
            });
        }
    });
}

function envioMail() {
    $.ajax({
        url: 'aplicacion/controladores/mail/mail.php',
        type: 'POST',
        success: function (res) {
            console.log(res + ' Correo enviado a los Administradores...');
        }
    });
}