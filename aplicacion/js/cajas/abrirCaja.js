$(document).ready(function () {
    var $registerForm = $("#fromAperturaCaja").validate({
        rules: {
            monto: {required: true}

        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});

function abrirCaja() {
    $('#frmAperturaCajaModal').modal('show');
}
function guardarAperturaCaja() {
    var parametros = {};
    parametros['monto'] = $('#monto').val();
    parametros['observacion'] = $('#observacion').val();
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=cajas&controlador=cajas&metodo=aperturaCaja',
        type: 'POST',
        data: {parametros: parametros},
        success: function (res) {
            if (res !== '0') {
                $.smallBox({
                    title: "Caja Abierta",
                    content: "<i class='fa fa-clock-o'></i> <i>La Caja fue abierta con un monto de: "+parametros['monto']+"...</i>",
                    color: "#659265",
                    iconSmall: "fa fa-check fa-2x fadeInRight animated",
                    timeout: 4000
                });
                $('#frmAperturaCajaModal').modal('hide');
                $('#abrirCerrarCaja').removeClass('btn-success');
                $('#abrirCerrarCaja').addClass('btn-danger');
                $('#abrirCerrarCaja').html('<i class="fa fa-remove"></i> Cerrar Caja');
                $("#abrirCerrarCaja").attr("href", "javascript:cerrarCaja()");
                location.reload();
            }
        }
    });
}