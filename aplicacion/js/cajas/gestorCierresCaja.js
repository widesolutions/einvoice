$(document).ready(function () {
    recargarCierresCaja();
});
function recargarCierresCaja() {
    $('#listaCierreCaja').dataTable({
        "oLanguage": {"sUrl": "publico/js/Spanish.json "},
        "order": [[0, "desc"]]
    });
}

function cierreCajaPorFecha(fecha){
    $('#wid-id-4 header h2 b spam').html(fecha);
    $('#frmGestorCierreCajaModal').modal('show');
    $('#detalles_cierre_caja').load('aplicacion/vistas/cajas/frmImpresionCaja.php?fecha=' + fecha);
}
function imprimirGestorCierreCaja(){
    $("#body_impresion").print({
        globalStyles: true,
        mediaPrint: true,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
}