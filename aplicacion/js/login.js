$(function() {
    $("#login-form").validate({
        rules: {
            usuario: {
                required: true
            },
            password: {
                required: true,
                minlength: 3
            }
        }, errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        }
    });
});

function login() {
    var usuario = $("#usuario").val();
    var clave = $("#clave").val();
    var local = $("#local").val();
    var parametros={};
    parametros['usuario'] = usuario;
    parametros['clave'] = clave;
    parametros['local'] = local;
    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=seguridad&controlador=login&metodo=autenticar",
        type: 'post',
        data: {parametros:parametros},
        success: function(respuesta) {
            switch (respuesta) {
                case 'ok':
                    window.open("inicio.php","_self").focus();
                    break;
                case 'cambio':
                    window.open("cambioClave.php").focus();
                    break;
                    
                default :
                    
                    $.smallBox({
                        title: "Usuario y/o Contraseña Incorrectos..",
                        content: "<i class='icon-remove'></i> <i>Digite Nuevamente su Usuario y Contraseña..!</i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    $("#usuario").val('');
                    $("#clave").val('');
                    
                    break;
                    
            }
        }
    });
}
