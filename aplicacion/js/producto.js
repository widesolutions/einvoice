$(document).ready(function () {
    recargarProducto();
    $.validator.addMethod("comparacionprecios", function (value, element, param) {
        var $precio_venta = $(param);
        return parseFloat(value) > parseFloat($precio_venta.val());
    });
    var $registerForm = $("#productForm").validate({
        rules: {
            precio_compra: {required: true, number: true},
            cod_barras: {required: true, minlength: 3, alphanumeric: true},
            precio_venta: {comparacionprecios: '#precio_compra', required: true, number: true},
            descripcion: {required: true, minlength: 3},
            uni_medida: {required: true},
            iva: {required: true}
        },messages: {
            descripcion: {required: "la descripcion es necesaria", minlength: "debe ingresar mas de 3 caracteres"},
            precio_compra: {required: "Es necesario ingresar el precio compra"},
            codigo_barras: {alphanumeric: "No caracteres especiales"},
            precio_venta: {required: "Es necesario ingresar el precio venta", comparacionprecios: "no puede ser menor o igual al precio compra"},
            todo_dia: {required: "Es necesario ingresar el dia"},
            iva: {required: "debe ingresar el iva"}
        }, errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});
function nuevoProducto() {
    limpiarFormularioProducto();
    $('#frmProductoModal').modal('show');
}
function limpiarFormularioProducto() {
    $('#IDProducto').val('');
    $("#precio_compra").val('');  /*precio compra*/
    $("#precio_venta").val('');  /* precio venta*/
    $("#descripcion").val('');  /*nombre*/
    $("#cod_barras").val('');  /*codigo de barras*/
    $("#modelo").val('');  /*modelo*/
    $('#uni_medida').prop('selectedIndex', 0);/*Tipo de Usuario*/
    $('#iva').prop('selectedIndex', 0);/*Centro*/
}
function recargarProducto() {
    var dtTable = $('#listaProductos').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/productos/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}
function guardarProducto() {
    var idprod = $('#IDProducto').val();
    if (idprod === '') {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=guardaDatosProducto',
            datetype: "json",
            type: 'POST',
            data: $("#productForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $('#frmProductoModal').modal('hide');
                    $.smallBox({
                        title: "Correcto..!",
                        content: "Producto Guardado correctamente...",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    //limpiarFormularioProducto();
                    //$('#frmProductoModal').modal('show');
                    recargarProducto();
                } else {
                    if(res === '2'){
                        $.smallBox({
                        title: "Error..!",
                        content: "Producto Duplicado, ya existe en la base de datos...",
                        color: "#C46A69",
                        timeout: 7000,
                        icon: "fa fa-bell swing animated"
                    });
                    }else{
                    $.smallBox({
                        title: "Error..!",
                        content: "Problema para Almacenar el Producto, consulte con Sistemas...",
                        color: "#C46A69",
                        timeout: 7000,
                        icon: "fa fa-bell swing animated"
                    });
                }
                }
            }
        });
    } else {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=ActualizarProducto',
            datetype: "json",
            type: 'POST',
            data: $("#productForm").serialize(),
            success: function (res) {
                if (res === '1') {
                    $.smallBox({
                        title: "Correcto..!",
                        content: "Producto Actualizado correctamente...",
                        color: "#659265",
                        timeout: 7000,
                        icon: "fa fa-thumbs-up bounce animated"
                    });
                    recargarProducto();
                    $('#frmProductoModal').modal('show');
                }
            }
        });

    }
}
function carga_DatosIncialesProducto(edt) {
    $("#IDProducto").val(edt.datosProducto.IDProducto); //id del producto
    $("#precio_compra").val(edt.datosProducto.precio_compra);  /*Nombre*/
    $("#precio_venta").val(edt.datosProducto.precio_venta);  /*Apellido*/
    $("#descripcion").val(edt.datosProducto.descripcion);  /*Usuario*/
    $("#cod_barras").val(edt.datosProducto.cod_barras);  /*E-Mail*/
    $("#modelo").val(edt.datosProducto.modelo);/*Celular*/
    $("#uni_medida").val(edt.datosProducto.uni_medida);/*Celular*/
    $('#iva').prop('selectedIndex', edt.datosProducto.iva);
}
function ActualizarProducto(id) {
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=enviarDatosProducto',
        datetype: "json",
        type: 'POST',
        data: {codigoProducto: id},
        success: function (res) {
            var json_obj = $.parseJSON(res);
            limpiarFormularioProducto();
            carga_DatosIncialesProducto(json_obj);
            $('#frmProductoModal').modal('show');
            $('#productForm >header').text('Actualización de Datos Usuario')
        }
    });
}
function EliminarProducto(id, nombre) {
    $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro que desea eliminar el Producto <span class='txt-color-orangeDark'><strong>" + nombre + " </strong></span> ?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=EliminarProducto',
                datetype: "json",
                type: 'POST',
                data: {idprod: id},
                success: function (res) {
                    if (res === '1') {
                        $.smallBox({
                            title: "Producto Eliminado..!",
                            content: "Producto eliminado correctamente...",
                            color: "#659265",
                            timeout: 4000,
                            icon: "fa fa-thumbs-up bounce animated"
                        });
                        recargarProducto();
                    } else {
                        $.smallBox({
                            title: "Problema para Eliminar..!",
                            content: "Existe algun problema con la operación, contacte a sistemas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });
                    }
                }
            });
        }
    });
}
function VerificarBarras() {
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=productos&controlador=producto&metodo=BuscarBarras',
        datetype: "json",
        type: 'POST',
        data: $("#productForm").serialize(),
        success: function (res) {
            if (res === '0') {
                $.smallBox({
                    title: "Error..!",
                    content: "Codigo de Barras ya Registrado...",
                    color: "#C46A69",
                    timeout: 7000,
                    icon: "fa fa-bell swing animated"
                });
                limpiarFormularioProducto();
                $('#frmProductoModal').modal('hide');
                recargarProducto();
            } 
        }

    });
}
