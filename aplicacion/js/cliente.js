$(document).ready(function () {

    $('#frmClienteModal').on('hidden.bs.modal', function () {
        $('#fromClientes')[0].reset();
    });
    parametrizacionInicial();
});
function parametrizacionInicial() {
    $('#identificacion').focusout(function () {
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=validarCedulaExistente',
            datetype: "json",
            type: 'POST',
            data: {cedula: $(this).val()},
            success: function (res) {
                if (res === '1') {
                    $('#identificacion').parent('label').removeClass('state-success');
                    $('#identificacion').parent('label').addClass('state-error');
                    $('#identificacion').parent('label').parent('section').remove('em');
                    $('#identificacion').parent('label').parent('section').append('<em class="invalid existeCedula" for="identificacion">Cédula o Ruc ya existe</em>');
                    $("button[type=submit]").attr("disabled", "disabled");
                } else {
                    $(this).parent('label').removeClass('state-error');
                    $(this).parent('label').addClass('state-success');
                    $('#identificacion').parent('label').parent('section').remove('.existeCedula');
                    $("button[type=submit]").removeAttr("disabled");
                }
            }
        });
    });
    $("#identificacion").validarCedulaEC({
        events: "blur",
        onValid: function () {
            $(this).parent('label').removeClass('state-error');
            $(this).parent('label').addClass('state-success');
            $(this).parent('label').parent('section').children('em').remove();
            $("button[type=submit]").removeAttr("disabled");
        },
        onInvalid: function () {
            $(this).parent('label').removeClass('state-success');
            $(this).parent('label').addClass('state-error');
            $(this).parent('label').parent('section').children('em').remove();
            $(this).parent('label').parent('section').append('<em class="invalid errorCedula" for="identificacion">Cédula o Ruc no Valido</em>');
            $("button[type=submit]").attr("disabled", "disabled");
        }
    });


    var $registerForm = $("#fromClientes").validate({
        rules: {
            identificacion: {required: true, digits: true},
            nombres: {required: true},
            apellidos: {required: true},
            telefono1: {required: true, digits: true},
            email: {required: true, email: true},
            direccion: {required: true}
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

}
function cambioCedulaPasaporteCliente() {
    var tipoDocumento = $('#tipoDocumento').val();
    var html = '';
    if (tipoDocumento === 'C') {
        html = '<label class="control-label">Número de Cédula/Ruc:</label>' +
                '<label class="input"> ' +
                '<i class="icon-prepend fa fa-asterisk"></i>' +
                '<input type="text" class="soloNumero" id="identificacion" name="identificacion" placeholder="Cedula/Ruc del Cliente">' +
                '</label>';
    } else {
        html = '<label class="control-label">Número de Pasaporte:</label>' +
                '<label class="input"> ' +
                '<i class="icon-prepend fa fa-asterisk"></i>' +
                '<input type="text" class="text-uppercase" id="pasaporte" name="pasaporte" placeholder="Número de Pasaporte">' +
                '</label>';
    }
    $('#cedulaPasaporte').empty();
    $('#cedulaPasaporte').append(html);
    $('.soloNumero').keydown(function (event) {
        validacionNumeros(event);
    });
    parametrizacionInicial();
}
function crearCliente(tipo) {

    var parametros = {};
    parametros['nombres'] = $('#nombres').val();
    parametros['apellidos'] = $('#apellidos').val();
    parametros['tipoDocumento'] = $('#tipoDocumento').val();
    console.log($('#identificacion').val());
    console.log($('#pasaporte').val());
    parametros['identificacion'] = ($('#identificacion').val()!==undefined)?$('#identificacion').val():$('#pasaporte').val();
    parametros['telefono1'] = $('#telefono1').val();
    parametros['telefono2'] = $('#telefono2').val();
    parametros['direccion'] = $('#direccion').val();
    parametros['email'] = $('#email').val();
//    debugger;
    if ($('#nombres').val()!=='' || $('#apellidos').val()!=='' || $('#identificacion').val()!==''){
        if (tipo == 1) {
            var metodo = "crear";
            var mensaje = "Cliente Creado con exito.";
        }
        else {
            var metodo = "editar";
            var mensaje = "Cliente Editado con exito.";
            parametros['id'] = $('#cliente_id').val();
        }

        $.ajax({
            url: "aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=" + metodo,
            type: 'post',
            data: {parametros: parametros},
            success: function (res) {
                var respuesta = $.parseJSON(res);
                if (respuesta['insert'] == 1)
                {

                    $.smallBox({
                        title: mensaje,
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    $('#editar_cliente').attr('style', 'background: #739e73;color: white;cursor:pointer;');
                    $('#cliente_nombre').val(parametros['nombres'] + " " + parametros['apellidos']);
                    $('#cliente_identificacion').val(parametros['identificacion']);
                    $('#cliente_id').val(respuesta['id']);
                    $('#frmClienteModal').modal('hide');
                    $('#form_clientes')[0].reset();
                    $("form#form_clientes :input[type=text]").each(function () {
                        $(this).attr('class', '');
                    });
                }
                else
                {
                    $.smallBox({
                        title: "Se genero un errror al insertar en la base de datos.",
                        color: "#F78181",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                }
            }
        });
    }
}
function crearClienteEventos() {

    var parametros = {};
    parametros['nombres'] = $('#nombres').val();
    parametros['apellidos'] = $('#apellidos').val();
    parametros['identificacion'] = $('#identificacion').val();
    parametros['telefono1'] = $('#telefono1').val();
    parametros['telefono2'] = $('#telefono2').val();
    parametros['direccion'] = $('#direccion').val();
    parametros['email'] = $('#email').val();

    $.ajax({
        url: "aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=crear",
        type: 'post',
        data: {parametros: parametros},
        success: function (res) {
            var respuesta = $.parseJSON(res);
            if (respuesta['insert'] == true) {
                $.smallBox({
                    title: "Creación..!",
                    content: "Cliente " + parametros['nombres'] + " " + parametros['apellidos'] + ", creado Correctamente...",
                    color: "#659265",
                    timeout: 7000,
                    icon: "fa fa-thumbs-up bounce animated"
                });
                $('#cliente').val(parametros['nombres'] + " " + parametros['apellidos']);
                $('#codigoCliente').val(respuesta['id']);
                $('#frmClienteModal').modal('hide');
            } else {
                $.smallBox({
                    title: "Error..!",
                    content: "Se genero un errror al insertar en la base de datos...",
                    color: "#C46A69",
                    timeout: 7000,
                    icon: "fa fa-bell swing animated"
                });
            }
        }
    });
}
function editarCliente() {
    var id = $('#cliente_id').val();
    if (id !== "")
    {
        $('#form_clientes').attr('action', 'javascript:crearCliente(2)');
        $.ajax({
            url: 'aplicacion/rutasMetodos.php?modulo=clientes&controlador=clientes&metodo=getClientePorId',
            datetype: "json",
            type: 'POST',
            data: {id: id},
            success: function (res) {
                var obj = $.parseJSON(res);
                $('#tipoDocumento option[value="' + obj.datosCliente.tipoDocumento + '"]').attr("selected", true);
                cambioCedulaPasaporteCliente();
                console.log(obj.datosCliente.identificacion);
                $("#pasaporte").val(obj.datosCliente.identificacion);
                $('#nombres').val(obj.datosCliente.nombres);
                ($('#nombres').val() === '') ? $('#nombre').attr('disable', true) : $('#nombre').attr('disable', false);
                $('#identificacion').val(obj.datosCliente.identificacion);
                ($('#identificacion').val() === '') ? $('#identificacion').attr('disable', true) : $('#identificacion').attr('disable', false);
                $('#apellidos').val(obj.datosCliente.apellidos);
                ($('#apellidos').val() === '') ? $('#apellido').attr('disable', true) : $('#apellido').attr('disable', false);
                $('#telefono1').val(obj.datosCliente.telefono1);
                ($('#telefono1').val() === '') ? $('#telefono1').attr('disable', true) : $('#telefono1').attr('disable', false);
                $('#telefono2').val(obj.datosCliente.telefono2);
                ($('#telefono2').val() === '') ? $('#telefono2').attr('disable', true) : $('#telefono2').attr('disable', false);
                $('#email').val(obj.datosCliente.email);
                ($('#email').val() === '') ? $('#email').attr('disable', true) : $('#email').attr('disable', false);
                $('#direccion').val(obj.datosCliente.direccion);
                ($('#direccion').val() === '') ? $('#direccion').attr('disable', true) : $('#direccion').attr('disable', false);
                $('#frmClienteModal').modal('show');
            }
        });

    }
}