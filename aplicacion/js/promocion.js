$(document).ready(function () {
    recargarPromocion();
   
    $("#productoServicio").autocomplete({
        source: 'aplicacion/rutasMetodos.php?modulo=servicios&controlador=servicios&metodo=getServicioxNombre',
        select: function (event, ui) {
            $('#codigo_productoServicio').val(ui.item.id);
            $('#productoServicio').val(ui.item.value);
            return false;
        }
    });
    var valMayor = $("#mayor").val();
    var $registerForm = $("#promocionForm").validate({
        rules: {
            nombre: {required: true},
            dia: {required: true},
            tipo_descuento: {required: true},
            valor: {required: true},
            mayor: {required: true},
            menor: {required: true},
            fechaInicio: {required: true},
            fechaFin: {required: true},
            productoServicio: {required: true},
            codigo_productoServicio: {required: true}
        }, errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });


});
function limpiarFormularioPromocion() {
    $("#IDPromocion").val('');
    $("#nombre").val('');  /* precio del servicio*/
    $("#valor").val('');  /*nombre*/
    $("#fechaInicio").val('');  /* precio del servicio*/
    $("#fechaFin").val('');  /*nombre*/
    $("#mayor").val('');  /* precio del servicio*/
    $("#menor").val('');  /*nombre*/
    $("#productoServicio").val('');  /*nombre*/
    $("#codigo_productoServicio").val('');  /*nombre*/
    $("#porcentaje").prop("checked", true);
    $('#dia').prop('selectedIndex', 0);
}
function nuevaPromocion() {
    limpiarFormularioPromocion();
    $('#frmPromocionesModal').modal('show');

}
function actualizarPromociones(id) {
    var parametros = {};
    parametros['codigoPromocion'] = id;
    $.ajax({
        url: 'aplicacion/rutasMetodos.php?modulo=promociones&controlador=promociones&metodo=enviarDatosPromociones',
        type: 'POST',
        data: {parametros: parametros},
        success: function (res) {
            var json_obj = $.parseJSON(res);
            console.log(json_obj);
            limpiarFormularioPromocion();
            carga_DatosIncialesPromociones(json_obj);
            $('#frmPromocionesModal').modal('show');
            $('#promocionForm >header').text('Actualización de Promociones');
        }
    });
}
function carga_DatosIncialesPromociones(edt) {
    $("#IDPromocion").val(edt.datosPromociones.IDPromociones); //id del servicio
    $("#nombre").val(edt.datosPromociones.nombre);  /*precio compra*/
    $("#valor").val(edt.datosPromociones.valor);  /*cod barras serv*/
    $("#fechaInicio").val(edt.datosPromociones.fechaInicio);  /* precio del servicio*/
    $("#fechaFin").val(edt.datosPromociones.fechaFin);  /*nombre*/
    $("#mayor").val(edt.datosPromociones.mayor);  /* precio del servicio*/
    $("#menor").val(edt.datosPromociones.menor);
    $("#productoServicio").val(edt.datosPromociones.servicio);  /*nombre*/
    $("#codigo_productoServicio").val(edt.datosPromociones.servicio_id);  /*nombre*/
    $("input[name=tipo_descuento][value='" + edt.datosPromociones.tipo_descuento + "']").prop("checked", true);
    $('#dia option[value="' + edt.datosPromociones.dia + '"]').attr("selected", true);
}
function guardarPromociones() {
    var parametros = {};
    parametros['idpromocion'] = $('#IDPromocion').val(),
            parametros['nombre'] = $('#nombre').val(),
            parametros['valor'] = $('#valor').val(),
            parametros['fechaInicio'] = $('#fechaInicio').val(),
            parametros['fechaFin'] = $('#fechaFin').val(),
            parametros['mayor'] = $('#mayor').val(),
            parametros['menor'] = $('#menor').val(),
            parametros['codigo_productoServicio'] = $('#codigo_productoServicio').val(),
            parametros['tipo_descuento'] = $('input[name=tipo_descuento]:checked', '#promocionForm').val(),
            parametros['dia'] = $('#dia').val();
    if (parametros['codigo_productoServicio'] !== '') {
        $('#codigo_productoServicio').parent('label').removeClass('state-error');
        if (parametros['idpromocion'] === '') {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=promociones&controlador=promociones&metodo=guardaPromociones',
                datetype: "json",
                type: 'POST',
                data: {parametros: parametros},
                success: function (res) {
                    if (res === '1') {
                        $('#frmPromocionesModal').modal('hide');
                        $.smallBox({
                            title: "Correcto..!",
                            content: "Promoción Guardada correctamente...",
                            color: "#659265",
                            timeout: 7000,
                            icon: "fa fa-thumbs-up bounce animated"
                        });
                        recargarPromocion();

                    } else {
                        $.smallBox({
                            title: "Error..!",
                            content: "Problemas al Ingresar los Datos, consulte con Sistemas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });
                    }
                }
            });
        } else {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=promociones&controlador=promociones&metodo=actualizarPromociones',
                datetype: "json",
                type: 'POST',
                data: {parametros: parametros},
                success: function (res) {
                    if (res === '1') {
                        $('#frmPromocionesModal').modal('hide');
                        $.smallBox({
                            title: "Correcto..!",
                            content: "Promoción Actualizada correctamente...",
                            color: "#659265",
                            timeout: 7000,
                            icon: "fa fa-thumbs-up bounce animated"
                        });
                        recargarPromocion();
                    }
                }
            });
        }
    } else {
        $('#codigo_productoServicio').parent('label').addClass('state-error');
        $.smallBox({
            title: "Error..!",
            content: "El Servicio no es valido",
            color: "#C46A69",
            timeout: 7000,
            icon: "fa fa-bell swing animated"
        });
    }
}
function eliminarPromociones(id) {
    $.SmartMessageBox({
        title: "Espere..!",
        content: "Esta seguro de eliminar esta Promoción?",
        buttons: '[Aceptar][Cancelar]'
    }, function (ButtonPressed) {
        if (ButtonPressed === "Aceptar") {
            $.ajax({
                url: 'aplicacion/rutasMetodos.php?modulo=promociones&controlador=promociones&metodo=eliminarPromociones',
                datetype: "json",
                type: 'POST',
                data: {idpromo: id},
                success: function (res) {
                    if (res === '1') {
                        recargarPromocion();
                        $.smallBox({
                            title: "Dato Eliminado",
                            content: "<i class='fa fa-clock-o'></i> <i>Promoción eliminada correctamente...</i>",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });

                    } else {
                        $.smallBox({
                            title: "Error..!",
                            content: "Problema para Eliminar el tipo de Pago, consulte con Sistemas...",
                            color: "#C46A69",
                            timeout: 7000,
                            icon: "fa fa-bell swing animated"
                        });
                    }
                }
            });
            recargarPromocion();
        }
    });
}
function recargarPromocion() {
    var dtTable = $('#listaPromociones').dataTable({
        "bDestroy": true,
        "bRetrieve": true,
        "bStateSave": true,
        "bPaginate": true,
        "bServerSide": true,
        "sAjaxSource": "aplicacion/controladores/promociones/dataTable.php",
        "oLanguage": {
            "sEmptyTable": "No hay datos disponibles en la tabla",
            "sInfo": "Existen _TOTAL_ registros en total, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay entradas para mostrar",
            "sInfoFiltered": " - Filtrado de registros _MAX_",
            "sZeroRecords": "No hay registros que mostrar"
        }
    });
    dtTable.fnReloadAjax();
}
