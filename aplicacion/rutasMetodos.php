<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day

$modulo = isset($_GET['modulo']) ? $_GET['modulo'] : 'ninguno';
$controlador = isset($_GET['controlador']) ? $_GET['controlador'] : 'ninguno';
$metodo = isset($_GET['metodo']) ? $_GET['metodo'] : 'ninguno';
$parametros=$_POST['parametros'];
include_once 'controladores/' . $modulo . '/' . $controlador . '.php';
$clase = new $controlador;
echo $clase->$metodo($parametros);
