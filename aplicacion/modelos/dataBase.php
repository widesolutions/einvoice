<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/einvoice/config.php';


class database {

    function database() {
        $this->db = new mysqli(HOST_NAME, USER_NAME, USER_PASSWD, DB_NAME);        
        $this->db->query("SET NAMES 'utf8'");
        if ($this->db->connect_error) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        return $this->db;
    }

    function query($query) {
        $obj = $this->db->query($query);
        return $obj;
    }

    public function num_rows($query_object) {
        $num = mysql_num_rows($query_object);
        return $num;
    }

    function lastid() {

        return $this->db->insert_id;
    }
    
}
