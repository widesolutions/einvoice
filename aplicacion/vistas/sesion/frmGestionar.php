<style>
    .ui-autocomplete {
        z-index: 2000 !important;
    }
</style>
<div class="modal fade" id="frmGestionarInfante" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Infantes registrados</h4>
            </div>
            <div class="modal-body">
                <input id="cliente_id" type="hidden">
                <input id="sesion_id" type="hidden">
                
                <div class="row" id="botones_ifantes">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <a style="width: 99px;" href="javascript:nuevoInfante();" class="btn btn-labeled btn-success"> <span class="btn-label"><i class="fa fa-plus-square"></i></span>Nuevo </a>                            

                            <a style="width: 101px;" href="javascript:buscarInfante();" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="fa fa-search"></i></span>Buscar </a>

                            <a style="width: 107px;" href="javascript:historialSesiones();" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="fa fa-book"></i></span>Historial </a>
                        </div>
                    </div>
                </div>                 
                <table class="table" id="tabla_detalles_infantes">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Servicio</th>
                            <th>Hora Inicio</th>
                            <th>Acciones </th>
                        </tr>
                    </thead>

                </table>     
                <div class="panel panel-default" id="nuevo">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed" aria-expanded="false"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> Agregar un infante </a></h4>
                    </div>
                    <div id="collapseThree-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form id="form_infantes"  novalidate="novalidate" class="smart-form" action="javascript:crearInfante()" >
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="primer_nombre" id="primer_nombre" placeholder="Primer nombre">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="segundo_nombre" id="segundo_nombre" placeholder="Segundo nombre">
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="primer_apellido" id="primer_apellido" placeholder="Primer apellido">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo apellido">
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="Fecha de nacimiento" >
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="input"> <i class="icon-append fa fa-institution"></i>
                                                <input type="text" name="institucion_educativa" id="institucion_educativa" placeholder="Institucion educativa">
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">

                                        <section class="col col-6">
                                            <label class="select">
                                                <select id="servicio_nuevo" name="servicio_nuevo">

                                                </select> <i></i> 
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>  
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Guardar y agregar
                                    </button>
                                </footer>                    
                            </form>                
                        </div>
                    </div>
                </div>  
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>

            </div>

        </div>
    </div>
</div>

<div style="display:none">
    <div id="servicios">
        <select id="servicios_infantes">

        </select>
    </div>

    <div class="panel panel-default" id="historial">
        <div class="panel-heading">
            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed" aria-expanded="false"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> Historial de sesiones </a></h4>
        </div>
        <div id="collapseThree-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <table class="table" id="tabla_detalles_historial">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Servicio</th>
                            <th>Seleccionar</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>                
    </div>
    <div class="panel panel-default" id="buscar">
        <div class="panel-heading">
            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed" aria-expanded="false"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> Buscar un infante </a></h4>
        </div>
        <div id="collapseThree-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <form class="smart-form">
                    <section>
                        <label class="label">Buscar Infante</label>
                        <label class="input"> <i class="icon-append fa fa fa-search"></i>
                            <input type="text" placeholder="Información del infante" id="search_infante" onkeyup="searchInfantes(this.value)">
                        </label>
                    </section>
                </form>
                <table class="table" id="tabla_buscar_detalles_infantes">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Servicio</th>
                            <th>Seleccionar</th>
                        </tr>
                    </thead>
                </table>            
            </div>
        </div>
    </div>  
</div>
<div class="modal fade" id="frmGestionarAdicionales" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Adicionales registrados</h4>
            </div>
            <div class="modal-body" id="panel_adicionales">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-arrow-circle-up"></i></span>
                                <input class="form-control input-lg" placeholder="Cantidad" type="number" name="cantidad_adicional" id="cantidad_adicional">

                            </div>
                        </div>
                    </div>                     
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                                <input class="form-control input-lg" placeholder="Descripción" type="text" name="nombre_adicional" id="nombre_adicional">
                                <input id="id_adicional" name="id_adicional" type="hidden">
                                <input id="id_sesion" name="id_sesion" type="hidden">


                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <a style="width: 37px;height: 44px;" href="javascript:agregarAdicional();" class="btn btn-labeled btn-success"> <span class="btn-label" style="height: 44px;padding-top: 12px;"><i class="fa fa-plus-square"></i></span></a>                            

                        </div>
                    </div>
                </div>                 

                <table class="table" id="tabla_detalles_adicionales">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Cantidad</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>                    
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>

            </div>

        </div>
    </div>
</div>

