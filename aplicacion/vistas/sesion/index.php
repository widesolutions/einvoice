<div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" role="widget">
    <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2>Sesiones activas </h2>

        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
        <style>

            @media screen {
                .imprime {
                    display: none;
                }
            }

            @media print {
                .imprime {
                    display: block;
                }
            }   

            .rp {
                width: 110px;
                float: left;
                text-align: center;
                padding: 23px;
            }               
        </style>
    </header>
    <script src="aplicacion/js/sesiones/gestion.js" defer="defer"></script>
    <script src="aplicacion/js/cliente.js" defer="defer"></script>

    <script src="aplicacion/js/jQuery.print.js" defer = "defer" ></script>
    <input id="cliente_redirect" name="cliente_redirect" type="hidden" value="<?php echo $_GET['redirect'] ?>">
    <div role="content">

        <table id="sesiones" class="table table-striped table-bordered table-hover dataTable no-footer" >
            <thead>			                
                <tr role="row">
                    <th>
                        <i class="fa fa-fw fa-user"></i> Código 
                    </th>                    
                    <th>
                        <i class="fa fa-fw fa-user"></i> Nombre del cliente 
                    </th>
                    <th>
                        <i class="fa fa-fw fa-phone"></i> Teléfono del cliene 
                    </th>
                    <th>
                        <i class="fa fa-fw fa-group"></i> Infantes 
                    </th>  
                    <th>
                        <i class="fa fa-fw fa-list"></i> Adicionales 
                    </th> 
                    <th>
                        <i class="fa fa-trash-o fa-lg"></i> Cancelar 
                    </th>                     
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/sesion/frmGestionar.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/clientes/frmClienteSesiones.php';

