<div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" role="widget">
    <!-- widget options:
    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

    data-widget-colorbutton="false"
    data-widget-editbutton="false"
    data-widget-togglebutton="false"
    data-widget-deletebutton="false"
    data-widget-fullscreenbutton="false"
    data-widget-custombutton="false"
    data-widget-collapsed="true"
    data-widget-sortable="false"

    -->
    <style>
        #s2id_servicios_infantes 
        {
            width: 262px !important;

        }

        .select2-choice{
            height: 44px !important;
            padding-top: 7px !important;
        }        

        .select2-drop {
            width: 262px !important;
        }

        .rp {
            width: 230px;
            float: left;
            text-align: center;
            padding: 23px;
        }        


        @media screen {
            #impresiones {
                    display: none;
            }
        }

        @media print {
            #impresiones {
                display: block;
            }
        }        
    </style>    
    <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> </div><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
        <span class="widget-icon"> <i class="fa fa-check"></i> </span>
        <h2>Nueva Sesión</h2>

        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

    <!-- widget div-->
    <div role="content">

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->

        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body">

            <div class="row">
                <form id="wizard-1"  >
                    <div id="bootstrap-wizard-1" class="col-sm-12">
                        <div class="form-bootstrapWizard">
                            <ul class="bootstrapWizard form-wizard">
                                <li class="active" data-target="#step1">
                                    <a href="#tab1" data-toggle="tab" aria-expanded="true"> <span class="step">1</span> <span class="title">Registro del cliente</span> </a>
                                </li>
                                <li data-target="#step2" class="">
                                    <a href="#tab2" data-toggle="tab" aria-expanded="false"> <span class="step">2</span> <span class="title">Registro de infantes</span> </a>
                                </li>
                                <li data-target="#step3" class="">
                                    <a href="#tab3" data-toggle="tab" aria-expanded="false"> <span class="step">3</span> <span class="title">Registro de adicionales</span> </a>
                                </li>
                                <li data-target="#step4" class="">
                                    <a href="#tab4" data-toggle="tab" aria-expanded="false"> <span class="step">4</span> <span class="title">Crear la sesión</span> </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <br>
                                <h3><strong>Paso 1 </strong> - Registro del cliente</h3>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <a style="width: 99px;" href="javascript:nuevoCliente();" class="btn btn-labeled btn-success"> <span class="btn-label"><i class="fa fa-plus-square"></i></span>Nuevo </a>                            

                                            <a style="width: 101px;" href="javascript:buscarCliente();" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="fa fa-search"></i></span>Buscar </a>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" onclick="editarCliente()" id="editar_cliente" style="cursor: pointer;"><i class="fa fa-edit"></i></span>

                                                <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="Nombre del cliente" type="text" name="cliente_nombre" id="cliente_nombre" readonly="" >

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-indent"></i></span>
                                                <input class="form-control input-lg" placeholder="Identificación del cliente" type="text" name="cliente_cedula" id="cliente_identificacion" readonly="">
                                                <input type="hidden" id="cliente_id" name="cliente_id" value="">  
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="tab2">
                                <br>
                                <h3><strong>Paso 2</strong> - Registro de infantes</h3>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <a style="width: 99px;" href="javascript:nuevoInfante();" class="btn btn-labeled btn-success"> <span class="btn-label"><i class="fa fa-plus-square"></i></span>Nuevo </a>                            

                                            <a style="width: 101px;" href="javascript:buscarInfante();" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="fa fa-search"></i></span>Buscar </a>

                                            <a style="width: 107px;" href="javascript:historialSesiones();" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="fa fa-book"></i></span>Historial </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" onclick="editarInfante()" id="editar_infante" style="cursor: pointer;"><i class="fa fa-edit"></i></span>
                                                <span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="Nombre del infante" readonly="readonly" type="text" name="nombre_infante" id="nombre_infante">
                                                <input type="hidden" id="id_infante">    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select  id="servicios_infantes">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <a style="width: 37px;height: 44px;" href="javascript:agregarInfante();" class="btn btn-labeled btn-success"> <span class="btn-label" style="height: 44px;padding-top: 12px;"><i class="fa fa-plus-square"></i></span></a>                            
                                        </div>
                                    </div>
                                </div>       
                                <header role="heading"><div class="jarviswidget-ctrls" role="menu" >   </div>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    Lista de infantes 

                                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                </header>

                                <table id="lista_infantes" class="table table-striped table-bordered table-hover dataTable no-footer" width="100%" role="grid" aria-describedby="dt_basic_info" style="width: 100%;">
                                    <thead>			                
                                        <tr role="row">
                                            <th class="sorting_asc" >ID</th>
                                            <th ><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Nombre del infante</th>
                                            <th ><i class="fa fa-fw fa-calendar-o text-muted hidden-md hidden-sm hidden-xs"></i> Fecha de nacimiento</th>
                                            <th ><i class="fa fa-fw fa-list text-muted hidden-md hidden-sm hidden-xs"></i> Servicio</th>
                                            <th ><i class="fa fa-fw fa-remove text-muted hidden-md hidden-sm hidden-xs"></i> Eliminar</th>

                                        </tr>
                                    </thead> 
                                    <tbody></tbody>
                                </table>                                
                            </div>
                            <div class="tab-pane" id="tab3">
                                <br>
                                <h3><strong>Paso 3</strong> - Registro de adicionales</h3>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                                                <input class="form-control input-lg" placeholder="Descripción" type="text" name="nombre_adicional" id="nombre_adicional">
                                                <input id="id_adicional" name="id_adicional" type="hidden">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-arrow-circle-up"></i></span>
                                                <input class="form-control input-lg" placeholder="Cantidad" type="number" name="cantidad_adicional" id="cantidad_adicional">

                                            </div>
                                        </div>
                                    </div>                                     
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <a style="width: 37px;height: 44px;" href="javascript:agregarAdicional();" class="btn btn-labeled btn-success"> <span class="btn-label" style="height: 44px;padding-top: 12px;"><i class="fa fa-plus-square"></i></span></a>                            

                                        </div>
                                    </div>
                                </div>    

                                <header role="heading"><div class="jarviswidget-ctrls" role="menu" >   </div>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    Lista de adicionales

                                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                </header>

                                <table id="lista_adicionales" class="table table-striped table-bordered table-hover dataTable no-footer" width="100%" role="grid" aria-describedby="dt_basic_info" style="width: 100%;">
                                    <thead>			                
                                        <tr role="row">
                                            <th class="sorting_asc" >ID</th>
                                            <th ><i class="fa fa-fw fa-shopping-cart text-muted hidden-md hidden-sm hidden-xs"></i> Descripción</th>
                                            <th ><i class="fa fa-arrow-circle-up text-muted hidden-md hidden-sm hidden-xs"></i> Cantidad</th>
                                            <th ><i class="fa fa-fw fa-remove text-muted hidden-md hidden-sm hidden-xs"></i> Eliminar</th>

                                        </tr>
                                    </thead> 
                                </table>                                  
                            </div>
                            <div class="tab-pane" id="tab4">
                                <br>
                                <h3 id="paso4"><strong>Paso 4</strong> - Crear Sesión</h3>
                                <br>
                                <h1 id="titulo_completo" class="text-center text-success" ><strong><i class="fa fa-check fa-lg"></i> Completo!</strong></h1>
                                <h4 id="mensaje_completo" class="text-center" >Click en siguiente para Iniciar!!</h4>

                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-greenLight jarviswidget-sortable" id="lista_codigos" data-widget-editbutton="false" role="widget" style="width: 43%;margin: auto;display: none">

                                    <!-- widget div-->
                                    <div role="content">

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body no-padding">

                                            <div class="alert alert-info no-margin fade in text-center">
                                                <button class="close" data-dismiss="alert">
                                                    ×
                                                </button>
                                                <i class="fa-fw fa fa-info"></i>
                                                Imprime todos las manillas para la seguridad de los niños  <a href="javascript:imprimirTodasManillas();" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="fa fa-print"></i></span>Imprimir Todas</a>
                                            </div>
                                            <div class="table-responsive">

                                                <table class="table" id="codigos">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center;">  Código</th>
                                                            <th style="text-align: center;"> <i class="fa fa-user"></i> Nombre</th>
                                                            <th style="text-align: center;"> <i class="fa fa-print"></i> Imprimir</th>
                                                        </tr>
                                                    </thead>

                                                </table>



                                            </div>
                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <div id="impresiones" style="width: 280px;display:none;" >
                                </div>
                                <!-- end widget -->

                                <br>
                                <br>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="pager wizard no-margin">
                                            <!--<li class="previous first disabled">
                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                            </li>-->
                                            <li class="previous disabled">
                                                <a href="javascript:void(0);" class="btn btn-lg btn-default"> Atras </a>
                                            </li>
                                            <!--<li class="next last">
                                            <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                            </li>-->
                                            <li class="next">
                                                <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Siguiente </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>

        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
<script src="aplicacion/js/sesiones/sesion.js" defer="defer"></script>
<script src="aplicacion/js/cliente.js" defer="defer"></script>
<script src="aplicacion/js/infante.js" defer="defer"></script>
<script src="aplicacion/js/jQuery.print.js" defer = "defer" ></script>



<?php
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/clientes/frmClienteSesiones.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/infantes/frmInfante.php';
