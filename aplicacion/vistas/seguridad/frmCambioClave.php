<div class="modal fade" id="frmCambioClaveModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Registro</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <form id="cambioClave" class="smart-form" action="javascript:GuardarCambioClaveUsuario()" autocomplete="off">
                                <header>
                                    Cambio de Contraseña
                                </header>
                                <fieldset>
                                    <input type="hidden" id="IDuser" name="IDuser">
                                    <section>
                                        <label>Nueva Contraseña</label>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password2" placeholder="Contraseña" id="password2">
                                            <b class="tooltip tooltip-bottom-right">Debe ingresar una Contraseña de mínimo 8 y Máximo 20 Caracreres</b> </label>
                                    </section>
                                    <section>
                                        <label>Confirmar Nueva Contraseña</label>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="passwordConfirm2"  name="passwordConfirm2" placeholder="Confirmar Contraseña">
                                            <b class="tooltip tooltip-bottom-right">Debe ser la misma contraseña del campo anterior</b> </label>
                                    </section>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Actualizar
                                    </button>
                                </footer>
                            </form>						
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>