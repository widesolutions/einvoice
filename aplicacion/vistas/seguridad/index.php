<article class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevoUsuario()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Usuario
            </button>
        </fieldset>
    </div>
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2>Usuarios del Sistema </h2>

        </header>
        <div>
            <div class="widget-body no-padding">
                <table id="listaUsuarios" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> Nombre</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Usuario</th>
                            <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> E-Mail</th>
                            <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Tipo Usuario</th>
                            <th><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</article>
<script src="aplicacion/js/usuario.js" defer="defer"></script>
<?php
include 'frmUsuario.php';
include 'frmCambioClave.php';
