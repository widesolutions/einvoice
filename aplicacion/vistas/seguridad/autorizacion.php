<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/usuario/usuario.php");
$usuarios=new Usuario();
?>
<div id="dialog-autoriza" title="Dialog Simple Title">
    <p>Para realizar esta operación es necesaria la clave del 'ADMINISTRADOR'</p>
    <div class="hr hr-12 hr-double"></div>
    <div id="addtab" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 0px; max-height: none; height: auto;">
        <form id="autorizacionClave" class="smart-form" autocomplete="off">
            <fieldset>
                <input type="hidden" name="token" id="token" value="">
                <input type="hidden" name="parametros" id="parametros" value="">
                    <label><b>Seleccione el Usuario:</b></label>
                    <label class="select">
                        <select name="usuario" id="usuario">
                            <option disabled="" selected="" value="0">-- Seleecione --</option>
                            <?php $val=$usuarios->getUsuariosAdministradores();
                                while($row = $val->fetch_object()){?>
                                    <option value="<?php echo $row->usuario?>"><?php echo $row->nombre?></option>
                            <?php }?>
                        </select>
                        <i></i>
                    </label>
                <div class="form-group">
                    <label><b>Escriba la Contraseña:</b></label>
                    <label class="input">
                        <input id="claveAdmin" name="claveAdmin" class="form-control" type="password" placeholder="Clave del Usuario" value="">
                    </label>
                </div>
            </fieldset>
        </form>
    </div>
</div>