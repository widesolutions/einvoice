<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/krayon/aplicacion/controladores/usuario/usuario.php';
$usuario = new Usuario();
?>
<div class="modal fade" id="frmUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Registro </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <form id="frmUsuario" class="smart-form" action="javascript:guardarUsuario()">
                                <header>
                                    Formulario de Registro
                                </header>
                                <fieldset>
                                    <input type="hidden" id="IDuser" name="IDuser">
                                    <section>
                                        <label>Usuario</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" id="usuario" name="usuario" placeholder="Usuario">
                                            <b class="tooltip tooltip-bottom-right">Necesario para Registrar al Usuario, Mínimo 6 Caracteres</b> </label>
                                    </section>
                                    <section>
                                        <label>Email</label>
                                        <label class="input"> <i class="icon-append fa fa-envelope-o"></i>
                                            <input type="email" id="email" name="email" placeholder="E-mail">
                                            <b class="tooltip tooltip-bottom-right">Necesario para Validar la Cuenta</b> </label>
                                    </section>
                                    <section id="clave">
                                        <label>Contraseña</label>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password" placeholder="Password" id="password">
                                            <b class="tooltip tooltip-bottom-right">Debe ingresar una Contraseña de mínimo 8 y Máximo 20 Caracreres</b> </label>
                                    </section>
                                    <section  id="confirmaClave">
                                        <label>Confirmar Contraseña</label>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="passwordConfirm"  name="passwordConfirm" placeholder="Confirmar Password">
                                            <b class="tooltip tooltip-bottom-right">Debe ser la misma contraseña del campo anterior</b> </label>
                                    </section>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Nombre</label>
                                            <label class="input">
                                                <input type="text" id="nombre" name="nombre" placeholder="Nombre">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Apellido</label>
                                            <label class="input">
                                                <input type="text" id="apellido" name="apellido" placeholder="Apellido">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Celular</label>
                                            <label class="input">
                                                <input type="telf" id="celular" name="celular" placeholder="Teléfono Celular">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Cédula</label>
                                            <label class="input">
                                                <input type="telf" id="cedula" name="cedula" placeholder="Número de Cédula">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Tipo de Usuario</label>
                                            <label class="select">
                                                <select id="tipoUsuario" name="tipoUsuario">
                                                    <option value="0" selected="" disabled="">-- Tipo Usuario --</option>
                                                    <?php echo $usuario->comboTipoUsuario() ?>
                                                </select> 
                                                <i></i> 
                                            </label>
                                        </section>
                                    </div>	
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Registrar
                                    </button>
                                </footer>
                            </form>						
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>