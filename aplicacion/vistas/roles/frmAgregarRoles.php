<div class="modal fade" id="frmRolesModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de registro</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <form id="smart-form-Roles" class="smart-form" action="javascript:guardarRol()">
                                <header>
                                    Formulario de Registro
                                </header>
                                <fieldset>
                                    <input type="hidden" id="IDrol" name="IDrol">
                                    <section>
                                        <label class="label">Descripción</label>
                                        <label class="input"> <i class="icon-append fa fa-envelope-o"></i>
                                            <input type="text" id="descripcion" name="descripcion" placeholder="Descripción del Rol">
                                            <b class="tooltip tooltip-bottom-right">Necesario para identificar el Pabellon</b> </label>
                                    </section>
                                    <section>
                                        <label class="label">Observaciones</label>
                                        <label class="textarea"> <i class="icon-append fa fa-comment"></i>
                                            <textarea id="observacion"  name="observacion" placeholder="Observaciones"></textarea>
                                    </section>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Agregar
                                    </button>
                                </footer>
                            </form>						
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>