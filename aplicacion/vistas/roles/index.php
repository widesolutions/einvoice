<?php
session_start();
date_default_timezone_set('America/Bogota');
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/modelos/dataBase.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/roles/roles.php");
$dbmysql = new database();
$roles = new roles();
?>
<article class="col-sm-12 col-md-12 col-lg-6">
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false">
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2>Roles o Perfiles de usuario</h2>
            <div class="widget-toolbar">
                <div class="btn-group">
                    <button class="btn btn-xs btn-success btn-personal" data-toggle="modal" onclick="javascript:nuevoRol()">
                        <i class="fa fa-fw fa-plus"></i>  Agregar Rol
                    </button>
                </div>
            </div>
        </header>
        <div>
            <div class="jarviswidget-editbox">
            </div>
            <div class="widget-body no-padding">
                <div class="table-responsive">
                    <table id="listaRoles" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descripción</th>
                                <th>Observación</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody><?php
                            $val_s = $roles->listarRoles();
                            while ($row = $val_s->fetch_object()) {
                                $cadenaParametros = utf8_encode($row->id . ',' . "'$row->descripcion'");
                                if ($row->id != 1) {
                                    $observacion=(strlen($row->observacion)<=40)?$row->observacion:substr($row->observacion,0,40).'...';
                                    ?>
                                    <tr class="tablaRolesDetalle" id="<?php echo $row->id ?>" onclick="javascript:mostrarPermisosUsuario(<?php echo $row->id ?>)">
                                        <td><?php echo $row->id ?></td>
                                        <td><?php echo $row->descripcion ?></td>
                                        <td><?php echo $observacion ?></td>
                                        <td style="vertical-align: middle;">
                                            <div style="display: inline-flex;"><a class="btn btn-success btn-xs" title="Editar Rol" href="javascript:editarRol(<?php echo $row->id ?>)">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-xs <?php echo $row->id ?>  eliminaRol" title="Eliminar Rol" href="javascript:eliminarRol(<?php echo $cadenaParametros ?>)">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div></td>
                                    </tr>
                                    <?php
                                } else {
                                    if ($_SESSION["usu_rol_cod"] == 1) {
                                        ?>
                                        <tr class="tablaRolesDetalle" id="<?php echo $row->id ?>" onclick="javascript:mostrarPermisosUsuario(<?php echo $row->id ?>)">
                                            <td><?php echo $row->id ?></td>
                                            <td><?php echo $row->descripcion ?></td>
                                            <td><?php echo $row->observacion ?></td>
                                            <td><a class="btn btn-success btn-xs" title="Editar Rol" href="javascript:editarRol(<?php echo $row->id ?>)">
                                                    <i class="fa fa-pencil"></i>
                                                </a>

                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</article>
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" style="overflow-y: auto; height: 335px">
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2>Listado de permisos por Rol</h2>
            <div class="widget-toolbar">
                <div class="btn-group">
                    <button class="btn btn-xs btn-success btn-personal" data-toggle="modal" onclick="javascript:nuevoPermiso()">
                        <i class="fa fa-fw fa-plus"></i>  Agregar Permiso
                    </button>
                </div>
            </div>
        </header>
        <div>
            <div class="widget-body no-padding">
                <input type="hidden" id="IDRol" name="IDRol">
                <table id="tbPermisosUsuarios" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr id="tablaHorarios">
                            <th width="45px">Asignado</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</article>
<script src="aplicacion/js/roles.js" defer="defer"></script>
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/roles/frmAgregarPermisos.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/roles/frmAgregarRoles.php';
