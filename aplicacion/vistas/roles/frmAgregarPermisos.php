<div class="modal fade" id="frmPermisosModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Lista de Permisos</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <form id="smart-form-permisos" class="smart-form" action="javascript:guardarAsignaPermisos()">
                                <header>
                                    Formulario de Registro
                                </header>
                                <fieldset>
                                    <table id="tbPermisosDisponibles" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr id="tablaPermisos">
                                                <th width="45px">
                                                    Asignar
                                                </th>
                                                <th>Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Asignar
                                    </button>
                                </footer>
                            </form>						
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>