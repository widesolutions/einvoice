<div class="modal fade" id="frmCierreModal" tabindex="-1" role="dialog" aria-labelledby="ClienteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Detalle de cierre de caja</h4>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-money"></i> </span>
                        <h2>Detalles del cierre de caja</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="widget-body" id="detalles_cierre">
                                <table class="table table-hover" id="detalles_cierre_caja">
                                    <thead>
                                        <tr>
                                            <th >RUBRO</th>
                                            <th>TOTAL</th>
                                        </tr>
                                    </thead>

                                </table> 


                            </div>
                        </div>
                    </div>
                    <div class="widget-footer" style="text-align: center;height: 57px;">

                        <button type="submit"class="btn btn-danger" onclick="javascript:ejecutarCierreCaja()" style="text-align: center;">
                            Cerrar Caja
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
