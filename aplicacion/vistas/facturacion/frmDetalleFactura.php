<div class="modal fade" id="frmDetalleFacturaModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <form id="frmlistaDetalle" method="post" class="form-horizontal bv-form" novalidate="novalidate">
                        <fieldset>
                            <header>
                                <legend>DATOS CLIENTE</legend>
                                <table id="cabeceraDetalle" class="table table-striped table-bordered table-hover"   width="100%">
                                    <tr><td>No. Factura: </td><td><spam id="nfactura"></spam></td></tr>
                                    <tr><td>Fecha: </td><td><spam id="fecha"></spam></td></tr>
                                    <tr><td>Cliente: </td><td><spam id="cliente"></spam></td></tr>
                                    <tr><td>Ruc/Ci: </td><td><spam id="identificacion"></spam></td></tr>
                                </table>
                            </header>
                                <table id="listaDetalle" class="table table-striped table-bordered table-hover"   width="100%">
                                    <thead>
                                        <tr>
                                            <!--<th>ID</th>-->
                                            <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i>Cantidad</th>
                                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Detalle</th>
                                            <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <table id="cabeceraDetalle" class="table table-striped table-bordered table-hover"   width="100%">
                                    <tr><td style="text-align: right;width: 450px;">Sub Total 12: </td><td><spam id="subtotal"></spam></td></tr>
                                    <tr><td style="text-align: right;">Sub Total 0: </td><td><spam id="subsiniva"></spam></td></tr>
                                    <tr><td style="text-align: right;">Iva: </td><td><spam id="iva"></spam></td></tr>
                                    <tr><td style="text-align: right;">Total: </td><td><spam id="total"></spam></td></tr>
                                </table>
                            <div class="form-group has-feedback">
                                <div align="center" class="col-md-5">
                                    <table id="listaDetallePago" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i>FORMA DE PAGO</th>
                                                <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i>VALOR</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </fieldset>              
                    </form>


                </div>                
            </div>
        </div>
    </div>

</div>


