<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/facturas/facturas.php");
$facturas = new facturas();
date_default_timezone_set('America/Guayaquil');
?>
<style>#main {min-height: 800px;}</style>
<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">        
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>GESTOR DE FACTURAS </h2>
        </header>
        <div>
            <table id="listaFacturasGestion" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        <th><i class="fa fa-fw fa-slack txt-color-blue hidden-md hidden-sm hidden-xs"></i>No Factura</th>
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Cliente</th>
                        <th><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Fecha</th>
                        <th><i class="fa fa-fw fa-dollar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Valor</th>
                        <th><i class="fa fa-fw fa-unlock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Acciones</th>                                        
                    </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>
    </div>
</article>
<script src="aplicacion/js/factura.js" defer="defer"></script>
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/facturacion/frmDescuento.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/clientes/frmClienteFacturacion.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/facturacion/frmFormaPago.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/facturacion/frmDetalleFactura.php';
