<?php

header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
date_default_timezone_set('America/Guayaquil');
$idCliente = $_GET['idCliente'];
$codMovimiento = $_GET['mov'];
$sesion=$_GET['sesion'];
$infantes='';
require($_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php');
require($_SERVER['DOCUMENT_ROOT'] . '/krayon/publico/js/libs/fpdf/pdf_js.php');
$dbmysql = new database();
class PDF_AutoPrint extends PDF_JavaScript {

    function AutoPrint($dialog = false) {
        //Open the print dialog or start printing immediately on the standard printer
        $param = ($dialog ? 'true' : 'false');
        $script = "print($param);";
//        $script .= "$('#frmImpresionModal').modal('hide')";
        $this->IncludeJS($script);
    }

    function AutoPrintToPrinter($server, $printer, $dialog = false) {
        //Print on a shared printer (requires at least Acrobat 6)
        $script = "var pp = getPrintParams();";
        if ($dialog)
            $script .= "pp.interactive = pp.constants.interactionLevel.full;";
        else
            $script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
        $script .= "pp.printerName = '\\\\\\\\" . $server . "\\\\" . $printer . "';";
        $script .= "print(pp);";
//        $script .= "$('#frmImpresionModal').modal('hide')";
        $this->IncludeJS($script);
        
        
    }

}

$hora = date('H:i');
$fecha = date('Y-m-d');
$fechaHora = date('Y-m-d H:i');
$sqlMov = "SELECT m.*,u.nombre  FROM movimientos m,usuarios u WHERE m.`usuario_id`=u.id AND m.id=$codMovimiento";
$valMov = $dbmysql->query($sqlMov);
$rowMov = $valMov->fetch_object();
//CONSULTA DATS CLIENTES
$sqlCliente = "SELECT * FROM clientes WHERE id=$idCliente;";
$val = $dbmysql->query($sqlCliente);
$rowCliente = $val->fetch_object();
if($rowCliente==NULL){
    $rowCliente= array(
        "nombres"=>'CLIENTE',
        "apellidos"=>'FINAL',
        "id"=>'1',
        "identificacion"=>'999999999999',
        "telefono1"=>'(02)0000000',
        "direccion"=>'ninguna'
    );
}

$pdf = new PDF_AutoPrint();
$pdf->AddPage();
$pdf->SetFont('Courier', 'B', 12);
//        $pdf->MultiCell(0,10,$contenido);
//$pdf->SetMargins(5,0,0);
//CABECERA FACTURA
$pdf->Cell(25, 6, 'Nombre: ', 0, 0, 'L');
$pdf->Cell(50, 6, utf8_decode($rowCliente->nombres . ' ' . $rowCliente->apellidos), 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'Fecha: ', 0, 0, 'L');
$pdf->Cell(50, 6, $fechaHora, 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'Codigo: ', 0, 0, 'L');
$pdf->Cell(50, 6, $rowCliente->id, 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, 'Ruc/Ci: ', 0, 0, 'L');
$pdf->Cell(50, 6, $rowCliente->identificacion, 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, utf8_decode('Teléfono: '), 0, 0, 'L');
$pdf->Cell(50, 6, ($rowCliente->telefono1!=='')?$rowCliente->telefono1:$rowCliente->telefono2, 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 6, utf8_decode('Direccón: '), 0, 0, 'L');
$pdf->Cell(50, 6, utf8_decode($rowCliente->direccion), 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(75, 6, '=============================', 0, 0, 'L');
$pdf->Ln();
//DETALLE FACTURA
$pdf->Cell(15, 6, 'Cant. ', 0, 0, 'L');
$pdf->Cell(40, 6, 'Detalle', 0, 0, 'L');
$pdf->Cell(20, 6, 'Valor', 0, 0, 'C');
$pdf->Ln();
$sqlDetMov = "SELECT * FROM `detalles_movimientos` WHERE movimiento_id=$codMovimiento;";
$valDetMov = $dbmysql->query($sqlDetMov);
while ($rowDetMov = $valDetMov->fetch_object()) {
    $rowDetMov->producto_id;
    $tabla = ($rowDetMov->producto_id !== null) ? 'productos' : 'servicios';
    $campo = ($rowDetMov->producto_id !== null) ? 'producto_id' : 'servicio_id';
    $campo = $rowDetMov->$campo;
    $sql = "SELECT * FROM $tabla WHERE id='$campo'";
    $val = $dbmysql->query($sql);
    $row = $val->fetch_object();
    $nombreProducto = (strlen($row->nombre) > 12) ? substr($row->nombre, 0, 13) : $row->nombre;
    $pdf->Cell(15, 6, $rowDetMov->cantidad, 0, 0, 'C');
    $pdf->Cell(40, 6, utf8_decode($nombreProducto), 0, 0, 'L');
    $pdf->Cell(20, 6, $rowDetMov->valor_total, 0, 0, 'R');
    $pdf->Ln();
}
$pdf->Cell(75, 6, '----------', 0, 0, 'R');
$pdf->Ln();
//TOTALES FACTURA
$pdf->Cell(55, 6, 'Sub. Total 12: ', 0, 0, 'R');
$pdf->Cell(20, 6, $rowMov->subtotal, 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(55, 6, 'Sub. Total 0: ', 0, 0, 'R');
$pdf->Cell(20, 6, $rowMov->subtotal_sin_iva, 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(55, 6, 'Iva: ', 0, 0, 'R');
$pdf->Cell(20, 6, $rowMov->iva, 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(75, 6, '==========', 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(55, 6, 'Total: ', 0, 0, 'R');
$pdf->Cell(20, 6, $rowMov->total, 0, 0, 'R');
//FORMA DE PAGO FACTURA 
$pdf->Ln();
$pdf->Cell(75, 6, '==============================', 0, 0, 'L');
$pdf->Ln();
$sqlInfantes = "SELECT * FROM `sesiones_infantes` WHERE sesion_id='$sesion' AND estado='T'";
    $valInfantes = $dbmysql->query($sqlInfantes);
    while($rowInfante = $valInfantes->fetch_object()){
        $infantes .=($infantes=='')?$rowInfante->infante_id:', '.$rowInfante->infante_id;
    }
($infantes!=='')?$pdf->Cell(25, 6, utf8_decode('Niños: '), 0, 0, 'L'):'';
($infantes!=='')?$pdf->Cell(50, 6, $infantes, 0, 0, 'R'):'';
($infantes!=='')?$pdf->Ln():'';
$pdf->Cell(75, 6, 'Forma de Pago: ', 0, 0, 'L');
$pdf->Ln();
$sqlPagos = "SELECT * FROM `pagos_movimientos` pm, tipos_pagos tp WHERE pm.tipos_pago_id=tp.id AND pm.movimiento_id=$codMovimiento;";
$valPagos = $dbmysql->query($sqlPagos);
while ($rowPagos = $valPagos->fetch_object()) {
    $pdf->Cell(25, 6, $rowPagos->nombre . ': ', 0, 0, 'L');
    $pdf->Cell(50, 6, '$ ' . $rowPagos->valor, 0, 0, 'R');
    $pdf->Ln();
}
$pdf->Cell(75, 6, '-----------------------------', 0, 0, 'L');
$pdf->Ln();
//USUARIO ENVIO
$pdf->Cell(35, 6, 'Atendido por: ', 0, 0, 'L');
$pdf->Cell(40, 6, strtoupper(utf8_decode($rowMov->nombre)).'..', 0, 0, 'L');
$pdf->AutoPrint(true);
$pdf->Output();
$pdf->Close();