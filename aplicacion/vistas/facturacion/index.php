<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/facturas/facturas.php");
$facturas = new facturas();
date_default_timezone_set('America/Guayaquil');
$numeroFactura = $facturas->getNumeroFactura();
?>

<div class="col-sm-7" id="txtDatosCliente">
    <div class="btn-group">
        <a class="btn btn-sm btn-primary" href="javascript:editarCliente()">
            <i class="fa fa-edit"></i>
            Obtener Sesiones
        </a>
    </div>
    <form class="smart-form" action="javascript:gurdarClienteFactura();" autocomplete="off" id="fromClientesFactura" novalidate="novalidate">
        <input type="hidden" class="idCliente" id="idCliente" name="idCliente" value="1">
        <input type="hidden" class="codSesion" id="codSesion" name="codSesion" value="0">
        <input type="hidden" class="codInfantes" id="codInfantes" name="codInfantes" value="0">
        <input type="hidden" class="codFiesta" id="codFiesta" name="codFiesta" value="0">
        <input type="hidden" class="cambioCliente" id="cambioCliente" name="cambioCliente" value="N">
        <fieldset>
            <section>
                <label><b>CI/Ruc/Pasaporte:  </b></label>
                <label class="input txtOpcionalInput" style="display: none;">
                    <div class="row">
                        <section class="col col-6">
                            <label class="select">
                                <select id="tipoDocumentoCliente" name="tipoDocumentoCliente" class="form-control" onchange="javascript:cambioCedulaPasaporteNuevo();">
                                    <option value="">-- Seleccione --</option>
                                    <option value="C">Cédula / RUC</option>
                                    <option value="P">Pasaporte</option>
                                </select>
                                <i></i>
                            </label>
                        </section>
                        <section class="col col-6" id="cedulaPasaporteNuevo">
<!--                            <label class="input"> 
                                <input type="text"> 
                            </label>-->
                        </section>
                    </div>
                </label>
                <i class="txtOpcionalLabel" id="txtRucCliente"> 9999999999</i>
            </section>
            <div class="row">
                <section class="col col-6">
                    <h3 class="semi-bold">
                        <label class="input txtOpcionalInput" style="display: none;"><input type="text" class="nombreCliente text-uppercase" id="nombreCliente" name="nombreCliente" placeholder="Nombre"></label>
                        <b class="txtOpcionalLabel" id="txtNombreCliente">Consumidor Final</b>
                    </h3>
                </section>
                <section class="col col-6">
                    <h3 class="semi-bold">
                        <label class="input txtOpcionalInput text-uppercase" style="display: none;"><input type="text" class="apellidoCliente text-uppercase" id="apellidoCliente" name="apellidoCliente" placeholder="Apellido"></label>
                        <b class="txtOpcionalLabel" id="txtNombreCliente"></b>
                    </h3>
                </section> 
            </div>
            <div class="row">
                <section class="col col-6">
                    <label><b>Dirección:  </b></label>
                    <label class="input txtOpcionalInput" style="display: none;"><input type="text" class="direccionCliente text-uppercase" id="direccionCliente" name="direccionCliente" value="Ninguna"></label>
                    <i class="txtOpcionalLabel" id="txtDireccionCliente"> Ninguna</i>
                </section>
                <section class="col col-6">
                    <label><b>E-Mail:  </b></label>
                    <label class="input txtOpcionalInput" style="display: none;"><input type="text" class="correoCliente" id="correoCliente" name="correoCliente" value="Ninguno"> </label>
                    <i class="txtOpcionalLabel" id="txtCorreoCliente"> Ninguno</i>
                </section>
            </div>
            <div class="row">
                <section class="col col-6">
                    <label><b>Teléfono:  </b></label>
                    <label class="input txtOpcionalInput" style="display: none;"><input type="text" class="telefonoCliente soloNumero" id="telefonoCliente" name="telefonoCliente" value="(02) 000-0000"> </label>
                    <i class="txtOpcionalLabel" id="txtTelefonoCliente"> (02) 000-0000</i>
                    <a class="btn btn-xs btn-default txtOpcionalLabel" href="javascript:modificarClienteFactura();">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                </section>
                <section class="col col-6">
                    <div class="btn-group" style="margin: 19px 0px 0px 70px;">
                        <button type="submit" class="btn btn-sm btn-primary txtOpcionalInput"  >
                            <i class="glyphicon glyphicon-saved"></i> Registrar
                        </button>
                    </div>
                </section>
            </div>
        </fieldset>
    </form>
</div>
<div class="col-sm-5 col-lg-3">
    <div>
        <div>
            <strong>FACTURA NO :</strong>
            <span class="pull-right text-danger"><?php echo $numeroFactura; ?><input type="hidden" id="numeroFactura" name="numeroFactura" value="<?php echo $numeroFactura; ?>"></span>
        </div>
    </div>
    <div>
        <div class="font-md">
            <strong>FECHA :</strong>
            <span class="pull-right">
                <i class="fa fa-calendar"></i>
                <?php echo date('Y-m-d'); ?>
            </span>
        </div>
    </div>
    <br>
    <div class="well well-sm bg-color-darken txt-color-white no-border">
        <div class="fa-lg">
            Total Factura :
            <span class="pull-right total" > $ <span id="totalGeneral">00.00</span> </span>
        </div>
    </div>
</div>

<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2>Factura </h2>

        </header>
        <div>
            <div class="widget-body no-padding">

                <form id="checkout-form" class="smart-form" novalidate="novalidate" action="javascript:metodosdePago();">
                    <table id="listaUsuarios" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> Codigo</th>
                                <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Descripción</th>
                                <th>Cantidad</th>
                                <th>Descuento <i id="txtTipoDescuento" class="text-right text-success" style="float: right;"></i></th>
                                <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> V/Unitario</th>
                                <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> V/Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="itemFac">
                                <td><span id="txtcodigo1"></span><label class="input"><input type="hidden" class="productoServicio1" name="productoServicio1"  id="productoServicio1"><input type="text" class="codigoProducto1" name="codigoProducto1" id="codigoProducto1" size="10"></label></td>
                                <td><label class="input"><input type="text" name="descripcionProducto1" id="descripcionProducto1" class="descripcionProducto1" size="30"></label></td>
                                <td><label class="input txtFactura soloNumero"><input type="text" name="cantidadProducto1" id="cantidadProducto1" class="cantidadProducto1" size="4" maxlength="4"></label></td>
                                <td class="aplicaDescuento"><label class="input state-disabled soloNumero"><input type="text" name="descuento1" id="descuento1" class="descuento1" disabled="disabled" size="4" maxlength="4" value="0">
                                        <a class="btn btn-xs btn-default" href="javascript:obtenerDescuento();">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </a></label></td>
                                <td style="vertical-align: middle"><label class="input state-disabled txtFactura soloNumero txtValoresDecimales" ><spam id="txtProductoUnidateios1">0.00</spam><input type="hidden" name="vunitarioProducto1" disabled="disabled" id="vunitarioProducto1" class="vunitarioProducto1" size="3"></label></td>
                                <td style="display: inline-flex;width: 150px;vertical-align: middle;"><label class="input state-disabled txtFactura soloNumero"  style="width: 75px; text-align: center; font-weight: bold; font-size: medium;"><spam id="txtProductoTotales1">0.00</spam><input class="totales" type="hidden" name="totalProducto1"  disabled="disabled" id="totalProducto1" size="3"><input type="hidden" name="ivanoiva1"  id="ivanoiva1"></label>
                                    <a class="btn btn-success btn-circle btnAgregar" href="javascript:agregarItemFactura();">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a></td>
                            </tr>
                        </tbody>
                    </table>
                    <footer>
                        <div style="font-size: 1.4em;">
                            <div class="row">
                                <section class="col col-9" style="text-align: right;">
                                    <label class="control-label">Sub-Total</label>
                                </section>
                                <section class="col col-3" style="width: 102px;text-align: right">
                                    <input type="hidden" name="subTotal" id="subTotal"><i id="txtSubTotal">00.00</i>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-9" style="text-align: right;">
                                    <label class="control-label">Sub-Total sin Iva</label>
                                </section>
                                <section class="col col-3" style="width: 102px;text-align: right">
                                    <input type="hidden" name="subTotalSinIva" id="subTotalSinIva"><i id="txtSubTotalSinIva">00.00</i>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-9" style="text-align: right;">
                                    <label class="control-label">Iva</label>
                                </section>
                                <section class="col col-3" style="width: 102px;text-align: right">
                                    <input type="hidden" name="iva" id="iva"><i id="txtIva">00.00</i>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-9" style="text-align: right;">
                                    <label class="control-label">Total</label>
                                </section>
                                <section class="col col-3" style="width: 102px;text-align: right">
                                    <h3><strong><input type="hidden" name="total" id="total"><span class="text-success total" id="txtTotal">00.00</span></strong></h3>
                                </section>
                            </div>

                        </div>
                    </footer>

                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Facturar
                        </button>
                    </footer>
                </form>
            </div>
        </div>
    </div>
</article>
<div id="areaDeImpresion" style="display: none;"></div>
<div id="temporalEnvioMail" style="display: none;"></div>
<script src="aplicacion/js/factura.js" defer="defer"></script>
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/facturacion/frmDescuento.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/clientes/frmClienteFacturacion.php';
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/facturacion/frmFormaPago.php';

