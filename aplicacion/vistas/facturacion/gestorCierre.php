<?php
session_start();

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/cajas/cajas.php';
$cajas = new cajas();
?>
<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">        
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>GESTOR DE CIERRE DE CAJA</h2>
        </header>
        <div>
            <table id="listaCierreCaja" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        <th><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Fecha</th>
                        <th><i class="fa fa-fw fa-bell txt-color-blue hidden-md hidden-sm hidden-xs"></i>Caja</th>
                        <th><i class="fa fa-fw fa-times-circle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Hora Apertura</th>
                        <th><i class="fa fa-fw fa-times-circle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Hora Cierre</th>
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Usuario Apertura</th>                                        
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Monto Apertura</th>                                        
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Acciones</th>                                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $resul = $cajas->MostrarCierreCaja();
                    while ($row = $resul->fetch_object()) {
                        $fecha_cierre = $row->fecha;
                        ?>
                    <tr  align="center" style="text-align: center;">
                            <td><spam style="font-size: 1.2em;color: #a90329;"><b><?php echo $row->fecha; ?></b></spam></td>
                            <td><?php echo $row->nombrecaja; ?></td>
                            <td><?php echo $row->hora_apertura; ?></td>
                            <td><?php echo $row->hora_cierre; ?></td>
                            <td><?php echo $row->nombreapertura; ?></td>
                            <td><?php echo $row->apertura; ?></td>
                            <td align="center">
                                <a class="btn btn-labeled btn-primary" title="Ver Caja" href="javascript:cierreCajaPorFecha('<?php echo $row->fecha; ?>');">
                                    <span class="btn-label">
                                        <i class="glyphicon glyphicon-th-list"></i>
                                    </span>
                                    Ver
                                </a>

                            </td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</article>
<div class="modal fade" id="frmGestorCierreCajaModal" tabindex="-1" role="dialog" aria-labelledby="ClienteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Cierre de caja </h4>
            </div>
            <div class="modal-body" id="body_impresion">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-money"></i> </span>
                        <h2>Detalles del cierre del <b><spam id="fechaCierre"></spam></b></h2>				
                    </header>
                    <div >
                        <div class="widget-body no-padding">
                            <div class="widget-body" id="detalles_cierre">
                                <div style="text-align: center;"><img src="publico/img/logo_kidztime.png" alt="kidzTime" width="180px" style="margin: 10px"/></div>
                                <table class="table table-hover" id="detalles_cierre_caja"></table> 
                            </div>
                        </div>
                    </div>
                    <div class="widget-footer" style="text-align: center;height: 57px;" id="footer-cierre-caja">

                        <button type="submit"class="btn btn-info" onclick="javascript:imprimirGestorCierreCaja()" style="text-align: center;">
                            Imprimir Cierre
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="aplicacion/js/cajas/gestorCierresCaja.js" defer="defer"></script>
