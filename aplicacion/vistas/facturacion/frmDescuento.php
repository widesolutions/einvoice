<div class="modal fade" id="frmDescuentoModal" tabindex="-1" role="dialog" aria-labelledby="DescuentoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Registro </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    <input type="hidden" name="codigoProSer" id="codigoProSer" value="">
                                    <input type="hidden" name="tipoItem" id="tipoItem" value="0">
                                    Aplicar el Siguiente Descuento a <i id="txtCodigoProSer"></i>
                                </header>
                                <fieldset>
                                    <section class="col col-6">
                                        <div class="inline-group">
                                            <label class="radio">
                                                <input type="radio" checked="checked" name="tipoDescuento" id="tipoDescPorcentaje" value="P">
                                                <i></i>
                                                Porcentaje
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="tipoDescuento" id="tipoDesValor" value="V">
                                                <i></i>
                                                Valor
                                            </label>
                                        </div>
                                    </section>
                                    <section class="col col-6">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-dollar"></i>
                                                <input type="text" name="descuentoAsigna" id="descuentoAsigna" class="soloNumero" size="2">
                                            </label>
                                    </section>
                                </fieldset>
                                <footer>
                                    <button type="button" class="btn btn-primary" id="btnDescuentos">
                                        Asignar
                                    </button>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
