<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/tipoPagos/tipoPagos.php");
$tipoPagos = new tipoPagos();
?>
<div class="modal fade" id="frmFormaPagoModal" tabindex="-1" role="dialog" aria-labelledby="FormaPagoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Registro </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Metodos de Pago
                                    <spam class="text-center" id="valorApagarTxt" style="color: red;font-size: 1.2em;font-weight: bold;left: 140px;position: relative;">$ <b></b></spam>
                                    <input type="hidden" name="valorApagar" class="valorApagar" id="valorApagar">
                                    <strong><i class="text-success pull-right" id="txtValorApagar">00.00</i></strong>
                                </header>
                                <table id="tablaFormaPago" class="table table-bordered table-hover" width="80%">
                                    <thead>
                                        <tr>
                                            <th>Selec.</th>
                                            <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> Forma Pago</th>
                                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Valor a Pagar</th>
                                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Número Documento</th>
                                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Tarjeta</th>
                                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $valcmb = $tipoPagos->getTipoPagos();
                                        while ($row = $valcmb->fetch_object()) {
                                            ?>
                                        <tr id="fila-<?php echo strtolower($row->nombre) ?>" class="fila-<?php echo strtolower($row->nombre) ?>">
                                                <td><label class="checkbox"><input type="checkbox" class="checkMetodoPago" name="check-<?php echo strtolower($row->nombre) ?>" id="check-<?php echo strtolower($row->nombre) ?>" style="left: 31px;top: 11px; "></label></td>
                                                <td><label class="input"><input type="hidden" name="codigoTipoPago" id="codigoTipoPago" class="codigoTipoPago" value="<?php echo $row->id ?>"><?php echo $row->nombre ?></label></td>
                                                <td><label class="input state-disabled"><input type="text" name="<?php echo strtolower($row->nombre) ?>" disabled="disabled" id="<?php echo strtolower($row->nombre) ?>" class="valorPagar soloNumero" placeholder="$ 00.00" ></label></td>
                                                <td><?php echo ($row->valores_adicionales == 'S') ? '<label class="input state-disabled"><input type="text" disabled="disabled" name="numeroDocumento" id="numeroDocumento" class="numeroDocumento soloNumero" placeholder="NUMERO"></label>' : ''; ?></td>
                                                <td><?php echo (strtolower($row->nombre) == 'tarjeta') ? '<label class="input state-disabled"><input type="hidden" name="tipoDocumentoAdicional" id="tipoDocumentoAdicional" class="tipoDocumentoAdicional"></label><img class="imagenTarjeta" width="44" height="44" alt="visa" src="publico/img/invoice/visa.png">' : ''; ?></td>
                                                <td><?php echo ($row->valores_adicionales == 'S') ? '<a class="btn btn-success btn-circle" href="javascript:agregarMasItems(\''.strtolower($row->nombre).'\','.$row->id.');"><i class="glyphicon glyphicon-plus"></i></a>':''; ?></td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                                <footer>
                                    <div class="col-sm-7">
                                        <div class="payment-methods">
                                            <img width="64" height="64" alt="paypal" src="publico/img/invoice/paypal.png">
                                            <img width="64" height="64" alt="american express" src="publico/img/invoice/americanexpress.png">
                                            <img width="64" height="64" alt="mastercard" src="publico/img/invoice/mastercard.png">
                                            <img width="64" height="64" alt="visa" src="publico/img/invoice/visa.png">
                                        </div>
                                    </div>
                                    <button type="button" id="botonFacturar" class="btn btn-primary" onclick="javascript:validarMetodoPago();">
                                        Facturar
                                    </button>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="aplicacion/js/formaPago.js" defer="defer"></script>