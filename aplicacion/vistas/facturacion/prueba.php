<div class="modal fade" id="frmImpresionModal" tabindex="-1" role="dialog" aria-labelledby="DescuentoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <div class="widget-body no-padding">
                        <div class="smart-form">
                            <div id="contenedorFactura">
                                <iframe width="552" height="350" id="contenidoFactura"
                                        src="">
                                </iframe>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default"  type="button" onclick="location.href='?modulo=facturacion&vista=gestorFacturas&opMenu=26'"> Re-imprimir </button>
                <button class="btn btn-primary" data-dismiss="modal" type="button"> Finalizar </button>
            </div>
        </div>
    </div>
</div>

