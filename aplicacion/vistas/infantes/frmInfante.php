<div class="modal fade" id="frmInfanteModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Registro de Infante</h4>
            </div>
            <form id="form_infantes"  novalidate="novalidate" class="smart-form" action="javascript:crearInfante()" >
                <div class="modal-body">
                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input"> <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="primer_nombre" id="primer_nombre" placeholder="Primer nombre" class="texto-mayusculas">
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input"> <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="segundo_nombre" id="segundo_nombre" placeholder="Segundo nombre" class="texto-mayusculas">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-6">
                                    <label class="input"> <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="primer_apellido" id="primer_apellido" placeholder="Primer apellido" class="texto-mayusculas">
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input"> <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo apellido" class="texto-mayusculas">
                                    </label>
                                </section>
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                        <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="Fecha de nacimiento" >
                                    </label>
                                </section>
                                <section class="col col-6">
                                   <label class="input"> <i class="icon-append fa fa-institution"></i>
                                        <input type="text" name="institucion_educativa" id="institucion_educativa" placeholder="Institucion educativa" class="texto-mayusculas">
                                    </label>
                                </section>
                            </div>
                        </fieldset>

                </div>
                <footer>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                    </button>
                </footer>
            </form>

        </div>
    </div>
</div>


<!-- Buscar infante-->

<div class="modal fade" id="frmBuscarInfanteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Infantes</h4>
            </div>
            <div class="modal-body">
                <form class="smart-form">
                    <section>
                        <label class="label">Buscar Infante</label>
                        <label class="input"> <i class="icon-append fa fa fa-search"></i>
                            <input type="text" placeholder="Información del infante" id="search_infante" onkeyup="searchInfantes(this.value)">
                        </label>
                    </section>
                </form>
                <table class="table" id="tabla_detalles_infantes">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Seleccionar</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>