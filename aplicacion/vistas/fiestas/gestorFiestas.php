<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/fiestas/fiestas.php");
$fiestas = new fiestas();
date_default_timezone_set('America/Guayaquil');
?>
<article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">        
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>GESTOR DE FIESTAS </h2>
        </header>
        <div>
            <table id="listaFiestasGestion" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Cliente</th>
                        <th><i class="fa fa-fw fa-user-md txt-color-blue hidden-md hidden-sm hidden-xs"></i>Contratante</th>
                        <th><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Fecha</th>
                        <th><i class="fa fa-fw fa-times-circle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Hora Inicio</th>                                        
                        <th><i class="fa fa-fw fa-times-circle txt-color-blue hidden-md hidden-sm hidden-xs"></i>Hora Fin</th>                                        
                        <th><i class="fa fa-fw fa-pencil txt-color-blue hidden-md hidden-sm hidden-xs"></i>Tema</th>                                        
                        
                        <th><i class="fa fa-fw fa-child txt-color-blue hidden-md hidden-sm hidden-xs"></i>Infantes</th>                                        
                        <th><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>Salon</th>
                        <th><i class="fa fa-fw fa-unlock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                                $resul=$fiestas->listarFiestas();
                                    while ($row = $resul->fetch_object()) {
                                ?>
                                            <tr>
                                                <td><?php  echo $row->apellido,$row->nombre; ?></td>
                                                <td><?php echo $row->contratante;?></td>
                                                <td><?php echo $row->fecha; ?></td>
                                                <td><?php echo $row->hora_inicio;?></td>
                                                <td><?php echo $row->hora_fin;?></td>
                                                <td><?php echo $row->tema;?></td>
                                                <td><?php echo $row->infantes;?></td>
                                                <td><?php echo $row->salon;?></td>
                                                <td align="center">
                                                    <a class="btn btn-labeled btn-primary" title="Ver Fiesta" href="javascript:verDetalleFiesta(<?php echo $row->fiesta_id; ?>);">
                                                                    <span class="btn-label">
                                                                        <i class="glyphicon glyphicon-th-list"></i>
                                                                    </span>
                                                                    Ver
                                                                </a>
                                                    <a class="btn btn-labeled btn-info" title="Imprimir" href="javascript:imprimirDocumentoCaja(<?php $row->fiesta_id; ?>);">
                                                                    <span class="btn-label">
                                                                        <i class="glyphicon glyphicon-print"></i>
                                                                    </span>
                                                                    Imprimir
                                                                </a>
                                                </td>
                                                
                                            </tr>
                                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</article>
<!--<script src="aplicacion/js/fiestas/fiestas.js" defer="defer"></script>-->
<script src="aplicacion/js/fiestas/fiesta.js" defer="defer"></script>
<?php 
include gestorFiestas.php;
?>
