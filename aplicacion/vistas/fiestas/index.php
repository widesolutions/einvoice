<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/servicios/servicios.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/krayon/aplicacion/controladores/salones/salones.php");
$servicios = new servicios();
$salones = new salones();
?>
<style>
    .MessageBoxContainer input, .MessageBoxContainer select {
        width: 46px !important;
        padding: 5px;
    }

    .popover.top {
        margin-top: 29px;
        z-index: 10000;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-calendar fa-fw "></i> 
            Calendario
            <span>>
                Agregar Eventos
            </span>
        </h1>
    </div>

</div>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-3">
        <div class="jarviswidget jarviswidget-color-blueDark">
            <header>
                <h2> Agregar Eventos </h2>
            </header>
            <div>
                <div class="widget-body">
                    <form id="add-event-form" class="smart-form" action="javascript: addEvent();" novalidate="novalidate">
                        <fieldset>
                            <div class="form-group">
                                <label>Seleccione el Tipo</label>
                                <div class="btn-group btn-group-sm btn-group-justified" data-toggle="buttons">
                                    <label class="btn btn-default">
                                        <input type="radio" name="eventos" id="evento" value="evento" >
                                        <i class="fa fa-user text-muted"></i> Eventos
                                    </label>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="eventos" id="fiesta" value="fiesta" checked="checked">
                                        <i class="fa fa-clock-o text-muted"></i> Fiestas
                                    </label>
                                </div>
                            </div>
                            <div class="panel-group smart-accordion-default" id="accordion-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-1" > <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> Datos Contratante #1 </a></h4>
                                    </div>
                                    <div id="collapseOne-1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <fieldset>
                                                <section>
                                                    <label>Nombre Contratante</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        <input class="texto-mayusculas" id="contratante" name="contratante"type="text" placeholder="Nombre del Contratante">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Correo Contratante</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        <input id="correoContratante" name="correoContratante"type="email" placeholder="Email Contratante">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Telefono Contratante</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        <input class="soloNumero" id="telefonoContratante" name="telefonoContratante"type="text" placeholder="Teléfono del Contratante">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Anfitrión</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-child"></i>
                                                        <input   name="anfitrion" id="anfitrion" type="text" placeholder="Anfitrión">
                                                    </label>
                                                </section>                                                
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> Datos del Evento #2 </a></h4>
                                    </div>
                                    <div id="collapseTwo-1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <fieldset>
                                                <section>
                                                    <label>Cliente</label><spam style="float: right"><a class="btn btn-primary btn-xs" href="javascript:nuevoClienteEventos();"><i class="fa fa-gear"></i> Nuevo</a><a class="btn btn btn-success btn-xs" href="javascript:consultarCliente();"><i class="fa fa-gear"></i> Buscar</a></spam>
                                                    <input id="codigoCliente" name="codigoCliente"type="hidden" value="0">
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        <input class="disabled texto-mayusculas"  disabled="disabled" id="cliente" name="cliente"type="text" placeholder="Nombre del Cliente">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Servicio</label>
                                                    <label class="select">
                                                        <select name="servicio" class="form-control" id="servicio">
                                                            <option disabled="" selected="" value="0">-- Seleccione --</option>
                                                            <?php
                                                            $val = $servicios->listarServicios();
                                                            while ($row = $val->fetch_object()) {
                                                                ?>
                                                                <option value="<?php echo $row->id ?>"><?php echo $row->nombre ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Cantidad de Infantes</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        <input class="disabled soloNumero"  id="cantidadInfantes" name="cantidadInfantes"type="number" placeholder="Cantidad de Infantes">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Anticipo</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-money"></i>
                                                        <input class="disabled soloNumero"  id="anticipo" name="anticipo"type="number" placeholder="Anticipo">
                                                    </label>
                                                </section>                                                

                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i> Descripción del Evento #3 </a></h4>
                                    </div>
                                    <div id="collapseThree-1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <fieldset>
                                                <section>
                                                    <label>Tema del Evento</label>
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        <input class="form-control"  id="title" name="title" type="text" placeholder="Tema del Evento">
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Salon</label>
                                                    <label class="select">
                                                        <select name="salon" class="form-control" id="salon">
                                                            <option disabled="" selected="" value="0">-- Seleccione --</option>
                                                            <?php
                                                            $val2 = $salones->listarSalones();
                                                            while ($row = $val2->fetch_object()) {
                                                                ?>
                                                                <option value="<?php echo $row->id ?>"><?php echo $row->nombre ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </section>
                                                <section>
                                                    <label>Descripcion</label>
                                                    <label class="textarea">
                                                        <i class="icon-append fa fa-comment"></i>
                                                        <textarea class="form-control" placeholder="Descripción del Evento" rows="3" id="description"></textarea>
                                                        <p class="note">Maximo 40 caracteres</p>
                                                    </label>
                                                </section>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group">
                                <label>Seleccione color para el Evento</label>
                                <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
                                    <label class="btn bg-color-darken active">
                                        <input type="radio" name="priority" id="option1" value="bg-color-darken txt-color-white" checked>
                                        <i class="fa fa-check txt-color-white"></i> </label>
                                    <label class="btn bg-color-blue">
                                        <input type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white">
                                        <i class="fa fa-check txt-color-white"></i> </label>
                                    <label class="btn bg-color-orange">
                                        <input type="radio" name="priority" id="option3" value="bg-color-orange txt-color-white">
                                        <i class="fa fa-check txt-color-white"></i> </label>
                                    <label class="btn bg-color-greenLight">
                                        <input type="radio" name="priority" id="option4" value="bg-color-greenLight txt-color-white">
                                        <i class="fa fa-check txt-color-white"></i> </label>
                                    <label class="btn bg-color-blueLight">
                                        <input type="radio" name="priority" id="option5" value="bg-color-blueLight txt-color-white">
                                        <i class="fa fa-check txt-color-white"></i> </label>
                                    <label class="btn bg-color-red">
                                        <input type="radio" name="priority" id="option6" value="bg-color-red txt-color-white">
                                        <i class="fa fa-check txt-color-white"></i> </label>
                                </div>
                            </div>

                        </fieldset>
                        <footer>
                            <button class="btn btn-default" type="submit" > Agregar Evento </button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
        <!-- end widget -->

        <div class="well well-sm" id="event-container">
            <form>
                <fieldset>
                    <legend>
                        Eventos en Espera de Asignación
                    </legend>
                    <ul id='external-events' class="list-unstyled">
                    </ul>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="drop-remove" class="checkbox style-0" checked="checked">
                            <span>Eliminar Evento luego de Asignar Fecha</span> </label>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-9">

        <!-- new widget -->
        <div class="jarviswidget jarviswidget-color-blueDark">
            <header>
                <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
                <h2> Mis Eventos </h2>
                <div class="widget-toolbar">
                    <!-- add: non-hidden - to disable auto hide -->
                    <div class="btn-group">
                        <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                            Cambiar <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu js-status-update pull-right">
                            <li>
                                <a href="javascript:void(0);" id="mt">Mes</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" id="ag">Agenda</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" id="td">Hoy</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>

            <!-- widget div-->
            <div>

                <div class="widget-body no-padding">
                    <!-- content goes here -->
                    <div class="widget-body-toolbar">

                        <div id="calendar-buttons">

                            <div class="btn-group">
                                <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="calendar"></div>

                    <!-- end content -->
                </div>

            </div>
            <!-- end widget div -->
        </div>
        <!-- end widget -->

    </div>

</div>

<script src="aplicacion/js/sesiones/sesion.js" defer="defer"></script>
<script src="aplicacion/js/fiestas/fiestasCreacion.js" defer="defer"></script>
<script src="aplicacion/js/fiestas/fiestasGestion.js" defer="defer"></script>
<script src="publico/js/plugin/fullcalendar/lang/es.js" defer="defer"></script>
<script src="publico/js/plugin/clockpicker/clockpicker.min.js" defer="defer"></script>


<?php
include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/fiestas/frmDetallesFiestas.php';

include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/clientes/frmClienteSesiones.php';
