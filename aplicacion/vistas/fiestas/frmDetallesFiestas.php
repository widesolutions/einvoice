<style>
    .ui-autocomplete
    {
        z-index: 10000 !important;
    }

    @media (max-width: 800px) { 
        button{
            padding: 20px 20px !important;
        }
        .col-xs-4{
            margin-top: 10px;
        }

        .btn-success{
            padding: 10px 10px !important;
        }
        .btn-warning{
            padding: 10px 10px !important;
        }        
    }   


    @media screen {
        #seccion_impresion {
            display: none;
        }
    }

    @media print {
        #seccion_impresion {
            display: block;
        }
    }      
</style>
<div class="modal fade in" id="frmDetalleFiesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"><div class="modal-backdrop fade in" style="height: 539px;"></div>
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel">Detalles de la fiesta</h4>
            </div>

            <div class="modal-body">
                <form class="form"  id="form_detalles_fiestas" action="javascript:guardarDetallesFiesta()" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="category"> Cliente</label>
                                <input type="text" class="form-control" placeholder="Cliente" required="" id="cliente_calendario">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="category"> Anfitrión</label>
                                <input type="text" class="form-control" placeholder="Anfitrión" id="anfitrion_calendario" name="anfitrion_calendario" required="" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="category"> Anticipo</label>
                                <input type="text" class="form-control" placeholder="Anticipo" id="anticipo_calendario" name="anticipo_calendario" required="">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Descripción" id="observacion_calendario" name="observacion_calendario" rows="2" required=""></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="category"> Servicio</label>
                                <select class="form-control" id="servicio_calendario" name="servicio_calendario">
                                    <?php
                                    $val = $servicios->listarServicios();
                                    while ($row = $val->fetch_object()) {
                                        ?>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tags"> Hora</label>
                                <input type="text" class="form-control" id="hora" name="hora" placeholder="Hora" data-autoclose="true" readonly="">
                                <input type="hidden" id="fiesta_id" name="fiesta_id">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tags"> # Infantes</label>
                                <input type="number" class="form-control" id="numero_infantes" name="numero_infantes" placeholder="# Infantes" data-autoclose="true" required>
                            </div>
                        </div>                    
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="category"> Salón</label>
                                <select class="form-control" id="salon_calendario" name="salon_calendario">
                                    <?php
                                    $val2 = $salones->listarSalones();
                                    while ($row = $val2->fetch_object()) {
                                        ?>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tags"> Tema</label>
                                <input type="text" class="form-control" id="tema" name="tema" placeholder="Tema" data-autoclose="true">
                            </div>
                        </div>
                        <div class="col-md-3" >
                            <button type="button" class="btn btn-warning" style="margin-top: 23px;" onclick="javascript:imprimirBrazaletes()" >
                                <span class="glyphicon glyphicon-print"></span> Brazaletes
                            </button>                            
                        </div>
                        <div id="seccion_impresion" >

                        </div>

                    </div>                    
                </form>
                <legend>
                    Adicionales de la fiesta
                </legend>
                <div class="row well well-sm well-primary">
                    <form class="form"  id="form_adicionales">
                        <div class=" col-md-6">

                            <div class="form-group">
                                <input type="text" class="form-control" value="" id="detalle_adicional" placeholder="Adicional" required="">
                                <input type="hidden" id="adicional_id">
                                <input type="hidden" id="adicional_tipo">
                            </div>

                        </div>
                        <div class="col-xs-4 col-md-2">
                            <div class="form-group">
                                <input type="number" class="form-control" value="" id="cantidad_adicional" placeholder="#" required="">
                            </div>

                        </div> 
                        <div class="col-xs-4 col-md-2">
                            <div class="form-group">
                                <input type="number" class="form-control" value="" id="valor_adicional" placeholder="$ Valor" required="">
                            </div>

                        </div>

                        <div class="col-xs-4 col-md-2">

                            <div class="form-group">
                                <button type="button" class="btn btn-success btn-sm" id="agregar_adicional" >
                                    <span class="glyphicon glyphicon-check"></span> Agregar
                                </button>
                            </div>
                        </div>
                        <br>
                        <hr class="success">

                        <div  id="adicionales">    

                        </div>                        
                    </form>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" onclick="javascript:CancelarFiesta()" >
                    <span class="glyphicon glyphicon-remove-circle"></span> Eliminar Fiesta
                </button>
                <button type="button" class="btn btn-info pull-left" onclick="javascript:finalizarFiesta()" >
                    <span class="glyphicon glyphicon-download-alt"></span> Finalizar Fiesta
                </button>                
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <button type="button" class="btn btn-primary" id="btn_enviar" >
                    <span class="glyphicon glyphicon-save"></span> Guardar
                </button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

