<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/sesiones/sesiones.php';

$sesion = new sesiones();

if (isset($_GET['fecha_inicio'])) {
    $fecha_inicio = $_GET['fecha_inicio'];
    $fecha_fin = $_GET['fecha_fin'];
} else {
    $fecha_inicio = date('Y-m-d');
    $fecha_fin = date('Y-m-d');
}

$sesiones = $sesion->ListaConcurrenciaPorFecha($fecha_inicio, $fecha_fin);
?>
<div class="content">
    <div class="jarviswidget jarviswidget-sortable" id="wid-id-7" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
        <header role="heading"><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);" aria-expanded="false"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
            <h2><strong>Seleccione</strong> <i>La fecha del periodo</i></h2>				

            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
        </header>
        <div class="content">
            <div class="widget-body no-padding">

                <form id="order-form" class="smart-form" novalidate="novalidate" action="inicio.php?modulo=reportes&vista=InfanteMasVisitas&opMenu=33" method="get" >
                    <input name="modulo" value="<?php echo $_GET['modulo'] ?>" type="hidden">
                    <input name="vista" value="<?php echo $_GET['vista'] ?>" type="hidden">
                    <input name="opMenu" value="<?php echo $_GET['opMenu'] ?>" type="hidden">

                    <fieldset>
                        <div class="row"> 
                            <section class="col col-6">
                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="fecha_inicio" id="fecha_inicio" placeholder="Fecha de inicio" class="form-control" value="<?php echo $fecha_inicio ?>">
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="fecha_fin" id="fecha_fin" placeholder="Fecha final" class="form-control" value="<?php echo $fecha_fin ?>">
                                </label>
                            </section>

                        </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Generar Reporte
                        </button>
                    </footer>
                </form>

            </div>

        </div>
    </div>

</div>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2>Concurrencia de infantes</h2>

    </header>
    <div>
        <div class="jarviswidget-editbox">
        </div>
        <div class="widget-body no-padding">
            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Código Inf.</th>
                        <th>Fecha</th>
                        <th>Servicio</th> 
                        <th>Cliente</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>F. Nacimiento </th>
                        <th>Hora de Inico</th>
                        <th>Hora final</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    while ($row = $sesiones->fetch_object()) {
                        ?>
                        <tr>
                            <td><?php echo $row->infante_id ?></td>
                            <td><?php echo $row->fecha ?></td>
                            <td><?php echo $row->servicio ?></td>
                            <td><?php echo $row->nombre_cliente ?></td>
                            <td><?php echo $row->telefono_cliente ?></td> 
                            <td><?php echo $row->correo_cliente ?></td>                            
                            <td><?php echo $row->primer_nombre ?></td>
                            <td><?php echo $row->primer_apellido ?></td>
                            <td><?php echo $row->fecha_nacimiento ?></td>
                            <td><?php echo $row->hora_inicio ?></td>
                            <td><?php echo $row->hora_fin ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="aplicacion/js/reportes/concurrencia.js" defer="defer"></script>

