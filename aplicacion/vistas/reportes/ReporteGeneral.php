<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/reportes/reportes.php';

$reporte = new reportes();

if (isset($_GET['fecha_inicio'])) {
    $fecha_inicio = $_GET['fecha_inicio'];
    $fecha_fin = $_GET['fecha_fin'];
} else {
    $fecha_inicio = date('Y-m-d');
    $fecha_fin = date('Y-m-d');
}

$reportes = $reporte->reporteFacturas($fecha_inicio, $fecha_fin);
?>
<div class="content">
    <div class="jarviswidget jarviswidget-sortable" id="wid-id-7" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
        <header role="heading"><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);" aria-expanded="false"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
            <h2><strong>Seleccione</strong> <i>La fecha del periodo</i></h2>				

            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
        </header>
        <div class="content">
            <div class="widget-body no-padding">

                <form id="order-form" class="smart-form" novalidate="novalidate" action="inicio.php?modulo=reportes&vista=InfanteMasVisitas&opMenu=33" method="get" >
                    <input name="modulo" value="<?php echo $_GET['modulo'] ?>" type="hidden">
                    <input name="vista" value="<?php echo $_GET['vista'] ?>" type="hidden">
                    <input name="opMenu" value="<?php echo $_GET['opMenu'] ?>" type="hidden">

                    <fieldset>
                        <div class="row"> 
                            <section class="col col-6">
                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="fecha_inicio" id="fecha_inicio" placeholder="Fecha de inicio" class="form-control" value="<?php echo $fecha_inicio ?>">
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="fecha_fin" id="fecha_fin" placeholder="Fecha final" class="form-control" value="<?php echo $fecha_fin ?>">
                                </label>
                            </section>

                        </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Generar Reporte
                        </button>
                    </footer>
                </form>

            </div>

        </div>
    </div>

</div>

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2>Resultados de movimientos</h2>

    </header>
    <div>
        <div class="jarviswidget-editbox">
        </div>
        <div class="widget-body no-padding">
            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th >Día</th>
                        <th>Mes</th>
                        <th>Año</th>
                        <th>Número de factura</th>     
                        <th >Subtotal</th>
                        <th>Iva</th>
                        <th>Sin_iva</th>
                        <th>Total</th>                        
                        <th >Cliente</th>
                        <th>CI</th>
                        <th>Estado</th> 
                        <th style="display: none">Nº Niños</th>
                        <th style="display: none">Nº Bebes</th>
                        <th style="display: none">Nª Horas</th>
                        <th style="display: none">Fiestas</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    while ($row = $reportes->fetch_object()) {
                        ?>
                        <tr>
                            <td><?php echo $row->fecha ?></td>
                            <td><?php echo $row->dia ?></td>
                            <td><?php echo $row->mes ?></td>
                            <td><?php echo $row->año ?></td>
                            <td><?php echo $row->numero_factura ?></td>
                            <td><?php echo number_format($row->subtotal, 2) ?></td>
                            <td><?php echo number_format($row->iva, 2) ?></td>
                            <td><?php echo $row->subtotal_sin_iva ?></td>
                            <td><?php echo $row->total ?></td>       
                            <td><?php echo $row->cliente ?></td>
                            <td><?php echo $row->identificacion ?></td>
                            <td><?php echo $row->estado ?></td>     
                            <td style="display: none"><?php echo $row->cantidad_niños ?></td>
                            <td style="display: none"><?php echo $row->cantidad_bebes ?></td>
                            <td style="display: none"><?php echo $row->cantidad_horas ?></td>                             
                            <td style="display: none"><?php echo $row->fiestas ?></td>                             
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="aplicacion/js/reportes/servicioMasVendido.js" defer="defer"></script>

