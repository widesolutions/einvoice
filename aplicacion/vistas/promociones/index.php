<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevaPromocion()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Promoción
            </button>
        </fieldset> 
        <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
            <header>
                <span class="widget-icon"> <i ></i> </span>
                <h2>Promociones </h2>
            </header>
            <div>
                <table id="listaPromociones" class="table table-striped table-bordered table-hover"   width="100%">                       
                    <thead>
                        <tr>
                            <th><i class="fa fa-fw txt-color-blue hidden-md hidden-sm hidden-xs"></i> ID</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Día</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Nombre</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Servicio</th>
                            <th><i class="fa fa-fw fa-asterisk txt-color-blue hidden-md hidden-sm hidden-xs"></i> Condición Cálculo</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Valor</th>
                            <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</article>
<script src="aplicacion/js/promocion.js" defer="defer"></script>
<?php
include 'frmPromociones.php';

