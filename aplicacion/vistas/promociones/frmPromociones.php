<style>
    #ui-id-2, .ui-autocomplete{z-index: 9999 !important;}
    #productoServicio{padding-left: 37px;}
</style>
<div class="modal fade" id="frmPromocionesModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Promociones </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>Ingresar Promociones</header>
                            </div>
                            <br>
                            <form id="promocionForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarPromociones()">
                                <fieldset>
                                    <input type="hidden" id="IDPromocion" name="IDPromocion">
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Nombre</label>
                                            <label class="input"> <i class="icon-prepend fa fa-align-justify"></i>
                                                <input id="nombre" name="nombre" type="text" class="text-uppercase" name="lname" placeholder="Nombre de la Promoción">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Día</label>
                                            <label class="select">
                                                <select id="dia" name="dia">
                                                    <option value="" selected="" disabled="">-- Seleccione --</option>
                                                    <option value="8"> Todos </option>
                                                    <option value="1"> Lunes </option>
                                                    <option value="2"> Martes </option>
                                                    <option value="3"> Miércoles </option>
                                                    <option value="4"> Jueves </option>
                                                    <option value="5"> Viernes </option>
                                                    <option value="6"> Sábado </option>
                                                    <option value="7"> Domingo </option>
                                                </select> <i></i> </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Fecha Inicio</label>
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                <input id="fechaInicio" name="fechaInicio" class="datepicker" type="text" data-dateformat="yy-mm-dd" placeholder="Fecha Inicio" name="request">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Fecha Fin</label>
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                <input id="fechaFin" name="fechaFin" class="datepicker" type="text" data-dateformat="yy-mm-dd" placeholder="Fecha Fin" name="request">
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend> Datos del Descuento </legend>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Tipo Cálculo</label>
                                            <div class="inline-group">
                                                <label class="radio">
                                                    <input type="radio" checked="checked" name="tipo_descuento" id="tipo_descuento_porcentaje" value="P">
                                                    <i></i>
                                                    Porcentaje
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="tipo_descuento" id="tipo_descuento_valor" value="V">
                                                    <i></i>
                                                    Valor
                                                </label>
                                            </div>

                                        </section>
                                        <section class="col col-6">
                                            <label>Valor</label>
                                            <label class="input"> <i class="icon-prepend fa fa-align-justify"></i>
                                                <input id="valor" name="valor" type="text" class="soloNumero" placeholder="Valor de Descuento">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-9">
                                        <label>Producto/Servicio</label>
                                        <label class="input"> <i class="icon-prepend fa fa-align-justify"></i>
                                            <input id="productoServicio" name="productoServicio" type="text" class="text-uppercase" placeholder="Servicio">
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label>Código</label>
                                        <label class="input">
                                            <input id="codigo_productoServicio" name="codigo_productoServicio" type="text" class="disabled" disabled="" required="">
                                        </label>
                                    </section>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend> Tramo: </legend>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Si Cantidad es mayor a: </label>
                                            <label class="input"> <i class="icon-prepend fa fa-align-justify"></i>
                                                <input id="mayor" name="mayor" type="text" class="soloNumero" placeholder="Cantidad Mayor a?">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Y menor o igual a: </label>
                                            <label class="input"> <i class="icon-prepend fa fa-align-justify"></i>
                                                <input id="menor" name="menor" type="text" class="soloNumero" placeholder="Cantidad Menor a?">
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Aceptar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

