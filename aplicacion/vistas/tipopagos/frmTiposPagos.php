<div class="modal fade" id="frmTipoPagoModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Tipo de Pagos </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>Ingresar Tipo de Pagos</header>
                            </div>
                            <br>
                            <form id="tipoPagosForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarTipoPagos()">
                                <fieldset>
                                    <input type="hidden" id="IDTipoPago" name="IDTipoPago">
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Nombre</label>
                                            <label class="input"> <i class="icon-prepend fa fa-align-justify"></i>
                                                <input id="nombre" name="nombre" type="text" class="text-uppercase" name="lname" placeholder="Nombre tipo de pago">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Valores Adicionales</label>
                                            <label class="select">
                                                <select id="valores_adicionales" name="valores_adicionales">
                                                    <option value="0" selected="" disabled="">-- Seleccione --</option>
                                                    <option value="S"> Si </option>
                                                    <option value="N"> No </option>
                                                </select> <i></i> </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label>Observación</label>
                                        <label class="textarea">
                                            <i class="icon-append fa fa-comment"></i>
                                            <textarea placeholder="Observaciones del Tipo de Pago" name="observacion" id="observacion" rows="3"></textarea>
                                        </label>
                                    </section>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Aceptar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

