<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevoTipoPago()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Tipo Pago
            </button>
        </fieldset> 
        <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
            <header>
                <span class="widget-icon"> <i ></i> </span>
                <h2>Tipo de Pagos </h2>
            </header>
            <div>
                <table id="listaTipoPagos" class="table table-striped table-bordered table-hover"   width="100%">                       
                    <thead>
                        <tr>
                            <th><i class="fa fa-fw txt-color-blue hidden-md hidden-sm hidden-xs"></i> ID</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> NOMBRE</th>
                            <th><i class="fa fa-fw fa-asterisk txt-color-blue hidden-md hidden-sm hidden-xs"></i> OBSERVACION</th>
                            <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</article>
<script src="aplicacion/js/tipopagos.js" defer="defer"></script>
<?php
include 'frmTiposPagos.php';

