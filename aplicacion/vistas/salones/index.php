<?php
// SALONES
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/salones/salones.php';
$salones = new salones();
?>
<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevoSalon()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Salon
            </button>
        </fieldset>

    </div>
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">        
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>Salones del Sistema </h2>
        </header>
        <div>
            <table id="listaSalon" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>Nombre</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Capacidad</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Ubicacion</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $resul = $salones->listarSalones();
                    while ($row = $resul->fetch_object()) {
                       
                        ?>
                        <tr>
                            <td><?php echo $row->nombre; ?></td>
                            <td><?php echo $row->capacidad; ?></td>
                            <td><?php echo $row->ubicacion; ?></td>
                            <td align="center"><button class="btn btn-success btn-xs" data-original-title="Editar Salon" onclick="javascript:actualizarSalon(<?php echo $row->id; ?>);"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btn-xs" data-original-title="Eliminar" onclick="javascript:eliminarSalon(<?php echo $row->id; ?>);"><i class="fa fa-trash-o"></i></button></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</article>
<script src="aplicacion/js/salones.js" defer="defer"></script>
<?php include 'frmSalon.php';

