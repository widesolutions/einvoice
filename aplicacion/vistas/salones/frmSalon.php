<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php';
$salones = new salones();
?>
<div class="modal fade" id="frmSalonModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Salon </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Ingresar Salon
                                </header>
                             </div>
                            <br>
                            <form id="salonForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarSalon()">
                                <fieldset>
                                    <input type="hidden" id="IDSalon" name="IDSalon">
                                </fieldset>
                                <div class="row">
					<section class="col col-6">
                                            <label>Nombre</label>
					<label class="input"> <i class="icon-prepend fa fa-pencil-square"></i>
					<input id="nombre" name="nombre" type="text" name="lname" placeholder="Ingrese el nombre del salon">
					</label>
					</section>
                                    <section class="col col-6">
                                            <label>Capacidad</label>
					<label class="input"> <i class="icon-prepend fa fa-sort-numeric-desc"></i>
					<input id="capacidad" name="capacidad" type="text" name="lname" placeholder="Ingrese el nombre del salon">
					</label>
                                            </section>
					</div> 
                                <fieldset>
                                    
                                            <label>Ubicación</label>
					<label class="input"> <i class="icon-prepend fa fa-map-marker"></i>
					<input id="ubicacion" name="ubicacion" type="text" name="lname" placeholder="Ingrese la ubicacion del salon">
					</label>
					
                                    
                                </fieldset>
                                <footer>
			<button type="submit" class="btn btn-primary">
			Aceptar
			</button>
			</footer>
                            </form>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>


