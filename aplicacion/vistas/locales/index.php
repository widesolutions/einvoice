<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevoLocal()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Local
            </button>
        </fieldset>        
    </div>
    <br>
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>Locales </h2>
        </header>
        <div>
            <br>
            <table id="listaLocales" class="table table-striped table-bordered table-hover"   width="100%">                       
                <thead>
                    <tr>
                        <th><i class="fa fa-fw  txt-color-blue hidden-md hidden-sm hidden-xs"></i> Id</th>
                        <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> Nombre</th>
                        <th><i class="fa fa-fw fa-tag txt-color-blue hidden-md hidden-sm hidden-xs"></i> Ubicación</th>
                        <th><i class="fa fa-fw fa-phone txt-color-blue hidden-md hidden-sm hidden-xs"></i> Teléfono</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Acciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

    </div>
</article>
<script src="aplicacion/js/locales.js" defer="defer"></script>
<?php include 'frmLocales.php';
    
