<div class="modal fade" id="frmLocalesModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Local </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Ingresar Local
                                </header>
                            </div>
                            <form id="localForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarLocal()">
                                <input type="hidden" id="IDLocal" name="IDLocal">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Nombre</label>
                                            <label class="input"> <i class="icon-prepend fa fa-pencil-square"></i>
                                                <input id="nombre" name="nombre" type="text" class="text-uppercase" placeholder="Nombre de Local">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Telefono</label>
                                            <label class="input"> <i class="icon-prepend fa fa-phone-square"></i>
                                                <input id="telefono" name="telefono" type="text" class="soloNumero" placeholder="Telefono del Local">
                                            </label>
                                        </section>
                                    </div> 
                                        <section>
                                            <label>Ubicación</label>
                                            <label class="input"> <i class="icon-prepend fa fa-map-marker"></i>
                                                <input id="ubicacion" name="ubicacion" type="text" class="text-uppercase" placeholder="Ubicación">
                                            </label>
                                        </section>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Aceptar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>

</div>
