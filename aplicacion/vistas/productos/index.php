<article >
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevoProducto()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Producto
            </button>
        </fieldset>

    </div>
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">        
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>Productos del Sistema </h2>
        </header>
        <div>
            <table id="listaProductos" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Codigo Barras</th>
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Descripcion</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Modelo</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Precio Compra</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Precio Venta</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i>Acciones</th>                                        
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</article>
<script src="aplicacion/js/producto.js" defer="defer"></script>
<?php include 'frmProducto.php';
