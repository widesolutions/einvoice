<div class="modal fade" id="frmProductoModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Producto </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Ingresar Producto
                                </header>
                            </div>
                            <form id="productForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarProducto()">
                                <fieldset>
                                    <input type="hidden" id="IDProducto" name="IDProducto">
                                    <section>
                                        <label>Descripción del Producto</label>
                                        <label class="input"> <i class="icon-prepend fa fa-pencil-square"></i>
                                            <input id="descripcion" name="descripcion" class="text-uppercase" type="text" placeholder="Descripción Producto">
                                        </label>
                                    </section>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Precio Compra</label>
                                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                                                <input id="precio_compra" name="precio_compra" class="soloNumero" type="text" placeholder="Precio Compra">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Precio de Venta</label>
                                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                                                <input id="precio_venta" name="precio_venta"  class="soloNumero" type="text" placeholder="Precio Venta">
                                            </label>
                                        </section>
                                    </div>    
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Codigo de Barras</label>
                                            <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                <input id="cod_barras" name="cod_barras" class="text-uppercase" type="text" placeholder="Codigo de barras">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Modelo</label>
                                            <label class="input"> <i class="icon-prepend fa fa-star-o"></i>
                                                <input id="modelo" name="modelo"  type="text" placeholder="Modelo">
                                            </label>
                                        </section>  
                                    </div> 
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Unidad de Medida</label>
                                            <label class="select">
                                                <select id="uni_medida" name="uni_medida">
                                                    <option value="" selected="" disabled="">-- Seleccione --</option>
                                                    <option value="1">Unidad</option>
                                                </select> <i></i> </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Graba IVA</label>
                                            <label class="select">
                                                <select id="iva" name="iva">
                                                    <option value="" selected="" disabled="">-- Seleccione --</option>
                                                    <option value="1"> Si </option>
                                                    <option value="2"> No </option>
                                                </select> <i></i> </label>
                                        </section>
                                    </div>                                  			
                                </fieldset>				
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Aceptar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
