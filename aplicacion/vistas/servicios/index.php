<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevoServicio()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Servicio
            </button>
        </fieldset>           
    </div>
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>Servicios del Sistema </h2>
        </header>

        <div>
            <table id="listaServicios" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> ID</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> CODIGO BARRAS</th>
                        <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> NOMBRE</th>
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> PRECIO COMPRA</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> GRABA I.V.A</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> PRECIO VENTA</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> ACCIONES</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>
    </div>
</article>
<script src="aplicacion/js/servicios.js" defer="defer"></script>
<?php include 'frmServicio.php';
