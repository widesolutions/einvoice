<style>
    .ui-autocomplete {
        z-index: 10000!important;
    }
    inicio.phpm
</style>
<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:crearServicio()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Servicio
            </button>
        </fieldset>           
    </div>
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
        <header>
            <span class="widget-icon"> <i ></i> </span>
            <h2>Servicios del Sistema </h2>
        </header>

        <div>
            <table id="listaServicios" class="table table-striped table-bordered table-hover"   width="100%">
                <thead>
                    <tr>
                        <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> Id</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Servicio</th>
                        <th><i class="fa fa-fw fa-male txt-color-blue hidden-md hidden-sm hidden-xs"></i> Tiempo de tolerancia</th>
                        <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i> Día de aplicación</th>
                        <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i> Acciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>
    </div>
</article>
<div class="modal fade" id="frmServiciosModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de edición del Servicio </h2>				
                    </header>

                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Edición del Servicio
                                </header>
                            </div>
                            <br>
                            <form id="servicioEditarForm" class="smart-form" novalidate="novalidate"  action="javascript:editarRegistro()">
                                <fieldset>
                                    <input type="hidden" id="IDServicio" name="IDServicio">
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Tiempo de tolerancia</label>
                                            <label class="input"> <i class="icon-prepend fa fa-clock-o"></i>
                                                <input id="tiempo_tolerancia" name="tiempo_tolerancia"  type="text" class="soloNumero" placeholder="Tiempo de tolerancia">
                                                <input id="servicio_sesiones" name="servicio_sesiones" type="hidden">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Día de aplicación</label>

                                            <select id="dia" name="dia" class="form-control">
                                            </select>
                                        </section>
                                    </div>
                                </fieldset>         			
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="frmServiciosCrearModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario para agregar el Servicio </h2>				
                    </header>

                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Agregar Servicio de sesiones
                                </header>
                            </div>
                            <br>
                            <form id="servicioCrearForm" class="smart-form" novalidate="novalidate"  action="javascript:storeRegistro()">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-10">
                                            <label>Servicio</label>
                                            <label class="input">
                                                <input id="servicio" name="servicio"  type="text"  placeholder="Servicio">
                                                <input id="servicio_id" name="servicio_id" type="hidden">
                                            </label>
                                        </section>
                                    </div>                                    
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Tiempo de tolerancia</label>
                                            <label class="input"> <i class="icon-prepend fa fa-clock-o"></i>
                                                <input id="tiempo_tolerancia_c" name="tiempo_tolerancia_c"  type="text" class="soloNumero" placeholder="Tiempo de tolerancia">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Día de aplicación</label>

                                            <select id="dia_c" name="dia_c" class="form-control">
                                                <option value="0"  selected="">Lunes</option><option value="1">Martes</option><option value="2">Miercoles</option><option value="3">jueves</option><option value="4">Viernes</option><option value="5">Sabado</option><option value="6">Domingo</option><option value="7">Todos los días</option>
                                            </select>
                                        </section>
                                    </div>
                                </fieldset>         			
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="aplicacion/js/servicios_sesiones.js" defer="defer"></script>
