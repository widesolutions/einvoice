<div class="modal fade" id="frmServicioModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Servicio </h2>				
                    </header>

                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Ingresar Servicio
                                </header>
                            </div>
                            <br>
                            <form id="servicioForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarServicio()">
                                <fieldset>
                                    <input type="hidden" id="IDServicio" name="IDServicio">
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Descripción</label>
                                            <label class="input"> <i class="icon-prepend fa fa-pencil-square"></i>
                                                <input id="descripcion" name="descripcion"  type="text" class="text-uppercase" placeholder="Descripcion del servicio">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Código de Barras</label>
                                            <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                <input id="codigo_barras" name="codigo_barras"  type="text" class="text-uppercase" placeholder="Codigo de barras">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Precio de Compra</label>
                                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                                                <input id="precio_compra" name="precio_compra" type="text" class="soloNumero" placeholder="Ingrese el precio del servicio">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Precio de Venta</label>
                                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                                                <input id="precio_venta" name="precio_venta" type="text" class="soloNumero" placeholder="Ingrese el precio venta">
                                            </label>
                                        </section>
                                    </div>                                                                    				
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>El Servicio Graba IVA?</label>
                                            <label class="select">
                                                <select id="iva" name="iva">
                                                    <option value="" selected="" disabled="">-- Seleccione --</option>
                                                    <option value="1"> Si</option>
                                                    <option value="2"> No</option>
                                                </select> <i></i> </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>El servicio se ejecutara todo el Dia?</label>
                                            <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                <label class="select">
                                                    <select id="todo_dia" name="todo_dia">
                                                        <option value="" selected="" disabled="">-- Seleccione --</option>
                                                        <option value="1"> Si</option>
                                                        <option value="0"> No</option>
                                                    </select> <i></i> </label>
                                            </label>
                                        </section>
                                    </div>  
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Cantidad Mínima</label>
                                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                                                <input id="minimo" name="minimo" type="number" class="soloNumero" placeholder="Cantidad Mínima">
                                            </label>
                                        </section> 
                                    </div>
                                </fieldset>				
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Aceptar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

