<?php
date_default_timezone_set('America/Guayaquil');

require($_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/modelos/dataBase.php');
require($_SERVER['DOCUMENT_ROOT'] . '/krayon/publico/js/libs/fpdf/pdf_js.php');
require($_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/sesiones/sesiones.php');
require($_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/controladores/cajas/cajas.php');

$dbmysql = new database();
$sesiones = new sesiones();
$cajas = new cajas();

$fecha = (isset($_GET['fecha']))?$_GET['fecha']:date('Y-m-d');

$valorCaja = 0;
$valorTotal = 0;
$sqlMov = "SELECT pm.`id`,pm.tipos_pago_id,pm.`movimiento_id`,tp.nombre ,ROUND(SUM(pm.`valor`),2) as valor,m.fecha FROM `pagos_movimientos` pm,tipos_pagos tp, movimientos m WHERE tp.id=pm.tipos_pago_id AND m.id=pm.movimiento_id AND m.fecha='$fecha' AND m.estados_movimiento_id=1 GROUP BY `tipos_pago_id`";
$valMov = $dbmysql->query($sqlMov);
?>
<html lang="es">
    <table border="1">
        <img src="publico/img/logo_kidztime.png" alt=""/>
        <thead>
            <tr>
                <th colspan="3" style="width: 400px;border-bottom: 1px #000 dashed;">REPORTE DE VENTAS</th>
            </tr>
            <?php
            $sqlCaja = "SELECT * FROM caja_diaria where fecha='$fecha'";
            $valCaja = $dbmysql->query($sqlCaja);
            $rowCaja = $valCaja->fetch_object();
            $valorAperturaCaja = $rowCaja->monto_apertura;
            ?>    
        </thead>

        <tr>
            <th>Apertura de Caja :</th>
            <td colspan="2" style="text-align:right;">$ <?php echo number_format($valorAperturaCaja,2); ?></td>
        </tr>
        <?php
        while ($rowMov = $valMov->fetch_object()) {
            if ($rowMov->tipos_pago_id == 1) {
                $valorCaja = $valorCaja + $rowMov->valor;
            } else {
                $valorCredito = $rowMov->valor;
            }
            ?>

            <tr>
                <th>Ventas <?php echo strtolower($rowMov->nombre); ?> :</th>
                <td colspan="2" style="text-align:right;">$ <?php echo number_format($rowMov->valor,2); ?></td>
            </tr>

        <?php } $valorTotal = $valorCaja + $valorCredito; ?>
        <?php ?>
        <tr>
            <th>Total en Caja :</th>
            <td colspan="2" style="text-align:right;">$ <?php echo number_format($valorCaja + $valorAperturaCaja,2); ?></td>
        </tr>
        <tr>
            <th>Total Vendido :</th>
            <td colspan="2" style="text-align:right;">$ <?php echo number_format($valorTotal,2); ?></td>
        </tr>

    </table>
    <table border="1">
        <thead>
            <tr>
                <th colspan="3" style="width: 400px;border-bottom: 1px #000 dashed;">REPORTE DE CONCURRENCIA</th>
            </tr>     
            <tr>
                <th>Nombre</th>
                <th style="text-align:right;">Cantidad</th>
                <th style="text-align:right;">Total</th>
            </tr> 
        </thead>
        <tbody>
            <?php
            $sesionesCierre = $sesiones->getConcurrenciCierre(date('y-m-d'));
            foreach ($sesionesCierre as $sesion) {
                ?>
                <tr>
                    <td>TOTAL <?php echo $sesion['nombre'] ?> </td>
                    <td style="text-align:right;"><?php echo $sesion['numero'] ?> </td>
                    <td style="text-align:right;">$ <?php echo $sesion['total'] ?> </td>
                </tr> 

            <?php } ?>
        </tbody>
    </table>

    <table border="1">
        <thead>
            <tr>
                <th colspan="3" style="width: 400px;border-bottom: 1px #000 dashed;">REPORTE DE PRODUCTOS</th>
            </tr>     
            <tr>
                <th>Nombre</th>
                <th style="text-align:right;">Cantidad</th>
                <th style="text-align:right;">Total</th>
            </tr> 
        </thead>
        <tbody>
            <?php
            $productosSesiones = $cajas->getproductosCierre(date('y-m-d'));

            foreach ($productosSesiones as $producto) {
                ?>
                <tr>
                    <td>TOTAL <?php echo $producto['nombre'] ?> </td>
                    <td style="text-align:right;"><?php echo $producto['numero'] ?> </td>
                    <td style="text-align:right;">$ <?php echo $producto['total'] ?> </td>
                </tr> 

            <?php } ?>
        </tbody>
    </table>    
</html>
