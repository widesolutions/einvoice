<div class="modal fade" id="frmAperturaCajaModal" tabindex="-1" role="dialog" aria-labelledby="ClienteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Apertura de <?php echo $_SESSION['caja'];?></h4>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Apertura de Caja </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="widget-body">
                                <form id="fromAperturaCaja" class="smart-form"  action="javascript:guardarAperturaCaja()" autocomplete="off" novalidate="novalidate">
                                                <div class="panel-body no-padding">
                                                    <fieldset>
                                                        <div class="row">
                                                            <section class="col col-4">
                                                                <label class="text-info">Fecha</label>
                                                                <label class="input"><h3> <?php echo date('Y-m-d');?></h3></label>
                                                            </section>
                                                            <section class="col col-4">
                                                                <label class="text-info">Hora de Apertura</label>
                                                                <label class="input"><h3> <?php echo date('H:i');?></h3></label>
                                                            </section>
                                                            <section class="col col-4">
                                                                <label class="text-info">Caja</label>
                                                                <label class="input"><h3> <?php echo $_SESSION['caja'];?></h3></label>
                                                            </section>
                                                        </div>
                                                        </fieldset>
                                                        <fieldset>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label class="text-info">Usuario de Apertura</label>
                                                                <label class="input"><h3> <?php echo $_SESSION["usu_real_nombre"];?></h3></label>
                                                            </section>
                                                            <section class="col col-6">
                                                                <label class="text-info">Monto de Apertura de Caja</label>
                                                                <label class="input"> 
                                                                    <i class="icon-prepend fa fa-phone"></i>
                                                                    <input type="number" class="soloNumero" id="monto" name="monto" placeholder="Monto de Apertura de Caja">
                                                            </section>
                                                        </div>
                                                        <section>
                                                            <label class="label">Observación</label>
                                                            <label class="textarea">
                                                                <i class="icon-append fa fa-comment"></i>
                                                                <textarea id="observacion" placeholder="Observaciones en la Apertura" name="observacion" rows="3"></textarea>
                                                            </label>
                                                        </section>
                                                    </fieldset>
                                                </div>
                                    <footer>
                                        <button type="submit"class="btn btn-primary">
                                            Abrir Caja
                                        </button>
                                    </footer>
                                </form>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
