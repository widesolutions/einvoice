<?php
session_start();
?>
<article>
    <div class="botonesSuperiores">
        <fieldset>
            <button id="agregarEvento" class="btn btn-labeled btn-primary btn-personal"  data-toggle="modal" onclick="javascript:nuevaCaja()">
                <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                Agregar Caja
            </button>
        </fieldset>  
        <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
            <header>
                <span class="widget-icon"> <i ></i> </span>
                <h2>Cajas </h2>
            </header>
            <div>
                <table id="listaCajas" class="table table-striped table-bordered table-hover"   width="100%">                       
                    <thead>
                        <tr>
                            <th><i class="fa fa-fw txt-color-blue hidden-md hidden-sm hidden-xs"></i> ID</th>
                            <th><i class="fa fa-fw fa-tasks txt-color-blue hidden-md hidden-sm hidden-xs"></i>IP</th>
                            <th><i class="fa fa-fw fa-user txt-color-blue hidden-md hidden-sm hidden-xs"></i>NOMBRE</th>
                            <th><i class="fa fa-fw fa-building txt-color-blue hidden-md hidden-sm hidden-xs"></i>EMISION</th>
                            <th><i class="fa fa-fw fa-file-text-o txt-color-blue hidden-md hidden-sm hidden-xs"></i>SERIE</th>
                            <th><i class="fa fa-fw fa-lock txt-color-blue hidden-md hidden-sm hidden-xs"></i>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</article>
<script src="aplicacion/js/cajas/cajas.js" defer="defer"></script>
<?php include 'frmCajas.php';