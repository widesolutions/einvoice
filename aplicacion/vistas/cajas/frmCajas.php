<div class="modal fade" id="frmCajasModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Ingreso de Cajas </h2>				
                    </header>

                    <div>
                        <div class="widget-body no-padding">
                            <div class="smart-form">
                                <header>
                                    Ingresar Caja
                                </header>
                            </div>
                            <br>
                            <form id="cajaForm" class="smart-form" novalidate="novalidate"  action="javascript:guardarCaja()">
                                <fieldset>
                                    <input type="hidden" id="IDCaja" name="IDCaja">
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Direccion IP</label>
                                            <label class="input"> <i class="icon-prepend fa fa-dedent"></i>
                                                <input id="ip" name="ip" type="text" class="soloNumero" placeholder="Direccion IP de la caja">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Nombre</label>
                                            <label class="input"> <i class="icon-prepend fa fa-pencil-square"></i>
                                                <input id="nombre" name="nombre" class="text-uppercase" type="text" placeholder="Nombre de la caja">
                                            </label>
                                        </section>
                                    </div>                                                                    				
                                    <div class="row">
                                        <section class="col col-6">
                                            <label>Serie</label>
                                            <label class="input"> <i class="icon-prepend fa fa-pencil-square"></i>
                                                <input id="serie" name="serie"  type="text" class="soloNumero" placeholder="Secuencial de la Factura">
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label>Punto Emision</label>
                                            <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                <input id="emision" name="emision"  type="text" maxlength="3" class="soloNumero" placeholder="Codigo del Punto de Venta">
                                            </label>
                                        </section>
                                    </div>                                  			
                                </fieldset>				
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Aceptar
                                    </button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


