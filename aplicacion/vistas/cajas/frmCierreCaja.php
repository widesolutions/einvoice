<div class="modal fade" id="frmCierreCajaModal" tabindex="-1" role="dialog" aria-labelledby="ClienteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Cierre de <?php echo $_SESSION['caja']; ?></h4>
            </div>
            <div class="modal-body" id="body_impresion">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-money"></i> </span>
                        <h2>Detalles del cierre de caja</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="widget-body" id="detalles_cierre">
                                <div style="text-align: center;"><img src="publico/img/logo_kidztime.png" alt="kidzTime" width="180px" style="margin: 10px"/></div>
                                <table class="table table-hover" id="detalles_cierre_caja_fin"></table> 
                            </div>
                        </div>
                    </div>
                    <div class="widget-footer" style="text-align: center;height: 57px;" id="footer-cierre-caja">
                        <button type="submit"class="btn btn-danger" onclick="javascript:ejecutarCierreCaja()" style="text-align: center;">
                            Cerrar Caja
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
