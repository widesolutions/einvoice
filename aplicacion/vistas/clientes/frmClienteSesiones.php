<div class="modal fade" id="frmClienteModal" tabindex="-1" role="dialog" aria-labelledby="PagoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Registro del Cliente</h4>
            </div>
            <form id="fromClientes" method="post" novalidate="novalidate" class="smart-form" action="javascript:crearCliente()" >
                <div class="modal-body">
                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label>Tipo Documento:</label>
                                <label class="select">
                                    <select id="tipoDocumento" name="tipoDocumento" class="form-control" onchange="javascript:cambioCedulaPasaporteCliente();">
                                        <option value="">-- Seleccione --</option>
                                        <option value="C">Cédula / RUC</option>
                                        <option value="P">Pasaporte</option>
                                    </select>
                                    <i></i>
                                </label>
                            </section>
                            <section class="col col-6" id="cedulaPasaporte"></section>
                        </div>
                        <div class="row">
                            <section class="col col-6">
                                <label>Nombres</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" name="nombres" id="nombres" placeholder="Nombres" class="texto-mayusculas">
                                </label>
                            </section>

                            <section class="col col-6">
                                <label>Apellidos</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text"  name="apellidos" id="apellidos" placeholder="Apellidos" class="texto-mayusculas">
                                </label>
                            </section>
                        </div>
                            <section>
                                <label>E-mail</label>
                                <label class="input"> <i class="icon-append fa fa-send"></i>
                                    <input type="text" name="email" id="email" placeholder="E-mail">
                                </label>
                            </section> 
                        <div class="row">
                            <section class="col col-6">
                                <label>Teléfono 1</label>
                                <label class="input"> <i class="icon-append fa fa-phone"></i>
                                    <input type="text" class="soloNumero" name="telefono1" id="telefono1" placeholder="Teléfono 1">
                                </label>
                            </section>

                            <section class="col col-6">
                                <label>Teléfono 2</label>
                                <label class="input"> <i class="icon-append fa fa-phone"></i>
                                    <input type="text" class="soloNumero" name="telefono2" id="telefono2" placeholder="Teléfono 2">
                                </label>
                            </section>  
                        </div>
                        <section>
                            <label>Dirección</label>
                            <label class="input"> <i class="icon-append fa fa-home"></i>
                                <input type="text" name="direccion" id="direccion" placeholder="Dirección" class="texto-mayusculas">
                            </label>
                        </section>   


                    </fieldset>

                </div>
                <footer>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                    </button>
                </footer>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="frmBuscarClienteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Clientes</h4>
            </div>
            <div class="modal-body">
                <form class="smart-form">
                    <section>
                        <input type="hidden" id="llamado" name="llamado" value="0">
                        <label class="label">Buscar Cliente</label>
                        <label class="input"> <i class="icon-append fa fa fa-search"></i>
                            <input type="text" placeholder="Información del cliente" id="search_cliente" onkeyup="searchClientes(this.value)">
                        </label>
                    </section>
                </form>
                <table class="table" id="tabla_detalles">
                    <thead>
                        <tr>
                            <th>Nro. Identificación</th>
                            <th>Nombre</th>
                            <th>Seleccionar</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="frmHistorialModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Historial de sesiones del cliente</h4>
            </div>
            <div class="modal-body" >
                <table class="table" id="tabla_detalles_historial">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Seleccionar</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="aplicacion/js/cliente.js" defer="defer"></script>