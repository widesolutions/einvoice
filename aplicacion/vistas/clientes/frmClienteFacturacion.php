<div class="modal fade" id="frmClienteModal" tabindex="-1" role="dialog" aria-labelledby="ClienteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Datos del Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Formulario de Registro </h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="widget-body">
                                <form id="fromClientesFacturacion" class="smart-form"  action="javascript:agregarDatosFacturacion()" autocomplete="off" novalidate="novalidate">
                                    <div class="panel-group smart-accordion-default" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Datos del Cliente </a></h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body no-padding">
                                                    <fieldset>
                                                        <input type="hidden" id="IDcliente" name="IDcliente">
                                                        <section>
                                                            <label class="control-label">Codigo</label>
                                                            <label class="input"> 
                                                                <i class="icon-prepend fa fa-asterisk"></i>
                                                                <input type="text" class="soloNumero" id="codigo" name="codigo" placeholder="Codigo Cliente">
                                                            </label>
                                                        </section>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label>Tipo Documento:</label>
                                                                <label class="select">
                                                                    <select id="tipoDocumento" name="tipoDocumento" class="form-control" onchange="javascript:cambioCedulaPasaporte();">
                                                                        <option value="">-- Seleccione --</option>
                                                                        <option value="C">Cédula / RUC</option>
                                                                        <option value="P">Pasaporte</option>
                                                                    </select>
                                                                    <i></i>
                                                                </label>
                                                            </section>
                                                            <section class="col col-6" id="cedulaPasaporte">

                                                            </section>


                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label class="control-label">Nombre</label>
                                                                <label class="input"> 
                                                                    <i class="icon-prepend fa fa-user"></i>
                                                                    <input type="text" id="nombre" name="nombre" placeholder="Nombre del Cliente">
                                                                    <b class="tooltip tooltip-bottom-right">Necesario para Registrar al Cliente</b> </label>
                                                            </section>
                                                            <section class="col col-6">
                                                                <label class="control-label">Apellido</label>
                                                                <label class="input"> 
                                                                    <i class="icon-prepend fa fa-user"></i>
                                                                    <input type="text" id="apellido" name="apellido" placeholder="Apellido del Cliente">
                                                                    <b class="tooltip tooltip-bottom-right">Necesario para Registrar al Cliente</b> </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label class="control-label">Telefono Fijo</label>
                                                                <label class="input"> 
                                                                    <i class="icon-prepend fa fa-phone"></i>
                                                                    <input type="text" class="soloNumero" id="telefono1" name="telefono1" placeholder="Telefono Fijo">
                                                                    <b class="tooltip tooltip-bottom-right">Necesario para Registrar al Cliente</b> </label>
                                                            </section>
                                                            <section class="col col-6">
                                                                <label class="control-label">Telefono Celular</label>
                                                                <label class="input"> 
                                                                    <i class="icon-prepend fa fa-phone"></i>
                                                                    <input type="text" class="soloNumero" id="telefono2" name="telefono2" placeholder="Telefono Celular">
                                                                    <b class="tooltip tooltip-bottom-right">Necesario para Registrar al Cliente</b> </label>
                                                            </section>
                                                        </div>
                                                        <section>
                                                            <label class="control-label">E-Mail</label>
                                                            <label class="input"> 
                                                                <i class="icon-prepend fa fa-envelope-o"></i>
                                                                <input type="text" id="email" name="email" placeholder="Correo Electronico">
                                                                <b class="tooltip tooltip-bottom-right">Necesario para Registrar al Cliente</b> </label>
                                                        </section>
                                                        <section>
                                                            <label class="label">Dirección</label>
                                                            <label class="textarea">
                                                                <i class="icon-append fa fa-comment"></i>
                                                                <textarea id="direccion" placeholder="Dirección del Cliente" name="direccion" rows="3"></textarea>
                                                            </label>
                                                        </section>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Sesiones Activas </a></h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <fieldset>
                                                        <div class="widget-body no-padding">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-striped" id="tablaSesionesActivas">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th> Nombres</th>
                                                                            <th> Hora Ingreso</th>
                                                                            <th> Transcurrido</th>
                                                                            <th> Tipo Servicio</th>
                                                                            <th> Promoción</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTree" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Fiestas / Eventos </a></h4>
                                            </div>
                                            <div id="collapseTree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <fieldset>
                                                        <div class="widget-body no-padding">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-striped" id="tablaFiestasEventos">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th><i class="fa fa-building"></i> Cumpleañero</th>
                                                                            <th><i class="fa fa-calendar"></i> Fecha</th>
                                                                            <th><i class="fa fa-unsorted"></i> Detalle</th>
                                                                            <th><i class="fa fa-money"></i> Tipo Servicio</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <footer>
                                        <button type="submit"class="btn btn-primary">
                                            Guardar
                                        </button>
                                    </footer>
                                </form>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="aplicacion/js/clienteFacturacion.js" defer="defer"></script>
