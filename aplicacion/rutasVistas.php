<?php

session_start();

class rutasVistas {

    public function rutas() {
        if ($_SESSION['autenticado'] == 'S') {
            $modulo = isset($_GET['modulo']) ? $_GET['modulo'] : 'ninguno';
            $vista = isset($_GET['vista']) ? $_GET['vista'] : 'ninguno';
            $_SESSION['contenido']= file_get_contents('vistas/' . $modulo . '/' . $vista . '.php');
        } else {
            header('location:index.php');
        }
    }

}
