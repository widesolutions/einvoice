<?php
include_once($_SERVER['DOCUMENT_ROOT']."/einvoice/aplicacion/controladores/locales/locales.php");
$locales=new locales();
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
    <head>
        <meta charset="utf-8">
        <title> EINVOICE .:. Sistema de Facturación</title>
        <meta name="description" content="Sistema de Facturacion">
        <meta name="author" content="WieSolutions">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-production-plugins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-skins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/general.css">
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-rtl.min.css"> 
        <link rel="stylesheet" type="text/css" media="screen" href="publico/css/demo.min.css">
        <!-- #FAVICONS -->
        <link rel="shortcut icon" href="publico/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="publico/img/favicon/favicon.ico" type="image/x-icon">
    </head>
    <body class="animated fadeInDown">
        <header id="header" style=" height: 91px;">
            <div id="logo-group">
                <span id="logo"> <img src="publico/img/einvoice.png" alt="SmartAdmin"> </span>
            </div>
        </header>
        <div id="main" role="main">
            <div id="content" class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                        <img src="publico/img/einvoice1.jpg" class="pull-right display-image" alt="Logo Einvoice" style="width:70%">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <div class="well no-padding">
                            <form action="javascript:login();" id="login-form" class="smart-form client-form">
                                <header>
                                    Login
                                </header>
                                <fieldset>
                                    <section>
                                        <label class="label">Local</label>
                                        <label class="select state-success"> <i class="icon-append fa fa-user"></i>
                                            <select class="valid" name="local" id="local">
                                                <option disabled="" selected="" value="0">-- Seleccione --</option>
                                                <?php
                                                $valcmb=$locales->getLocales();
                                                while ($row = $valcmb->fetch_object()) {
                                                    ?>
                                                    <option value="<?php echo $row->id ?>"><?php echo $row->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Seleccione un Centro de reclusión</b></label>
                                    </section>
                                    <section>
                                        <label class="label">Usuario</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" id="usuario" name="usuario">
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Por favor ingresa tu Usuario</b></label>
                                    </section>
                                    <section>
                                        <label class="label">Password</label>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="clave" name="clave">
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Ingresa tu contraseña</b> </label>
                                    </section>
                                </fieldset>
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Ingresar
                                    </button>
                                </footer>
                            </form>

                        </div>
                        <h5 class="text-center"> - Or sign in using -</h5>
                        <ul class="list-inline text-center">
                            <li>
                                <a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!--================================================== -->	
        <script src="publico/js/plugin/pace/pace.min.js"></script>
        <script src="publico/js/libs/jquery-2.1.1.min.js"></script>
        <script src="publico/js/libs/jquery-ui-1.10.3.min.js"></script>
        <script src="publico/js/app.config.js"></script>
        <script src="publico/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
        <script src="publico/js/bootstrap/bootstrap.min.js"></script>
        <script src="publico/js/plugin/jquery-validate/jquery.validate.min.js"></script>
        <script src="publico/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
        <script src="publico/js/notification/SmartNotification.min.js"></script>
        <script src="publico/js/app.min.js"></script>
        <script src="aplicacion/js/login.js"></script>
        <script type="text/javascript">
            runAllForms();
        </script>
    </body>
</html>
