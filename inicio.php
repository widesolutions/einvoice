<?php
session_start();
if ($_SESSION['autenticado'] == 'SI') {
    $codUsu = $_SESSION["user_id"];
    include_once("./aplicacion/controladores/seguridad/permisos.php");
    $permisos = new Permisos($codUsu);
    include_once("./aplicacion/controladores/sesiones/sesiones.php");
    $sesiones = new sesiones();
    $modulo = isset($_GET['modulo']) ? $_GET['modulo'] : '';
    $vista = isset($_GET['vista']) ? $_GET['vista'] : 'default';
    $ip = $permisos->obtenerDireccionIp();
    $equipo = $permisos->getCajaLocal();
    $estadoCaja = $permisos->obtenerEstadoCaja($_SESSION['id_caja']);
    $saNinos=$sesiones->getConcurrenciaActiva('N');
    $saBebes=$sesiones->getConcurrenciaActiva('B');
    $stNinos=$sesiones->getConcurrenciaTotalDia('N');
    $stBebes=$sesiones->getConcurrenciaTotalDia('B');
    $miIp = $ip;
    ob_start();
    include_once './aplicacion/vistas/' . $modulo . '/' . $vista . '.php';
    $contenido = ob_get_contents();
    ob_end_clean();
    ?>
    <!DOCTYPE html>
    <html lang="en-us">
        <head>
            <meta charset="utf-8">
            <title> Einvoice .:. Sistema de Facturación</title>
            <meta name="description" content="Sistema de Facturacion">
            <meta name="author" content="WieSolutions">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-production-plugins.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-production.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-skins.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/smartadmin-rtl.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="publico/css/general.css"> -->
            <!-- FAVICONS -->
            <link rel="shortcut icon" href="publico/img/favicon/favicon.ico" type="image/x-icon">
            <link rel="icon" href="publico/img/favicon/favicon.ico" type="image/x-icon">
        </head>
        <body class="desktop-detected pace-done full-screen fixed-header fixed-navigation fixed-ribbon smart-style-3">
            <header id="header">
                <div id="logo-group">
                    <span id="logo"> <img src="publico/img/logo.png" alt="krayon"  style="width: 55%"> </span>
                </div>
                <div class="pull-right">
                    <div id="hide-menu" class="btn-header pull-right">
                        <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                    </div>
                    <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
                        <li class="">
                            <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown"> 
                                <img src="publico/img/avatars/sunny.png" alt="John Doe" class="online" />  
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="aplicacion/controladores/seguridad/logout.php" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- search mobile button (this is hidden till mobile view port) -->
<!--				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>-->
				<!-- end search mobile button -->

				<!-- input: search field -->
<!--				<form action="search.html" class="header-search pull-right">
					<input id="search-fld"  type="text" name="param" placeholder="Buscar Cliente">
					<button type="submit">
						<i class="fa fa-search"></i>
					</button>
					<a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
				</form>-->
				<!-- end input: search field -->
                    <div id="logout" class="btn-header transparent pull-right">
                        <span> <a href="aplicacion/controladores/seguridad/logout.php" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
                    </div>
                    <div id="fullscreen" class="btn-header transparent pull-right">
                        <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                    </div>
                </div>
            </header>
            <aside id="left-panel">
                <div class="login-info">
                    <span>
                        <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <img src="publico/img/avatars/male.png" alt="me" class="online" /> 
                            <span>
                                <?php echo $_SESSION["usu_real_nombre"]; ?>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a> 
                    </span>
                </div>
                <nav>
                    <ul id="sysMenu">
                        <?php echo $permisos->construirMenu(); ?>
                    </ul>
                </nav>
                <span class="minifyme" data-action="minifyMenu"> 
                    <i class="fa fa-arrow-circle-left hit"></i> 
                </span>

            </aside>
            <div id="main" role="main">
                <div id="ribbon">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                            <span class="ribbon-button-alignment"> 
                                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                                    <i class="fa fa-refresh"></i>
                                </span> 
                            </span>
                            <ol class="breadcrumb">
                                <li>Home</li><li>Dashboard</li>
                            </ol>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                            <ul id="sparks" class="">
                                <li class="sparks-info" style="top: -10px;position: relative">
                                    <?php
                                    if ($estadoCaja == 'cerrada') {
                                        $class = 'btn-success';
                                        $logo = 'fa-plus';
                                        $href = 'javascript:abrirCaja()';
                                        $nombre = 'Abrir Caja';
                                    } else {
                                        $class = 'btn-danger';
                                        $logo = 'fa-remove';
                                        $href = 'javascript:cerrarCaja()';
                                        $nombre = 'Cerrar Caja';
                                    }
                                    ?>
                                    <a id="abrirCerrarCaja" class="btn btn-sm <?php echo $class; ?>"  href="<?php echo $href; ?>">
                                        <i class="fa <?php echo $logo; ?>"></i>
                                        <?php echo $nombre; ?>
                                    </a>
                                </li>
                                <li class="sparks-info">
                                    <h5>
                                        Sesiones Activas
                                        <span class="txt-color-blue"><i class="fa fa-reddit"></i> <?php echo $saBebes; ?> <i class="fa fa-child"></i><?php echo $saNinos; ?></span>
                                    </h5>
                                </li>
                                <li class="sparks-info">
                                    <h5>
                                        Sesiones Totales
                                        <span class="txt-color-blue"><i class="fa fa-reddit"></i> <?php echo $stBebes; ?> <i class="fa fa-child"></i> <?php echo $stNinos; ?></span>
                                    </h5>
                                </li>
                                <li class="sparks-info">
                                    <h5>
                                        Mi dirección IP
                                        <span class="txt-color-blue"><?php echo $miIp; ?></span>
                                    </h5>
                                </li>
                                <li class="sparks-info">
                                    <h5>
                                        <?php
                                        $x = explode(' ', $equipo);
                                        echo $x[0];
                                        ?>
                                        <span class="txt-color-purple">
                                            <i class="fa fa-arrow-circle-up"></i>
                                            <?php
                                            $x = explode(' ', $equipo);
                                            echo $x[1];
                                            ?>
                                        </span>
                                    </h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="content">

                    <?php echo $contenido; ?>
                </div>
            </div>
            <div class="page-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <span class="txt-color-white">Kryon <span class="hidden-xs"> - WIESOLUTIONS</span> © 2015-2016</span>
                    </div>
                </div>
            </div>
            <div id="shortcut">
                <ul>
                    <li>
                        <a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
                    </li>
                    <li>
                        <a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
                    </li>
                    <li>
                        <a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
                    </li>
                    <li>
                        <a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
                    </li>
                    <li>
                        <a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
                    </li>
                    <li>
                        <a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
                    </li>
                </ul>
            </div>
            <div id="cargando" style="display:none; color: green; position: absolute; height: 1000px; top: 0px; right: 0px; width: 100%; z-index: 999999; background: none repeat scroll 0% 0% rgb(0, 0, 0); opacity: 0.6;">
                <div id="destino" style="margin: 200px auto;background: #000; width: 100px;height: 100px; position: relative;">
                    <img src="publico/img/ajax-loader_blanco.gif" alt=""/>
                </div>
            </div>
            <!--================================================== -->
            <script data-pace-options='{ "restartOnRequestAfter": true }' src="publico/js/plugin/pace/pace.min.js"></script>
            <script src="publico/js/libs/jquery-2.1.1.min.js"></script>
            <script src="publico/js/libs/jquery-ui-1.10.3.min.js"></script>
            <script src="publico/js/app.config.js"></script>
            <script src="publico/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
            <script src="publico/js/bootstrap/bootstrap.min.js"></script>
            <script src="publico/js/notification/SmartNotification.min.js"></script>
            <script src="publico/js/smartwidgets/jarvis.widget.min.js"></script>
            <script src="publico/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="publico/js/plugin/sparkline/jquery.sparkline.min.js"></script>
            <script src="publico/js/plugin/jquery-validate/jquery.validate.min.js"></script>
            <script src="publico/js/plugin/jquery-validate/additional-methods.min.js"></script>
            <script src="publico/js/plugin/jquery-validate/jquery.validate.ruc.min.js"></script>
            <script src="publico/js/plugin/jquery-validate/jquery.creditCardValidator.js"></script>
            <script src="publico/js/plugin/jquery-validate/jqueryvalidatemessage.min.js"></script>
            <script src="publico/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
            <script src="publico/js/plugin/select2/select2.min.js"></script>
            <script src="publico/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
            <script src="publico/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
            <script src="publico/js/plugin/fastclick/fastclick.min.js"></script>
            <script src="publico/js/app.min.js"></script>
            <script src="aplicacion/js/general.js"></script>
            <script src="publico/js/speech/voicecommand.min.js"></script>
            <script src="publico/js/smart-chat-ui/smart.chat.ui.min.js"></script>
            <script src="publico/js/smart-chat-ui/smart.chat.manager.min.js"></script>
            <script src="publico/js/plugin/flot/jquery.flot.cust.min.js"></script>
            <script src="publico/js/plugin/flot/jquery.flot.resize.min.js"></script>
            <script src="publico/js/plugin/flot/jquery.flot.time.min.js"></script>
            <script src="publico/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
            <script src="publico/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
            <script src="publico/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
            <script src="publico/js/plugin/moment/moment.min.js"></script>
            <script src="publico/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
            <script src="publico/js/plugin/datatables/jquery.dataTables.min.js"></script>
            <script src="publico/js/plugin/datatables/fnReloadAjax.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.colVis.min.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.tableTools.min.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.autoFill.min.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.colReorder.min.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.responsive.min.js"></script>
            <script src="publico/js/plugin/datatables/dataTables.scroller.min.js"></script>
            <script src="publico/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
            <script src="publico/js/plugin/fuelux/wizard/wizard.min.js"></script>
            <script src="publico/js/plugin/jquery_barcode/jquery-barcode.min.js"></script>
            <script src="publico/js/jQuery.print.js"></script>
            <script src="aplicacion/js/cajas/abrirCaja.js"></script>
            <script src="aplicacion/js/cajas/cerrarCaja.js"></script>
            <script>$(document).ready(function () {
                    pageSetUp();
                });</script>
        </body>
    </html>
    <?php
    include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/cajas/frmAperturaCaja.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/cajas/frmCierreCaja.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/facturacion/prueba.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/krayon/aplicacion/vistas/seguridad/autorizacion.php';
} else {
    header('location:index.php');
}